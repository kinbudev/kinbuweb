<div class="container">
    <div class="row">
        <div class="col-xs-12" style="overflow:auto">
            <table class="table table-striped exchanges {{$status}}">
                <caption>{{ $caption }}</caption>
                <thead>
                    <tr>
                        <th rowspan="2">ID</th>
                        <th rowspan="2">Publicacion</th>
                        <th rowspan="2">Usuario</th>
                        <th colspan="2">Detalles</th>
                        <th rowspan="2">Fecha</th>
                    </tr>
                    <tr>
                        <th>Posibles</th>
                        <th>Seleccionado</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $key => $value)
                    <tr data-ex-id="{{ $value->id }}">
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->publication->id }}</td>
                        <td>{{ $value->user->name }}</td>
                        <td>{{ implode($value->details) }}</td>
                        <td>{{ $value->for_publication ? $value->for_publication->id : "" }}</td>
                        <td>{{ $value->created_at }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">{{ $empty_msg }}</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
