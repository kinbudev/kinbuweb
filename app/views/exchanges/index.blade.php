@extends('...default')

@section('location')
Intercambios
@stop

@section('content')
<section class="main-body">
    @include('exchanges.table', [
        'status' => "ready",
        'caption' => "En espera",
        'data' => $data->ready,
        'empty_msg' => "No tienes intercambios en espera"
    ])
    @include('exchanges.table', [
        'status' => "accepted",
        'caption' => "Aceptados",
        'data' => $data->accepted,
        'empty_msg' => "No tienes intercambios aceptados"
    ])
    @include('exchanges.table', [
        'status' => "rejected",
        'caption' => "Rechazados",
        'data' => $data->rejected,
        'empty_msg' => "No tienes intercambios rechazados"
    ])
    @include('exchanges.table', [
        'status' => "exchanged",
        'caption' => "Intercambiados",
        'data' => $data->exchanged,
        'empty_msg' => "No tienes intercambios finalizados"
    ])
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
    (function ($, k) {
        $('table.exchanges.ready tbody tr').on('click', function (e) {
            var exId = $(this).data('exId')

            var handleExchangeSelection = function (e) {
                var item = $(this);
                var wasActive = item.hasClass('active');
                $('.opciones ul li.active').toggleClass('active');
                if (!wasActive) {
                    $(this).toggleClass('active');
                }
            }

            var handleExchangeAcceptance = function (e) {
                var accepted = $('.opciones ul li.active');
                if (accepted.length > 0) {
                    var acceptedId = accepted.data('pbId');
                    $.ajax({
                        url: '{{ route('exchanges') }}' + '/' + exId + '/accept',
                        method: 'POST',
                        data: {_token: '{{ csrf_token() }}', publication: acceptedId}
                    }).done(function (response) {
                        if (response.success) {
                            $.ajax({
                                url: '{{ route('exchanges') }}' + '/' + exId + '/payment',
                                method: 'GET'
                            }).done(function (response) {
                                var body = $('<div />').append([
                                    $('<p />', {
                                        html: "Libro a intercambiar:"
                                    }),
                                    $('<img />', {
                                        src: '//' + response.book.image,
                                        width: 160,
                                        style: "display:block; margin: 0 auto;"
                                    }),
                                    $('<h3 />', {
                                        html: 'Env&iacute;o: $' + response.amount
                                    })
                                ]);
                                k.Msg.create("Paga el env&iacute;o del libro para continuar", body, [response.payuform]);
                            }).fail(function (request, status) {
                                k.Notification.show("Ocurri&oacute; un error");
                            });
                        } else {
                            k.Notification.show("Ocurri&oacute; un error");
                        }
                    }).fail(function (request, status) {
                        k.Notification.show("Ocurri&oacute; un error");
                    });
                } else {
                    alert("Debe selecionar uno de los libros.");
                }
            }

            var handleExchangeRejection = function (e) {
                $.ajax({
                    url: '{{ route('exchanges') }}' + '/' + exId + '/reject',
                    method: 'POST',
                    data: {_token: '{{ csrf_token() }}'}
                }).done(function (response) {
                    if (response.success) {
                        location.reload()
                    } else {
                        k.Notification.show("Ocurri&oacute; un error");
                    }
                }).fail(function (request, status) {
                    k.Notification.show("Ocurri&oacute; un error");
                });
            }

            var doneLoadingExchange = function (response) {
                if (response.success) {
                    var btn_ok = $("<button/>",{
                        type: 'button',
                        class: 'btn btn-primary',
                        'data-dismiss': 'modal',
                        text: 'Aceptar'
                    });
                    var btn_rechazar = $("<button/>",{
                        type: 'button',
                        class: 'btn btn-danger',
                        'data-dismiss': 'modal',
                        text: 'Rechazar'
                    });
                    var btn_cerrar = $("<button/>",{
                        type: 'button',
                        class: 'btn btn-default',
                        'data-dismiss': 'modal',
                        text: 'Cerrar'
                    });

                    var body = $("<div/>", {
                        class: 'opciones',
                        text: "Selecciona un libro"
                    });

                    var opciones = [];
                    response.data.publications.forEach(function (publication, i) {
                        opciones.push($("<li />", {
                            'data-pb-id': publication.id,
                            class: 'list-group-item',
                            html: publication.book.title
                        }));
                    });

                    body.append($("<ul />", { class: 'list-group'}).append(opciones));

                    k.Msg.create("Intercambia tu libro " + response.data.title, body, [btn_ok, btn_rechazar, btn_cerrar]);

                    $('.opciones ul li').on('click', handleExchangeSelection);

                    btn_ok.on('click', handleExchangeAcceptance);

                    btn_rechazar.on('click', handleExchangeRejection);
                } else {
                    //Ocurrio un error
                }
            };

            $.ajax({
                url: '{{ route('exchanges') }}' + '/' + exId,
                method: 'GET'
            }).done(doneLoadingExchange).fail(function (request, status) {
                console.log(status);
            });

        });

        $('#payuform').submit(function (event) {
            event.preventDefault();
            var params = [
                'referenceCode', 'amount'
            ];
            var form = this;
            var values = {_token: '{{ csrf_token() }}'};
            $.each($(form).serializeArray(), function (index, el) {
                if (params.indexOf(el.name) >= 0) {
                    values[el.name] = el.value;
                }
            });
            $.each($('.data-fieldset').serializeArray(), function (index, el) {
                if (params.indexOf(el.name) >= 0) {
                    values[el.name] = el.value;
                }
            });
            $.ajax({
                //url: '{{-- route('exchange_transaction') --}}',
                method: 'POST',
                data: values,
            }).done(function (response) {
                if (response.success) {
                    form.submit();
                } else {
                    var content = $('<div />').append(
                        $('<span />', {
                            class: 'glyphicon glyphicon-alert',
                            'aria-hidden': true
                        })
                    ).append(response.mensaje);
                    KINBU.Notification.show(content);
                }
            }).fail(function (request, status) {
                KINBU.Notification.error(status + " " + request.status + ": " + request.statusText);
            });
        });
    })(jQuery, KINBU);
</script>
@stop
