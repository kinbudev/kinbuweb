@extends('...default')

@section('location')
    Busqueda {{ $query }}
@stop

@section('content')

<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
            <div class="hidden-md hidden-lg" style="text-align: center;">
            <button id="show-books" type="button" class="btn violet-btn" style="margin: 0 5px 0 5px;">Libros</button>
            <button id="show-users" type="button" class="btn gray-btn" style="margin: 0 5px 0 5px;">Lectores</button>
            </div>
            <div id="books" class="col-xs-12 col-md-6">
                <h2>Libros</h2>

                @if(count($elitePublications) > 0)

                <div class="row" style="border: 3px solid; border-radius: 6px; color:#F2BD22;">
                    <h4 style="font-style: italic;"> Recomendados </h4>
                    @foreach($elitePublications as $key => $publication)
                        @include('publications.small', compact('publication'))
                    @endforeach 
                </div>
                @endif

                @forelse($standardPublications as $key => $publication)
                    @include('publications.small', compact('publication'))
                @empty
                @endforelse
                

                @forelse($basicPublications as $key => $publication)
                    @include('publications.small', compact('publication'))
                @empty
                @endforelse

                @if(  count($elitePublications) == 0 &&  count($standardPublications) == 0 &&  count($basicPublications) == 0)

                    <div class="">
                        No se encontraron libros con esos criterios
                    </div>
                @endif

            </div>

            <div id="users" class="col-xs-12 col-md-6">
                <h2>Usuarios</h2>
                @forelse($users as $key => $user)
                    @include('users.badge', ['user' => $user, 'with_name' => true])
                @empty
                    <div class="">
                        No se encontraron usuarios con esos criterios
                    </div>
                @endforelse
            </div>

        </div>
    </div>
</section>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function () {
    if ($(window).width() <= 768){
    $('#users').hide();
    $('#show-users').click(function() {
        $('#users').show();
        $('#books').hide();
        $('#show-users').toggleClass('violet-btn gray-btn');
        $('#show-books').toggleClass('violet-btn gray-btn');
    });
    $('#show-books').click(function() {
        $('#users').hide();
        $('#books').show();
        $('#show-users').toggleClass('violet-btn gray-btn');
        $('#show-books').toggleClass('violet-btn gray-btn');
    });
    }
}());
</script>
@stop