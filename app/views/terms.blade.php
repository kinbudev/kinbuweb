@extends('...default')

@stop

@section('location')
    Terminos y Condiciones
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Términos y Condiciones</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <h5>El contenido de libros sólo es de referencia y no es de
                            la propiedad de Kinbu. Sí tienes alguna queja, reclamo o
                            eres el propietario del libro y no deseas que sea mostrado
                            en nuestra plataforma, puedes escribir al correo: soporte@kinbu.co
                            con las especificaciones de permanencia.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Términos y condiciones Kinbu</h2>
                        <h5>De manera inicial, damos las gracias por usar los servicios Kinbu.</h5>
                        <h5>Al acceder y utilizar este servicio, usted acepta y accede a estar obligado
                            por los términos y disposiciones de este acuerdo. Asimismo, al utilizar estos
                            servicios particulares, usted estará sujeto a toda regla o guía de uso correspondiente
                            que se haya publicado para dichos servicios. Toda participación en este servicio
                            constituirá la aceptación de este acuerdo. Si no acepta cumplir con lo anterior,
                            por favor, absténgase de su utilización.</h5>
                            <h5>Página y software, las características y la funcionalidad son total propiedad
                                de KINBU S.A.S y están protegidos por derechos de autor internacionales, leyes
                                de propiedad intelectual o de derechos de propiedad.</h5>
                            <h5>Kinbu S.AS, en adelante “La plataforma” es una marca registrada legalmente en
                                Colombia la cual tiene como objeto principal el préstamos de servicios para que
                                los usuarios puedan ejecutar compra, venta o intercambios de libros entre ellos,
                                por cualquier medio conocido o por conocer. Igualmente, la sociedad tendrá como
                                objeto principal realizar cualquier acto lícito de comercio. Sí llega a presentarse
                                un inconveniente o conflicto con la plataforma será resuelta en los tribunales de la
                                Republica de Colombia y con la constitución de dicho país. Los siguientes términos y
                                condiciones son escritos en español y dada cualquier traducción será puesta como
                                prioritaria el idioma original.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>SERVICIOS SOCIALES</h2>
                        <h5>Los servicios sociales comprenden todas y cada una de las actividades que hacen las
                            personas que usan Kinbu en adelante “Usuarios” entre ellos y hacia los libros, refiriéndose
                            así a una relación de usuario a libro, donde se pueden realizar comentarios, valoraciones,
                            mensajes y cualquier acto que sea de un usuario hacia su entorno. No se permitirán ofensas
                            ni ningún acto ofensivo o de acoso entre usuarios, esto conllevará a una suspensión
                            indefinida por parte de la plataforma al usuario.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>POLITICAS DE ENVÍO</h2>
                        <h5>Los envíos serán distribuidos por medio de la transportadora TCC S.A, teniendo ésta
                            su actuar desde el momento de la solicitud de un libro. Se brinda éste servicio para
                            facilitar y mejorar la calidad de los envíos. Por tal, la efectividad del envío se
                            encuentra proporcional por ésta empresa y se presenta un cese de responsabilidad por
                            parte de Kinbu S.A.S.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>POLITICAS DE VENTA</h2>
                        <h5>Las ventas son efectuadas por usuarios o directamente con Kinbu, tal como se especifique
                            en la descripción de la venta. Las ventas serán presentadas como Publicaciones, datos
                            presentados a usuarios del sitio, están protegidos por los derechos de Kinbu. La propiedad
                            de éstas publicaciones dado caso sean publicadas por un usuario, será una relación de
                            propiedad entre el usuario y La Plataforma, debido a que la información es ofrecida por
                            el usuario, más sin embargo éstas publicaciones se podrán suspender, ocultar, manejar
                            su visibilidad o permanencia en el sitio dependiendo a la seguridad del Sitio y los
                            usuarios. La Plataforma siempre estará orientada a la seguridad y veracidad de la
                            información para la satisfacción de sus usuarios.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>PAGOS</h2>
                        <h5>Los pagos serán presentados por medio del Proveedor de servicios de pagos en línea PayU
                            Colombia S.A.S, teniendo ésta su actuar desde el momento que se realice la solicitud de
                            pago por cualquier servicios prestado por Kinbu. Se brinda éste servicio para facilitar
                            y mejorar la calidad de pagos. Por tal, la efectividad, recibo, efecto, seguridad o
                            notificación del pago se encuentra condicionada por ésta empresa y se presenta un cese
                            de responsabilidad por parte de Kinbu S.A.S, La Plataforma, se encontrará siempre
                            dispuesta a favorecer el beneficio de sus usuarios y calidad de su experiencia de
                            pagos.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>VENDEDORES DE LIBROS O USUARIOS SOLIDARIOS</h2>
                        <h5>Entiéndase por Vendedores a cualquier tipo de sociedad o persona que maneje una alianza
                            o alto volumen de, ventas o intercambios, con La Plataforma. Ofreciendo publicaciones,
                            enriqueciendo y mejorando la experiencia del Usuario. Personas que vendan o intercambien
                            libros con otras personas se entenderá por Usuarios solidarios. Presentado cualquier
                            inconveniente podrá dirigirse a la Política de Ventas y obtener más información.
                            La Plataforma se abstiene de asegurar una total veracidad de dicha información,
                            de igual forma, Kinbu siempre está enfocado en ofrecer una experiencia completa a
                            todos sus Usuarios y brindar seguridad en sus servicios.</h5>
                            <h5>Pago de ventas: El pago de los libros vendidos se fijará a 7 días hábiles
                                haberse efectuado la venta y será transferida por medio de consignación
                                bancaria en cuentas banco Bancolombia sin costo adicional.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>DEVOLUCIONES Y GARANTÍAS</h2>
                        <h5>Todo usuario que haya realizado compra de libro nuevo o usado tendrá servicio
                            de garantía y devolución en los 5 (cinco) día siguientes a su entrega, después
                            que se cumpla los siguientes requisitos; Sí el libro comprado es nuevo: las causas
                            deben ser por, libro diferente al comprado o malas condiciones. Para libros usados:
                            Libro diferente al comprado o condiciones diferentes a las presentadas al momento
                            de la compra. O Cualquier estado que La Plataforma califique aprobada para Devolución
                            o garantía.</h5>
                        <h5>Derecho al retracto: Sí al momento de comprar un libro cambia de opinión, usted
                            tiene derecho al 100% (cien por ciento) sobre el precio del libro después de 5
                            (días) después de efectuar la compra en La Plataforma. Dado caso sea entregado
                            el producto en éste plazo, deberá entregarlo en su mismo empaque sin abrir y
                            contactarnos.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>RENUNCIA DE RESPONSABILIDAD<br/></h2>
                        <h5>A pesar de todos los esfuerzos hechos por el equipo Kinbu, siempre se nos escapan
                            cosas, somos conscientes de ello. Debido a esto renunciamos a la responsabilidad
                            general por los perjuicios que podrían afectar moral, física, psicológica, económica
                            o de alguna otra forma a nuestros usuarios o colaboradores, lamentamos mucho esto.
                            Por esto siempre nos enfocaremos en mejorar nuestros servicios, ofrecer mejores
                            experiencias de usuario y aumentar cada día más la seguridad y satisfacción de todos
                            lo que interactúen a través de Kinbu.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h4>Bienvenidos a éste gran servicio.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
