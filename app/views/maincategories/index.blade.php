@extends('default')

@section('location')
All categories
@stop

@section('content')
<section class="main-body">
    <div class="container">
    @if(!$maincategories->isEmpty())
        @foreach($maincategories as $maincategory)
        <div class="row">
            <h2>
                {{ link_to_route('allcategories.show', $maincategory->name, ['allcategories'=>$maincategory->id]) }}
                <small>{{ $maincategory->description }}</small>
            </h2>
            <div class="body col-sm-12">
                @if($maincategory->categories->isEmpty())
                    No categories
                @else
                    @foreach($maincategory->categories->sortBy('name')->chunk(3) as $row)
                    <div class="row">
                        @foreach($row as $category)
                        <div class="col-md-4">
                            <div>{{ link_to_route('categories.show', $category->name, ['categories'=>$category->id]) }}</div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        @endforeach
    @else
        <p class="bg-danger text-danger">No categories</p>
    @endif
    </div>
</section>
@stop
