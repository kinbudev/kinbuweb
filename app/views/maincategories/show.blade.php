@extends('default')

@section('location')

@stop

@section('view')
<div class="page-header">
    <h1>{{ $maincategory->name }} <small>{{ $maincategory->description }}</small></h1>
</div>
    @if($maincategory->categories->isEmpty())
        No categories
    @else
        @foreach($maincategory->categories->sortBy('name')->chunk(3) as $row)
        <div class="row">
            @foreach($row as $category)
            <div class="col-md-4">
            {{ link_to_route('categories.show', $category->name, ['categories'=>$category->id]) }}
            </div>
            @endforeach
        </div>
        @endforeach
    @endif
@stop
