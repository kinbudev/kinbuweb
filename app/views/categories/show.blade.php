@extends('default')

@section('location')
{{ $category->name }}
@stop

@section('view')
<div class="page-header">
    <h1>{{ $category->name }} <small>{{ $category->description }}</small></h1>
</div>
<div class="container">
    @if($category->books->isEmpty())
        <h3>No Books</h3>
    @else
        <h3>Books</h3>
        @foreach($category->books->sortBy('title')->chunk(3) as $row)
        <div class="row">
            @foreach($row as $book)
            <div class="col-md-4">
                <h2>{{ link_to_route('books.show', $book->title, ['books'=>$book->id]) }}</h2>
                <div class="body">
                    {{ $book->about }}
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    @endif
</div>

<div class="container">
    @if($category->authors->isEmpty())
        <h3>No Authors</h3>
    @else
        <h3>Authors</h3>
        @foreach($category->authors->sortBy('title')->chunk(3) as $row)
        <div class="row">
            @foreach($row as $author)
            <div class="col-md-4">
                <h2>{{ link_to_route('authors.show', $author->firstname.' '.$author->lastname, ['authors'=>$author->id]) }}</h2>
                <div class="body">
                    {{ $author->about }}
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    @endif
</div>
@stop
