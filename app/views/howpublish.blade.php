@extends('...default')

@section('location')
Publica un libro
@stop

@section('addedcss')  
@parent
{{ HTML::style("css/kbookpublication.css") }}
@stop

@section('content')
<section class="main-body">
    <div class="container">

        <h2 class="purple-text">Publica un libro</h2>
        <br />

        <div style="font-family: 'NeoSans-Light'; text-align: justify;">
        	Al vender en Kinbu no sólo contribuyes a que miles de lectores puedan acceder a libros nuevos y usados, también a hacer los libros más accesibles a personas que no tienen el suficiente dinero para comprar libros nuevos o en los lugares donde viven no están esos libros.
        	<br><br>
        	Recerda que en algún momento quisimos un libro, pero no tuvimos el suficiente dinero o estaba agotado. <p style="font-weight: bold;">Hoy tienes el poder para cambiar eso en otro lector.</p>
        </div>
    </div>
        <div style="text-align: justify; font-weight: bold; margin-left: 15px; margin-right: 15px;" class="row">
        	<span class="col-sm-3 col-xs-12" style="text-align: center; font-family: 'NeoSans-Light';">
        	<img src="http://i.imgur.com/9hhr3Lq.png" style="padding-left: 20px; width: 171.25px; height:140px"> <br>
        	Gana dinero para comprar más libros o lo que quieras.
        	</span>

        	<span class="col-sm-3 col-xs-12" style="text-align: center; font-family: 'NeoSans-Light';">
        	<img src="http://i.imgur.com/Fzc0Hb2.png" style="padding-left: 20px; width: 171.25px; height:140px"> <br>
        	<span>Haz amigos y comparte lecturas en común.</span>
        	</span>

        	<span class="col-sm-3 col-xs-12" style="text-align: center; font-family: 'NeoSans-Light';">
        	<img src="http://i.imgur.com/Kwdp8rq.png" style="padding-left: 20px; width: 171.25px; height:140px"> <br>
        	<span>Nos encargamos de recoger, envíar el libro y la garantía.</span>
        	</span>

        	<span class="col-sm-3 col-xs-12" style="text-align: center; font-family: 'NeoSans-Light';">
        	<img src="http://i.imgur.com/fAndWiZ.png" style="padding-left: 20px; width: 171.25px; height:140px"> <br>
        	<span>Contribuye para que hayan más libros nuevos y usados en venta.</span>
        	</span>

        </div>

    <div class="container" style="text-align: justify;">
    	<br>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">1</span>
    	<h3 style="margin-left: 40px;">Tener el libro a vender</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	Lo primero, que debes tener en la cuenta, es tener el libro o los libros que deseas vender. Recuerda que sí es usado, debes colocar éste estado en el formato de publicación.
    	<br><br>
    	<span style="font-weight: bold"> Sí es usado:</span>
    	<br><br>
    	Sí el libro que venderás a los demás lectores en Kinbu, es usado; Recuerda que debes especificar con qué características cuenta (ej. Páginas completas, Sin rayoes o manchas, etc.) Sí colocaste que el libro se encuentra sin manchas, pero las tiene, el comprador puede reclamar devolucción de éste libro y se te enviará devuelta el libro sin el pago de éste.
    	<br>
    	</div>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">2</span>
    	<h3 style="margin-left: 40px;">Envío</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	En Kinbu no te preocupas por nada diferente a publicar tu libro. Nosotros nos encargamos de su envío, del pago y de la atención al lector.
    	<br><br>
    	Al momento de que un libro se haya vendido te avisaremos vía correo y con una notificación al iniciar sesión en Kinbu, sólo esperas que TCC en nuestra representación venga a buscar el libro.
    	<br><br>
    	<b>No te precupas por el empaque ni embalaje</b>
    	<br><br>
    	Al momento que vayamos a recoger el libro, sólamente entregalo al transportador sin empaque ni embalaje, nosotros nos ocuparemos de todo ese tedioso proceso.
    	<br>
    	</div>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">3</span>
    	<h3 style="margin-left: 40px;">Llenar el formato de publicación</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	El formato para publicar un libro lo puedes encontrar cliqueando en el botón "Publicar un libro" en tú perfil. En éste contrarás toda al información necesaría para hacer visible una venta o "Publicación" a los demás lectores en Kinbu.
    	<br>
    	</div>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">4</span>
    	<h3 style="margin-left: 40px;">Fotos</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	Sí colocas fotos enventualmente venderás más libros. Asegurate de tomar fotos en sus tapas y dentro de él (sí éste es usado). La primera foto que selecciones para subir, será la foto de portada de tu publicación, asegurate que esté alineada y sea su tapa principal.
    	<br>
    	<br>
    	Ejemplo:
    	<br>
    	<img class="img-responsive" style="margin: auto;" src="http://i.imgur.com/sW6jjP9.jpg?1" width="195px" height="260px">
    	<br>
    	</div>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">5</span>
    	<h3 style="margin-left: 40px;">Visibilidad</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	La visibilidad o "plaza" dependerá de la comisión que estés dispuesto a pagar una vez tu libro se venda, no tienes ningún gasto adicional antes. 
    	<br>
    	<br>
    	Existen tres tipos de plazas para ofrecer tu publicación:
    	<br>
    	<br>
    	<b>Kinbu Basic:</b> Con éste tipo de plaza tendrás una visibilidad básica a través del mundo Kinbu, al momento de realizar una búsqueda los lectores te pondrán encontrar según la precisión del titulo y competirás con todas las demás publicaciones.
    	<br> <br>
    	<b>Kinbu Standard:</b> Con ésta plaza, tendrás visibilidad entre los primeros puestos cuando un usuarios realice una búsqueda, así será más facil encontrarte.
    	<br><br>
    	<b>Kinbu Elite:</b>Con Kinbu Elite, no tienes ni que preocuparte por que tus potenciales compradores búsquen algún titulo, ya que estarás en la página principal en la sección "Ventas destacadas" preparada sólo para ti. Ésta es la mejor plaza, y se recomendamos usarla para una rápida venta.
    	<br><br>
    	Cada vez que publiques un libro tendrá su respectiva plaza y también se mostrará en tu perfil.
    	<br>
    	</div>

    	<div>
    	<span style="float: left; background-color:#F2BD22;border-radius: 4px; color: #fff; font-weight: bold;font-family:'OpenSans'; font-size: 17px; width: 30px; height: 30px; text-align: center;line-height: 31px;">6</span>
    	<h3 style="margin-left: 40px;">Pagos y devoluciones</h3>
    	</div>
    	<div style="font-family: 'NeoSans-Light';">
    	Los pagos se realizará siempre en la cuenta que especifiques para cada libro o en tú perfil desde el botón "Editar Perfil" cada 15 días, ni un día más. Sólo aceptamos cuentas Bancolombia, por ahora. 
    	<br><br>
    	<b>Devoluciones</b>
    	En dado caso que haya habido un problema con tu venta, cómo colocar caracteristicas de libros usados que sí tenian defectos. O envíar un libro erroneo, simplemente se enviará nuevamente tu libro a tu casa y el pago no se realizará.
    	<br>
    	</div>

    </div>

<br>
<br>
</section>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function ($) {
    
}(jQuery));
</script>
@endsection
