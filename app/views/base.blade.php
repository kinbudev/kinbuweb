<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="libros, lectores, kinbu">
    <meta name="description" content="Compra y vende libros nuevos o usados.">
    <meta NAME="ROBOTS" CONTENT="NOODP">
    <meta name="author" content="kinbu">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#662e93" />
    {{ ''; $bootstrap = '3.3.7' }}
    <title>@yield('title')</title>
    {{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/$bootstrap/css/bootstrap.min.css") }}
    {{ HTML::style("//fonts.googleapis.com/css?family=Open+Sans") }}
    {{ HTML::style("//fonts.googleapis.com/css?family=Source+Sans+Pro") }}
    {{ HTML::style("css/k.css") }}
    @yield('addedcss')
</head>
<body>
    <div id="msg-modal" class="modal fade" aria-labelledby="msg-modal-title">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"></div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div><!-- /.modal -->
@yield('body')

{{ HTML::script("//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js") }}
{{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/$bootstrap/js/bootstrap.min.js") }}
@yield('addedjs')
<script type="text/javascript">
    window.Msg = function (title, message, footer) {
        var modal = $("#msg-modal");
        var modal_title = modal.find("h4.modal-title");
        var modal_body = modal.find("div.modal-body");
        var modal_footer = modal.find("div.modal-footer");
        modal_title.html("");
        modal_body.html("");
        modal_footer.html("");
        modal_title.append(title);
        modal_body.append(message);
        modal_footer.append(footer);
        modal.modal("show");
        return modal;
    }
</script>
</body>
</html>
