<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Error | Kinbu.co</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/css/errors/base.css">  
   <link rel="stylesheet" href="/css/errors/main.css">
   <link rel="stylesheet" href="/css/errors/vendor.css">     

   <!-- script
   ================================================== -->
	<script src="/js/errors/modernizr.js"></script>

   <!-- favicons
	================================================== -->
	<link rel="icon" type="image/png" href="favicon.ico">

</head>

<body>

	<!-- header 
   ================================================== -->
   <header class="main-header">
   	<div class="row">
   		<div class="logo">
	         <a href="https://kinbu.co/market">Market</a>
	      </div>   		
   	</div>   

   	<a class="menu-toggle" href="#"><span>Menu</span></a>	
   </header> <!-- /header -->

   <!-- navigation 
   ================================================== -->
   <nav id="menu-nav-wrap">

   	<h5>Lugares donde ir</h5>   	
		<ul class="nav-list">
			<li><a href="https//Kinbu.co" title="">Home</a></li>
			@if (Auth::check())<li><a href="https://Kinbu.co/user/{{Auth::user()->username}}" title="">Perfil</a></li>@endif
			<li><a href="https://Kinbu.co/login" title="">Iniciar sesión</a></li>
			<li><a href="https://Kinbu.co/signup" title="">Registrarse</a></li>					
			<li><a href="/support" title="">Contacto</a></li>						
		</ul>

		<h5>Comparte experiencias. Comparte libros.</h5>  
		<p>Ha ocurrido un error en algún lugar de la página, intenta entrar al Home o sí estás registrado inicia sesión. De otro modo, te invitamos a que te registres y disfrutes de libros impresos y compartas con otros lectores.</p>

	</nav>

	<!-- main content
   ================================================== -->
   <main id="main-404-content" class="main-content-particle-js">

   	<div class="content-wrap">

		   <div class="shadow-overlay"></div>

		   <div class="main-content">
		   	<div class="row">
		   		<div class="col-twelve">
			  		
			  			<h1 class="kern-this">Error.</h1>
			  			<p>Ha existido algún error, comunícate pronto con soporte.					Intenta con los links de abajo, busca opciones en el menú izquierdo
						o intenta hacer una búsqueda de libros y lectores.
			  			</p>

			  			<div class="search">
				      	<form action="https://kinbu.co/search" method="GET" accept-charset="UTF-8" role="search">
								<input type="text" id="s" name="q" class="search-field" placeholder="Busca…">
								<button id="search-navbar" type="submit" class="invisible">
                            	</button>
							</form>
				      </div>	   			

			   	</div> <!-- /twelve --> 		   			
		   	</div> <!-- /row -->    		 		
		   </div> <!-- /main-content --> 

		   <footer>
		   	<div class="row">

		   		<div class="col-seven tab-full social-links pull-right">
			   		<ul>
				   		<li><a href="http://facebook.com/KinbuCo"><i class="fa fa-facebook"></i></a></li>
					      <li><a href="http://google.com/KinbuCol"><i class="fa fa-google-plus"></i></a></li>
					      <li><a href="http://twitter.com/KinbuCo"><i class="fa fa-twitter"></i></a></li>  			
				   	</ul>
			   	</div>
		   			
		  			<div class="col-five tab-full bottom-links">
			   		<ul class="links">
				   		<li><a href="https://kinbu.co">Homepage</a></li>
				         <li><a href="https://kinbu.co/market">Market</a></li>
				         <li><a href="/bug">Reportar Error</a></li>			                    
				   	</ul>

				   	<div class="credits invisible">
				   		<p>Design by <a href="http://www.styleshout.com/" title="styleshout">styleshout</a></p>
				   	</div>
			   	</div>   		   		

		   	</div> <!-- /row -->    		  		
		   </footer>

		</div> <!-- /content-wrap -->
   
   </main> <!-- /main-404-content -->

   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
   <script src="/js/errors/jquery-2.1.3.min.js"></script>
   <script src="/js/errors/plugins.js"></script>
   <script src="/js/errors/main.js"></script>

</body>

</html>