{{ Form::open(['url' => Config::get('payu.URL'), 'method' => 'POST', 'id' => 'payuform']) }}
{{ Form::hidden('merchantId', Config::get('payu.merchantId')) }}
{{ Form::hidden('accountId', $accountId) }}
{{ Form::hidden('description', $description) }}
{{ Form::hidden('referenceCode', $referenceCode) }}
{{ Form::hidden('amount', $amount) }}
{{ Form::hidden('tax', $tax) }}
{{ Form::hidden('taxReturnBase', $taxReturnBase) }}
{{ Form::hidden('currency', Config::get('payu.currency')) }}
{{ Form::hidden('signature', $signature) }}
{{ Form::hidden('test', Config::get('payu.test')) }}
{{ Form::hidden('buyerEmail', Auth::user()->email) }}
{{ Form::hidden('responseUrl', Config::get('payu.responseUrl')) }}
{{ Form::hidden('confirmationUrl', Config::get('payu.confirmationUrl')) }}
@if($amount > '0')
{{ Form::button(isset($btntext) ? $btntext : 'Comprar', [ 'type' => 'submit', 'role' => 'button', 'id' => 'btn_comprar', 'class' => 'btn btn-lg btn-primary btn-comprar' ]) }}
@endif
{{ Form::close() }}
