@extends('...default')

@section('location')
 Cart in process
@stop

@section('addedcss')
@parent
<style media="screen">
.list-group-item .media-object {
    float: left;
}
.list-group-item .caption {
    margin-left: 125px;
}
.list-group-item .price-delete {
    text-align: right;
}

.btn.btn-comprar {
    width: 100%;
}

</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Carrito</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-group">
                    @forelse($items as $key => $item)
                        @include('shoppingcart.cartitem', ['item' => $item, 'removable' => false])
                    @empty
                        <li class="list-group-item">No has agregado libros a tu carrito</li>
                    @endforelse
                </ul>
                <div class="pull-right">
                    Subtotal:&nbsp;<span class="pull-right">${{ $subtotal }}</span>
                    <br />
                    Total Envio:&nbsp;<span class="pull-right">${{ $total_envio }}</span>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="overflow:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Transacción</th>
                                    <th>Estado</th>
                                    <th>Referencia</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($cart->transactions()->get() as $key => $t)
                                    <tr>
                                        <td>{{ $t->id }}</td>
                                        <td>@if($t->state_pol == 4) Aprobada @elseif($t->state_pol == 6) rechazada @else {{ isset($t->state_pol)?$t->state_pol:'No confirmada' }} @endif</td>
                                        <td>{{ $t->reference_pol }}</td>
                                        <td>{{ $t->transaction_date }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No tiene transacciones</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {
}());
</script>
@endsection
