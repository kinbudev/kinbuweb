<li class="list-group-item">
    <div class="row">
        <div class="col-xs-6">
            {{ $item->image }}
            <div class="caption">
            <h3>{{ $item->published_book_title }}</h3>
            <p>{{ $item->user->name }}</p>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="price-delete">
                <div class="price">
                    $ {{ number_format($item->price,0,',','.') }}
                </div>
                @if(!isset($removable) || $removable !== false)
                    {{ link_to_route('remove_from_cart', 'Eliminar',
                        ['id' => $item->id],
                        ['id' => $item->id, 'class'=> 'btn_eliminar purple-text']) }}
                @endif
            </div>
        </div>
    </div>
</li>
