@extends('...default')

@section('location')
 Cart
@stop

@section('addedcss')
@parent
<style media="screen">
.list-group-item .media-object {
    float: left;
}
.list-group-item .caption {
    margin-left: 5px;
}
.list-group-item .price-delete {
    text-align: right;
}

.btn.btn-comprar {
    width: 100%;
    border-radius: 0px;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
fieldset.data-fieldset {
    border: 1px groove #DCD;
    padding: 0 1.4em;
}
legend.data-legend {
    width: auto;
    padding: 0 10px;
    border-bottom: none;
    margin-bottom: 10px;
}
.no-contact {
  display: block; /* display: block works as well */
}

.no-contact .btn[disabled] {
  /* don't let button block mouse events from reaching wrapper */
  pointer-events: none;
}

.no-contact.disabled {
  /* OPTIONAL pointer-events setting above blocks cursor setting, so set it he
here */
  cursor: not-allowed;
}
/* Popover */
.popover {
    border-color: #662e93;
}
/* Popover Arrow */
.arrow {
    border-top-color: #662e93 !important;
}

</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Carrito</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-group">
                    @forelse($items as $key => $item)
                        @include('shoppingcart.cartitem', ['item' => $item])
                    @empty
                        <li class="list-group-item">No has agregado libros a tu carrito</li>
                    @endforelse
                </ul>
                <div class="pull-left">
                    <span style="margin-right: 5px;">{{ link_to_route('carts', "Compras en proceso:  ", [],['class'=> 'purple-text']) }} </span>
                    <span class="pull-right"> {{ $inactive_carts->count() }}</span>
                </div>
                <div class="pull-right">
                    Cupon:<span class="pull-right row"><input type="value" name="coupon" class="form-control col-md-6" style="height: 20px; width: 80px; margin-left: 20px; display: inline;"> <button id="applycoupon" class="btn btn-primary btn-comprar col-md-6 buy-block" style="width: 60px; padding: 0; margin-top: -1px; margin-right: 15px;">Aplicar</button></span>
                    <br />
                    Subtotal:&nbsp;<span id="subtotal" class="pull-right buy-block">${{ $subtotal }}</span>
                    <br />
                    Envío:&nbsp;<span class="pull-right buy-block">${{ $total_envio }}</span>
                    <br />
                    Total:&nbsp;<span id="total" class="pull-right buy-block">${{ number_format($amount,0,',','.') }}</span>
                    <div class="no-contact disabled" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Agrega información de envío para tu compra.">
                    {{ $payuform }}
                    </div>
                </div>
                <div class="clearfix"></div>
                @include('users.contact', [
                    'name' => $cart->pickup_fullname,
                    'address' => $cart->pickup_address,
                    'phone' => $cart->pickup_phone,
                    'city' => $cart->pickup_city,
                    'additional_top' => "Estos datos se guardaran s&oacute;lo para éste pedido"
                ])
            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {
/*If users contect fields are empty */
$(document).ready(function() {
    var $submit = $(".btn-comprar"),
        $inputs = $('input[type=text]');

    function checkEmpty() {

        // filter over the empty inputs

        return $inputs.filter(function() {
            return !$.trim(this.value);
        }).length === 0;
    }

    $inputs.on('keyup blur', function() {
        $submit.prop("disabled", !checkEmpty());
        if(!checkEmpty()){
            $('.no-contact').popover()
        }
    }).keyup();  // trigger any one
});
    

    /* if my var reload isn't set locally.. in the first time it will be true */
if (!localStorage.getItem("reload")) {
    /* set reload locally and then reload the page */
    localStorage.setItem("reload", "true");
    location.reload();
}
/* after reload clear the localStorage */
else {
    localStorage.removeItem("reload");
    // localStorage.clear(); // an option
}
    $(".btn_eliminar").click(function(event) {
        event.preventDefault();
        var me = $(this);
        var id = me.attr('id');
        var route = me.attr('href');
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}'}
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje);
                me.parents("li.list-group-item").remove();
                setTimeout( function(){location.reload(true);}, 1500);
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
        return false;
    });
    $('#payuform').submit(function(event) {
        event.preventDefault();
        var params = [
            'referenceCode', 'amount',
            'pickup_fullname', 'pickup_address', 'pickup_phone', 'pickup_city'
        ];
        var form = this;
        var values = {_token: '{{ csrf_token() }}'};
        $.each($(form).serializeArray(), function(index, el) {
            if (params.indexOf(el.name) >= 0) {
                values[el.name] = el.value;
            }
        });
        $.each($('.data-fieldset').serializeArray(), function(index, el) {
            if (params.indexOf(el.name) >= 0) {
                values[el.name] = el.value;
            }
        });
        $.ajax({
            url: '{{ route('transaction') }}',
            method: 'POST',
            data: values,
        }).done(function (response) {
            if (response.success) {
                form.submit();
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
    });

    // Create our number formatter.
    var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    currencyDisplay: 'symbol',
    minimumFractionDigits: 0,
    // the default value for minimumFractionDigits depends on the currency
    // and is usually already 2
    });

    //TODO: Improve discounts with appliying criteria of books
    $("#applycoupon").click(function(){
        if($('input[name=coupon]').val() == "conoce" || $('input[name=coupon]').val() == "CONOCE" || $('input[name=coupon]').val() == "Conoce") {
        var subtotal = {{$subtotalnumber}};
        var total = {{$amount}}
        var discount = 0.2;
        var deduce = subtotal*discount;
        var newsubtotal = subtotal - deduce;
        var newsubtotalcomm = formatter.format(newsubtotal);
        var newsubtotaldot = newsubtotalcomm.replace(/,/g, '.');
        var newtotal = total - deduce;
        var newtotalcomm = formatter.format(newtotal);
        var newtotaldot = newtotalcomm.replace(/,/g, '.');
        $("#subtotal").fadeOut(function () {
            $("#subtotal").empty();
            $(this).append(newsubtotaldot);
        }).fadeIn();
        $("#total").fadeOut(function () {
            $("#total").empty();
            $(this).append(newtotaldot);
        }).fadeIn();
        $("input[name=amount]").removeProp("value");
        $("input[name=amount]").prop("value", newtotal);
        $("#applycoupon").fadeOut(function () {
            $(this).html("Aplicado");
        }).fadeIn();
        $("#applycoupon").prop("disabled", true);
        }
        else{
            var mensaje = "Cupon no válido, revisa nuevamente.";
            KINBU.Notification.show(mensaje, {fixed: true});
        }
    });
}());
</script>
@endsection
