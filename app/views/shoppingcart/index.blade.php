@extends('...default')

@section('location')
 Carts in process
@stop

@section('addedcss')
@parent
<style media="screen">
</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="overflow:auto">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Compra</th>
                            <th>Items</th>
                            <th>Transactions</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($carts as $key => $cart)
                            <tr>
                                <td>{{ link_to_route('carts.show', "#".$cart->id,
                                    ['id' => $cart->id]) }}</td>
                                <td>{{ $cart->items()->count() }}</td>
                                <td>{{ $cart->transactions()->count() }}</td>
                                <td>{{ $cart->created_at }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No tienes carritos en proceso</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {
}());
</script>
@endsection
