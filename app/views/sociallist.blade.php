<h5>{{ $message }}</h5>
<ul class="list-inline">
    @foreach($social as $provider => $link)
        <li><a href="{{ $link }}">
                {{ HTML::image('/img/FFFFFF-0.png', $provider, ['class' => "icons-social $provider"]) }}
            </a>
        </li>
    @endforeach
</ul>
