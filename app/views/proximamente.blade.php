@extends('base')

@section('title')
    Kinbu.co
@stop

@section('addedcss')
    <style>
        * {
            font-family: 'NeoSans', sans-serif;
        }

        html, body {
            margin: 0;
            height: 100%;
        }

        body {
            background: #662e93 url("/img/Kinbu-coming-soon-bg.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        img.logo {
            padding: 15px 0;
            margin: 0 auto;
        }

        div.email-form {
            padding-top: 20px;
        }
        label.email-label {
            font-size: 16px;
            margin-bottom: 0;
            color: #ccc;
        }

        input#email {
            height: 24px;
            color: #333;
        }

        input.btn-enviar {
            margin-top: 5px;
            height: 24px;
            color: #333;
            font-size: 14px;
        }

        div.top-margin {
            margin-top: 15px;
        }

        div.container.opening {
            max-width: 540px;
        }

        @media (min-width: 768px) and (max-width: 991px) {
            div.top-margin {
                margin-top: 30px;
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
            div.top-margin {
                margin-top: 60px;
            }
        }

        @media (min-width: 1200px) {
            div.top-margin {
                margin-top: 120px;
            }
        }

        p.large-text {
            font-size: 24px;
            color: #ddd;
        }

        p.open-sans {
            font-family: 'OpenSans', sans-serif;
            font-weight: 500;
        }

        @media (max-width: 480px) {
            .social-buttons {
                height: 100px;
            }

            .social-buttons > .col-xs-4 {
                text-align: center;
                margin: 0 auto;
                float: none;
            }
        }

    </style>

    <script src="https://apis.google.com/js/platform.js" async defer>
        {
            lang: 'es-419'
        }
    </script>

@stop

@section('body')

    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '375519502620981',
                xfbml: true,
                version: 'v2.2'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div class="container-fluid">

        <div class="text-center top-margin">
            <div class="container opening">
                <p class="large-text open-sans">Estamos construyendo una nueva forma de interactuar entre libros y
                    lectores.</p>
            </div>
            {{ HTML::image('img/Kinbu-logo.svg', 'logo', ['class' => 'img-responsive logo']) }}
            <p class="large-text">Comparte experiencias. Comparte libros.</p>

            <div class="email-form">
                {{ Form::open(['id' => 'emailform', 'role'=>'form', 'class' => 'form-inline']) }}

                <div class="form-group">
                    {{ Form::label('email', 'Se el primero en probarlo y disfruta la mejor experiencia de lectura:',
                    ['class' => 'email-label']) }}
                    {{ Form::email('email', null, ['required', 'placeholder' => 'email']) }}
                    {{ Form::submit('Enviar', ['class' => 'btn-enviar']) }}
                </div>
                {{ Form::close() }}
            </div>

            <br/>

            <div class="row social-buttons">
                <div class="col-xs-4">
                    <div class="fb-like" data-href="https://www.facebook.com/KinbuCo" data-layout="button"
                         data-action="like" data-show-faces="false" data-share="true"></div>
                </div>
                <div class="col-xs-4">
                    <a class="twitter-follow-button" href="https://twitter.com/KinbuCo" data-show-count="false"
                       data-lang="es">Follow {{'@'}}KinbuCo</a>
                    <script>window.twttr = (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0], t = window.twttr || {};
                            if (d.getElementById(id))return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "https://platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs);
                            t._e = [];
                            t.ready = function (f) {
                                t._e.push(f);
                            };
                            return t;
                        }(document, "script", "twitter-wjs"));</script>
                </div>
                <div class="col-xs-4">
                    <div class="g-follow" data-annotation="none" data-height="24"
                         data-href="https://plus.google.com/104317363212553386445" data-rel="publisher"></div>
                </div>
            </div>

        </div>

    </div>
@stop

@section('addedjs')
    <script>
        $(document).ready(function () {
            $('#emailform').submit(function (event) {
                event.preventDefault();

                var $form = $(this);
                var values = $form.serialize();
                var url = $form.attr('action');

                var post = $.post(url, values);
                post.done(function (data) {
                    $('#emailform').fadeOut('slow', function () {
                        if (data.success) {
                            $('.email-form').append('<p id="thank-you" class="large-text">Gracias!</p>');
                            $('#thank-you').delay(600).fadeOut('slow', function () {
                                $(this).remove();
                            });
                            $(this).trigger('reset');
                        } else {
                            $('.email-form').append('<div id="email-error"></div>');
                            var emailError = $('#email-error');
                            var errors = data.errors.email;
                            for (var i = 0; i < errors.length; i++) {
                                emailError.append('<p class="large-text">' + errors[i] + '</p>');
                            }
                            emailError.delay(600).fadeOut('slow', function () {
                                $(this).remove();
                            });
                        }
                    }).delay(1200).fadeIn('fast');
                });
            });
        });
    </script>
@stop