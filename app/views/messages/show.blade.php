<div class="panel panel-default">
    <div class="panel-heading">
        {{ $user->name }}
    </div>
    <div class="panel-body">
        @foreach($messages as $key => $message)
        <div class="message @if($message->sent_by_user) emisor @else receptor @endif">
            {{ $message->content }}
            <br />
            <small>{{ $message->since }}</small>
        </div>
        <div class="clearfix"></div>
        @endforeach
    </div>
    <div class="panel-footer"></div>
</div>
