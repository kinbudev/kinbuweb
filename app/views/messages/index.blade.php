@extends('...default')

@section('location')
Mensajes
@stop

@section('addedcss')
@parent
<style>
.tab-content {
    position: relative;
}
.loading {
    position: absolute;
    top: 5px;
    right: 5px;
    opacity: 0;
    text-align: center;
    transition: opacity 0.25s ease-out;
}
.loading.on {
    opacity: 1;
}
.loading span {
    color: #662e93;
    animation: spin 1s linear infinite;
}

@keyframes spin {
	from { transform: rotate(0deg);}
	25%  { transform: rotate(60deg);}
    50%  { transform: rotate(180deg);}
    75%  { transform: rotate(300deg);}
	to   { transform: rotate(360deg);}
}
.message {
    display: inline-block;
    padding: 8px;
    margin-bottom: 3px;
    border: 2px solid #662e93;
    border-radius: 8px;
    box-shadow: -1px 2px #868686;
}
.emisor {
    box-shadow: 1px 2px #868686;
    margin-left:150px;
    float: right!important;
}
.emisor small{
    float: right!important;
}
.receptor {
    margin-right:150px;
}
</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <button id="btn_amigos" type="button" class="btn btn-primary friends-violet-btn" role="button" class="profile-btn-primary-friends">
            Amigos
        </button>
        <div class="row" style="padding-top:15px;padding-bottom:15px">
            <div class="col-xs-4">
                <ul id="conversations" class="nav nav-pills nav-stacked" role="tablist">
                    @foreach($messages as $key => $conversation)
                    <li role="presentation" class="@if($key === 0) active @endif">
                        <a role="tab" data-toggle="pill" data-user="{{ $conversation->username }}">
                            {{ $conversation->username }}
                            <span class="badge pull-right">{{ $conversation->since }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-8">
                <div class="tab-content">
                    <div class="loading on">
                        <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
                    </div>
                    <div class="conversation">
                        <div class="panel"></div>
                    </div>
                    {{ Form::open(['route' => 'messages.store', 'method' => 'POST',
                        'id' => 'form-send-message', 'role'=>'form']) }}
                        <div class="input-group">
                            <textarea name="message" rows="1" class="form-control" style="resize:none"></textarea>
                            <span class="input-group-btn">
                                <button id="btn-send-message" type="button" name="send" class="btn btn-default">Enviar</button>
                            </span>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
    (function ($) {
        var chat = $(".conversation");
        var loading = $(".loading");

        var activeChat = $("#conversations .active").find("a").data("user");

        var changeChat = function (user) {
            loading.addClass('on');
            var current = chat.find(".panel")
            $.ajax({
                url: "/messages/" + user
            }).done(function (response) {
                current.replaceWith(response);
                loading.removeClass('on');
            }).fail(function (request, state) {
                KINBU.Notification.error(state + " " + request.status + ": "+ request.statusText);
            });
        }

        changeChat(activeChat);

        $("#conversations a").click(function (e) {
            e.preventDefault();
            activeChat = $(this).data("user");
            changeChat(activeChat);
        });

        $("#btn-send-message").click(function (e) {
            e.preventDefault();
            var form = $("#form-send-message");
            $("<input />", {type:"hidden", name:"user"}).val(activeChat).appendTo(form);
            var data = form.serialize();
            data.user = activeChat;
            console.log(data);
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: data
            }).done(function (response) {
                if (response.success) {
                    form[0].reset();
                    var conversation = $(".conversation .panel-body");
                    var message = $("<div />", {class:"message emisor"})
                        .html(response.element.content)
                        .appendTo(conversation);
                    message.append($("<br />"))
                        .append($("<small />").html(response.element.since));
                    $("<div />", {class:"clearfix"}).appendTo(conversation);
                } else {
                    KINBU.Notification.show(response.message);
                }
            }).fail(function (request, state) {
                KINBU.Notification.error(state + " " + request.status + ": "+ request.statusText);
            });
            return false;
        })
        $("#btn_amigos").click(function (event) {
            var btn_ok = $("<button/>",{
                type: 'button',
                class: 'btn btn-primary',
                'data-dismiss': 'modal',
                text: 'Aceptar'
            });
            var btn_cerrar = $("<button/>",{
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cerrar'
            });
            $.ajax({
                url: '{{ route('users.relationships', Auth::user()->username) }}',
                method: 'GET',
                data: {_token: '{{ csrf_token() }}', only_friends:true}
            }).done(function (response) {
                KINBU.Msg.create("Amigos", response, [btn_ok, btn_cerrar]);
            }).fail(function (request, status) {
                console.log(status);
            });
        });
    }(jQuery));
</script>
@stop
