@extends('...default')

@section('location')
 Pedidos
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="overflow:auto">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Estado</th>
                            <th>Carrito</th>
                            <th>ID Transaccion</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($orders as $key => $order)
                            <tr>
                                <td>{{ link_to_route('orders.show', "#".$order->id, ['id' => $order->id]) }}</td>
                                <td>{{ $order->status }}</td>
                                <td>{{ link_to_route('carts.show', "#".$order->cart_id,
                                    ['id' => $order->cart_id]) }}</td>
                                <td>{{ $order->transaction->first()->payu_id }}</td>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No tienes pedidos</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop
