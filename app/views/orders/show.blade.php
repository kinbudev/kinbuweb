@extends('...default')

@section('location')
 Pedido
@stop

@section('addedcss')
@parent
<style media="screen">

</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Informacion Pedido</h2>
                <p>Usuario:
                    <span>{{ link_to_route('users.show', $order->user->name, ['users' => $order->user->username] ) }}</span>
                </p>
                <p>Estado:
                    <span>{{ $order->status }}</span>
                </p>
                <p>ID Transaccion PayU:
                    <span>{{ $order->transaction->first()->payu_id }}</span>
                </p>
                <p>Fecha:
                    <span>{{ $order->created_at }}</span>
                </p>
                <div class="row">
                    <div class="col-xs-12" style="overflow:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Libro</th>
                                    <th>Estado</th>
                                    <th>Guia de Envio</th>
                                    <th>Fecha Creaci&oacute;n</th>
                                    <th>Fecha Actualizaci&oacute;n</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($order->shipments as $key => $shipment)
                                    <tr>
                                        <td>
                                            {{ link_to_route(
                                                'users.publications.show',
                                                $shipment->publication->published_book_title,
                                                [
                                                    'users' => $shipment->publication->user->username,
                                                    'publications' => $shipment->publication->id
                                                ]) }}
                                        </td>
                                        <td>{{ $shipment->status }}</td>
                                        <td>{{ $shipment->identifier }}</td>
                                        <td>{{ $shipment->created_at }}</td>
                                        <td>{{ $shipment->updated_at }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No tienes pedidos</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {

}());
</script>
@endsection
