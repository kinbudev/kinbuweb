<div class="row">
    <div class="col-sm-12" style="overflow:auto">
        <table class="table table-striped">
            <thead>
                <tr>
                @foreach($keys as $key => $value)
                    <th style="min-width:100px">{{ $value }}</th>
                @endforeach
                @if($actions['edit']['can'] || $actions['delete']['can'])
                    <th style="min-width:200px">Actions</th>
                @endif
                @if(isset($model['details']) && count($model['details']))
                    <th style="min-width:100px"> </th>
                @endif
                </tr>
            </thead>
            <tbody>
            @foreach($elements as $element)
                <tr>
                @foreach($keys as $key => $value)
                    <td>{{ $element->$key }}</td>
                @endforeach
                @if($actions['edit']['can'] || $actions['delete']['can'])
                    <td>
                        @if($model['parent'])
                            {{ Form::open(['route' => ['admin.' . $model['parent'] . '.' . $model['name'] . '.destroy', $parent_id, $element->id], 'method' => 'delete', ]) }}
                            @if($actions['edit']['can'])
                                {{ link_to_route('admin.' . $model['parent'] . '.' . $model['name'] . '.edit', $actions['edit']['text'], [$parent_id, $element->id], ['class' => 'btn btn-primary']) }}
                            @endif
                        @else
                            {{ Form::open(['route' => ['admin.' . $model['name'] . '.destroy', $element->id], 'method' => 'delete', ]) }}
                            @if($actions['edit']['can'])
                                {{ link_to_route('admin.' . $model['name'] . '.edit', $actions['edit']['text'], [$element->id], ['class' => 'btn btn-primary']) }}
                            @endif
                        @endif
                        @if($actions['delete']['can'])
                            {{ Form::submit($actions['delete']['text'], ['class' => 'btn btn-danger']) }}
                        @endif
                        {{ Form::close() }}
                    </td>
                @endif
                @if(isset($model['details']) && count($model['details']))
                    <td>
                        @if($model['parent'])
                            {{ link_to_route('admin.' . $model['parent'] . '.' . $model['name'] . '.show', 'Details', [$parent_id, $element->id], ['class' => 'btn btn-info']) }}
                        @else
                            {{ link_to_route('admin.' . $model['name'] . '.show', 'Details', [$element->id], ['class' => 'btn btn-info']) }}
                        @endif
                    </td>
                @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
