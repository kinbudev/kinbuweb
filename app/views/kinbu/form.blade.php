@extends('...default')

@section('location')
    {{ $location }}
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <h2>{{ $page_header }}</h2>
        @foreach($errors->all('<span class=error>:message</span>') as $message)
            {{$message}}
        @endforeach
        @if(isset($element))
            @if($model['parent'])
                {{ '';$route = ['admin.' . $model['parent'] . '.' . $model['name'] . '.update', $parent_id, $element->id] }}
            @else
                {{ '';$route = ['admin.' . $model['name'] . '.update', $element->id] }}
            @endif
            {{ Form::model($element, [
                'route' => $route,
                'method' => 'patch',
                'files' => (isset($model['has_file']) && $model['has_file'] === TRUE)
            ]) }}
        @else
            @if($model['parent'])
                {{ '';$route = ['admin.' . $model['parent'] . '.' . $model['name'] . '.store', $parent_id] }}
            @else
                {{ '';$route = ['admin.' . $model['name'] . '.store'] }}
            @endif
            {{ Form::open(['route' => $route, 'files' => (isset($model['has_file']) && $model['has_file'] === TRUE)]) }}
        @endif
        @foreach($attrs as $key => $attr)
            <div class="form-group">
                @if($attr['type'] !== 'hidden')
                    {{ Form::label($key, $attr['title']) }}
                @endif
                {{ Form::$attr['type']($key, null, ['class' => 'form-control']) }}
            </div>
        @endforeach
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-default']) }}
                {{ HTML::link(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) }}
            </div>
        {{ Form::close() }}
    </div>
</section>
@stop
