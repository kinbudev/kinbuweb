@extends('...default')

@section('location')
    {{ $location }}
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <h2>{{ $page_header }}</h2>
        <p>
        @if($model['parent'])
            <a href="{{ route('admin.' . $model['parent'] . '.' . $model['name'] . '.index', $parent_id) }}">
        @else
            <a href="{{ route('admin.' . $model['name'] . '.index') }}">
        @endif
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                Back
            </a>
        </p>
        @foreach($keys as $key => $attr)
        <div>
            {{ $attr }} : {{ $element->$key }}
        </div>
        @endforeach
        @foreach($model['details'] as $key => $value)
            {{ '';$instance = new $value }}
            <h3>{{ $instance->admin['title'] }}</h3>
            @if($instance->admin['actions']['create']['can'])
                <p>{{ link_to_route('admin.' . $model['name'] . '.' . $instance->admin['model']['name'] . '.create', $instance->admin['actions']['create']['text'], $element->id) }}</p>
            @endif
            @include('kinbu.grid', [
                'model' => $instance->admin['model'],
                'actions' => $instance->admin['actions'],
                'elements' => $element->$key()->get(),
                'keys' => $instance->admin['grid_attrs'],
                'parent_id' => $element->id
            ])
        @endforeach
    </div>
</section>
@stop
