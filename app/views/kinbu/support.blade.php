@extends('...default')

@section('location')
	Soporte

@stop

@section('addedcss')
{{ HTML::style("css/k.css") }}
@parent
<style>
    .publication {
        margin: 16px;
    }
    .publication-content {
        margin-left: 160px;
    }
    .column-title {
        text-align: center;
    }
    .form-support {
    	margin: auto;
    	max-width: 700px;
    	min-width: 300px;
    }
    .btn-kinbu {
    	background-color: #7038a3;
    	color: #F9F9F9;
    }
    .alert-kinbu {
    	border-color: #662e93 !important;
    	border-top-width: 5px;
    	border-right-width: 5px;
    	border-bottom-width: 5px;
    	border-left-width: 5px;
    	color: #662e93;
    	background-color: #ffffff;
    }
</style>
@stop

@section('content')
	<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
        	<h3 style="text-align: center;">Estamos para ayudarte</h3> <br/>
        	{{--Mensaje de correo envíado. --}}
        	@if(Session::has('message'))
        	<div style="max-width: 700px; margin: auto;" class="alert alert-succes alert-kinbu alert-dismissible" role="alert">
        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	<span aria-hidden="true">×</span>
        	</button>
        	<div class="alert-body">{{ Session::get('message') }}</div>
        	</div><br/><br><br>

            <a style="max-width: 700px; margin: auto;" href="/market" class="btn btn-lg btn-block btn-kinbu" role="button">Ir a Marketplace</a>

        	@else

			<form action="{{ action('MailController@supportmail') }}" enctype="multipart/form-data" method="POST" class="form-support">
			<div class="row" style="padding-right: 15px; padding-left: 15px;">
			@if(Auth::check())
			<input type="text" class="invisible" name="email" value="{{Auth::user()->email}}">
            <input type="text" class="invisible" name="name" value="{{Auth::user()->person->firstname}}">
			@else
			<div class="publication-soldbox">Para brindarte respuesta</div>
            <input style="margin-bottom: 15px;" type="text" class="form-control" placeholder="Nombre" required name="name">
            <input style="margin-bottom: 15px;" type="text" class="form-control" placeholder="Correo" required name="email">
			@endif
			<div class="publication-soldbox">Asunto</div>
			<input style="margin-bottom: 15px;" name="subject" type="text" class="form-control" placeholder="Asunto">
			<input type="text" class="hidden" name="user_id" value="@if(Auth::check()) {{Auth::user()->id}} @endif">
			<div class="publication-soldbox">¿En qué te podemos ayudar?</div>
			<textarea style="margin-bottom: 15px;" name="text" class="form-control" rows="3" placeholder=""></textarea>
			</div>
			<br>
			<input class="btn btn-lg btn-block btn-kinbu" type="submit" name="submit" value="Enviar"/>
			</form>

        	@endif

		</div>
	</div>
	</section>
@stop