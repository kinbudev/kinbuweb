@extends('...default')

@section('location')
    {{ $title }}
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <h2>{{ $title }} <small>Admin</small></h2>
        <p>
        @if($model['parent'])
            <a href="{{ route('admin.' . $model['parent'] . '.show', $parent_id) }}">
        @else
            <a href="{{ route('admin') }}">
        @endif
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                Back to admin
            </a>
        </p>
        @if($actions['create']['can'])
        <p>
            @if($model['parent'])
            {{ link_to_route('admin.' . $model['parent'] . '.' . $model['name'] . '.create', $actions['create']['text'], [$parent_id]) }}
            @else
            {{ link_to_route('admin.' . ($model['parent']?$model['parent'].'.':'') . $model['name'] . '.create', $actions['create']['text']) }}
            @endif
        </p>
        @endif

        @include('kinbu.grid', [
            'model' => $model,
            'actions' => $actions,
            'elements' => $elements,
            'keys' => $keys
        ])
    </div>
</section>
@stop
