@extends('base')

@section('title')
    Inicia sesi&oacute;n
@stop

@section('addedcss')
@parent
{{ HTML::style("css/session.css") }}
<style media="screen">
div.register > .btn-register {
    transition: background 0.25s ease-out;
}
div.register>.btn-register:hover, div.register>.btn-register:focus {
    color: #662e93;
    background: #EEE;
    border: 3px solid #EEE;
}
</style>
@stop

@section('body')
    {{ HTML::image('/img/Kinbu-logo.svg', 'Kinbu', ['height' => 70, 'class' => 'logo']) }}
    
        {{--Mensaje confirmación contraseña reiniciada--}}
        @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert" style="text-align: center;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <div class="alert-body">{{ Session::get('message') }}</div>
        </div>
        @endif

    <section class="col-xs-offset-3 col-sm-offset-4 col-xs-6 col-sm-4 form">
        <div>
            <h3>Inicia sesi&oacute;n en Kinbu</h3>
            @foreach($errors->all('<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <br/>
        {{ Form::open(['route'=>'login', 'role'=>'login']) }}
        <div class="form-group">
            {{Form::label('identifier', 'Correo o usuario', ['class' => 'sr-only'])}}
            {{Form::text('identifier', null, ['placeholder'=>'Correo o Usuario', 'class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('password', 'Contrase&ntilde;a', ['class' => 'sr-only'])}}
            {{Form::password('password', ['placeholder'=>'Contrase&ntilde;a', 'class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::submit('Inicia sesi&oacute;n', ['class'=>'btn btn-lg btn-block btn-kinbu'])}}
        </div>
        {{ Form::close() }}
        @include('sociallist', ['message' => "Inicia sesi&oacute;n a trav&eacute;s de", 'social' => $social])
        <div>
        {{ link_to_action('RemindersController@getRemind', "¿No recuerdas tu contraseña? recupérala",[], ['style' => 'color:blueviolet']) }}
        </div>
    </section>
    <div class="register">
        <h4 class="calltoaction">¿No tienes una cuenta?</h4>
        {{ link_to_route(
            'signup',
            "Reg&iacute;strate",
            null,
            [
                'class' => 'btn btn-md btn-register',
                'role'  => 'button'
            ]
        ) }}
    </div>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function () {
$("a").one("click", function() {
    $(this).click(function () { return false; });
});
}());
</script>
<script type="text/javascript">
(function () {
$(":submit").one("click", function() {
    $(this).click(function () { return false; });
});
}());
</script>
@endsection
