@extends('base')

@section('title')
    Reg&iacute;strate en Kinbu
@stop

@section('addedcss')
@parent
{{ HTML::style("css/session.css") }}
@stop

@section('body')
    {{ HTML::image('/img/Kinbu-logo.svg', 'Kinbu', ['height' => 70, 'class' => 'logo']) }}
    <section class="col-xs-offset-3 col-sm-offset-4 col-xs-6 col-sm-4 form">
        <div>
            <h3>Reg&iacute;strate en Kinbu</h3>
        </div>
        <br/>
        @if($user = Session::get('user'))
            {{ Form::model($user, ['route'=>'users.store', 'autocomplete']) }}
        @else
            {{ Form::open(['route'=>'users.store', 'autocomplete']) }}
        @endif

        <div class="form-group">
            {{ Form::label('username', 'Usuario', ['class' => 'sr-only']) }}
            {{ Form::text('username', null, ['placeholder' => 'Usuario', 'required', 'minlength' => 6, 'class' => 'form-control', ]) }}
            @foreach($errors->get('username', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Contrase&ntilde;a', ['class' => 'sr-only']) }}
            {{ Form::password('password', ['placeholder' => 'Contrase&ntilde;a', 'required', 'minlength' => 8, 'class' => 'form-control']) }}
            @foreach($errors->get('password', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('password_confirm', 'Confirmar Contrase&ntilde;a', ['class' => 'sr-only']) }}
            {{ Form::password('password_confirmation', ['placeholder' => 'Confirmar Contrase&ntilde;a', 'required', 'minlength' => 8, 'class' => 'form-control']) }}
            @foreach($errors->get('password_confirmation', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email', ['class' => 'sr-only']) }}
            {{ Form::email('email', null, ['placeholder' => 'Email', 'required', 'class' => 'form-control']) }}
            @foreach($errors->get('email', '<span  class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('firstname', 'Nombres', ['class' => 'sr-only']) }}
            {{ Form::text('firstname', null, ['placeholder' => 'Nombres', 'required', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('lastname', 'Apellidos', ['class' => 'sr-only']) }}
            {{ Form::text('lastname', null, ['placeholder' => 'Apellidos', 'required', 'class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::submit('Registrar', ['class' => 'btn btn-lg btn-block btn-kinbu'])}}
        </div>
        {{ Form::close() }}
        @include('sociallist', ['message' => "O reg&iacute;strate con", 'social' => $social])
        <span>¿Ya est&aacute;s registrado? {{ link_to_route('login', 'Inicia sesi&oacute;n') }}</span>
    </section>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function () {
$("a").one("click", function() {
    $(this).click(function () { return false; });
});
}());
</script>
<script type="text/javascript">
(function () {
$(":submit").one("click", function() {
    $(this).click(function () { return false; });
});
}());
</script>
@endsection

