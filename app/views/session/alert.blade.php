<div class="{{ $addedCls }} alert alert-{{ $type }} @if($dismissable)alert-dismissable @endif" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    {{ $message }}
</div>