@section('addedcss')
@parent
{{ HTML::style("css/session.css") }}
{{ HTML::style("css/k.css") }}
@stop
        <div style="text-align: center;">
            <h3>Reg&iacute;strate en Kinbu para vender</h3>
        </div>
        <br/>
        @if($user = Session::get('user'))
            {{ Form::model($user, ['route'=>'users.store2', 'autocomplete']) }}
        @else
            {{ Form::open(['route'=>'users.store2', 'autocomplete']) }}
        @endif

        <div class="form-group">
            {{ Form::label('username', 'Usuario', ['class' => 'sr-only']) }}
            {{ Form::text('username', null, ['placeholder' => 'Usuario', 'required', 'minlength' => 6, 'class' => 'form-control', ]) }}
            @foreach($errors->get('username', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Contrase&ntilde;a', ['class' => 'sr-only']) }}
            {{ Form::password('password', ['placeholder' => 'Contrase&ntilde;a', 'required', 'minlength' => 8, 'class' => 'form-control']) }}
            @foreach($errors->get('password', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('password_confirm', 'Confirmar Contrase&ntilde;a', ['class' => 'sr-only']) }}
            {{ Form::password('password_confirmation', ['placeholder' => 'Confirmar Contrase&ntilde;a', 'required', 'minlength' => 8, 'class' => 'form-control']) }}
            @foreach($errors->get('password_confirmation', '<span class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email', ['class' => 'sr-only']) }}
            {{ Form::email('email', null, ['placeholder' => 'Email', 'required', 'class' => 'form-control']) }}
            @foreach($errors->get('email', '<span  class=error>:message</span>') as $message)
                {{$message}}
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('firstname', 'Nombres', ['class' => 'sr-only']) }}
            {{ Form::text('firstname', null, ['placeholder' => 'Nombres', 'required', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('lastname', 'Apellidos', ['class' => 'sr-only']) }}
            {{ Form::text('lastname', null, ['placeholder' => 'Apellidos', 'required', 'class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::submit('Registrar', ['class' => 'btn btn-lg btn-block btn-kinbu'])}}
        </div>
        {{ Form::close() }}
        <div style="text-align: center;">
        @include('sociallist', ['message' => "O reg&iacute;strate con", 'social' => $social])
        <span>¿Ya est&aacute;s registrado? {{ link_to_route('login', 'Inicia sesi&oacute;n') }}</span>
        </div>