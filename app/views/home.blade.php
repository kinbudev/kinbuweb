@extends('base')

@section('title')
    @yield('location')Kinbu.co
@stop

@section('addedcss')
    {{ HTML::style("css/khome.css") }}
@stop

@section('body')
    <header class="homebotron">
        @if($message=Session::get('message'))
            @include('session.alert', [
                'addedCls' => 'pull-left',
                'type' => 'warning',
                'dismissable' => true,
                'message' => $message
            ])
        @endif
        <div class="pull-right">
            @if(Auth::check())
                {{ link_to_route('logout', "Cerrar sesi&oacute;n", null, ['class'=>'btn btn-md btn-primary btn-session', 'role'=>'button']) }}
            @else
                {{ link_to_route('login', "Iniciar sesi&oacute;n", null, ['class'=>'btn btn-md btn-primary btn-session', 'role'=>'button']) }}
            @endif

        </div>
        <div class="color-layer">
            <div class="text-center" id="welcomemsg">
                @if(Auth::check())
                    {{ link_to_route('home', '', null, ['class'=>'kinbu-logo']) }}
                @else
                    <div class="kinbu-logo"></div>
                @endif
                <p class="lead">
                    Comparte experiencias. Comparte libros.
                </p>
                <p>
                    {{ link_to_route('signup', "Reg&iacute;strate", null, ['class'=>'btn btn-md btn-primary btn-register', 'role'=>'button']) }}
                </p>
            </div>
        </div>
    </header>

    <section class="page-summary gray">
        <div class="container">
            <div class="media">
                <div class="media-left">
                    <img src="/img/FFFFFF-0.png" alt="Social" class="sprite-01 everyone-sprite">
                </div>
                <div class="media-body">
                    <h3 class="media-heading">Una lectura social</h3>

                    <p>Únete a Kinbu y a todos los lectores que te esperan para compartir pensamientos, lecturas y comentarios.</p>

                    <p>Hacer de tu lectura una experiencia social es ahora más sencillo. Gana puntos y medallas por registrar los libros que lees, y avanza niveles cada vez que calificas un libro. Comparte con otros lectores, ávidos y novatos, conviértete en el rey de la lectura.</p>
                    <p><a class="parrafo-boton" href="market">Entra y descúbrelo</a></p>
                </div>
            </div>
        </div>
    </section>

    <section class="page-summary">
        <div class="container">
            <div class="media">
                <div class="media-body text-right">
                    <h3 class="media-heading">Libros sin complicaciones</h3>

                    <p class="text-center visible-xs">Ingresa a Kinbu y no gastes más tiempo ni dinero. Intercambia libros en Kinbu con miles de
                        lectores que están esperando por ti. También puedes comprar y venderlos de una manera
                        segura y sencilla.</p>

                    <p class="hidden-xs">Ingresa a Kinbu y no gastes más tiempo ni dinero. Intercambia libros en Kinbu con miles de
                        lectores que están esperando por ti. También puedes comprar y venderlos de una manera
                        segura y sencilla.</p>

                </div>
                <div class="media-right">
                    <img src="/img/FFFFFF-0.png" alt="Marketplace" class="sprite-01 interchange">
                </div>
            </div>
        </div>
    </section>

    <section class="page-summary-purple">
        <div class="container">
            <div class="media">
                <div class="media-left">
                    <img src="/img/FFFFFF-0.png" alt="Marketplace 2" class="sprite-01 bookstore">
                </div>
                <div class="media-body">
                    <h3 class="media-heading">Todo en un mismo lugar</h3>

                    <p>No tienes que ir a muchas librerías de tu ciudad para esperar respuestas como “El libro está
                        agotado” o “No tenemos el libro”. Desde Kinbu, podrás ver en que librería está el libro que
                        buscas, a qué precio se encuentra, si esta en existencias y hasta lo puedes comprar sin salir
                        de tu casa.</p>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer">
        @include('social')
        <div class="white-footer">
            <h5>2016, Kinbu S.A.S Todos los derechos reservados.<br/></h5>
            <h5>Barranquilla, Colombia</h5>
        </div>
    </footer>

@stop

@section('addedjs')
    {{ HTML::script('/js/svg.js') }}
@stop
