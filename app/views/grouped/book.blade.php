@extends('...default')

@section('location')
All books
@stop

@section('content')
<section class="main-body">
    <div class="container">
    @if($no_books)
    <p class="bg-danger text-danger">No books</p>
    @elseif($books)
        @include('grouped.list')
    @endif
    </div>
</div>
@stop
