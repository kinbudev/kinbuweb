@foreach(array_chunk($books->all(), 3) as $row)
    <div class="row">
    @foreach($row as $book)
        @include('books.small', compact('book'))
    @endforeach
    </div>
@endforeach
{{ $books->appends(Request::only('q'))->links() }}
