@extends('base')

@section('title')
    @yield('location') | Kinbu.co
@stop

@section('addedcss')
{{ HTML::style("css/kbase.css") }}
{{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/hopscotch/0.2.5/css/hopscotch.min.css") }}
{{ HTML::style("//cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.css") }}
<style media="screen">
    .footer a {
        text-decoration: none;
        color: inherit;
    }
    .footer a:hover, .footer a:focus {
        text-decoration: underline;
    }
    a.alert-body {
        color: inherit;
    }
    .form-control:focus {
        border-color: #662e93;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgb(102, 46, 147);
    }
    .button-header:hover {
        background-color: #662e93 !important;
        color: #ffffff !important;
        box-sizing: border-box;
        box-shadow:inset 0px 0px 0px 2px #ffffff;
        -moz-box-shadow:inset 0px 0px 0px 2px #ffffff;
        -webkit-box-shadow:inset 0px 0px 0px 2px #ffffff;
    }
</style>
@stop

@section('body')

    <nav class="navbar navbar-default navbar-fixed-top kinbunav" role="navigation">
        <div class="container">
            <div class="navbar-header headeritem">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                    <span class="icon-bar icon-bar1"></span><span class="icon-bar icon-bar2"></span><span class="icon-bar icon-bar3"></span>
                </button>
                <a href="{{ route('home') }}" class='navbar-brand'>
                    {{ HTML::image('/img/Kinbu-logo.svg', 'Kinbu', ['height' => 30, 'style="margin-top:-5px;"']) }}
                </a>
            </div>
            @yield('userLevel')
            {{--Informacion que se colapsa en pantallas pequeñas--}}
            <div class="collapse navbar-collapse navbar-right" id="navbar">
                <ul class="nav navbar-nav">
                    @if (Auth::check())
                    {{ Form::open(['route'=>'search', 'method'=>'get', 'class'=>'navbar-form navbar-left', 'role'=>'search']) }}
                    <div class="form-group">
                        <div class="inner-addon right-addon">
                            {{ HTML::image('img/FFFFFF-0.png', 'search', ['id'=>'search-navbar', 'class' => 'icon kinbu-sprite-01 search hidden-xs']) }}

                            {{--Botón de búsqueda para pantallas tipo tablet y más pequeñas--}}
                            <button id="search-navbar" type="submit" class="btn btn-default visible-xs icon kinbu-sprite-01 search">
                            <span class="glyphicon glyphicon-search visible-xs" aria-hidden="true"></span>
                            </button>

                            {{ Form::input('search','q', null, ['id' => 'search-input', 'class'=>'form-control inactive', 'placeholder'=>'Busca libros', 'size'=>'30']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                    <li class="dropdown headeritem" id="usermenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" title="{{ Auth::user()->username }}">
                            @if(empty(Auth::user()->imgurl))
                            {{ Auth::user()->getGravatar(34, 'mm', 'g', true, ['class' => 'userpp']) }}
                            @else
                            <img src="{{Auth::user()->imgurl}}" class="userpp" width="40px" height="40px">
                            @endif
                            {{ HTML::image('img/FFFFFF-0.png', 'menu', ['class' => 'icon kinbu-sprite-01 caret-down caret-menu']) }}
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->isAdmin())
                            <li>{{ link_to_route('admin', "Admin") }}</li>
                            @endif
                            <li>{{ link_to_route('users.show', 'Mi cuenta', ['users'=>Auth::user()->username]) }}</li>
                            <li>{{ link_to_route('market', "Marketplace") }}</li>
                            <li>{{ link_to_route('orders.index', "Pedidos") }}</li>
                            <li>{{ link_to_route('user.settings', "Ajustes") }}</li>
                            <li>{{ link_to_route('logout', "Cerrar Sesi&oacute;n") }}</li>
                        </ul>
                    </li>
                    <li class="dropdown headeritem cart" >
                        <a href="{{ route('cart') }}" class="cart" style="padding-right: 0px;" role="button">
                            {{ '';$cart = Auth::user()->active_carts()->first(); $book_amount = isset($cart) ? $cart->items()->count() : 0 }}
                            <span class="glyphicon glyphicon-shopping-cart gi-1-8x" aria-hidden="true"></span>
                            <span class="item-count" style="margin-left: -3px; ">{{ $book_amount }}</span>
                        </a>
                    </li>
                    {{--Sí el usuario no está en login muestra esto--}}
                    @elseif (Auth::guest())
                    {{ Form::open(['route'=>'search', 'method'=>'get', 'class'=>'navbar-form navbar-left', 'role'=>'search']) }}
                    <div class="form-group">
                        <div class="inner-addon right-addon">
                            {{ HTML::image('img/FFFFFF-0.png', 'search', ['id'=>'search-navbar', 'class' => 'icon kinbu-sprite-01 search']) }}

                            {{--Botón de búsqueda para pantallas tipo tablet y más pequeñas--}}
                            <button id="search-navbar" type="submit" class="btn btn-default visible-xs icon kinbu-sprite-01 search">
                            <span class="glyphicon glyphicon-search visible-xs" aria-hidden="true"></span>
                            </button>

                            {{ Form::input('search','q', null, ['id' => 'search-input', 'class'=>'form-control inactive', 'placeholder'=>'Busca libros', 'size'=>'30']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                    @if(URL::full() != "http://kinbu.localhost/market" && URL::full() != "https://kinbu.co/market" && URL::full() != "https://beta.kinbu.co/market" )
                    <li class="headeritem"><a class="btn btn-default button-header" style="margin-top: 8px; margin-bottom: 8px; padding: 7px; color: #662e93; text-decoration: none; margin-right: 5px; margin-left: 5px; transition: background 0.25s ease-out;" href="/market" role="button">Da un vistazo</a> </li>
                    @endif
                    <li class="headeritem"><a class="btn btn-default button-header" style="margin-top: 8px; margin-bottom: 8px; padding: 7px; color: #662e93; text-decoration: none; margin-right: 5px; margin-left: 5px; transition: background 0.25s ease-out;" href="/login" role="button">Ingresa</a></li>
                    <li class="headeritem"><a class="btn btn-default button-header" style="margin-top: 8px; margin-bottom: 8px; padding: 7px; color: #662e93; text-decoration: none; margin-right: 5px; margin-left: 5px; transition: background 0.25s ease-out;" href="/signup" role="button">Regístrate</a></li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse-->
        </div>
    </nav>

    @yield('content')
    @yield('form')
    @yield('view')

    <div class="notification-container">

        {{--Mensaje de carrito vacio (viene de ShoppingCartController@cart) se le colocó "messages" para que no interrumpiera con las otras variables "message"--}}
        @if(Session::has('messages'))
        <div class="alert alert-warning alert-kinbu alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <div class="alert-body">{{ Session::get('messages') }}</div>
        </div>
        @endif

    </div>
    @section('addedjs')
    <script type="text/javascript">
    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.com/#x15.4.4.18
    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function forEach(a, b) {
            'use strict';
            var T, k;
            if (this == null) { throw new TypeError("this is null or not defined"); }
            var c, O = Object(this), d = O.length >>> 0;
            if ({}.toString.call(a) !== "[object Function]") {
                throw new TypeError(a + " is not a function");
            }
            if (arguments.length >= 2) { T = b; }
            k = 0;
            while (k < d) {
                if (k in O) { c = O[k]; a.call(T, c, k, O); }
                k++;
            }
        };
    }
    </script>
    {{ HTML::script("/js/kinbu.js") }}
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/hopscotch/0.2.5/js/hopscotch.min.js") }}
    {{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.js") }}
    @if(Auth::check())
    <script type="text/javascript">
        var KINBU = (function ($, k) {
            var constants = {
                UserProfileUrl: "{{ route('users.show', Auth::user()->username) }}",
                CompletedTourUrl: "{{ route('users.completed_tour', Auth::user()->username) }}",
                GetNotifications: "{{ route('notifications') }}",
            };
            k.Constants = $.extend({}, k.Constants, constants);
            return k;
        }(jQuery, KINBU))
    </script>
    @endif

    <script type="text/javascript">
    //Deja la barra de busqueda sin fondo en pantallas pequeñas
    
    if ($(window).width() < 768) {
        $("#search-input").css("background-color", "#ffffff");
    }
    </script>

    <script type="text/javascript">
    (function ($, k) {
        $(".book-cover-img").on('error', function (e) {
            var img = $(this);
            var backupImgSrc = '/img/Kinbu_placeholder.svg';
            if (img.attr('src') != backupImgSrc) {
                $(this).attr('src', backupImgSrc);
            }
        })
        //Añade efectos a barra de busqueda
        $("input#search-input").hover(
            function(){
            $(this).focus();
            $('img.kinbu-sprite-01.search').addClass("gray");
            },
            function(){
            setTimeout(function(){
                $("input#search-input").blur();
            }, 10000);
            setTimeout(function(){
                $('img.kinbu-sprite-01.search.gray').removeClass("gray");
            }, 10000);
            }
        );
        $('#usermenu').on('show.bs.dropdown', function (e) {
            $(this).find('img.caret-menu').toggleClass('caret-down caret-up');
        });
        $('#usermenu').on('hidden.bs.dropdown', function (e) {
            $(this).find('img.caret-menu').toggleClass('caret-down caret-up');
        });
        
        @if (Auth::check())
        $.ajax({
            url: k.Constants.GetNotifications
        }).done(function (response) {
            if ('requesters' in response) {
                var mensaje = "Tienes ";
                if (response.requesters > 1) {
                    mensaje += response.requesters + " nuevas peticiones";
                } else {
                    mensaje += "una nueva peticion";
                }
                mensaje += " de amistad.";
                KINBU.Notification.show(mensaje, {fixed: true});
            }
            if ('friend_users' in response) {
                var nombres = response.friend_users.map(function (user, index, array) {
                    return user.name;
                })
                var mensaje = nombres.join(", ") + " ha" + ((response.friend_users.length > 1)?"n":"") + " aceptado tu solicitud de amistad";
                KINBU.Notification.show(mensaje, {fixed: true});
            }
            if ('shipments' in response) {
                response.shipments.forEach(function (shipment, index, array) {
                    var mensaje = "Tu pedido " + shipment.book + " ha cambiado de estado a: ";
                    mensaje += shipment.shipment_status;
                    mensaje = $("<a />", {
                        href: "/orders/" + shipment.order_id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('published_books1' in response) {
                response.published_books1.forEach(function (published_book, index, array) {
                    var mensaje = published_book.name + " empez&oacute; a vender " + published_book.title;
                    mensaje = $("<a />", {
                        href: "/users/" + published_book.username + "/publications/" + published_book.id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('published_books2' in response) {
                response.published_books2.forEach(function (published_book, index, array) {
                    var mensaje = published_book.name + " empez&oacute; a vender " + published_book.title;
                    mensaje = $("<a />", {
                        href: "/users/" + published_book.username + "/publications/" + published_book.id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('books_read' in response) {
                var accionTextoMapa = {
                    read: "ha le&iacute;o",
                    reading: "est&aacute; leyendo",
                    'will read': "leer&aacute;"
                };
                response.books_read.forEach(function (book_read, index, array) {
                    var accion = accionTextoMapa[book_read.pvalue];
                    var mensaje = book_read.name + " ha indicado que " + accion + " " + book_read.title;
                    mensaje = $("<a />", {
                        href: "/books/" + book_read.book_id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('book_comments' in response) {
                response.book_comments.forEach(function (comment, index, array) {
                    var mensaje = comment.name + " coment&oacute; en " + comment.title;
                    mensaje = $("<a />", {
                        href: "/books/" + comment.id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('selling_books' in response) {
                response.selling_books.forEach(function (publication, index, array) {
                    var mensaje = "Compraron tu libro " + publication.title;
                    mensaje = $("<a />", {
                        href: "#",
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('book_comments' in response) {
                response.book_comments.forEach(function (comment, index, array) {
                    var mensaje = comment.name + " coment&oacute; en " + comment.title;
                    mensaje = $("<a />", {
                        href: "/books/" + comment.id,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('messages' in response) {
                response.messages.forEach(function (message, index, array) {
                    var mensaje = message.name + " te ha enviado un mensaje ";
                    mensaje = $("<a />", {
                        href: "/messages?" + message.username,
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
            if ('exchanges' in response) {
                response.exchanges.forEach(function (exchange, index, array) {
                    var mensaje = exchange.actor + " quiere intercambiar tu libro " + exchange.title;
                    mensaje = $("<a />", {
                        href: "/exchanges",
                        class: "alert-body",
                        html: mensaje
                    });
                    KINBU.Notification.show(mensaje, {fixed: true});
                })
            }
        }).fail(function (request, state) {
            KINBU.Notification.error(state + " " + request.status + ": "+ request.statusText);
        });
        @endif

        $('.book-rating').each(function (idx, rating) {
            var bookRating = $(rating);
            var rating = bookRating.data('rating');
            bookRating.rateYo({
                readOnly: true,
                starWidth: "16px",
                ratedFill:"#F2BD22",
                rating: rating
            });
        });
    })(jQuery, KINBU);
    </script>
    @stop

    <footer class="footer">
        <div class="container">
            <div class="white-footer" id="infofooter">
                <h5>Comparte experiencias. Comparte libros.</h5>
                <h5> <a class="btn btn-default" style="padding-top: 0px; padding-bottom: 0px; color: #E7AE18; text-decoration: none; margin-right: 10px;" href="/support" role="button">Soporte</a> <a class="btn btn-default" style="padding-top: 0px; padding-bottom: 0px; color: #E7AE18; text-decoration: none; margin-left: 10px;" href="/bug" role="button">Mejoras</a></h5>
                <h5>2016, Kinbu S.A.S Todos los derechos reservados.</h5>
                <h5> Hecho con <span class="glyphicon glyphicon-heart" aria-hidden="true" style="color: red;"></span> desde Barranquilla, Colombia</h5>
                {{ link_to_route('terms','Términos y Condiciones') }}
                -
                {{ link_to_route('privacy', 'Política de privacidad') }}
            </div>
        </div>
    </footer>
@stop
