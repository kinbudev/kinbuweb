@foreach($comments as $comment)
<div class="row">
    <div class="col-sm-12">
        <div style="padding:8px 0">
            {{ $comment->content }}
            <small>
                Hace {{ Functions::getFirstYMDHIS(date_diff(date_create('now'), $comment->created_at)) }} en
                <strong>
                    {{ link_to_route('books.show', $comment->book->title, ['id' => $comment->book->id]) }}
                </strong>
            </small>
        </div>
    </div>
</div>
@endforeach
