{{ Form::open(['route' => 'BookComments.store']) }}
{{ Form::hidden('user_id', $user->id) }}
@if(isset($book))
{{ Form::hidden('book_id', $book->id) }}
@else
{{--TODO book selector--}}
@endif
<div class="form-group">
    {{ Form::textarea('content', null, ['placeholder' => 'Comentario', 'class'=>'comment-text','rows'=>4]) }}
</div>
{{ Form::submit('Comenta') }}

@foreach($errors->get('comment', '<span  class=error>:message</span>') as $message)
    {{$message}}
@endforeach

{{ Form::close() }}
