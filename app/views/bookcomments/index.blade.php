@foreach($comments as $comment)

    @include('users.badge', [
        'user' => $comment->user,
        'comment' => $comment,
        'included_body' => 'bookcomments.show'
    ])

@endforeach
