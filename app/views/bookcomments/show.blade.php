<div>
    <div>{{ $comment->content }}</div>
    <strong>
        @if(Auth::check() && Auth::user()->id == $comment->user->id)
        <div>
            <span><i class="glyphicon glyphicon-pencil"></i>Edit</span>
            <span><i class="glyphicon glyphicon-remove"></i>Delete</span>
        </div>
            {{ link_to_route('users.show', 'You', ['users'=>$comment->user->username], ['class' => "purple-text"]) }}
        @else
            {{ link_to_route('users.show', $comment->user->person->name, ['users'=>$comment->user->username], ['class' => "purple-text"]) }}
        @endif
    </strong>
    <span>{{$comment->created_at_formatted }}</span>
</div>
