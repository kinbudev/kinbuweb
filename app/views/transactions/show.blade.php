@extends('...default')

@section('location')
 Transaccion
@stop

@section('addedcss')
@parent
<style media="screen">

</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Informacion Pedido</h2>
                <p>
                    Estado Transaccion: @if($estado_tx == 4) Aprobada @elseif($estado_tx == 6) Rechazada @elseif($estado_tx == 7) Pendiente @else {{ $estado_tx }} @endif
                    @if($not_confirmed)
                    <a href>Refrescar</a>
                    @elseif ($estado_tx != 4 || $estado_tx != 'APPROVED')
                    {{ link_to_route('carts.show', "Revisa tu carrito", ['id' => $cart_id]) }}
                    @endif
                </p>
                <p>
                    Codigo Referencia: {{ $reference_code }}
                </p>
                <p>
                    Descripcion: {{ $description }}
                </p>
                <p>
                    Precio: {{ $currency }}${{ number_format($TX_VALUE) }}
                </p>

                <div class="row">
                    <div class="col-xs-4">
                        <div class="@if($not_confirmed) well @endif">
                            <h4>Confimaci&oacute;n</h4>
                            <p>
                                @if($not_confirmed)
                                En espera por confirmaci&oacute;n de PayU. Esto puede tomar un tiempo
                                @else
                                Pago confirmado
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="">
                            <h4>Preparaci&oacute;n</h4>
                            <p>Prepararemos tu pedido para que est&eacute; listo lo m&aacute;s pronto posible</p>
                            @if(!$not_confirmed && $estado_tx == 4 || $estado_tx == 'APPROVED')
                                {{ link_to_route('orders.index', "Revisa tus pedidos") }} para obtener m&aacute;s informaci&oacute;n.
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="">
                            <h4>Env&iacute;o</h4>
                            <p>Se te notificar&aacute; cuando enviemos tu pedido</p>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:15px">
                    <div class="col-xs-5">
                        <h4>Informaci&oacute;n del pago</h4>
                        M&eacute;todo de pago: {{ $lap_payment_method_type }}
                        <br />
                        Entidad: {{ $lap_payment_method }}
                        <br />
                        Cuotas: {{ $installments_number }}
                        <br />
                        @if(isset($cus) && !empty($cus))
                        <br />
                        CUS: {{ $cus }}
                        <br />
                        Banco: {{ $pse_bank }}
                        @endif
                    </div>
                    <div class="col-xs-7">
                        <h4>Informaci&oacute;n del usuario</h4>
                        {{ Auth::user()->name }}
                        <br />
                        {{ Auth::user()->person->address }}
                        <br />
                        {{ Auth::user()->person->city }}
                        <br />
                        {{ Auth::user()->person->phone }}
                        <br />
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {

}());
</script>
@endsection
