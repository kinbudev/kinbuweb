<a href="
@if(isset($route))
{{ $route }}
@else
{{ route('users.publications.show', [$publication->user->username, $publication->url]) }}
@endif
" class="thumbnail" style="width: 246px; height: 312px;">
    {{-- $book->getCoverImg('M', 'false', true, 'Cover Image', ['class' => 'book-cover-img', 'width' => 150, 'height' => 200]) --}}
    {{ $publication->image }}
    <div class="caption">
        <h3>{{ $publication->published_book_title }}</h3>
    </div>
</a>
