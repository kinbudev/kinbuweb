@extends('...default')

@section('location')
Edita tu libro
@stop
@section('addedcss')  
@parent
{{ HTML::style("css/kbookpublication.css") }}
{{ HTML::style("css/kbookpublicationedit.css") }}
<style>
    .fade {
    transition: 2s ease-in-out;
    }
</style>
@stop

@section('content')
<div class="modal fade" id="crop">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edita tu imágen</h4>
            </div>
            <div class="modal-body" id="imgtocrop">
                
            </div>
            <div id="cropfooter" class="modal-footer">
                <button id="closecrop" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
<section class="main-body">
    <div class="container">
    @if(Auth::check())
        {{ Form::open([
            'route' => ['users.publications.update', $user->username, $publication->id],
            'method' => "GET",
            'files' => true,
            'class' => "form-horizontal book-publication"
            ]) }}
    @endif

        <h2 class="purple-text">Edita tu libro</h2>
        <br />
        <div style="display: none;" {{--Eliminar div para habilitar intercambios--}}>
        <h3>Tipo de oferta</h3>
        <div class="form-section publication-type">

            @foreach($publication_types->slice(0, 2) as $key => $type)
            <div class="checkbox">
                <input type="checkbox" id="publicationtype{{$key}}" name="publication_type[]" value="{{$type->id}}" {{ $key == 0 ? 'checked' : '' }}>
                <label for="publicationtype{{$key}}">{{ $type->description }}</label>
            </div>
            @endforeach

        </div>
        <br />
        </div>
        <h3>Informaci&oacute;n del libro</h3>
        {{var_dump($publication)}}<br/>
        <div class="form-section book-info">

            <div class="form-group @if($errors->has('book_id'))has-error @endif">
                {{ Form::label('book_id', "Titulo del libro*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    {{ Form::textarea('published_book_title', $publication->published_book_title, ['rows' => 1, 'required', 'class'=>'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('cover_type', "Tapa*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($cover_type as $key => $value)
                        @if($value->status == 1)
                            <div class="radio radio-inline">
                                <input type="radio" id="cover{{$key}}" name="cover_type" value="{{$value->id}}" @if($key == 0) @endif>
                                <label for="cover{{$key}}" class="option-label">{{ $value->description }}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('book_status', "Estado*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($book_status as $key => $value)
                        @if($value->status == 1)
                        <div class="radio radio-inline">
                            <input type="radio" id="bookstatus{{$key}}" name="book_status" value="{{$value->id}}" @if($key == 0) checked @endif>
                            <label for="bookstatus{{$key}}" class="option-label">{{ $value->description }}</label>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group" id="characteristic_types">
                {{ Form::label('characteristic_types', "Caracteristicas", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($characteristic_types as $key => $value)
                    <div class="checkbox checkbox-inline">
                        <input type="checkbox" id="characteristic_type{{$key}}" name="characteristic_types[]" value="{{$value->id}}">
                        <label for="characteristic_type{{$key}}" class="option-label">{{ $value->description }}</label>
                    </div>
                    @endforeach
                    <div class="help-block">Asegurate de seleccionar todas las caracteristicas de tu libro</div>
                </div>
            </div>

            <div class="form-group @if($errors->has('description'))has-error @endif">
                {{ Form::label('description', "Descripci&oacute;n*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    {{ Form::textarea('description', $publication->description, ['rows' => 1, 'class'=>'form-control']) }}
                    @if($errors->has('description'))
                        {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

            <div id="precio_group" class="form-group @if($errors->has('price'))has-error @endif">
                {{ Form::label('price', "Precio*", ['class' => "col-xs-3  control-label"]) }}
                <div class="col-xs-9">
                    {{ Form::number('price', number_format($publication->price,0,',',''), ['step' => 'any', 'min' => 1000, 'class' => 'form-control']) }}
                    @if($errors->has('price'))
                        {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('amount', "Cantidad*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-3">
                    {{ Form::number('amount', $publication->amount, ['step' => 1, 'min' => 1, 'class' => 'form-control']) }}
                    @if($errors->has('price'))
                        {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

        </div>
        <br />
        
        <h3>Fotos actuales</h3>
        <div class="section-photos">
            <div class="help-block">Asegurate que la suma total de fotos no superen 10MB</div>
            {{ Form::label('photo', " ") }}
            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <div class="row btn-pbedit">

                @if(is_object($photo1))
                <a href="#" id="btn-pbicrop0" class="btn-pbicrop" style="display: none;"></a>
                {{ link_to_route('rotatePhoto', '',
                        ['id' => $photo1->id],
                        ['id' => $photo1->id, 'id'=> 'btn-pbirotate0', 'class'=> 'btn-pbirotate']) }}
                {{ link_to_route('removePhoto', '',
                        ['id' => $photo1->id],
                        ['id' => $photo1->id, 'class'=> 'btn-pbidelete', 'style'=> '']) }}
                @else
                <div id="uploading-img"></div>
                @endif
                </div>
                <label for="photos" class="custom-file-upload btn btn-info option-label">
                    <span class="add-photo-icon" aria-hidden="true"></span>
                    Agrega una foto de perfil para tu libro
                </label>
                <input id="photos" class="form-control droppable" multiple="" name="photos[]" type="file">
                <div id="image-list" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <div class="row btn-pbedit">

                @if(is_object($photo2))
                <a href="#" id="btn-pbicrop1" class="btn-pbicrop" style="display: none;"></a>
                {{ link_to_route('rotatePhoto', '',
                        ['id' => $photo2->id],
                        ['id' => $photo2->id, 'id'=> 'btn-pbirotate1', 'class'=> 'btn-pbirotate']) }}
                {{ link_to_route('removePhoto', '',
                        ['id' => $photo2->id],
                        ['id' => $photo2->id, 'class'=> 'btn-pbidelete', 'style'=> '']) }}
                @else
                <div id="uploading-img"></div>
                @endif
                </div>
                <label for="photos2" class="custom-file-upload btn btn-info option-label">
                <span class="add-photo-icon" aria-hidden="true"></span>
                Agrega una foto de tu libro
                </label>
                <input id="photos2" class="form-control droppable" multiple="" name="photos[]" type="file">
                <div id="image-list2" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <div class="row btn-pbedit">
                @if(is_object($photo3))
                <a href="#" id="btn-pbicrop2" class="btn-pbicrop" style="display: none;"></a>
                {{ link_to_route('rotatePhoto', '',
                        ['id' => $photo3->id],
                        ['id' => $photo3->id, 'id'=> 'btn-pbirotate2', 'class'=> 'btn-pbirotate']) }}
                {{ link_to_route('removePhoto', '',
                        ['id' => $photo3->id],
                        ['id' => $photo3->id, 'class'=> 'btn-pbidelete', 'style'=> '']) }}
                @else
                <div id="uploading-img"></div>
                @endif
                </div>
                <label for="photos3" class="custom-file-upload btn btn-info option-label">
                <span class="add-photo-icon" aria-hidden="true"></span>
                Agrega una foto de tu libro
                </label>
                <input id="photos3" class="form-control droppable" multiple="" name="photos[]" type="file">
                <div id="image-list3" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>
        </div>

        <div>
            <h3>Plaza de venta</h3>

            <div class="section-plaza option-label" id="plaza_type" role="group" data-toggle="buttons">
                @foreach($plaza_types as $key => $plaza)
                <label for="plazatype{{$key}}" class="btn btn-default btn-plaza @if($key == 2) active @endif" data-controls="{{$plaza->pvalue}}">
                    <input type="radio" id="plazatype{{$key}}" name="plaza_type" value="{{$plaza->id}}" @if($key == 2) checked @endif>
                    {{ explode(";", $plaza->description)[0] }}
                </label>
                @endforeach
            </div>
            <div class="plaza-description help-block">
            @foreach($plaza_types as $key => $plaza)
                <div id="{{$plaza->pvalue}}" class="@if($key == 2) active @endif">
                    {{ explode(";", $plaza->description)[1] }}
                </div>
            @endforeach
            </div>
        </div>
        <br />

        <br /><br />
        <div class="buttons-publish">
        @if(Auth::check())
        {{ Form::submit('Editar', ['class' => 'btn btn-success btn-lg btn-block btn-publish']) }}
        {{ HTML::link(URL::previous(), 'Cancelar', ['class' => 'btn btn-danger btn-lg btn-block btn-cancel']) }}
        @endif
        </div>
        {{ Form::close() }}

        <br />

    </div>
</section>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function ($) {
    @if(Auth::check())
    @else
    setTimeout( function(){$("#register").modal('show');}, 7500);
    @endif
    /*$(".btn-publish").one("click", function() {
    $(this).click(function () { return false; });
    });*/
    $('#photos').bind('change', function() { var fileName = ''; fileName = '1 archivo seleccionado'; $('#file-selected').html(fileName); })

    var str = $('.account_number').text().replace(/Número de cuenta/g, 'Número de celular');
    $('#type-pay2').click(function() {
    if($('#type-pay2').is(':checked')) { 
        $('.account_number').text(str);
        $( "#typeofpay" ).val( function( index, val ) {
            return val + "+";
            })  
        }
    });
    var str1 = $('.account_number').text().replace(/Número de celular/g, 'Número de cuenta');
    $('#type-pay').click(function() {
    if($('#type-pay').is(':checked')) { 
        $('.account_number').text(str1);
        $( "#typeofpay" ).val(''); 
        }
    });

    var publicationTypeCheckboxes = $("[name='publication_type[]']"),
        price = $('#price').parent().parent(),
        plazaType = $('#plaza_type').parent(),
        amount = $("#amount").parent().parent(),
        bookStatusRadios = $("[name='book_status']"),
        characteristicTypes = $("#characteristic_types");

    var publicationTypeCheckboxesState = function () {
        var which = [];
        publicationTypeCheckboxes.each(function (index, item) {
            if (item.checked) {
                which.push(item.id);
            }
        });
        if (which.length > 0) {
            if (which.length == 2) {
                return 'both'
            } else {
                return which[0];
            }
        }
    }

    var checkPublicationTypeCheckboxes = function (e) {
        var state = publicationTypeCheckboxesState();
        console.log(state);
        switch (state) {
            case 'both': //Ambos
            case 'publicationtype0': //Venta
                console.log("PPPP");
                price.show();
                plazaType.show();
                var selected = bookStatusRadios.filter(":checked")[0].id
                if (selected == 'bookstatus0') { //Nuevo
                    amount.show();
                } else {
                    amount.hide();
                }
                break;
            case 'publicationtype1': //Intercambio
                console.log("UUUU");
                price.hide();
                plazaType.hide();
                amount.hide()
                break;
            default: //Ninguno seleccionado
                console.log(e);
                publicationTypeCheckboxes[0].checked = true;
                break;

        }
    }

    publicationTypeCheckboxes.on('change', checkPublicationTypeCheckboxes);

    var checkBookStatusRadios = function (e) {
        var selected = bookStatusRadios.filter(":checked")[0].id
        if (selected == 'bookstatus0') { //Nuevo
            characteristicTypes.hide();
            var ptcState = publicationTypeCheckboxesState();
            if (ptcState == 'publicationtype1') { //Intercambio
                amount.hide();
            } else {
                amount.show();
            }
        } else {
            characteristicTypes.show();
            amount.hide();
        }
    }

    bookStatusRadios.on('change', checkBookStatusRadios);

    checkBookStatusRadios();
    checkPublicationTypeCheckboxes();

    $("#escoger_libro").on('click', function(e) {
        var url = "{{ URL::route('books.index') }}";
        var get_libros = $.get(url);
        get_libros.done(function(data) {
            var btn_ok = $("<button/>", {
                type: 'button',
                class: 'btn btn-primary',
                'data-dismiss': 'modal',
                text: 'Aceptar'
            });
            var btn_cancelar = $("<button/>", {
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cancelar'
            });

            var search = function () {
                var val = $.trim(searchInput[0].value);
                if (val.length > 3) {
                    searchDiv.removeClass("has-error");
                    $.get("{{ route("books.index") }}?q=" + val).done(function(data) {
                        modal_body.html("");
                        modal_body.append(data);
                    });
                } else {
                    searchDiv.addClass("has-error");
                }
            };

            var header = $("<div />", {style:'display:inline-table'}).html("Escoger Libro&nbsp;");
            var searchDiv = $("<div />", { class:'input-group', style:'display:inherit;margin-left:100px' });
            var searchInput = $("<input />", {
                type:"search", name:"q",
                placeholder:"Busca", size:30,
                class:"form-control"
            }).appendTo(searchDiv.appendTo(header))
                .on('change', search);

            $("<span />", {
                class: 'glyphicon glyphicon-search'
            }).appendTo($("<button />", {
                class: 'btn btn-default', type:'button'
            }).appendTo($("<span />", {
                class: 'input-group-btn'
            }).appendTo(searchDiv)).on('click', search));

            var modal = KINBU.Msg.create(header, data, [btn_cancelar, btn_ok]);
            var modal_body = modal.modal.find("div.modal-body");
            modal_body.on('click', '.pagination a', function(e) {
                e.preventDefault();
                var pageUrl = $(this).attr('href');
                console.log(pageUrl);
                var get_libros = $.get(pageUrl);
                get_libros.done(function(data) {
                    modal_body.html("");
                    modal_body.append(data);
                });
            });
            modal_body.on('click', '.row a', function(e) {
                e.preventDefault();
                $("#book_id")[0].value = $(this).attr('href').split('books/')[1];
                $("#book_name").text($(this).find("h4").text());
            });

            modal.modal.on('hidden.bs.modal', function (e) {
                modal_body.off('click', '.row a');
                modal_body.off('click', '.pagination a');
            });
        });

    });
    $("#photos").change(function(event) {
        var files = event.target.files;
        $("#image-list").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });
    $("#photos2").change(function(event) {
        var files = event.target.files;
        $("#image-list2").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list2").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });
    $("#photos3").change(function(event) {
        var files = event.target.files;
        $("#image-list3").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list3").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });

    //Receiving current info of publication
    $(".btn-plaza").on('click', function (e) {
        var control = $(this).data('controls');
        $(".plaza-description .active").toggleClass('active');
        $(".plaza-description #" + control).addClass('active');
    });

    @if($publication->cover_type == 9) {
    	$('[value=9]').prop('checked', true);
    	$('[value=8]').prop('checked', false);
        }
    @else{
    	$('[value=9]').prop('checked', false);
    	$('[value=8]').prop('checked', true);
    }
    @endif
    @if($publication->book_status == 200) {
    	$('[value=200]').prop('checked', true);
    	$('[value=201]').prop('checked', false);
        }
    @else{
    	$('[value=200]').prop('checked', false);
    	$('[value=201]').prop('checked', true);
        characteristicTypes.show();
        amount.hide();
        @forelse($publication->characteristics()->get() as $key => $characteristic)
        $('[value={{$characteristic->id}}]').prop('checked', true);
        @empty
        @endforelse
    }
    @endif
    switch({{$publication->plaza_type}}){
    case(206):
    	$('[value=206]').prop('checked', true); $('[for=plazatype0]').addClass("active");
    	$('[value=207]').prop('checked', false); $('[for=plazatype1]').removeClass("active");
    	$('[value=208]').prop('checked', false); $('[for=plazatype2]').removeClass("active");
        break;
    case(207):
    	$('[value=206]').prop('checked', false); $('[for=plazatype0]').removeClass("active");
    	$('[value=207]').prop('checked', true); $('[for=plazatype1]').addClass("active");
    	$('[value=208]').prop('checked', false); $('[for=plazatype2]').removeClass("active");
    	break;
    case(208):
    	$('[value=206]').prop('checked', false); $('[for=plazatype0]').removeClass("active");
    	$('[value=207]').prop('checked', false); $('[for=plazatype1]').removeClass("active");
    	$('[value=208]').prop('checked', true); $('[for=plazatype2]').addClass("active");
    	break
    }
    //Loading current images
    @if(is_object($photo1))
    var thumbnail = $("<img />", {
        class: '{{$photo1->id}} photo-thumbnail img-responsive',
        src: '../../../../{{$photo1->image_url}}',
        title: '{{$photo1->image_file_name}}'
    });
    var div = $("<div />", {
        class : 'layerimg0',
        html: thumbnail
    });
    $('[for=photos]').append(div);
    @else

    @endif

    @if(is_object($photo2))
    var thumbnail = $("<img />", {
        class: '{{$photo2->id}} photo-thumbnail img-responsive',
        src: '../../../../{{$photo2->image_url}}',
        title: '{{$photo2->image_file_name}}'
    });
    var div = $("<div />", {
        class : 'layerimg1',
        html: thumbnail
    });
    $('[for=photos2]').append(div);
    @else

    @endif

    @if(is_object($photo3))
    var thumbnail = $("<img />", {
        class: '{{$photo3->id}} photo-thumbnail img-responsive',
        src: '../../../../{{$photo3->image_url}}',
        title: '{{$photo3->image_file_name}}'
    });
    var div = $("<div />", {
        class : 'layerimg2',
        html: thumbnail
    });
    $('[for=photos3]').append(div);
    @else

    @endif

    
    //Crop images JS
    @if(is_object($photo1))
    $("#btn-pbicrop0").click(function() {
        $("#crop").modal('show');
        var imgcrop = $("<img />", {
            class: '{{$photo1->id}} imgcrop img-responsive',
            src: '../../../../{{$photo1->image_url}}',
            title: '{{$photo1->image_file_name}}'
        });
        var div = $("<div />", {
            class : 'divcrop',
            html: imgcrop
        });
        $("#imgtocrop").append(div);
        $("#cropfooter").append('{{ link_to_route('remove_from_cart', 'Cortar', ['id' => $photo1->id], ['id' => $photo1->id, 'class'=> 'btn-crop btn btn-success', 'role'=> 'button']) }}');
    });
    @else

    @endif


    @if(is_object($photo2))
    $("#btn-pbicrop1").click(function() {
        $("#crop").modal('show');
        var imgcrop = $("<img />", {
            class: '{{$photo2->id}} imgcrop img-responsive',
            src: '../../../../{{$photo2->image_url}}',
            title: '{{$photo2->image_file_name}}'
        });
        var div = $("<div />", {
            class : 'divcrop',
            html: imgcrop
        });
        $("#imgtocrop").append(div);
    });
    @else

    @endif


    @if(is_object($photo3))
    $("#btn-pbicrop2").click(function() {
        $("#crop").modal('show');
        var imgcrop = $("<img />", {
            class: '{{$photo3->id}} imgcrop img-responsive',
            src: '../../../../{{$photo3->image_url}}',
            title: '{{$photo3->image_file_name}}'
        });
        var div = $("<div />", {
            class : 'divcrop',
            html: imgcrop
        });
        $("#imgtocrop").append(div);
    });
    @else

    @endif


    $("#closecrop").click(function(){
        $("#imgtocrop").empty();
        $(".btn-crop").remove();
    });

    //Rotate images JS
    @if(is_object($photo1))
    var degrees0 = 0;
    $("#btn-pbirotate0").click(function(event) {
        event.preventDefault();
        var me = $(this);
        var id = me.attr('id');
        var route = me.attr('href');
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}'}
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje);
                degrees0 += 90;
                $('.{{$photo1->id}}').css({

                'transform': 'rotate(' + degrees0 + 'deg)',
                '-ms-transform': 'rotate(' + degrees0 + 'deg)',
                '-moz-transform': 'rotate(' + degrees0 + 'deg)',
                '-webkit-transform': 'rotate(' + degrees0 + 'deg)',
                '-o-transform': 'rotate(' + degrees0 + 'deg)'
                });
                $('.layerimg0').toggleClass("scaleimg")
                $('.{{$photo1->id}}').toggleClass("compensatescale")
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
        return false;
    });
    @else

    @endif


    @if(is_object($photo2))
    var degrees1 = 0;
    $("#btn-pbirotate1").click(function(event) {
        event.preventDefault();
        var me = $(this);
        var id = me.attr('id');
        var route = me.attr('href');
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}'}
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje);
                degrees1 += 90;
                $('.{{$photo2->id}}').css({

                'transform': 'rotate(' + degrees1 + 'deg)',
                '-ms-transform': 'rotate(' + degrees1 + 'deg)',
                '-moz-transform': 'rotate(' + degrees1 + 'deg)',
                '-webkit-transform': 'rotate(' + degrees1 + 'deg)',
                '-o-transform': 'rotate(' + degrees1 + 'deg)'
                });
                $('.layerimg1').toggleClass("scaleimg")
                $('.{{$photo2->id}}').toggleClass("compensatescale")
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
        return false;
    });
    @else

    @endif


    @if(is_object($photo3))
    var degrees2 = 0;
    $("#btn-pbirotate2").click(function(event) {
        event.preventDefault();
        var me = $(this);
        var id = me.attr('id');
        var route = me.attr('href');
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}'}
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje);
                degrees2 += 90;
                $('.{{$photo3->id}}').css({

                'transform': 'rotate(' + degrees2 + 'deg)',
                '-ms-transform': 'rotate(' + degrees2 + 'deg)',
                '-moz-transform': 'rotate(' + degrees2 + 'deg)',
                '-webkit-transform': 'rotate(' + degrees2 + 'deg)',
                '-o-transform': 'rotate(' + degrees2 + 'deg)'
                });
                $('.layerimg2').toggleClass("scaleimg")
                $('.{{$photo3->id}}').toggleClass("compensatescale")
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
        return false;
    });
    @else

    @endif

    //Deleting images JS
    $(".btn-pbidelete").click(function(event) {
        event.preventDefault();
        var me = $(this);
        var id = me.attr('id');
        var route = me.attr('href');
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}'}
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje);
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
        return false;
    });
    var route = $(".form-horizontal").prop("action");
    var newroute = (route+'/update');
    $(".form-horizontal").prop("action", newroute);
}(jQuery));
</script>
@endsection
