@extends('...default')

@section('location')
    Publicaciones de
    @if(isset($user))
        {{ $user->username }}
    {{-- @elseif(isset($book))
        {{ $book->title }}
        --}}
    @endif

@stop

@section('addedcss')
@parent
<style>
    .publication {
        margin: 16px;
    }
    .publication-content {
        margin-left: 160px;
    }
    .column-title {
        text-align: center;
    }
</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <h4>Publicaciones de
            @if(isset($user))
                {{ $user->username }}
            @elseif(isset($book))
                {{ $book->title }}
            @endif
        </h4>
        <div class="row">
            <div class="col-md-6">
                <h5 class="column-title">A la venta <span class="badge">{{ $on_sale->count() }}</span></h5>
                @forelse($on_sale as $key => $publication)
                <div class="row">
                    <div class="publication">
                    @if(isset($user))
                        <a href="{{ route('users.publications.show', [$user->username, $publication->url]) }}"
                            class="btn btn-default" style="text-align: left; display: block">
                            {{ $publication->image }}
                            <div class="publication-content">
                                <h4>{{ $publication->published_book_title }}</h4>
                                <ul class="list-unstyled">
                                    <li><strong>Precio:</strong> ${{ number_format($publication->price,0,',','.') }}</li>
                                    <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                                    <li><strong>Tapa:</strong> {{ $publication->cover_description }}</li>
                                    <li><strong>Cantidad:</strong> {{ $publication->amount }}</li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                     {{-- @elseif(isset($book))
                        <a href="{{ route('users.publications.show', ['publications'=>$publication->url, 'users'=>$publication->user->username]) }}"
                            class="btn btn-default" style="text-align: left; display: block">
                            {{ {{ $publication->image }} }}
                            <div class="publication-content">
                                <h4>{{ $publication->user->name }} <small>Nivel</small> <span class="badge">{{ $publication->user->level }}</span></h4>
                                <ul class="list-unstyled">
                                    <li><strong>Precio:</strong> ${{ number_format($publication->price,0,',','.') }}</li>
                                    <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                                    <li><strong>Cover:</strong> {{ $publication->cover_description }}</li>
                                    <li><strong>Cantidad:</strong> {{ $publication->amount }}</li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </a> --}}
                    @endif
                    </div>
                </div>
                @empty
                <div>
                    @if(isset($user))
                        {{ $user->username }} no ha publicado
                    {{-- @elseif(isset($book))
                        {{ $publication->published_book_title }} no est&aacute; a la venta 
                        --}}
                    @endif
                </div>
                @endforelse
            </div>
            <div class="col-md-6">
                <h5 class="column-title">Intercambia <span class="badge">{{ $on_exchange->count() }}</span></h5>
                @forelse($on_exchange as $key => $publication)
                <div class="row">
                    <div class="publication">
                    @if(isset($user))
                        <a href="{{ route('users.publications.show', [$user->username, $publication->url]) }}"
                            class="btn btn-default" style="text-align: left; display: block">
                            {{ $publication->image }}
                            <div class="publication-content">
                                <h4>{{ $publication->published_book_title }}</h4>
                                <ul class="list-unstyled">
                                    @if($publication->type == 'both')
                                    <li><strong>Precio:</strong> Intercambio o ${{ number_format($publication->price,0,',','.') }}</li>
                                    @else
                                    <li><strong>Intercambio</strong></li>
                                    @endif
                                    <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                                    <li><strong>Tapa:</strong> {{ $publication->cover_description }}</li>
                                    <li><strong>Cantidad:</strong> {{ $publication->amount }}</li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    {{-- @elseif(isset($book))
                        <a href="{{ route('books.publications.show', [$book->id, $publication->url]) }}"
                            class="btn btn-default" style="text-align: left; display: block">
                            {{ $publication->user->getGravatar(80, 'mm', 'g', true, ['class' => 'pull-left', 'width' => 100, 'height' => 100]) }}
                            <div class="publication-content">
                                <h4>{{ $publication->user->name }} <small>Nivel</small> <span class="badge">{{ $publication->user->level }}</span></h4>
                                <ul class="list-unstyled">
                                    <li><strong>Precio:</strong> ${{ number_format($publication->price,0,',','.') }}</li>
                                    <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                                    <li><strong>Cover:</strong> {{ $publication->cover_description }}</li>
                                    <li><strong>Cantidad:</strong> {{ $publication->amount }}</li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </a> --}}
                    @endif 
                    </div>
                </div>
                @empty
                <div>
                    @if(isset($user))
                        {{ $user->username }} no ha publicado
                    {{-- @elseif(isset($book))
                        {{ $book->title }} no est&aacute; a la venta
                        --}}
                    @endif
                </div>
                @endforelse
            </div>
        </div>
    </div>
</section>
@stop
