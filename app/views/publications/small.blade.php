<div class="col-sm-4 book" style="height: 200px">
    <a href="{{ route('users.publications.show', ['publications'=>$publication->url, 'users'=>$publication->user->username]) }}"> {{--TODO: busqueda por nombre sigue sin servir cliquear redirija a vista de publicaciion--}}
        <div class="book-cover">
            {{ $publication->image }}
            <div class="inner-book-content">
                <h4>{{ $publication->published_book_title }}</h4>
                {{-- <div class="book-rating" data-rating="{{ $book->rating() }}"></div> --}}
                <strong class="book-price" @if($publication->type == 'both') style="font-size: 15px" @endif>
                    @if(in_array($publication->type, ['exchange', 'both']))
                    Intercambio
                    @endif
                    @if($publication->type == 'both')
                    o
                    @endif
                    @if(in_array($publication->type, ['sale', 'both']))
                    Desde COP ${{ number_format($publication->price,0,',','.') }}
                    @endif
                </strong>
            </div>
        </div>
    </a>
</div>