@extends('...default')

@section('location')
{{$publication->published_book_title}} Comprar en
@stop

@section('addedcss')
@parent
<style media="screen">

.book-cover-img{
    height: 200px !important;
}

.buy-or-exchange{
    margin-right: 15px;
}

#btn_intercambiar{
    width: 120px;
    background-color: #7038a3;
    color: #F9F9F9;
}

#btn_intercambiar:hover{
    background-color: #6828a2;
}

#btn_agregar_carrito{
    width: 120px;
}

#btn_ver_carrito{
    width: 120px;
}

ul.characteristics {
    padding-left: 10px;
}
ul.characteristics li:before {
    color: blueviolet;
    content: "\e067";
    font-family: 'Glyphicons Halflings';
    font-size: 9px;
    float: left;
    margin-top: 4px;
    padding-right: 10px;
}
.book.exchangable {
    box-shadow: 0 2px 5px rgba(0,0,0,0.16), 0 2px 10px rgba(0,0,0,0.12);
    border-radius: 2px;
    margin: 0.5rem 0 1rem 0;
    background-color: rgba(200, 200, 200, 0.1);
}
.book.exchangable .book-content {
    padding: 20px;
    border-radius: 0 0 2px 2px;
}
.book.exchangable .book-content .book-title {
    line-height: 48px;
}
.book.exchangable .book-title {
    font-size: 24px;
    font-weight: 300;
}
.book.exchangable .book-content p {
    margin: 0;
    padding: 0;
}
.book.exchangable.active {
    color: white;
    background-color: #4D54A5;
}
</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-xs-12 col-sm-6 col-md-5 col-lg-4">
                <div style="display:inline-flex">
                    @include('publications.thumbnail', ['publication' => $publication])
                </div>
            </div>
            <div class="col-lg-offset-2 col-xs-12 col-sm-6 col-md-5 col-lg-4">
                <h4 style="text-align:center">Publicado por:</h4>
                @include('users.badge', ['user' => $publication->user, 'with_name' => true,'usertime' => ''])
            </div>
        </div>

        <br>

        <div class="row">
            @if(Auth::check())
                @if(Auth::user()->has_location_data)
                <div class="buy-or-exchange pull-right row">

                    @if($publication->type == 'exchange' || $publication->type == 'both')
                    <button type="button" role="button" id="btn_intercambiar" class="btn col-xs-6" style="margin-right: 15px" @if(Auth::user()->username === $publication->user->username) disabled @endif>Intercambiar</button>
                    @endif

                    @if($publication->type == 'sale' || $publication->type == 'both')
                        @if($is_in_cart)
                        {{ link_to_route('cart', 'Ver en el carrito', null, ['role'=> 'button', 'id'=>'btn_ver_carrito', 'class'=>'btn btn-warning col-xs-6']) }}
                        @elseif(!$publication->can_be_bought)
                        <div class="userlevel" style="width: 130px">
                        Libro Comprado
                        </div>
                        @else
                        <button type="button" role="button" id="btn_agregar_carrito" class="btn btn-success col-xs-6" @if(Auth::user()->username === $publication->user->username) disabled @endif>Agregar al carrito</button>
                        @endif
                    @endif

                </div>

                @else
                <div>
                    No tienes informacion de contacto, por favor actualizala
                    para habilitarte las compras e intercambios:
                    <span>
                        {{ link_to_route('users.edit', 'Mi cuenta', ['users'=>Auth::user()->username]) }}
                    </span>
                </div>
                @endif

            @else
            <div class="pull-right" style="margin-right: 15px;">
            {{ link_to_route('signup', "Reg&iacute;strate para comprar", null, ['class'=>'btn btn-primary btn-register', 'role'=>'button']) }}
            </div>
            @endif



            <div class="col-xs-12">
                <div class="detalles">
                    <h4>Detalles</h4>
                    <ul class='list-unstyled'>
                        @if($publication->type == 'sale' || $publication->type == 'both')
                        <li><strong>Precio:</strong> ${{ number_format($publication->price,0,',','.') }}</li>
                        @endif
                        @if($publication->book_state == 'good')
                        <li><strong>Cantidad:</strong> {{ $publication->amount }}</li>
                        @endif
                        <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                        <li><strong>Tapa:</strong> {{ $publication->cover_description }}</li>
                        <li><strong>Caracter&iacute;sticas:</strong>
                            <ul class="list-unstyled characteristics">
                                @forelse($publication->characteristics()->get() as $key => $characteristic)
                                <li>@if($characteristic->description){{ $characteristic->description }}@else{{ $characteristic->pvalue }}@endif</li>
                                @empty
                                <li>EL usuario no ha agregado caracteristicas</li>
                                @endforelse
                            </ul>
                        </li>
                        <li><strong>Descripci&oacute;n:</strong> {{ $publication->description }}</li>
                        <li><strong>Fotos:</strong> <br/>
                            <div style="text-align: center;">
                            @forelse($publication->photos()->get() as $key => $photo)
                            <div style="max-width: 70%; max-height: 80%; display: inline-block; text-align: left;">
                            {{ $photo->image }}
                            </div>
                            <br>
                            @empty
                            EL usuario no ha agregado fotos
                            @endforelse
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {
    $("#btn_agregar_carrito").click(function(event) {
        var me = $(this);
        var route = "{{ route('add_to_cart') }}";
        $.ajax({
            url: route,
            method: 'POST',
            data: {_token: '{{ csrf_token() }}', publication_id: {{ $publication->id }} }
        }).done(function (response) {
            if (response.success) {
                KINBU.Notification.show(response.mensaje, {fixed: true});
                me.replaceWith('{{ link_to_route('cart', 'Ver en el carrito', null, ['role'=> 'button', 'class'=>'btn btn-warning']) }}');
            } else {
                var content = $('<div />').append(
                    $('<span />', {
                        class: 'glyphicon glyphicon-alert',
                        'aria-hidden': true
                    })
                ).append(response.mensaje);
                KINBU.Notification.show(content);
            }
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
    });

    $("#btn_intercambiar").click(function(event) {
        var me = $(this);
        var for_exchange = "{{ route('for_exchange') }}";
        var books = [];
        $.ajax({
            url: for_exchange
        }).done(function (response) {
            var btn_aceptar = $("<button/>",{
                type: 'button',
                class: 'btn btn-primary',
                text: 'Aceptar'
            });
            var btn_cancelar = $("<button/>",{
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cancelar'
            });
            var botones = [];
            var body = $("<div />", {
                class: "books"
            });
            if (response.success && response.data.length > 0) {
                botones.push(btn_aceptar);
                response.data.forEach(function (publication, idx) {
                    var title = $("<span />", {
                        class: "book-title",
                        html: publication.book.title
                    });
                    var content = $("<div />", {
                        class: "book-content"
                    }).append([
                        title,
                        $("<p />", { html: publication.description }),
                        $("<p />", { html: publication.price })
                    ]);
                    $("<div />", {
                        class: "book exchangable",
                        'data-publication-id': publication.id
                    }).append(content).appendTo(body);
                });
            } else {
                var no_books = $("<span />", {
                    text: "No tienes libros para intercambiar, publica libros como intercambio en tu perfil "
                });

                /* Se busca crear un llamado a la acción con link a formato de publicación

                var action_call = $("<a />",{
                    href:"users.publications.create"
                }); */

                body.append(no_books/*, action_call*/);
            }
            botones.push(btn_cancelar);
            var msg = KINBU.Msg.create("Selecciona hasta 3 libros como propuesta de intercambio", body, botones);

            $('.book.exchangable').on('click', function (e) {
                $(this).toggleClass('active');
            });
            btn_aceptar.on('click', function (e) {
                var books = $('.book.exchangable.active');
                if (books.length > 0) {
                    var publicationIds = [];
                    books.each(function (i,el) {
                        publicationIds.push($(el).data('publicationId'));
                    });
                    var route = "{{ route('exchange') }}";
                    $.ajax({
                        url: route,
                        method: 'POST',
                        data: {_token: '{{ csrf_token() }}', publication_id: {{ $publication->id }}, publications: publicationIds }
                    }).done(function (response) {
                        if (response.success) {
                            KINBU.Notification.show(response.message, {fixed: true});
                        } else {
                            var content = $('<div />').append(
                                $('<span />', {
                                    class: 'glyphicon glyphicon-alert',
                                    'aria-hidden': true
                                })
                            ).append(response.message);
                            KINBU.Notification.show(content);
                        }
                    }).fail(function (request, status) {
                        KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
                    });
                    msg.modal.modal('hide');
                } else {
                    alert("Debes escoger por lo menos un libro.")
                }
            });
        }).fail(function (request, status) {
            KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
        });
    });
}());
</script>
@endsection
