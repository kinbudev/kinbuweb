@extends('...default')

@section('location')
    Publicaciones de
    @if(isset($user))
        {{ $user->username }}
    @endif

@stop

@section('addedcss')
{{ HTML::style("css/k.css") }}
@parent
<style>
    .publication {
        margin: 16px;
    }
    .publication-content {
        margin-left: 160px;
    }
    .column-title {
        text-align: center;
    }

</style>
@stop

@section('content')
<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
                <h3 class="column-title"> Tus libros vendidos <span class="badge"></span></h3>
                <br>
                <div class="publication-soldbox">
                @forelse($sold as $key => $publication)
                <div class="row">
                        <div class="col-sm-6" style="width: 20%; cursor: pointer;">{{ $publication->image }}</div>
                        <div class="col-sm-6 pull-left" style="margin-top: 10px;">
                            <ul>
                                <li><strong>Título</strong> <a href="{{ route('users.publications.show', [$user->username, $publication->url]) }}"> <span class="purple-text">{{ $publication->published_book_title }} </span></a></li>
                                <br>
                                <li><strong>Precio de venta:</strong> ${{ number_format($publication->price,0,',','.') }}</li>
                                <br>
                                <li><strong>Estado:</strong> {{ $publication->book_state_description }}</li>
                                <br>
                                <li><strong>Tapa:</strong> {{ $publication->cover_description }}</li>
                                <br>
                                <li><strong>Cantidad restante:</strong> {{ $publication->amount }}</li>
                            </ul>
                        </div>
                </div> <br/><br><br>
                @empty
                <div style="text-align: center !important;">Aún no has vendido tu primer libro, nos encargamos de todos los procesos, tú sólo <a class="purple-text" href="{{ route('users.publications.create',["users" => $user->username]) }}">publica tú libro</a> y gana dinero ayudando a tener un mundo más lector.</div>
                @endforelse
                </div>
        </div>
    </div>
</section>
@stop