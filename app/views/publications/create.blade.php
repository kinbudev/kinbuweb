@extends('...default')

@section('location')
Publica un libro
@stop

@section('addedcss')  
@parent
{{ HTML::style("css/kbookpublication.css") }}
<style>
    .fade {
    transition: 2s ease-in-out;
    }
</style>
@stop

@section('content')
@if(!Auth::check())
<div class="modal fade" id="register" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #663d91;">
            {{ HTML::image('/img/Kinbu-logo.svg', 'Kinbu', ['height' => 70, 'class' => 'logo']) }}
            </div>
            <div class="modal-body" style="background-color: #F9F9F9;">
                @include('session.badgesignup')
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="hidden">{{Session::put('url.intended', URL::full());}}</div>{{--Need evaluations.--}}
@endif
<section class="main-body">
    <div class="container">
    @if(Auth::check())
        {{ Form::open([
            'route' => ['users.publications.store', $user->username],
            'files' => true,
            'class' => "form-horizontal book-publication"
            ]) }}
    @else
        {{ Form::open([
            'route' => ['signup'],
            'class' => "form-horizontal book-publication"
            ]) }}
    @endif

        <h2 class="purple-text">Publica un libro</h2>
        <br />
        {{ link_to_route('howpublish', "¿Por qué y cómo vender libros en Kinbu?",[], ['style' => 'color:blueviolet']) }}
        <div style="display: none;" {{--Eliminar div para habilitar intercambios--}}>
        <h3>Tipo de oferta</h3>
        <div class="form-section publication-type">

            @foreach($publication_types->slice(0, 2) as $key => $type)
            <div class="checkbox">
                <input type="checkbox" id="publicationtype{{$key}}" name="publication_type[]" value="{{$type->id}}" {{ $key == 0 ? 'checked' : '' }}>
                <label for="publicationtype{{$key}}">{{ $type->description }}</label>
            </div>
            @endforeach

        </div>
        <br />
        </div>

        <h3>Informaci&oacute;n del libro</h3>

        <div class="form-section book-info">

            <div class="form-group @if($errors->has('book_id'))has-error @endif">
                {{ Form::label('book_id', "Titulo del libro*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    <!--span id="book_name">@if(isset($book)) {{ $book->title }} @endif</span> //ESTE ES UN BOTON QUE SE UTILIZA PARA ESCOGER LIBROS YA EXISTENTES EN LA BASE DE DATOS Y NO TENER QUE ESCRIBIR UN TITULO
                        @if(isset($book))
                            {{ Form::hidden('book_id', $book->id) }}
                        @else
                            {{ Form::hidden('book_id') }}
                            <button type="button" name="button" class="btn btn-default" id="escoger_libro">Escoger Libro</button>
                        @endif
                        @if($errors->has('book_id'))
                            {{ $errors->first('book_id', '<span class="help-block">:message</span>') }}
                        @endif
                    </span-->
                    {{ Form::textarea('published_book_title', null, ['rows' => 1, 'required', 'class'=>'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('cover_type', "Tapa*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($cover_type as $key => $value)
                        @if($value->status == 1)
                            <div class="radio radio-inline">
                                <input type="radio" id="cover{{$key}}" name="cover_type" value="{{$value->id}}" @if($key == 0) checked @endif>
                                <label for="cover{{$key}}" class="option-label">{{ $value->description }}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('book_status', "Estado*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($book_status as $key => $value)
                        @if($value->status == 1)
                        <div class="radio radio-inline">
                            <input type="radio" id="bookstatus{{$key}}" name="book_status" value="{{$value->id}}" @if($key == 0) checked @endif>
                            <label for="bookstatus{{$key}}" class="option-label">{{ $value->description }}</label>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="form-group" id="characteristic_types">
                {{ Form::label('characteristic_types', "Caracteristicas", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    @foreach($characteristic_types as $key => $value)
                    <div class="checkbox checkbox-inline">
                        <input type="checkbox" id="characteristic_type{{$key}}" name="characteristic_types[]" value="{{$value->id}}">
                        <label for="characteristic_type{{$key}}" class="option-label">{{ $value->description }}</label>
                    </div>
                    @endforeach
                    <div class="help-block">Asegurate de seleccionar todas las caracteristicas de tu libro</div>
                </div>
            </div>

            <div class="form-group @if($errors->has('description'))has-error @endif">
                {{ Form::label('description', "Descripci&oacute;n*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                    {{ Form::textarea('description', null, ['rows' => 1, 'class'=>'form-control']) }}
                    @if($errors->has('description'))
                        {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

            <div id="precio_group" class="form-group @if($errors->has('price'))has-error @endif">
                {{ Form::label('price', "Precio*", ['class' => "col-xs-3  control-label"]) }}
                <div class="col-xs-9">
                    {{ Form::number('price', 10000, ['step' => 'any', 'min' => 1000, 'class' => 'form-control']) }}
                    @if($errors->has('price'))
                        {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('amount', "Cantidad*", ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-3">
                    {{ Form::number('amount', 1, ['step' => 1, 'min' => 1, 'class' => 'form-control']) }}
                    @if($errors->has('price'))
                        {{ $errors->first('price', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

        </div>
        <br />
        
        <h3>Fotos</h3>
        <div class="section-photos">
            <div class="help-block">Asegurate que la suma total de fotos no superen 10MB</div>
            {{ Form::label('photos', " ") }}
            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <label for="photos" class="custom-file-upload btn btn-info option-label">
                    <span class="add-photo-icon" aria-hidden="true"></span>
                    Agrega una foto de perfil para tu libro
                </label>
                <input id="photos" class="form-control droppable" multiple="" name="photos[]" type="file">
                <div id="image-list" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <label for="photos2" class="custom-file-upload btn btn-info option-label">
                <span class="add-photo-icon" aria-hidden="true"></span>
                Agrega una foto de tu libro
                </label>
                <input id="photos2" class="form-control droppable" multiple="" name="photos2[]" type="file">
                <div id="image-list2" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 upload-block">
                <label for="photos3" class="custom-file-upload btn btn-info option-label">
                <span class="add-photo-icon" aria-hidden="true"></span>
                Agrega una foto de tu libro
                </label>
                <input id="photos3" class="form-control droppable" multiple="" name="photos3[]" type="file">
                <div id="image-list3" class="col-md-12 col-sm-12 col-xs-12"></div>
            </div>
        </div>

        <div>
            <h3>Plaza de venta</h3>

            <div class="section-plaza option-label" id="plaza_type" role="group" data-toggle="buttons">
                @foreach($plaza_types as $key => $plaza)
                <label for="plazatype{{$key}}" class="btn btn-default btn-plaza @if($key == 2) active @endif" data-controls="{{$plaza->pvalue}}">
                    <input type="radio" id="plazatype{{$key}}" name="plaza_type" value="{{$plaza->id}}" @if($key == 2) checked @endif>
                    {{ explode(";", $plaza->description)[0] }}
                </label>
                @endforeach
            </div>
            <div class="plaza-description help-block">
            @foreach($plaza_types as $key => $plaza)
                <div id="{{$plaza->pvalue}}" class="@if($key == 2) active @endif">
                    {{ explode(";", $plaza->description)[1] }}
                </div>
            @endforeach
            </div>
        </div>
        <br />

        <h3>Datos de recolección</h3>
        <div class="form-section contact-data">
            <div class="form-group @if($errors->has('pickup_fullname'))has-error @endif">
                {{ Form::label('pickup_fullname', 'Nombre*', ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                @if(Auth::check())
                    {{ Form::text('pickup_fullname', $user->person->firstname.' '.$user->person->lastname, ['required', 'class' => 'form-control']) }}
                @else
                    {{ Form::text('pickup_fullname', 'Tu nombre', ['required', 'class' => 'form-control']) }}
                @endif
                    @if($errors->has('pickup_fullname'))
                        {{ $errors->first('pickup_fullname', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>
            <div class="form-group @if($errors->has('pickup_address'))has-error @endif">
                {{ Form::label('pickup_address', 'Direcci&oacute;n*', ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                @if(Auth::check())
                    {{ Form::text('pickup_address', $user->person->address, ['required', 'class' => 'form-control']) }}
                @else
                    {{ Form::text('pickup_address', 'Tu dirección', ['required', 'class' => 'form-control']) }}
                @endif
                    @if($errors->has('pickup_address'))
                        {{ $errors->first('pickup_address', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>

            <div class="form-group @if($errors->has('pickup_phone'))has-error @endif">
                {{ Form::label('pickup_phone', 'Telefono*', ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                @if(Auth::check())
                    {{ Form::text('pickup_phone', $user->person->phone, ['required', 'class' => 'form-control']) }}
                @else
                    {{ Form::text('pickup_phone', 'Tu número telefónico', ['required', 'class' => 'form-control']) }}
                @endif
                    @if($errors->has('pickup_phone'))
                        {{ $errors->first('pickup_phone', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>
            
            <div class="help-block">(S&oacute;lo ciudades principales de Colombia)</div>
            <div class="form-group @if($errors->has('pickup_city'))has-error @endif">
                {{ Form::label('pickup_city', 'Ciudad*', ['class' => "col-xs-3 control-label"]) }}
                <div class="col-xs-9">
                @if(Auth::check())
                    {{ Form::text('pickup_city', $user->person->city, ['required', 'class' => 'form-control']) }}
                @else
                    {{ Form::text('pickup_city', 'La ciudad donde vives', ['required', 'class' => 'form-control']) }}
                @endif
                    @if($errors->has('pickup_city'))
                        {{ $errors->first('pickup_city', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>
        </div>
        <br />

        <h3>Informaci&oacute;n bancaria</h3>
        <div class="form-section bank-data">
            <div class="help-block">(S&oacute;lo cuentas Bancolombia)</div>
            <div class="help-block">(Sí no cuentas con cuenta Bancolombia, realizamos transferencia a tu celular a través de <a style="color:blueviolet" target="_blank" href="https://www.nequi.com.co/">Nequi</a> y retiras en cajeros Bancolombia sin costo.)</div>
            <div class="col-xs-12">
                        <div class="radio radio-inline">
                            <input type="radio" id="type-pay" name="type-pay" value="Pago Bancolombia" @if(empty($phonepay)) checked @else @endif>
                            <label for="type-pay" class="option-label">Pago Bancolombia</label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" id="type-pay2" name="type-pay" value="Pago Celular" @if(empty($account_number)) checked @else @endif>
                            <label for="type-pay2" class="option-label">Pago en Smartphone</label>
                        </div>
                        <input  id="typeofpay" type="text" class="hidden" name="typeofpay">
            </div>
            <br/><br>

            <div class="form-group @if($errors->has('pickup_fullname'))has-error @endif">
                @if(empty($account_number))
                {{Form::label('person[account_number]', 'N&uacute;mero de celular', ['class' => "col-xs-3 control-label account_number"])}}
                @else
                {{Form::label('person[account_number]', 'N&uacute;mero de cuenta', ['class' => "col-xs-3 control-label account_number"])}}
                @endif
                <div class="col-xs-9">
                @if(Auth::check())
                    @if(empty($account_number))
                    {{Form::text('person[account_number]', $phonepay, ['required', 'placeholder'=>'N&uacute;mero de celular', 'required', 'class' => 'form-control'])}}
                    @else
                    {{Form::text('person[account_number]', $account_number, ['required', 'placeholder'=>'N&uacute;mero de cuenta', 'required', 'class' => 'form-control'])}}
                    @endif
                @else
                    {{Form::text('person[account_number]', 'Donde te pagaremos', ['required', 'placeholder'=>'N&uacute;mero de cuenta', 'required', 'class' => 'form-control'])}}
                @endif
                    @if($errors->has('person[account_number]'))
                        {{ $errors->first('person[account_number]', '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </div>
            @if(Auth::check())
            <div class="help-block">
                Recibe el pago en tu celular a tu cuenta de Nequi, que puedes descargar gratis desde PlayStore (Android) o AppStore (iOS).
                As&iacute; de f&aacute;cil es en Kinbu con los libros.
            </div>
            @endif
        </div>

        <br /><br />
        <div class="buttons-publish">
        @if(Auth::check())
        {{ Form::submit('Publicar', ['class' => 'btn btn-success btn-lg btn-block btn-publish']) }}
        {{ HTML::link(URL::previous(), 'Cancelar', ['class' => 'btn btn-danger btn-lg btn-block btn-cancel']) }}
        @else
        {{ Form::submit('Publicar', ['class' => 'btn btn-success btn-lg btn-block btn-publish']) }}
        @endif
        </div>
        {{ Form::close() }}

        <br />

    </div>
</section>
@stop
@section('addedjs')
@parent
<script type="text/javascript">
(function ($) {
    @if(Auth::check())
    @else
    setTimeout( function(){$("#register").modal('show');}, 7500);
    @endif
    /*$(".btn-publish").one("click", function() {
    $(this).click(function () { return false; });
    });*/
    $('#photos').bind('change', function() { var fileName = ''; fileName = '1 archivo seleccionado'; $('#file-selected').html(fileName); })

    var str = $('.account_number').text().replace(/Número de cuenta/g, 'Número de celular');
    $('#type-pay2').click(function() {
    if($('#type-pay2').is(':checked')) { 
        $('.account_number').text(str);
        $( "#typeofpay" ).val( function( index, val ) {
            return val + "+";
            })  
        }
    });
    var str1 = $('.account_number').text().replace(/Número de celular/g, 'Número de cuenta');
    $('#type-pay').click(function() {
    if($('#type-pay').is(':checked')) { 
        $('.account_number').text(str1);
        $( "#typeofpay" ).val(''); 
        }
    });

    var publicationTypeCheckboxes = $("[name='publication_type[]']"),
        price = $('#price').parent().parent(),
        plazaType = $('#plaza_type').parent(),
        amount = $("#amount").parent().parent(),
        bookStatusRadios = $("[name='book_status']"),
        characteristicTypes = $("#characteristic_types");

    var publicationTypeCheckboxesState = function () {
        var which = [];
        publicationTypeCheckboxes.each(function (index, item) {
            if (item.checked) {
                which.push(item.id);
            }
        });
        if (which.length > 0) {
            if (which.length == 2) {
                return 'both'
            } else {
                return which[0];
            }
        }
    }

    var checkPublicationTypeCheckboxes = function (e) {
        var state = publicationTypeCheckboxesState();
        console.log(state);
        switch (state) {
            case 'both': //Ambos
            case 'publicationtype0': //Venta
                console.log("PPPP");
                price.show();
                plazaType.show();
                var selected = bookStatusRadios.filter(":checked")[0].id
                if (selected == 'bookstatus0') { //Nuevo
                    amount.show();
                } else {
                    amount.hide();
                }
                break;
            case 'publicationtype1': //Intercambio
                console.log("UUUU");
                price.hide();
                plazaType.hide();
                amount.hide()
                break;
            default: //Ninguno seleccionado
                console.log(e);
                publicationTypeCheckboxes[0].checked = true;
                break;

        }
    }

    publicationTypeCheckboxes.on('change', checkPublicationTypeCheckboxes);

    var checkBookStatusRadios = function (e) {
        var selected = bookStatusRadios.filter(":checked")[0].id
        if (selected == 'bookstatus0') { //Nuevo
            characteristicTypes.hide();
            var ptcState = publicationTypeCheckboxesState();
            if (ptcState == 'publicationtype1') { //Intercambio
                amount.hide();
            } else {
                amount.show();
            }
        } else {
            characteristicTypes.show();
            amount.hide();
        }
    }

    bookStatusRadios.on('change', checkBookStatusRadios);

    checkBookStatusRadios();
    checkPublicationTypeCheckboxes();

    $("#escoger_libro").on('click', function(e) {
        var url = "{{ URL::route('books.index') }}";
        var get_libros = $.get(url);
        get_libros.done(function(data) {
            var btn_ok = $("<button/>", {
                type: 'button',
                class: 'btn btn-primary',
                'data-dismiss': 'modal',
                text: 'Aceptar'
            });
            var btn_cancelar = $("<button/>", {
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cancelar'
            });

            var search = function () {
                var val = $.trim(searchInput[0].value);
                if (val.length > 3) {
                    searchDiv.removeClass("has-error");
                    $.get("{{ route("books.index") }}?q=" + val).done(function(data) {
                        modal_body.html("");
                        modal_body.append(data);
                    });
                } else {
                    searchDiv.addClass("has-error");
                }
            };

            var header = $("<div />", {style:'display:inline-table'}).html("Escoger Libro&nbsp;");
            var searchDiv = $("<div />", { class:'input-group', style:'display:inherit;margin-left:100px' });
            var searchInput = $("<input />", {
                type:"search", name:"q",
                placeholder:"Busca", size:30,
                class:"form-control"
            }).appendTo(searchDiv.appendTo(header))
                .on('change', search);

            $("<span />", {
                class: 'glyphicon glyphicon-search'
            }).appendTo($("<button />", {
                class: 'btn btn-default', type:'button'
            }).appendTo($("<span />", {
                class: 'input-group-btn'
            }).appendTo(searchDiv)).on('click', search));

            var modal = KINBU.Msg.create(header, data, [btn_cancelar, btn_ok]);
            var modal_body = modal.modal.find("div.modal-body");
            modal_body.on('click', '.pagination a', function(e) {
                e.preventDefault();
                var pageUrl = $(this).attr('href');
                console.log(pageUrl);
                var get_libros = $.get(pageUrl);
                get_libros.done(function(data) {
                    modal_body.html("");
                    modal_body.append(data);
                });
            });
            modal_body.on('click', '.row a', function(e) {
                e.preventDefault();
                $("#book_id")[0].value = $(this).attr('href').split('books/')[1];
                $("#book_name").text($(this).find("h4").text());
            });

            modal.modal.on('hidden.bs.modal', function (e) {
                modal_body.off('click', '.row a');
                modal_body.off('click', '.pagination a');
            });
        });

    });
    $("#photos").change(function(event) {
        var files = event.target.files;
        $("#image-list").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });
    $("#photos2").change(function(event) {
        var files = event.target.files;
        $("#image-list2").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list2").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });
    $("#photos3").change(function(event) {
        var files = event.target.files;
        $("#image-list3").empty();
        for (var i = 0, file; file = files[i]; ++i) {
            if (!file.type.match('image.*')) return;
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbnail = $("<img />", {
                        class: 'photo-thumbnail img-responsive',
                        style: 'margin: auto;',
                        src: e.target.result,
                        title: escape(theFile.name)
                    });
                    var span = $("<span />", {
                        html: thumbnail
                    });
                    $("#image-list3").append(span);
                }
            })(file);

            reader.readAsDataURL(file);
        }
    });

    $(".btn-plaza").on('click', function (e) {
        var control = $(this).data('controls');
        $(".plaza-description .active").toggleClass('active');
        $(".plaza-description #" + control).addClass('active');
    });
}(jQuery));
</script>
@endsection
