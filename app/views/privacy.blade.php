@extends('...default')

@stop

@section('location')
    Política de privacidad
@stop

@section('content')

<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Política de privacidad</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <h5>Políticas de privacidad para el tratamiento de datos personales de nuestros usuarios,
                            en la cual puede conocerse el tipo de datos recopilados, el tratamiento, seguridad,
                            calidad y manejo exclusivo de estos datos. Creemos en una relación Kinbu-Usuario, en
                            la cual los datos hacen parte de un servicio ágil, fácil y de calidad.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Uso de plataforma</h2>
                        <h5>Al momento de entrar a los servicios Kinbu, usted acepta la recopilación y uso de
                            sus datos personales, como también autoriza a Kinbu tratarlos y manejarlos para prestarle
                            un mejor servicio en todos los ámbitos que “La plataforma” Kinbu vea necesario. Sí en
                            algún momento no se encuentra de acuerdo con las presentes políticas absténgase del uso
                            de La plataforma. La recopilación de datos siempre se realiza enfocado a brindar un mejor
                            servicio.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Utilización de datos para servicios de Marketplace</h2>
                        <h5>Los datos recopilados de Marketplace se utilizan para la visualización de libros
                            recomendados al lector, también se pueden usar datos previos del usuario para presentar
                            las tendencias actuales sobre la interacción de libros actuales. Todos los datos
                            recopilados se utilizan para tener una mejor experiencia de usuario con la plataforma
                            y llegar a la satisfacción del usuario.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Utilización de datos para compras, ventas e intercambios</h2>
                        <h5>Datos como nombres, dirección de domicilio, teléfono y ciudad de domicilio, son
                            precisos para la compra de un libro, también otros datos como información de pago
                            será explicada en “uso de terceros”, todo tipo de dato que se dirija a la finalidad
                            de compra, será tratado con exclusividad para La Plataforma y sus fines los cuales
                            califique como necesarios, nos limitamos la responsabilidad de éstos, pero siempre
                            estaremos en búsqueda de la mejor calidad y satisfacción.</h5>
                        <h5>Para la venta de un libro en Kinbu, será necesario la recopilación y tratamiento
                            de datos como: información del producto, estado, descripción de éste datos de
                            domicilio del vendedor, información de cuenta Bancaria y demás datos precisos para
                            la venta de un libro en Kinbu</h5>
                        <h5>Los datos necesitados para intercambios serán información de producto a intercambiar,
                            datos de domicilio y demás datos que vea necesarios La Plataforma para llevar a cabo
                            éste servicio.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Uso de cookies</h2>
                        <h5>Kinbu hace uso de Cookies para una mejor experiencia en la plataforma, un flujo más
                            rápido a través de ella y el estudio de interacción del usuario con el Sitio web.
                            Siempre nos enfocaremos en preservar la dignidad personal de cada uno de nuestros
                            usuarios y hacer uso de datos para mantener siempre una gran experiencia de usuario.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>Uso de terceros (TCC, PayU y proveedores)</h2>
                        <h5>Los datos que recopilamos para los envíos, tales como direcciones, teléfonos, nombres;
                            Son utilizados exclusivamente para el transporte de los libros. Datos de pago y todo
                            tipo de dato bancario se encuentran seguros y encriptados con certificados SSL, en ningún
                            momento Kinbu visualiza ningún tipo de dato.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h2>PROTECCIÓN E INSTRASFERENCIA DE DATOS</h2>
                        <h5>Nos encontramos totalmente comprometidos con la seguridad de nuestros usuarios,
                            por esto hacemos uso de terceros, pagos seguros y medios para una gran experiencia
                            gratificante en nuestra plataforma. Cada dato suministrado en la plataforma será uso
                            exclusivo para una excelente experiencia de usuario y se usarán para brindar satisfacción
                            en lo mejor del mundo que es disfrutar de los libros.</h5>
                    </div>
                    <div class="col-sm-12">
                        <h4>(Cumplimos con ley 1582 de 2012)</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
