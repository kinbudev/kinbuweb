@extends('...default')
@section('location')
Recuperación de contraseña
@stop
@section('addedcss')
{{ HTML::style("css/k.css") }}
@parent
<style>
.btn-kinbu {
    background-color: #7038a3;
    color: #F9F9F9;
}
.btn-lg {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.alert-kinbu {
    border-color: #662e93 !important;
    border-top-width: 5px;
    border-right-width: 5px;
    border-bottom-width: 5px;
    border-left-width: 5px;
    color: #662e93;
    background-color: #ffffff;
}

</style>
@stop
@section('content')
<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h3 class="column-title"> Recuperar contraseña <span class="badge"></span></h3>
            <br>
            <div>
            	Te hemos envíado el link al correo para restablecer tu contraseña.
            </div>
            <br>

        <div class="alert alert-warning alert-kinbu alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <div class="alert-body">Envío confirmado</div>
        </div>
		</div>
	</div>
</section>
@stop