@extends('...default')
@section('location')
Nueva contraseña
@stop
@section('addedcss')
{{ HTML::style("css/k.css") }}
@parent
<style>
.btn-kinbu {
    background-color: #7038a3;
    color: #F9F9F9;
}
.btn-lg {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.alert-kinbu {
    border-color: #662e93 !important;
    border-top-width: 5px;
    border-right-width: 5px;
    border-bottom-width: 5px;
    border-left-width: 5px;
    color: #662e93;
    background-color: #ffffff;
}

</style>
@stop
@section('content')
<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h3 class="column-title"> Nueva contraseña <span class="badge"></span></h3>
            <br>
            <div>
            	Ingresa tu correo y tú nueva contraseña para acceder. Disfruta de tus lecturas sin problemas <span class="glyphicon glyphicon-heart" aria-hidden="true" style="color: red;"></span>
            </div>
            <br>
        {{--Mensaje de error sí correo no está registrado. --}}
        @if(Session::has('message'))
        <div class="alert alert-warning alert-kinbu alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <div class="alert-body">{{ Session::get('message') }}</div>
        </div>
        @endif
        {{--Mensaje de error sí token es invalido. --}}
        @if(Session::has('token'))
        <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <a href="/password/remind" style="text-decoration: none; color: inherit;">
        <div class="alert-body">{{ Session::get('token') }}</div>
        </div>
        </a>
        @endif
			<form action="{{ action('RemindersController@postReset') }}" method="POST">
				<div class="form-group">
    			<input type="hidden" name="token" value="{{ $token }}">
    			<input type="email" class="form-control" style="max-width: 300px; display: initial; text-align: center; margin-bottom: 15px;" name="email" placeholder="Correo"><br/>
    			<input type="password" class="form-control" style="max-width: 300px; display: initial; text-align: center; margin-bottom: 15px;" name="password" placeholder="Nueva contraseña" autocomplete="new-password"><br/>
    			<input type="password" class="form-control" style="max-width: 300px; display: initial; text-align: center; margin-bottom: 15px;" name="password_confirmation" placeholder="Repetir nueva contraseña"><br/>
    			</div>
    			<input type="submit" class="btn btn-lg btn-kinbu" style="min-width: 300px;" value="Cambiar contraseña">
			</form>
		</div>
	</div>
</section>
@stop