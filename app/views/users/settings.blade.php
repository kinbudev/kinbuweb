@extends('...default')

@section('title')
Configuraci&oacute;n
@stop

@section('addedcss')
@parent
<style>
.panel-messages {
    border-color: #c4c4c4;
}
.panel-messages .panel-heading {
    background-color: #662e93;
    color: #fff;
    border-color: #662e93;
}
.panel-primary {
    border-color: #c4c4c4;
}
.panel-primary .panel-heading {
    background-color: #662e93;
    border-color: #662e93;
}
.panel-footer {
    color: #662e93;
}
div > a{
    text-decoration: underline;
}
</style>
@endsection

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
        	<div class="col-xs-3 hidden">
                <div class="list-group" id="sidebar">
                	<a href="{{ route('exchanges') }}" class="list-group-item">Intercambios</a>
     				<a href="#" class="list-group-item">COnf 2</a>
     				<a href="#" class="list-group-item">Conf 2</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 col-md-7">
                                        <span class="glyphicon glyphicon-shopping-cart gi-4x" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-5">
                                        <div>Tu carrito</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('cart')}}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <span class="glyphicon glyphicon glyphicon-usd gi-4x" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div>Transacciones</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('carts')}}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver todos</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 col-md-7">
                                        <span class="glyphicon glyphicon-plane gi-4x" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-5">
                                        <div>Pedidos</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('orders.index')}}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver todos</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-messages">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 col-md-7">
                                        <span class="glyphicon glyphicon-comment gi-4x" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-xs-12 col-md-5">
                                        <div>Tus mensajes</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('messages.index')}}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver todos</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span9"></div>
        </div>
    </div>
</section>
@stop
