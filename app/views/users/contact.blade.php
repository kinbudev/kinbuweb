<fieldset class="form-group data-fieldset">
    <legend class="data-legend">Datos de envío</legend>
    @if(isset($additional_top))
        <div>{{ $additional_top }}</div>
        <br>
    @endif
    <div class="form-group @if($errors->has('pickup_fullname'))has-error @endif">
        {{ Form::label('pickup_fullname', 'Nombres') }}
        {{ Form::text('pickup_fullname', $name, ['required' => '', 'class' => 'form-control user-contact']) }}
        @if($errors->has('pickup_fullname'))
            {{ $errors->first('pickup_fullname', '<span class="help-block">:message</span>') }}
        @endif
    </div>
    <div class="form-group @if($errors->has('pickup_address'))has-error @endif">
        {{ Form::label('pickup_address', 'Direcci&oacute;n') }}
        {{ Form::text('pickup_address', $address, ['required' => '', 'class' => 'form-control user-contact']) }}
        @if($errors->has('pickup_address'))
            {{ $errors->first('pickup_address', '<span class="help-block">:message</span>') }}
        @endif
    </div>

    <div class="form-group @if($errors->has('pickup_phone'))has-error @endif">
        {{ Form::label('pickup_phone', 'Telefono') }}
        {{ Form::text('pickup_phone', $phone, ['required' => '', 'class' => 'form-control user-contact']) }}
        @if($errors->has('pickup_phone'))
            {{ $errors->first('pickup_phone', '<span class="help-block">:message</span>') }}
        @endif
    </div>
    <div class="help-block">(S&oacute;lo ciudades principales de Colombia)</div>
    <div class="form-group @if($errors->has('pickup_city'))has-error @endif">
        {{ Form::label('pickup_city', 'Ciudad') }}
        {{ Form::text('pickup_city', $city, ['required' => '', 'class' => 'form-control user-contact']) }}
        @if($errors->has('pickup_city'))
            {{ $errors->first('pickup_city', '<span class="help-block">:message</span>') }}
        @endif
    </div>
    @if(isset($additional_bottom))
        <div>{{ $additional_bottom }}</div>
    @endif
</fieldset>
