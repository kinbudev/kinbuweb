{{-- USER --}}
{{ Form::model($user, ['route'=>['users.update', $user->username], 'method'=>'PUT', 'id' => 'update_user', 'role'=>'login']) }}
<div class="form-group hidden">
    {{Form::label('username', $user->username)}}
</div>
<div class="form-group hidden">
    {{Form::label('email', $user->email)}}
</div>
<div class="form-group">
    {{Form::label('person[firstname]', 'Nombres')}}
    {{Form::text('person[firstname]', $user->person->firstname, ['placeholder'=>'Nombres', 'required', 'class' => 'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('person[lastname]', 'Apellidos')}}
    {{Form::text('person[lastname]', $user->person->lastname, ['placeholder'=>'Apellidos', 'required', 'class' => 'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('person[about_me]', 'Acerca de m&iacute;')}}
    @if(isset($user->person->about_me))
    {{Form::textarea('person[about_me]', $user->person->about_me, ['placeholder'=>'Acerca de m&iacute;', 'rows' => 1, 'class' => 'form-control'])}}
    @else
    {{Form::textarea('person[about_me]', null, ['placeholder'=>'Acerca de m&iacute;', 'rows' => 1, 'class' => 'form-control'])}}
    @endif
</div>
<div class="form-group">
    {{Form::label('person[address]', 'Direcci&oacute;n')}}
    @if(isset($user->person->address))
    {{Form::text('person[address]', $user->person->address, ['placeholder'=>'Direcci&oacute;n', 'required', 'class' => 'form-control'])}}
    @else
    {{Form::text('person[address]', null, ['placeholder'=>'Direcci&oacute;n', 'required', 'class' => 'form-control'])}}
    @endif
</div>
<div class="form-group">
    {{Form::label('person[phone]', 'Tel&eacute;fono')}}
    @if(isset($user->person->phone))
    {{Form::text('person[phone]', $user->person->phone, ['placeholder'=>'Tel&eacute;fono', 'required', 'class' => 'form-control'])}}
    @else
    {{Form::text('person[phone]', null, ['placeholder'=>'Tel&eacute;fono', 'required', 'class' => 'form-control'])}}
    @endif
</div>
<div class="form-group">
    {{Form::label('person[city]', 'Ciudad')}}
    @if(isset($user->person->city))
    {{Form::text('person[city]', $user->person->city, ['placeholder'=>'Ciudad', 'required', 'class' => 'form-control'])}}
    @else
    {{Form::text('person[city]', null, ['placeholder'=>'Ciudad', 'required', 'class' => 'form-control'])}}
    @endif
</div>
@include('users.bank', [
    'additional_bottom' => "En &eacute;stos momentos s&oacute;lo aceptamos cuentas Bancolombia para hacer las transferencias de tus pagos.
        Cuentas Bancolombia, ahorro o corriente.
        As&iacute; de f&aacute;cil es en Kinbu con los libros."
])
@if(isset($user->authentications[0]->user_id))
@else
<div class="form-group">
    {{Form::label('password', 'Tu Contrase&ntilde;a')}}
    {{Form::password('password', ['placeholder'=>'Contrase&ntilde;a actual', 'required', 'class' => 'form-control', 'autocomplete'=>'new-password'])}}
</div>
@endif
<div class="form-group">
    {{Form::submit('Guardar los cambios', ['class'=>'btn btn-lg btn-block'])}}
</div>
{{ Form::close() }}
