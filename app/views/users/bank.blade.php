<fieldset class="form-group data-fieldset">
    <legend class="data-legend">Informaci&oacute;n bancaria</legend>
    @if(isset($additional_top))
        <div>{{ $additional_top }}</div>
    @endif
    <div class="form-group @if($errors->has('pickup_fullname'))has-error @endif">
        {{Form::label('person[account_number]', 'N&uacute;mero de cuenta')}} (S&oacute;lo cuentas Bancolombia)
        {{Form::text('person[account_number]', isset($person)?$person->account_number:null, ['placeholder'=>'N&uacute;mero de cuenta', 'required', 'class' => 'form-control'])}}
        @if($errors->has('person[account_number]'))
            {{ $errors->first('person[account_number]', '<span class="help-block">:message</span>') }}
        @endif
    </div>
    @if(isset($additional_bottom))
        <div>{{ $additional_bottom }}</div>
    @endif
</fieldset>
