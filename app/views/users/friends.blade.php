<div class='container' style='width: auto;'>
    @forelse($friends->chunk(2) as $row)
    <div class="row">
        @foreach($row as $friend)
        <div class="media col-xs-12 col-sm-12 col-md-6 col-lg-6 friend_badge" id="{{ $friend->id }}">
            <div class="media-left">
                <!--a href="{{ route('users.show', ['users' => $friend->username]) }}"-->
                    @if(empty($friend->imgurl))
                    {{ $friend->gravatar }}
                    @else
                    <img src="{{$friend->imgurl}}" class="userpp" width="80px" height="80px">
                    @endif
                <!--/a-->
            </div>
            <div class="media-body">
                <div>{{ $friend->name }}</div>
                <div>Nivel: {{ $friend->level }}</div>
            </div>
        </div>
        @endforeach
    </div>
    @empty
        Sin Amigos
    @endforelse
</div>
