@extends('default')

@section('location')
    Medallas de {{ Auth::user()->name }}
    en&nbsp;
@stop

@section('addedcss')
    @parent
    {{ HTML::style("css/kuser.css") }}
    <style media="screen">
        .medal:first-child {
            margin-top: 15px;
        }
    </style>
@stop

@section('content')
<section class="main-body">
    <div class="container">

        <section id="mis-medallas" class="feed-section">
            <h3 class="header">
                <span class="title purple-text">MEDALLAS</span>
            </h3>
            <div class="medals-wrapper">
                <div class="row medals">
                    @forelse($medals as $medal)
                        @include('medals.show_full', ['medal' => $medal])
                        @foreach($medal->advance_medals as $key => $medalAdvanced)
                            @include('medals.show_full', ['medal' => $medalAdvanced])
                        @endforeach
                    @empty
                    <p class="empty no-medals">{{ Auth::user()->name }} no tiene medallas.</p>
                    @endforelse
                </div>
            </div>
        </section>

    </div>
</section>
@stop

@section('addedjs')
    @parent
@stop
