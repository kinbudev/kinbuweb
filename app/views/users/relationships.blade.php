<div class="">
    <ul class="nav nav-tabs" role="tablist" id="friends-tabs">
        <li role="presentation" class="active">
            <a href="#mis-amigos" aria-controls="mis-amigos" role="tab" data-toggle="tab">Mis Amigos</a></li>
        <li role="presentation">
            <a href="#requests" aria-controls="requests" role="tab" data-toggle="tab">Requests</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="mis-amigos">
            <div class="panel">
                <div class="panel-body" id="friends">
                    @include('users.friends')
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="requests">
            <div class="panel">
                <div class="panel-body">
                    <div class='container' style='width: auto;'>
                        @forelse($requesters->chunk(2) as $row)
                        <div class="row">
                            @foreach($row as $requester)
                            <div class="media col-xs-12 col-sm-12 col-md-6 col-lg-6 friend_badge friend_badge" id="{{ $requester->id }}">
                                <div class="media-left">
                                    <!--a href="{{ route('users.show', ['users' => $requester->username]) }}"-->
                                        @if(empty($requester->imgurl))
                                        {{ $requester->gravatar }}
                                        @else
                                        <img src="{{$requester->imgurl}}" class="userpp" width="80px" height="80px">
                                        @endif
                                    <!--/a-->
                                </div>
                                <div class="media-body">
                                    <div>{{ $requester->name }}</div>
                                    <div>Nivel: {{ $requester->level }}</div>
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-primary btn_aceptar_request" style="border-radius: 0; margin-bottom: 5px;">Aceptar</button>
                                        <button type="button" class="btn btn-warning btn_ignorar_request" style="border-radius: 0;">Ignorar</button>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @empty
                            Sin Peticiones
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
