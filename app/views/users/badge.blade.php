<div class="media">
    <div class="media-left">
        <div>
        @if(isset($user))
            <a href="/users/{{ $user->username }}">
                @if(empty($user->imgurl))
                {{ $user->getGravatar(100, 'mm', 'g', true, ['class' => 'media-object userpp']) }}
                @else
                <img src="{{$user->imgurl}}" class="media-object userpp" width="100px" height="100px">
                @endif
            </a>
        @else
            Error
        @endif
        </div>
    </div>
    <div class="media-body">
    @if(isset($with_name) && $with_name)
        <h5 class="sm-1-badge-margin">{{ $user->name }}</h5>
        <h6 class="sm-badge-margin" style="font-weight: 200 !important; font-family: sans-serif;">{{ $user->person->firstname }} {{ $user->person->lastname }}</h6>
    @endif
    @if(isset($included_body))
        @include($included_body)
    @else
        {{-- TODO: Ratings <div class="">Rating: {{ $user->rating()}}</div>--}}
        <div class="">
            <span class="purple-text"> Nivel:</span> {{ $user->level }}
        </div>
        <div class="">
            <span class="purple-text">Miembro desde hace:</span>
            <span>{{ $user->member_time }}</span>
        </div>
    @endif

    </div>
</div>
