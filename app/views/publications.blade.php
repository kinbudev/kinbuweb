@extends('...default')

@section('location')
    Libros en | Kinbu.co
@stop

@section('content')

<section class="main-body" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
            <div id="books" class="col-xs-12 col-md-12">
                <h2>Libros</h2>
                @forelse($bookpublications as $key => $publication)
                    @include('publications.small', compact('publication'))
                @empty
                <div class="">
                    <p style="color: blueviolet;">Al parecer no hay libros por aquí, ¿Te gustaría publicar alguno?</p>
                    <a href="{{ route('users.publications.create',['users'=>Auth::user()->username]) }}" class="btn btn-invitation gold-btn" role="button"> <p class="gold-btn-text">Publica </p> </a>
                </div>
                @endforelse

            </div>
        </div>
    </div>
</section>
@stop