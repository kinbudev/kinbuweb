<div class="media medal"
    @if($is_self || $medal->owned_by_user)
    data-toggle="popover" data-trigger="hover"
    data-placement="top" data-html="true"
    data-title="<strong>Medalla:</strong> {{ $medal->name }}"
    data-content="
    <strong>
        @if($is_self)
            Para ganarla
            @if($medal->owned_by_user)
                hiciste:
            @else
                debes:
            @endif
        @else
            Otorgada por:
        @endif
    </strong><p>{{ $medal->description }}</p>"
    @endif
    >
    <div class="media-left @if(!$medal->owned_by_user)not @endif">
        {{ $medal->image }}
    </div>
</div>
