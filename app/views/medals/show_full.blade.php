<div class="media medal col-xs-6">
    <div class="media-left @if(!$medal->owned_by_user)not @endif">
        {{ $medal->image }}
    </div>
    <div class="media-body">
        <h4 class="media-heading"><strong>Medalla:</strong> {{ $medal->name }}</h4>
        <strong>
            Para ganarla
            @if($medal->owned_by_user)
                hiciste:
            @else
                debes:
            @endif
        </strong>
        {{ $medal->description }}
        <br />
        <p>
            <strong>Progreso:</strong>
            {{ implode($medal->progress['text'], $medal->progress['glue']) }}
        </p>
    </div>
</div>
