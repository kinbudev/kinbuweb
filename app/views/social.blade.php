<ul class="list-inline social">
    <li>
        <a href="//facebook.com/KinbuCo">
            <img src="/img/FFFFFF-0.png" alt="facebook" class="sprite-01 facebook">
        </a>
    </li>
    <li>
        <a href="//twitter.com/KinbuCo">
            <img src="/img/FFFFFF-0.png" alt="twitter" class="sprite-01 twitter">
        </a>
    </li>
    <li>
        <a href="//google.com/KinbuCol" rel="publisher">
            <img src="/img/FFFFFF-0.png" alt="g+" class="sprite-01 google">
        </a>
    </li>
</ul>
