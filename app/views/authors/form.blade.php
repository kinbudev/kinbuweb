<fieldset class="form-group data-fieldset">
    <legend class="data-legend">Informaci&oacute;n autor</legend>
    <button type="button" name="button" class="btn btn-default" id="escoger_autor">Escoger Autor</button>
    {{ Form::hidden("author[id]", null) }}
    <div class="form-group @if($errors->has('author[firstname]'))has-error @endif">
        {{Form::label("author[firstname]", 'Nombres')}}
        {{Form::text("author[firstname]", null, ['class' => 'form-control'])}}
        @if($errors->has("author[firstname]"))
            {{ $errors->first("author[firstname]", '<span class="help-block">:message</span>') }}
        @endif
    </div>
    <div class="form-group @if($errors->has('author[lastname]'))has-error @endif">
        {{Form::label("author[lastname]", 'Apellidos')}}
        {{Form::text("author[lastname]", null, ['class' => 'form-control'])}}
        @if($errors->has("author[lastname]"))
            {{ $errors->first("author[lastname]", '<span class="help-block">:message</span>') }}
        @endif
    </div>
    <div class="form-group @if($errors->has('author[about]'))has-error @endif">
        {{Form::label("author[about]", 'Acerca de')}}
        {{Form::text("author[about]", null, ['class' => 'form-control'])}}
        @if($errors->has("author[about]"))
            {{ $errors->first("author[about]", '<span class="help-block">:message</span>') }}
        @endif
    </div>
</fieldset>
