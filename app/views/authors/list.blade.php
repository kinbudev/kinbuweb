@foreach(array_chunk($authors->all(), 3) as $row)
    <div class="row">
    @foreach($row as $author)
        <div class="col-sm-4 author">
            <a href="{{ route('authors.show', ['id'=>$author->id]) }}"
                data-k-author="{{ e(json_encode($author)) }}">
                {{ $author->name }} <small>{{ $author->about }}</small>
            </a>
        </div>
    @endforeach
    </div>
@endforeach
{{ $authors->appends(Request::only('q'))->links() }}
