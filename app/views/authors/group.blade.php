@extends('...default')

@section('location')
All books
@stop

@section('content')
<section class="main-body">
    <div class="container">
    @if($no_authors)
    <p class="bg-danger text-danger">No authors</p>
    @elseif($authors)
        @include('authors.list')
    @endif
    </div>
</div>
@stop
