<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Solicitud de mejora/bug</title>


<style type="text/css">
img {
max-width: 100%;
}
body {
-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
}
body {
background-color: #f6f6f6;
}
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }
  h1 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h2 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h3 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h4 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h1 {
    font-size: 22px !important;
  }
  h2 {
    font-size: 18px !important;
  }
  h3 {
    font-size: 16px !important;
  }
  .container {
    padding: 0 !important; width: 100% !important;
  }
  .content {
    padding: 0 !important;
  }
  .content-wrap {
    padding: 10px !important;
  }
  .invoice {
    width: 100% !important;
  }
  .friends-violet-btn {
    background: linear-gradient(#662D91, #492482) !important;
    box-shadow: 0 1px 1.5px 1px rgba(0, 0, 0,.4) !important;
    color: #FFF !important;
    background-color: #492482 !important;
    border-color: transparent !important;
  }
}
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
		<td class="container" width="600" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
			<div class="content" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="http://schema.org/ConfirmAction" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
				<td class="content-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
							<meta itemprop="name" content="Confirm Email" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" />
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
							<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
							<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
							Solicitud envíada por: {{$name}} ID:({{$user_id}}) a las: {{$date}}
									</td>
							</tr>
							<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
							<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
							Solicitud de mejora/bug: {{$text}}
									</td>
							</tr>
				</tr>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
				<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
					</td>
				</tr>
				</td>
					</tr>
					</table>
					<div class="footer" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">

				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" valign="top" style="padding: 9.0px;">
					  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" style="padding-left: 9.0px;padding-right: 9.0px;">
					  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="left" valign="top" style="padding-top: 9.0px;padding-right: 9.0px;padding-left: 9.0px;">
					  <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" valign="top" style="">
					  <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" valign="top" style="padding-right: 10.0px;padding-bottom: 9.0px;">
					  <a href="https://twitter.com/KinbuCo" target="_blank" style="">
					  <img id="634726000000054001_imgsrc_url_1" alt="Twitter" width="48" style="width: 48.0px;max-width: 48.0px;display: block;border: 0;height: auto;outline: none;text-decoration: none;" src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-96.png">
					  </a>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" valign="top" style="padding-right: 10.0px;padding-bottom: 9.0px;">
					  <a href="https://www.facebook.com/KinbuCo" target="_blank" style=""><img id="634726000000054001_imgsrc_url_2" alt="Facebook" width="48" style="width: 48.0px;max-width: 48.0px;display: block;border: 0;height: auto;outline: none;text-decoration: none;" src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-96.png">
					  </a>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;">
					  <tbody>
					  <tr>
					  <td align="center" valign="top" style="padding-right: 0;padding-bottom: 9.0px;">
					  <a href="https://plus.google.com/+KinbuCol" target="_blank" style="">
					  <img id="634726000000054001_imgsrc_url_3" alt="Google Plus" width="48" style="width: 48.0px;max-width: 48.0px;display: block;border: 0;height: auto;outline: none;text-decoration: none;" src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-googleplus-96.png">
					  </a>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  </td>
					  </tr>
					  </tbody>
					  </table>
					  </td>
					  </tr>
					  </tbody>
				</table>


				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
				<tbody>
				<tr>
				<td valign="top" style="padding-top: 9.0px;">
				<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100.0%;min-width: 100.0%;border-collapse: collapse;" width="100%">
				<tbody>
				<tr>
				<td valign="top" style="padding-top: 0;padding-right: 18.0px;padding-bottom: 9.0px;padding-left: 18.0px;color: rgb(101,101,101);font-family: Helvetica;font-size: 12.0px;line-height: 150.0%;text-align: center;">
				<span style="font-size: 15.0px;">
				<span style="font-family: source sans pro , helvetica neue , helvetica , arial , sans-serif;"><em>Copyright © 2016 Kinbu S.A.S, Todos los derechos reservados.</em><br> <br> <strong>Nuestra dirección de correo es:</strong><br> 
				Barranquilla, Colombia
				<br> <br> 
				<br>
				</span></span><br>
				 &nbsp;</td>
				 </tr>
				 </tbody>
				 </table>
				 </td>
				 </tr>
				 </tbody>
				 </table>

					</div>
					</div>
		</td>
		<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
		</td>
	</tr>
	</table>
	</body>
</html>