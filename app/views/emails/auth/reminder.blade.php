<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		
		<div style="text-align: center; border-bottom-style: inset; border-bottom-width: 2px;">
		{{ HTML::image('/img/Kinbu-Logo.png', 'Kinbu', ['style="margin-top:-5px;"']) }}
		</div>
		<h2 style="text-align: center; font-family: 'Source Sans Pro';">Recuperación de contraseña</h2>

		<div style="text-align: center; font-family: 'Source Sans Pro', sans-serif;" >
			Para obtener una nueva contraseña, haz click en el siguiente botón:
			<br><br><br>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
				<tr>
				<td style="padding-top: 0;padding-right: 18.0px;padding-bottom: 18.0px;padding-left: 18.0px;" valign="top" align="center">
				<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;background-color: rgb(112,56,163);">
					<tr>
					<td align="center" valign="middle" style="font-family: 'Open Sans' , 'Helvetica Neue' , Helvetica , Arial , sans-serif;font-size: 20.0px;padding: 15.0px;">
					<a title="Entrar a Kinbu" href="{{ URL::to('password/reset', array($token)) }}" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100.0%;text-align: center;text-decoration: none;color: rgb(255,255,255);display: block;">Obtener contraseña</a>
					</td>
					</tr>
				</table>
				</td>
				</tr>      
			</table>

			<br>
			Éste link, expirará en {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
		<br>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100.0%;border-collapse: collapse;">
				<tbody>
				<tr>
				<td valign="top" style="padding-top: 9.0px;">
				<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100.0%;min-width: 100.0%;border-collapse: collapse;" width="100%">
				<tbody>
				<tr>
				<td valign="top" style="padding-top: 0;padding-right: 18.0px;padding-bottom: 9.0px;padding-left: 18.0px;color: rgb(101,101,101);font-family: Helvetica;font-size: 12.0px;line-height: 150.0%;text-align: center;">
				<span style="font-size: 15.0px;">
				<span style="font-family: source sans pro , helvetica neue , helvetica , arial , sans-serif;"><em>Copyright © 2016 Kinbu S.A.S, Todos los derechos reservados.</em><br> <br> <strong>Nuestra dirección de correo es:</strong><br> 
				Barranquilla, Colombia
				<br> <br> 
				<br>
				</span></span><br>
				 &nbsp;</td>
				 </tr>
				 </tbody>
				 </table>
				 </td>
				 </tr>
				 </tbody>
		</table>
	</body>
</html>
