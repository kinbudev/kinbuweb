@extends('default')

@section('location')
Administraci&oacute; de Pedido # {{ $order->id }}
@stop

@section('addedcss')
@parent
<style media="screen">
.shipment_id {
    display: none;
}
</style>
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Informacion Pedido</h2>
                <p>Usuario:
                    <span>{{ link_to_route('users.show', $order->user->name, ['users' => $order->user->username] ) }}</span>
                </p>
                <p>Estado:
                    <span>{{ $order->status }}</span>
                </p>
                <p>ID Transaccion PayU:
                    <span>{{ $order->transaction->first()->payu_id }}</span>
                </p>
                <p>Fecha:
                    <span>{{ $order->created_at }}</span>
                </p>
                <div class="row">
                    <div class="col-xs-12" style="overflow:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Publicaci&oacute;n</th>
                                    <th>Estado</th>
                                    <th>Guia de Envio</th>
                                    <th>Fecha Creaci&oacute;n</th>
                                    <th>Fecha Actualizaci&oacute;n</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($order->shipments as $key => $shipment)
                                    <tr>
                                        <td>
                                            {{ link_to_route(
                                                'users.publications.show',
                                                $shipment->publication->published_book_title,
                                                [
                                                    'users' => $shipment->publication->user->username,
                                                    'publications' => $shipment->publication->id
                                                ]) }}
                                        </td>
                                        <td>{{ $shipment->status }}</td>
                                        <td>{{ $shipment->identifier }}</td>
                                        <td>{{ $shipment->created_at }}</td>
                                        <td>{{ $shipment->updated_at }}</td>
                                        <td>
                                            <button type="button" role="button"
                                                class="btn btn-primary btn-shipment"
                                                data-shipment-id="{{ $shipment->id }}"
                                                data-shipment-status="{{ $shipment->shipment_status }}"
                                                data-shipment-identifier="{{ $shipment->identifier }}"
                                                data-shipment-publication="{{ e(json_encode($shipment->publication)) }}">
                                                <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No tienes pedidos</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function () {
    var statuses = [];
    $.ajax({
        url: '{{ route('orders.shipment_statuses') }}'
    }).done(function (response) {
        statuses = response.map(function (item, idx, array) {
            return $("<option/>",{
                value: item.id,
                text: item.description
            });
        });
    }).fail(function (request, state) {
        KINBU.Notification.error(state + " " + request.status + ": "+ request.statusText);
    });
    $("button.btn-shipment").on('click', function (e) {
        var shipmentPublication = $(this).data('shipmentPublication'); console.log(shipmentPublication);
        var p_shipment_user = $("<p />", {
            text: "Vendedor: "
        }).append($("<span />").append($("<a />", {
            href: ("/users/" + shipmentPublication.user.username),
            text: shipmentPublication.user.name
        })));
        var p_shipment_fullname = $("<p />", {
            text: "Nombre completo: "
        }).append($("<span />", {
            text: shipmentPublication.pickup_fullname
        }));
        var p_shipment_address = $("<p />", {
            text: "Direccion: "
        }).append($("<span />", {
            text: shipmentPublication.pickup_address
        }));
        var p_shipment_phone = $("<p />", {
            text: "Telefono: "
        }).append($("<span />", {
            text: shipmentPublication.pickup_phone
        }));
        var p_shipment_city = $("<p />", {
            text: "Ciudad: "
        }).append($("<span />", {
            text: shipmentPublication.pickup_city
        }));
        var p_shipment_account = $("<p />", {
            text: "Numero de cuenta: "
        }).append($("<span />", {
            text: shipmentPublication.user.person.account_number
        }));
        var div_shipment = $("<div />").append([p_shipment_user, p_shipment_fullname, p_shipment_address, p_shipment_phone, p_shipment_city, p_shipment_account]);
        var shipmentId = $(this).data('shipmentId');
        var shipmentStatus = $(this).data('shipmentStatus');
        var shipmentIdentifier = $(this).data('shipmentIdentifier');
        var btn_ok = $("<button/>",{
            type: 'button',
            class: 'btn btn-primary',
            'data-dismiss': 'modal',
            text: 'Aceptar'
        });
        var btn_cerrar = $("<button/>",{
            type: 'button',
            class: 'btn btn-default',
            'data-dismiss': 'modal',
            text: 'Cerrar'
        });
        $(btn_ok).on('click', function (e) {
            $.ajax({
                method: 'GET',
                url: document.URL + '/shipment_update/' + shipmentId,
                data: {
                    status: shipmentStatus,
                    identifier: shipmentIdentifier
                }
            }).done(function (response) {
                KINBU.Notification.show(response.message);
                setTimeout( function(){location.reload(true);}, 2000);

            }).fail(function (request, state) {
                KINBU.Notification.error(state + " " + request.status + ": "+ request.statusText);
            });
        });
        var identifier = $("<input/>",{
            type: 'text',
            class: 'form-control',
            placeholder: 'Identificador de envio',
            value: shipmentIdentifier
        });

        $(identifier).on('input', function (e, a, b, c, d) {
            shipmentIdentifier = $(identifier).val();
        });

        var status = $("<select />",{
            class: 'form-control'
        }).append(statuses);
        status.val(shipmentStatus);

        $(status).on('change', function (e) {
            shipmentStatus = $(status).val();
        });

        KINBU.Msg.create("Administraci&oacute;n de env&iacute;o", [div_shipment, identifier, status], [btn_ok, btn_cerrar]);
    });
}());
</script>
@endsection
