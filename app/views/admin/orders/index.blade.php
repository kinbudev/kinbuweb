@extends('default')

@section('location')
Administracion de Pedidos
@stop

@section('content')
<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="overflow:auto">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#Pedido</th>
                            <th>Usuario</th>
                            <th>Carrito</th>
                            <th>Estado</th>
                            <th>Transacion</th>
                            <th>ID Transaccion</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($orders as $key => $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ link_to_route('users.show', $order->user->name, ['users' => $order->user->username] ) }}</td>
                                <td>{{ $order->cart_id }}</td>
                                <td>{{ $order->status }}</td>
                                <td>{{ $order->transaction_id }}</td>
                                <td>{{ $order->transaction->first()->payu_id }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>
                                    {{ link_to_route('admin.orders.show', 'Administrar', [$order->id], ['class' => 'btn btn-info']) }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No tienes pedidos</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop
