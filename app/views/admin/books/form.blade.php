@extends('...default')

@section('location')

@stop

@section('content')
<section class="main-body">
    <div class="container">
        @if(isset($book))
            {{ Form::model($book, ['method' => 'patch', 'route' => ['admin.books.update', $book->id] ]) }}
        @else
            {{ Form::open(['route' => 'admin.books.store']) }}
        @endif

        <div class="form-group @if($errors->has('ISBN'))has-error @endif">
            {{ Form::label('ISBN', 'ISBN') }}
            {{ Form::text('ISBN', null, ['required', 'class' => 'form-control']) }}
            @if($errors->has('ISBN'))
                {{ $errors->first('ISBN', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('title'))has-error @endif">
            {{ Form::label('title', "Titulo") }}
            {{ Form::text('title', null, ['required', 'class'=>'form-control']) }}
            @if($errors->has('title'))
                {{ $errors->first('title', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('about'))has-error @endif">
            {{ Form::label('about', "Sinopsis") }}
            {{ Form::textarea('about', null, ['rows' => 1, 'class'=>'form-control']) }}
            @if($errors->has('about'))
                {{ $errors->first('about', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('release_date'))has-error @endif">
            {{ Form::label('release_date', "Fecha Lanzamiento") }}
            {{ Form::input('date', 'release_date', null, ['rows' => 1, 'class'=>'form-control']) }}
            @if($errors->has('release_date'))
                {{ $errors->first('release_date', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('editorial'))has-error @endif">
            {{ Form::label('editorial', "Editorial") }}
            {{ Form::text('editorial', null, ['class'=>'form-control']) }}
            @if($errors->has('editorial'))
                {{ $errors->first('editorial', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('pages'))has-error @endif">
            {{ Form::label('pages', "Paginas") }}
            {{ Form::text('pages', null, ['class'=>'form-control']) }}
            @if($errors->has('pages'))
                {{ $errors->first('pages', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('image'))has-error @endif">
            {{ Form::label('image', "Imagen") }}
            {{ Form::text('image', null, ['class'=>'form-control']) }}
            @if($errors->has('image'))
                {{ $errors->first('image', '<span class="help-block">:message</span>') }}
            @endif
        </div>

        <div class="form-group">
            {{ Form::label('language', "Lenguaje") }}
            {{ Form::select('language', $languages->lists('description', 'id'), 51, ['class' => 'form-control']) }}
        </div>

        @if(isset($book))
            @forelse($book->authors as $key => $author)
            <fieldset class="form-group data-fieldset">
                <legend class="data-legend">Informaci&oacute;n autor</legend>
                {{ Form::hidden("authors[$key][id]", $author->id) }}
                <div class="form-group @if($errors->has('authors[.$key.][firstname]'))has-error @endif">
                    {{Form::label("authors[$key][firstname]", 'Nombres')}}
                    {{Form::text("authors[$key][firstname]", $author->firstname, ['class' => 'form-control'])}}
                    @if($errors->has("authors[$key][firstname]"))
                        {{ $errors->first("authors[$key][firstname]", '<span class="help-block">:message</span>') }}
                    @endif
                </div>
                <div class="form-group @if($errors->has('authors[.$key.][lastname]'))has-error @endif">
                    {{Form::label("authors[$key][lastname]", 'Apellidos')}}
                    {{Form::text("authors[$key][lastname]", $author->lastname, ['class' => 'form-control'])}}
                    @if($errors->has("authors[$key][lastname]"))
                        {{ $errors->first("authors[$key][lastname]", '<span class="help-block">:message</span>') }}
                    @endif
                </div>
                <div class="form-group @if($errors->has('authors[.$key.][about]'))has-error @endif">
                    {{Form::label("authors[$key][about]", 'Acerca de')}}
                    {{Form::text("authors[$key][about]", $author->about, ['class' => 'form-control'])}}
                    @if($errors->has("authors[$key][about]"))
                        {{ $errors->first("authors[$key][about]", '<span class="help-block">:message</span>') }}
                    @endif
                </div>
            </fieldset>
            @empty
                @include('authors.form')
            @endforelse
        @else
            @include('authors.form')
        @endif


        <div class="form-group">
            {{ Form::submit('Guardar', ['class' => 'btn btn-default']) }}
        </div>

        {{ Form::close() }}


    </div>
</section>
@stop

@section('addedjs')
@parent
<script type="text/javascript">
(function ($) {
    $("#escoger_autor").on('click', function(e) {
        var url = "{{ route('authors.index') }}";
        var get_autores = $.get(url);
        get_autores.done(function(data) {
            var btn_ok = $("<button/>", {
                type: 'button',
                class: 'btn btn-primary',
                'data-dismiss': 'modal',
                text: 'Aceptar'
            });
            var btn_cancelar = $("<button/>", {
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cancelar'
            });

            var search = function () {
                var val = $.trim(searchInput[0].value);
                if (val.length > 3) {
                    searchDiv.removeClass("has-error");
                    $.get(url+"?q=" + val).done(function(data) {
                        modal_body.html("");
                        modal_body.append(data);
                    });
                } else {
                    searchDiv.addClass("has-error");
                }
            };

            var header = $("<div />", {style:'display:inline-table'}).html("Escoger Libro&nbsp;");
            var searchDiv = $("<div />", { class:'input-group', style:'display:inherit;margin-left:100px' });
            var searchInput = $("<input />", {
                type:"search", name:"q",
                placeholder:"Busca", size:30,
                class:"form-control"
            }).appendTo(searchDiv.appendTo(header))
                .on('change', search);

            $("<span />", {
                class: 'glyphicon glyphicon-search'
            }).appendTo($("<button />", {
                class: 'btn btn-default', type:'button'
            }).appendTo($("<span />", {
                class: 'input-group-btn'
            }).appendTo(searchDiv)).on('click', search));

            var modal = KINBU.Msg.create(header, data, [btn_cancelar, btn_ok]);
            var modal_body = modal.modal.find("div.modal-body");
            modal_body.on('click', '.pagination a', function(e) {
                e.preventDefault();
                var pageUrl = $(this).attr('href');
                console.log(pageUrl);
                var get_autores = $.get(pageUrl);
                get_autores.done(function(data) {
                    modal_body.html("");
                    modal_body.append(data);
                });
            });
            modal_body.on('click', '.row a', function(e) {
                e.preventDefault();
                var author = $(this).data('kAuthor');
                $("[name='author[id]']")[0].value = author.id;
                $("[name='author[firstname]']")[0].value = author.firstname;
                $("[name='author[lastname]']")[0].value = author.lastname;
                $("[name='author[about]']")[0].value = author.about;
            });

            modal.modal.on('hidden.bs.modal', function (e) {
                modal_body.off('click', '.row a');
                modal_body.off('click', '.pagination a');
            });
        });

    });
}(jQuery));
</script>
@endsection
