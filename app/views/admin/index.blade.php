@extends('...default')

@section('location')
    Admin
@stop

@section('content')

<section class="main-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Admin <small>Home</small></h2>
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Menu</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <h4>{{ link_to_route('admin.parameters.index', 'Parameters') }} <small>{{ $param_count }}</small></h4>
                        <h4>{{ link_to_route('admin.levels.index', 'Levels') }} <small>{{ $level_count }}</small></h4>
                        <h4>{{ link_to_route('admin.activities.index', 'Activities') }} <small>{{ $activity_count }}</small></h4>
                        <h4>{{ link_to_route('admin.actions.index', 'Actions') }} <small>{{ $action_count }}</small></h4>
                        <h4>{{ link_to_route('admin.medals.index', 'Medals') }} <small>{{ $medal_count }}</small></h4>
                        <h4>{{ link_to_route('admin.prizes.index', 'Prizes') }} <small>{{ $prize_count }}</small></h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>{{ link_to_route('users.index', 'Users') }} <small>{{ $user_count }}</small></h4>
                        <h4>{{ link_to_route('admin.roles.index', 'Roles') }} <small>{{ $role_count }}</small></h4>
                        <h4>{{ link_to_route('allcategories.index', 'MainCategories') }} <small>{{ $maincategory_count }}</small></h4>
                        <h4>{{ link_to_route('books.index', 'Books') }} <small>{{ $book_count }}</small></h4>
                        <h4>{{ link_to_route('admin.orders.index', 'Orders') }} <small>{{ $order_count }}</small></h4>
                        <h4>{{ link_to_route('admin.books.index','Manage Books')}}</h4>
                        <h4>{{ link_to_route('admin.publications.index', 'Publications') }} <small>{{ $publication_count }}</small></h4>
                    </div>
                    <div class="col-sm-4">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
