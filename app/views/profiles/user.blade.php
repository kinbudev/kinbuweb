@extends('...default')

@section('location')
    {{ $user->name }}
    en&nbsp;
@stop

@section('addedcss')
    @parent
    {{ HTML::style("css/kuser.css") }}
@stop

@section('userLevel')
    @if (Auth::check())
        @if($is_self)
        {{--Nivel desaparece de barra de navegacion en pantallas pequeñas--}}
        <div class="hidden-xs userlevel">
            Nivel&nbsp;&nbsp;&nbsp;{{ Auth::user()->level }}
        </div>
        @endif
    @endif
@stop

@section('content')
<div class="modal fade" id="editor">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edita tu informaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                @include('users.edit')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="userpp">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Actualiza tu foto de perfil</h4>
            </div>
            <div class="modal-body">
                @include('profiles.upload')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<section class="main-body">
    <div class="container">

        <div class="row wrapper">

            <div class="col-sm-5 col-md-4 col-lg-3 profile-wrapper">
                <div class="profile">
                    <h4 class="profile-header">
                        {{ $user->name }}
                        @if($is_self)
                            <button type="button" class="pull-right btn btn-sm editor-blueviolet-btn" data-toggle="modal" data-target="#editor">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </button>
                        @endif
                    </h4>
                    <h4 class="profile-header">
                    <div style="height: 5px;"></div>
                        @if($is_self)
                            <button type="button" class="pull-right btn btn-sm editor-blueviolet-btn" data-toggle="modal" data-target="#userpp">
                                <i class="glyphicon glyphicon-camera"></i>
                            </button>
                        @endif
                    </h4>

                    <div class="media">
                        <div class="media-left">
                            <div class="changeuserpp">
                            @if(isset($user))
                            <a href="#" data-toggle="modal" data-target="#userpp">
                                @if(empty($user->imgurl))
                                {{ $user->getGravatar(90, 'mm', 'g', true, ['class' => 'media-object userpp']) }}
                                <div class="overlay">
                                <div class="change"> </div>
                                </div>
                                @else
                                <img src="{{$user->imgurl}}" class="media-object userpp" width="90px" height="90px">
                                <div class="overlay">
                                <div class="change"> </div>
                                </div>
                                @endif
                                </a>
                                @else
                                Error
                            @endif
                            </div>
                        </div>
                        <div class="media-body">
                            @if(isset($with_name) && $with_name)
                            <h5>{{ $user->name }}</h5>
                            @endif
                                @if(isset($included_body))
                                @include($included_body)
                                @else
                                {{-- TODO: Ratings <div class="">Rating: {{ $user->rating()}}</div>--}}
                                <div class="">
                                Nivel: {{ $user->level }}
                                </div>
                                <div class="">
                                <div class="purple-text">Miembro desde hace:</div>
                                <div>{{ $user->member_time }}</div>
                                </div>
                                @endif

                        </div>
                    </div>

                    <br/>

                    <div class="btn-group btn-group-justified" role="group">
                        @if($is_self)
                        <div id="btn_wrapper1" class="btn-group" role="group">
                            <button id="btn_amigos" type="button" class="btn btn-primary friends-violet-btn" role="button" class="profile-btn-primary-friends">
                                Amigos
                            </button>
                        </div>
                        <div id="btn_wrapper2" class="btn-group" role="group">
                            <a id="btn_mensajes" href="{{ route('messages.index', $user->username) }}"
                                class="btn btn-default messages-white-btn" role="button">
                                Mensajes
                            </a>
                        </div>
                        @elseif(Auth::check())
                        @if($is_logged_user_friend)
                        <div class="btn-group" role="group">
                            <a id="btn_mensaje" href="{{ route('messages.index', $user->username) }}"
                                class="btn btn-default messages-white-btn" role="button">
                                Mensaje
                            </a>
                        </div>
                        @else
                        <div id="btn_wrapper1" class="btn-group" role="group">
                            <button id="btn_agregar_amigo" type="button" class="btn btn-primary add-friends-violet-btn" role="button">
                                Agregar Amigo
                            </button>
                        </div>
                        <div id="btn_wrapper2" class="btn-group" role="group">
                            <a id="btn_mensaje" href="{{ route('messages.index', $user->username) }}"
                                class="btn btn-default messages-white-btn" role="button">
                                Mensaje
                            </a>
                        </div>
                        @endif
                        @else
                        {{ link_to_route('signup',
                            "Reg&iacute;strate para comunicarte",
                            null,
                            ['class'=>'btn btn-primary btn-register',
                            'role'=>'button']
                        ) }}
                        @endif
                    </div>

                    <br/>

                    <div class="">
                        {{-- <div class="text-uppercase">
                            {{ HTML::image('img/FFFFFF-0.png', 'books read', ['class' => 'kinbu-icons books_read']) }}
                            <strong>LIBROS LE&Iacute;DOS:
                            <span class="purple-text pull-right">{{ $books_read->count() }}</span></strong>
                        </div>
                        <div class="text-uppercase">
                            {{ HTML::image('img/FFFFFF-0.png', 'comments', ['class' => 'kinbu-icons comments']) }}
                            <strong>COMENTARIOS:
                             <span class="purple-text pull-right">{{ $comments_count }}</span></strong>
                        </div> --}}
                        <div class="text-uppercase">
                            {{ HTML::image('img/FFFFFF-0.png', 'books bought', ['class' => 'kinbu-icons books_bought']) }}
                            <strong>LIBROS COMPRADOS:
                             <span class="purple-text pull-right">{{ $user->books_bought()->count() }}</span></strong>
                        </div>
                        <div class="text-uppercase">
                            {{ HTML::image('img/FFFFFF-0.png', 'books sold', ['class' => 'kinbu-icons books_sold']) }}
                            <strong>LIBROS VENDIDOS:
                            <span class="purple-text pull-right">{{ "-" }}</samp></strong>
                        </div>
                    </div>

                    <br/>

                    <div class="">
                        <div class="text-uppercase purple-text"><strong>SOBRE M&Iacute;</strong></div>
                        <div class="">
                            <div class="wordwrap">
                                {{ $user->person->about_me }}
                            </div>
                        </div>
                    </div>
                    <div class="hidden-xs">
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>

                    <div class="">
                        {{-- <div class="text-uppercase purple-text"><strong>ACTUALMENTE LEYENDO</strong></div>
                        @forelse($books_reading as $key => $book)
                        <div class="">
                            {{ link_to_route('books.show', $book->title, ['id' => $book->id]) }}
                        </div>
                        @empty
                        <div class="">
                            {{ $user->username }} no est&aacute; leyendo ning&uacute;n libro actualmente.
                            <br/>
                            @if($is_self)
                            <a href="{{ route('home') }}">&iexcl;Busca uno!</a>
                            @else
                            <a href>&iexcl;Recomiendale uno!</a>
                            @endif
                        </div> 
                        @endforelse --}}
                    </div>

                    <div class="">
                        {{-- <div class="text-uppercase purple-text">
                            <strong>COMENTARIOS RECIENTES</strong>
                        </div>
                        <div class="recent-comments">
                            @if($comments->count() > 0)
                            @include('bookcomments.simple', ['comments' => $comments])
                            @else
                            <span class="alert-info">
                                No hay comentarios
                            </span>
                            @endif
                        </div> --}}
                    </div>

                </div>
            </div>{{-- end profile --}}

            <div class="col-sm-7 col-md-8 col-lg-9 feed-wrapper">
                <div class="feed">

                    <section id="mis-libros" class="feed-section">
                        <h3 class="header">
                            <span class="title purple-text">MIS LIBROS</span>
                        </h3>
                        <div class="books-wrapper">
                            @if($user->books_bought()->count() > 0)
                                <div class="books">
                            @else
                                <div class="no-books">
                            @endif
                            @forelse($user->books_bought()->get() as $publication)
                                @include('publications.thumbnail', ['publication' => $publication])
                            @empty
                            @if($is_self)
                                    No tienes
                                @else
                                    {{ $user->username }} no tiene
                                @endif
                                libros comprados.
                            @if($is_self)
                            Busca algunos.
                            @endif
                            @endforelse
                            </div>
                        </div>
                    </section>

                    <section id="se-vende" class="feed-section">
                        <h3 class="header">
                            <a href="{{ route('users.publications.index', ['users' => $user->username]) }}">
                                <span class="title purple-text">SE VENDE</span>
                            </a>
                        </h3>
                        <div class="books-wrapper">
                            @if($user->books_on_sale->count() > 0 || $user->books_on_exchange->count() > 0)
                                <div class="books">
                            @else
                                <div class="no-books">
                            @endif
                            @forelse($user->publications()->get() as $publication)
                            @if($publication->can_be_bought)
                                @include('publications.thumbnail', ['publication' => $publication, 'route' => route('users.publications.show', ['users' => $user->username, 'url'=>$publication->url])])
                            @endif
                            @empty
                            En el momento
                                @if($is_self)
                                    no tienes
                                @else
                                    {{ $user->username }} no tiene
                                @endif
                                libros a la venta.
                                @if($is_self)
                                {{ link_to_route('users.publications.create', '¿Deseas publicar uno?',
                                    ['users' => $user->username], ['style' => 'color:blueviolet']) }}
                                <br>
                                {{ link_to_route('howpublish', "¿Por qué y cómo vender libros en Kinbu?",[], ['style' => 'color:blueviolet']) }}
                                @endif
                            @endforelse
                            </div>
                        </div>
                        @if($is_self)
                            <div class="no-books" style="text-align: center;">
                                <a href="{{ route('users.publications.create',["users" => $user->username]) }}"
                                    class="btn publish-book-icon" role="button">
                                    <span class='publish-book-icon'></span>
                                    <div class="publish-book-text pull-right">Publicar libro</div>
                                </a>
                                <br><br>
                                @if(!empty($user->publications()->get()))
                                <a href="{{route('users.publications.sold',["users" => $user->username])}}">
                                    <div class="btn btn-primary friends-violet-btn">Libros vendidos</div>
                                </a>
                                @endif
                                <br>
                            </div>
                            @endif
                        <br>
                    </section>
                    {{--
                    <section id="mis-medallas" class="feed-section">
                        <h3 class="header">
                            <span class="title purple-text">MEDALLAS</span>
                        </h3>
                        <div class="medals-wrapper">
                            <div class="medals">
                                @forelse($medals as $medal)
                                    @include('medals.show', ['medal' => $medal])
                                    @foreach($medal->advance_medals as $key => $medalAdvanced)
                                        @include('medals.show', ['medal' => $medalAdvanced])
                                    @endforeach
                                @empty
                                <p class="empty no-medals">{{ $user->username }} no tiene medallas.</p>
                                @endforelse
                            </div>
                        </div>
                        @if($is_self)
                        {{ link_to_route('user.medals', 'Mira como vas...',
                            null, ['style' => 'color:blueviolet']) }}
                        @endif
                    </section>
                    --}}
                </div>
            </div>

        </div> {{--end row--}}

    </div>
</section>
@stop

@section('addedjs')
@parent
@if(Auth::check() && ! Auth::user()->hasMedal(1))
{{ HTML::script("/js/tour.js") }}
@endif
<script type="text/javascript">
    (function () {
        @if(Auth::check())
        @if(Auth::user()->username === $user->username)
            @if($open_edit === true)
                $("#editor").modal('show');
                $(":submit").one("click", function() {
                $(this).click(function () { return false; });
                });
            @endif
        @endif
        @endif
        $("#feed-tabs a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $("#btn_agregar_amigo").click(function (event) {
            var btn_aceptar = $("<button/>",{
                type: 'button',
                class: 'btn btn-primary',
                text: 'Aceptar'
            });
            $(btn_aceptar).on('click', function (e) {
                e.preventDefault();
                var me = $(this);
                $.ajax({
                    url: '{{ route('users.requestfriend', $user->username) }}',
                    method: 'POST',
                    data: {_token: '{{ csrf_token() }}'}
                }).done(function (response) {
                    if (response.result) {
                        titulo = "Peticion enviada";
                        mensaje = "Ya enviamos la peticion, te avisaremos cuando la acepte.";
                    } else {
                        titulo = "Error"
                        mensaje = response.mensaje;
                    }
                    me.remove();
                    btn_cancelar.text("Cerrar");
                    msg.modal_title.html("");
                    msg.modal_body.html("");
                    msg.modal_title.append(titulo);
                    msg.modal_body.append(mensaje);
                }).fail(function (request, status) {
                    console.log(status);
                });
                return false;
            });
            var btn_cancelar = $("<button/>",{
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cancelar'
            });
            //TODO
            var msg = KINBU.Msg.create("Agregar a {{ $user->name }}", "¿Deseas a {{ $user->name}} como amigo?", [btn_aceptar, btn_cancelar]);
        });
        $("#btn_amigos").click(function (event) {
            var btn_ok = $("<button/>",{
                type: 'button',
                class: 'btn btn-primary',
                'data-dismiss': 'modal',
                text: 'Aceptar'
            });
            var btn_cerrar = $("<button/>",{
                type: 'button',
                class: 'btn btn-default',
                'data-dismiss': 'modal',
                text: 'Cerrar'
            });
            $.ajax({
                url: '{{ route('users.relationships', $user->username) }}',
                method: 'GET',
                data: {_token: '{{ csrf_token() }}'}
            }).done(function (response) {
                KINBU.Msg.create("Amigos", response, [btn_ok, btn_cerrar]);
                var update_friend_request = function (div, tipo) {
                    var requester_id = div.attr('id');
                    $.ajax({
                        url: '{{ route('users.update_friend_request', $user->username) }}',
                        method: 'POST',
                        data: {_token: '{{ csrf_token() }}', requester_id: requester_id, tipo: tipo }
                    }).done(function (response) {
                        $("#friends").load('{{ route('users.friends', $user->username) }}', {_token: '{{ csrf_token() }}'}, function(){
                            div.hide();
                            $('#friends-tabs a[href="#requests"]').tab('show');
                        });
                        if (response.medals) {
                            var medals = KINBU.Notification.medals(response.medals);
                            $('#mis-medallas .medals .empty').remove();
                            medals.forEach(function (item, index, array) {
                                $('#mis-medallas .medals').append(item.content);
                            });
                        }
                    }).fail(function (request, status) {
                        console.log(status);
                    });
                }
                $(".btn_aceptar_request").click(function (event) {
                    var requester_div = $(this).closest(".friend_badge");
                    update_friend_request(requester_div, 'acknowledged');
                });
                $(".btn_ignorar_request").click(function (event) {
                    var requester_div = $(this).closest(".friend_badge");
                    update_friend_request(requester_div, 'ignored');
                });
            }).fail(function (request, status) {
                console.log(status);
            });
        });
        var totalComments = {{ $comments_count }};
        if (totalComments > 5) {
            var currentPage = 1;
            var start = currentPage * 5;
            $('.recent-comments').scroll(function (e) {
                var recentComments = $(this);
                if (recentComments.scrollTop() + recentComments.innerHeight() >= recentComments[0].scrollHeight) {
                    if (5*currentPage < totalComments) {
                        $.ajax({
                            url: '{{ route('users.comments', $user->username) }}',
                            method: 'POST',
                            data: {_token: '{{ csrf_token() }}', start: start }
                        }).done(function (response) {
                            //console.log(response);
                            var html = recentComments.html();
                            html += response;
                            recentComments.html(html);
                            ++currentPage;
                            start = currentPage * 5;
                        }).fail(function (request, status) {
                            console.log(request, status);
                        });
                    }
                }
            })
        }
        $("#update_user").submit(function (e) {
            e.preventDefault();
            var me = $(this),
                data = me.serialize(),
                action = me.attr('action'),
                method = me.attr('method');
            $.ajax({
                type: method,
                url: action,
                data: data
            }).done(function (response) {
                if (response.success) {
                    KINBU.Notification.show(response.message);
                    $("#editor").modal('hide');
                } else {
                    var content = $('<div />').append(
                        $('<span />', {
                            class: 'glyphicon glyphicon-alert',
                            'aria-hidden': true
                        })
                    ).append(" " + response.message);
                    KINBU.Notification.show(content);
                }
            }).fail(function (request, status) {
                KINBU.Notification.error(status + " " + request.status + ": "+ request.statusText);
            });
            return false;
        });
         $('.medal').popover()
    })();
</script>
@stop
