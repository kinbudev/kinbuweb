<style type="text/css">
	input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>
<form action="{{ action('ImgurController@uploadimg') }}" enctype="multipart/form-data" method="POST">
<h5> Escoge tu nueva imagen:</h5> <br/>
<div class="row" style="padding-right: 15px; padding-left: 15px;">
    <label for="file-upload" class="custom-file-upload btn gold-global-btn col-sm-6">
	   <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
      Seleccionar
    </label>
    <span class="form-control" id="file-selected">  </span>
    <input class="col-sm-6" name="img" size="35" type="file" id="file-upload"/>
</div>
<br>
<input class="btn violet-btn" type="submit" name="submit" value="Actualizar"/>
</form>
@section('addedjs')
@parent
<script type="text/javascript">
	$('#file-upload').bind('change', function() { var fileName = ''; fileName = '1 archivo seleccionado'; $('#file-selected').html(fileName); })
</script>
<script type="text/javascript">
(function () {
$(":submit").one("click", function() {
    $(this).click(function () { return false; });
});
}());
</script>
@stop