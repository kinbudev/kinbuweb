@extends('...default')

@section('location')
{{ $book->title }} en&nbsp;
@stop

@section('addedcss')
    @parent
    {{ HTML::style("css/kbook.css") }}
@stop
@section('content')
<div class="modal fade" id="friend_chooser">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Selecciona un amigo</h4>
            </div>
            <div id="friend_grid" class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_send_recomend">Enviar Recomendaci&oacute;n</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

    <section class="container main-body">
    <div class="media">
        <div class="media-left">
            {{ $book->getCoverImg('M', 'false', true, 'Cover Image', ['class' => 'media-object book-cover-img', 'width' => 150, 'height' => 200]) }}
            @if(Auth::check())
            <div class="btn-group btn-group-xs btn-group-justified" role="group" aria-label="book_status" style="min-width:150px">
                <a id="btn_read" class="btn btn-default kbtn-brs" role="button">Le&iacute;do</a>
                <a id="btn_reading" class="btn btn-default kbtn-brs" role="button">Leyendo</a>
                <a id="btn_will_read" class="btn btn-default kbtn-brs" role="button">Leer&eacute;</a>
            </div>
            @endif
        </div>
        <div class="media-body">

            <div class="container">
                <div class="row">
                    <div class="col-xs-4">
                        <h3 class="media-heading purple-text">
                            {{ $book->title }}
                            <br>
                            <small>
                                {{ implode(', ', array_map(function($author){ return $author->name;}, $book->authors->all())) }}
                            </small>
                        </h3>
                        <h5>ISBN: {{ $book->ISBN }}</h5>
                        <div><span class="yellow-text">N&deg; de p&aacute;ginas:</span> {{ $book->pages}}</div>
                        <!--div>Cover: {{ $book->cover_type }}</div-->
                        <div class="book-detail">
                            <span class="book-detail-text yellow-text">Editorial:</span>
                            <span class="book-detail-value">{{ $book->editorial}}</span>
                        </div>
                        <div class="book-detail">
                            <span class="book-detail-text yellow-text">Idioma:</span>
                            <span class="book-detail-value">{{ $book->language_value }}</span>
                        </div>
                        <div id="book_rating"></div>
                        <!--div>Rating: <span id="book_rating">{{ $book->rating() }}</span></div-->
                        <br>
                        @if(Auth::check() && Auth::user()->isAdmin())
                            {{ link_to_route('admin.books.edit', "edit books", ['books' => $book->id], ['class'=>'btn btn-default btn-get', 'role'=>'button']) }}
                        @endif
                    </div>
                    <div class="col-xs-6">
                        <div>
                            <div><h4 class="purple-text">Sinopsis</h4></div>
                                {{ $book->about}}
                            </div>
                            <div id="user-actions">
                                {{ link_to_route('books.publications.index', "D&oacute;nde Comprar", ['books' => $book->id], ['class'=>'btn btn-default btn-get', 'role'=>'button']) }}
                                @if(Auth::check())
                                    <div class="">
                                        <!--button id="btn_recommend" type="button"
                                            class="btn btn-primary"
                                            data-toggle="modal" data-target="#friend_chooser">
                                            Recommend
                                        </button-->
                                    </div>
                                @endif
                                <br>
                                Calificalo
                                @if(Auth::check())
                                    <div id="user_book_rating"></div>
                                @else
                                    {{ link_to_route('signup', "Reg&iacute;strate para calificar", null, ['class'=>'btn btn-md btn-primary btn-register', 'role'=>'button']) }}
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </section>

    <div class="row-section">
        <div class="container">
            @if($book->authors->count() == 1)
                <h4 class="purple-text">Sobre El Autor</h4>
                <h4>
                    {{ $book->authors->first()->name }} <small>{{ $book->authors->first()->about }}</small>
                </h4>
            @else
                <h4 class="purple-text">Sobre Los Autores</h4>
                <div>
                    @foreach( $book->authors as $author)
                        <h4>
                            {{ $author->name }} <small>{{ $author->about }}</small>
                        </h4>
                    @endforeach
                </div>
            @endif
        </div>
    </div>


    <div class="row-section">
        <div class="container">
            @if(Auth::check())
                @include('users.badge', ['included_body' => 'bookcomments.create', 'user' => Auth::user(), 'book' => $book])
            @endif
            <div>
                <h3 class="purple-text">Comentarios:</h3>
                @include('bookcomments.index')
            </div>
        </div>
    </div>


@stop

@section('addedjs')
    @parent
    <script type="text/javascript">
        (function () {
            $("#book_rating").rateYo({
                readOnly: true,
                starWidth: "16px",
                ratedFill:"#F2BD22",
                rating: {{ $book->rating() }}
            });

            $("#user_book_rating").rateYo({
                fullStar: true,
                ratedFill:"#F2BD22",
                rating: {{ $book_rating }},
                onInit: function (rating, rateYoInstance) {
                    if (rating !== 0) {
                        rateYoInstance.option("readOnly", true);
                    }
                }
            });
            $("#user_book_rating").rateYo().on("rateyo.set", function (e, data) {
                var rating = data.rating;
                $.ajax({
                    url: '{{ route('books.ratings.store', $book->id) }}',
                    method: 'POST',
                    data: {_token: '{{ csrf_token() }}',rating: rating}
                }).done(function (response) {
                    KINBU.Notification.showIn(response.mensaje, '#user-actions');
                    if (response.medals) {
                        KINBU.Notification.medals(response.medals);
                    }
                    if ('notifications' in response) {
                        response.notifications.forEach(function (notification, index, array) {
                            KINBU.Notification.show(notification);
                        })
                    }
                    $("#user_book_rating").rateYo("option", "readOnly", true);
                    // $.ajax({
                    //     url: '{{ route('books.ratings.index', $book->id) }}'
                    // }).done(function (response) {
                    //     $('#book_rating').text(response.rating);
                    // });
                }).fail(function (request, status) {
                    console.log(status);
                });
            });
            @if(Auth::check())
            $("#btn_recommend").click(function (event) {
                $.ajax({
                    url: '{{ route('users.friends', Auth::user()->username) }}',
                    method: 'POST',
                    data: {_token: '{{ csrf_token() }}'}
                }).done(function (response) {
                    $("#friend_chooser #friend_grid").html(response);
                    $("div.friend_badge").click(function (event) {
                        $(this).toggleClass("selected");
                    });
                }).fail(function (request, status) {
                    console.log(status);
                });
            });
            $("#btn_send_recomend").click(function (event) {
                var selected = $("#friend_grid .selected");
                if (selected.length > 0) {
                    var selected_friends = [];
                    selected.each(function (index,el) {
                        selected_friends.push($(el).attr('id'));
                    });
                    $.ajax({
                        url: '{{ route('recommend', $book->id) }}',
                        method: 'POST',
                        data: {_token: '{{ csrf_token() }}', friends_id: selected_friends}
                    }).done(function (response) {
                        if (response.medals) {
                            KINBU.Notification.medals(response.medals);
                        }
                        if ('notifications' in response) {
                            response.notifications.forEach(function (notification, index, array) {
                                KINBU.Notification.show(notification);
                            })
                        }
                    }).fail(function (request, status) {
                        console.log(status);
                    });
                } else {
                    var boton = $("<button/>",{
                        type: 'button',
                        class: 'btn btn-default',
                        'data-dismiss': 'modal',
                        text: 'Cancelar'
                    });
                    KINBU.Msg.create("Error", "Por favor selecciona por lo menos un amigo", boton);
                }
                $("#friend_chooser").modal('hide');
            });

            $('#btn_{{ $book_read_state }}').addClass('active');

            $('a.kbtn-brs').click(function (e) {
                var seletedBtn = this;
                var seletedBtnId = this.id;
                var bookReadState = seletedBtnId.replace(/^btn_/, '').replace('_', ' ');
                var notificationMsg = {
                    'read': "Compartiste que has le&iacute;do este libro",
                    'reading': "Compartiste que est&aacute;s leyendo este libro",
                    'will read': "Compartiste que leer&aacute;s este libro"
                };
                $.ajax({
                    url: '{{ route('book_user_read_status', $book->id) }}',
                    method: 'POST',
                    data: { _token: '{{ csrf_token() }}', read_state: bookReadState}
                }).done(function (data) {
                    if (data.success) {
                        $('a.kbtn-brs[id!="'+seletedBtnId+'"]').removeClass('active');
                        $(seletedBtn).toggleClass('active');
                        KINBU.Notification.show($("<div />", {html: notificationMsg[bookReadState]}));
                    }//TODO else if not success
                    if (data.medals) {
                        KINBU.Notification.medals(data.medals);
                    }
                    if ('notifications' in data) {
                        data.notifications.forEach(function (notification, index, array) {
                            KINBU.Notification.show(notification);
                        })
                    }
                }).fail(function (request, status) {
                    console.log(request, status);
                });
            });

            @endif

            @if(Session::has('knotifications'))

            @foreach(Session::get('knotifications') as $key => $value)
                KINBU.Notification.show($('<span />').html("{{ $value }}").text());
            @endforeach

            @endif


        })();
    </script>
@endsection
