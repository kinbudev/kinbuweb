@extends('default')

@section('location')
    Market
@stop

@section('addedcss')
    @parent
    {{ HTML::style("css/market.css") }}
@stop

@section('content')

    <section class="main-body">
        <div class="container">

            <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators hide">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="https://i.imgur.com/pUKuKEQ.jpg" title="From Rita Morais Fukuoka, Japan" style="height: 280px; width: 100%;">
                    <div class="carousel-caption">
                    <p> Libros nuevos y usados con garantía.</p>
                    <p class="carousel-description lead"> Lee más, gastando menos. </p>
                    {{ Form::open(['route'=>'search', 'method'=>'get', 'class'=>'navbar-form', 'role'=>'search']) }}
                    <div class="form-group">
                        <div class="inner-addon right-addon">
                            {{ HTML::image('img/FFFFFF-0.png', 'search', ['id'=>'search-navbar', 'class' => 'icon kinbu-sprite-01 search hidden-xs', 'style'=>'bottom: 0px;']) }}

                            {{--Botón de búsqueda para pantallas tipo tablet y más pequeñas--}}
                            <button id="search-navbar" type="submit" class="btn btn-default visible-xs icon kinbu-sprite-01 search">
                            <span class="glyphicon glyphicon-search visible-xs" aria-hidden="true"></span>
                            </button>

                            {{ Form::input('search','q', null, ['id' => 'search-input', 'class'=>'form-control search-input-carousel inactive', 'placeholder'=>'Busca libros', 'size'=>'30']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                    </div>
                </div>
                <div class="item">
                    <img src="https://i.imgur.com/RS8RJGM.jpg" title="From Josh Felise. Berkeley Public Library - West Branch, Berkeley, United States (Unplash license)" style="height: 280px; width: 100%;" class="hidden-sm hidden-md hidden-lg">
                    <img src="https://i.imgur.com/g4IyRkT.jpg" title="From Josh Felise. Berkeley Public Library - West Branch, Berkeley, United States (Unplash license)" style="height: 280px; width: 100%;" class="hidden-xs">
                    <div class="carousel-caption">
                    <p> Vende libros que hayas leído. </p>
                    <p class="carousel-description lead"> Permite que hayan más libros y crea amigos. Kinbu se encarga del resto. </p>
                    @if(Auth::check())<a href="{{ route('users.publications.create',['users'=>Auth::user()->username]) }}" class="btn btn-invitation gold-btn" role="button"> <p class="gold-btn-text">Publica </p> </a>
                    @endif
                    </div>
                </div>
            </div>

            <!-- Controls -->{{--
            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="sr-only">Next</span>
            </a>--}}
            
            </div>
            @if(Auth::check() && ! Auth::user()->hasMedal(1))
            {{--Ocultando boton de inicio del tour con clase "hidden-xs" en tablets y pantallas más pequeñas, z-index: para que el botón se encuentre delante de los demás elementos--}}
            <div class="hidden-xs" style="position:absolute;top:50px;z-index: 3;">
                <button type="button" role="button" id="start-tour-btn" class="btn btn-md btn-tour">Descubre Kinbu</button>
            </div>
            @endif
            {{--
            <div class="jumbotron">
                <p class="text-lead text-center">Compra, vende y</p>

                <p class="text-lead text-center">comparte libros con personas</p>

                <p class="text-lead text-center">cercanas a ti.</p>
            </div>
            --}}
    

            <div id="bellow-jumbotron" class="container"></div>

            <div id="bellow-items" class="container"></div>            
            <div class="hidden-xs">@include('market.slider', [ 'title' => "Destacados en venta", 'books' => $eliteBooks])</div>
            <div class="hidden-sm hidden-md hidden-lg">@include('market.slider', [ 'title' => "Destacados en venta", 'books' => $meliteBooks])</div>
            {{-- Slider de libros de intercambio @include('market.slider', [ 'title' => "En intercambio destacados", 'books' => $exchangeBooks]) --}}
            <div class="hidden-xs">@include('market.slider2', [ 'title' => "Personas han comprado", 'books' => $recentBoughtExchanged])</div>
            <div class="hidden-sm hidden-md hidden-lg">@include('market.slider2', [ 'title' => "Personas han comprado", 'books' => $mrecentBoughtExchanged])</div>
        </div>
        <br>
    </section>

@stop
@section('addedjs')
    @parent
    <div class="hidden-xs">
    @if(Auth::check() && ! Auth::user()->hasMedal(1) )
    {{ HTML::script("/js/tour.js") }}
    @endif
    </div>
    <script type="text/javascript">
        (function ($) {
            var psliders = $(".publications-slider");
            psliders.each(function (index) {
                var pslider = $(this);
                pslider.currentView = 0;
                pslider.updateChildrenVisibility = function () {
                    var me = this;
                    me.children('.row').each(function (i) {
                        if (i != me.currentView) {
                            $(this).addClass('hidden-row');
                        } else {
                            $(this).removeClass('hidden-row');
                        }
                    });
                };
                pslider.updateChildrenVisibility();
                pslider.countRowChildren = function () {
                    return this.children('.row').length;
                };
                pslider.moveToPrevRow = function () {
                    var me = this;
                    --me.currentView;
                    if (me.currentView < 0) {
                        me.currentView = me.countRowChildren() - 1;
                    }
                    pslider.updateChildrenVisibility();
                };
                pslider.moveToNextRow = function () {
                    var me = this;
                    ++me.currentView;
                    if (me.currentView >= me.countRowChildren()) {
                        me.currentView = 0;
                    }
                    pslider.updateChildrenVisibility();
                };

                pslider.find("a.slider-arrow.left-arrow").on('click', function (e) {
                    pslider.moveToPrevRow();
                });
                pslider.find("a.slider-arrow.right-arrow").on('click', function (e) {
                    pslider.moveToNextRow();
                });
            });
        }(jQuery));
    </script>
    
    
    <script type="text/javascript">
    //Añadiendo tiempo a la transición
    $("#carousel").carousel({
        interval: 5500,
        pause: "hover"
    });
    //Pausa de carousel sí está en móvil y enfoca la barra de búsqueda
    if ($(window).width() < 768) {
        $('.search-input-carousel').focus(function(){
            $("#carousel").carousel('pause');
        }).blur(function() {
            $("#carousel").carousel('cycle');
        });
    }
    </script>
@endsection
