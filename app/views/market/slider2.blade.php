@section('addedcss')
    @parent
    {{ HTML::style("css/kuser.css") }}
@stop
<h1 class="slider-title">{{ $title }}</h1>
    {{--Sí no hay libros en el momento--}}
    @if($books->isEmpty())
        <div class="publications-slider">
            <div class="row no-market-books" style="margin-left: 30px; margin-right: 30px; text-align: center;">
                <a href="{{route('publications.index')}}" style="text-align: center; font-size: 18px; margin: auto;">
                    <div class="btn friends-violet-btn" style="font-size: 18px; ">Explora Libros</div>
                </a>
            </div>
        <a class="slider-arrow left-arrow">
        {{ HTML::image('/img/kinbu-arrow-left.svg', 'chevron-left', ['class' => 'chevron-left']) }}
        </a>
        <a class="slider-arrow right-arrow">
        {{ HTML::image('/img/kinbu-arrow-right.svg', 'chevron-right', ['class' => 'chevron-right']) }}
        </a>
        </div>
    @else
<div class="publications-slider hidden-xs">
    @foreach($books as $key => $page)
    <div class="row" style="margin-left: 30px; margin-right: 30px;">
        @foreach($page as $keyPage => $subPage)
        <div class="col-md-6">
            <div class="row">
                @foreach($subPage as $subKeyPage => $publication)
                    @include('publications.small', compact('publication'))
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
    <a class="slider-arrow left-arrow">
        {{ HTML::image('/img/kinbu-arrow-left.svg', 'chevron-left', ['class' => 'chevron-left']) }}
    </a>
    <a class="slider-arrow right-arrow">
        {{ HTML::image('/img/kinbu-arrow-right.svg', 'chevron-right', ['class' => 'chevron-right']) }}
    </a>
</div>
<div class="publications-slider hidden-sm hidden-md hidden-lg">
    @foreach($books as $key => $page)
    <div class="row" style="margin-left: 30px; margin-right: 30px;">
        @foreach($page as $keyPage => $subPage)
        <div class="col-md-2">
            <div class="row">
                @foreach($subPage as $subKeyPage => $publication)
                    @include('publications.small', compact('publication'))
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
    <a class="slider-arrow left-arrow">
        {{ HTML::image('/img/kinbu-arrow-left.svg', 'chevron-left', ['class' => 'chevron-left']) }}
    </a>
    <a class="slider-arrow right-arrow">
        {{ HTML::image('/img/kinbu-arrow-right.svg', 'chevron-right', ['class' => 'chevron-right']) }}
    </a>
</div>
@endif