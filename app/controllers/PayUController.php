<?php

class PayUController extends BaseController
{
    protected $user;

    /**
     * @param User $users
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function response()
    {
        $this->user = Auth::user();

        $merchant_id = Input::get('merchantId');
        $merchant_name = Input::get('merchant_name');
        $merchant_address = Input::get('merchant_address');
        $merchant_url = Input::get('merchant_url');
        $telephone = Input::get('telephone');

        $currency = Input::get('currency');
        $risk = Input::get('risk');
        $TX_VALUE = Input::get('TX_VALUE');
        $new_price = number_format($TX_VALUE, 1, '.', '');
        $TX_ADMINISTRATIVE_FEE = Input::get('TX_ADMINISTRATIVE_FEE');
        $TX_TAX_ADMINISTRATIVE_FEE = Input::get('TX_TAX_ADMINISTRATIVE_FEE');
        $TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE = Input::get('TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE');

        $transaction_id = Input::get('transactionId');
        $transaction_state = Input::get('transactionState');
        $trazability_code = Input::get('trazabilityCode');

        $reference_code = Input::get('referenceCode');
        $signature_str = Config::get('payu.ApiKey')."~$merchant_id~$reference_code~$new_price~$currency~$transaction_state";
        $signature_made = md5($signature_str);

        $signature = Input::get('signature');

        $cus = Input::get('cus');
        $pse_bank = Input::get('pseBank');
        $pse_cycle = Input::get('pseCycle');
        $pse_reference1 = Input::get('pseReference1');
        $pse_reference2 = Input::get('pseReference2');
        $pse_reference3 = Input::get('pseReference3');

        $lap_response_code = Input::get('lapResponseCode');
        $lap_payment_method = Input::get('lapPaymentMethod');
        $lap_payment_method_type = Input::get('lapPaymentMethodType');
        $lap_transaction_state = Input::get('lapTransactionState');

        $installments_number = Input::get('installmentsNumber');
        $processing_date = Input::get('processingDate');

        $description = Input::get('description');
        $message = Input::get('message');

        $userTransaction = $this->user->transactions()
            ->whereReferenceCode($reference_code)
            ->wherePayuId($transaction_id)
            ->first();
        $notConfirmed = $userTransaction === null;

        $authorization_code = Input::get('authorizationCode');

        $buyer_email = Input::get('buyerEmail');

        $reference_pol = Input::get('reference_pol');
        $pol_response_code = Input::get('polResponseCode');
        $pol_payment_method = Input::get('polPaymentMethod');
        $pol_payment_method_type = Input::get('polPaymentMethodType');

        $extra1 = Input::get('extra1');
        $extra2 = Input::get('extra2');
        $extra3 = Input::get('extra3');

        $estado_tx = $message;
        switch ($transaction_state) {
            case 4:#aprovada
                break;
            case 5:#expirada
                break;
            case 6:#rechazada
                if ($pol_response_code == 9999) {
                    $estado_tx = $lapResponseCode;
                }
                break;
            case 104:#error
                if ($pol_response_code == 9999) {
                    $estado_tx = $lapResponseCode;
                }
                break;
            case 7:#pendiente
                if (empty($message)) {
                    $estado_tx = $lapResponseCode;
                }
                break;
            default:
                $estado_tx = $message;
                break;
        }

        $cart_id = null;
        if ($userTransaction !== null) {
            $estado_tx = $userTransaction->state_pol;
            $lap_payment_method_type = $userTransaction->payment_method_type;
            $lap_payment_method = $userTransaction->payment_method_name;
            $cus = $userTransaction->cus;
            $pse_bank = $userTransaction->pse_bank;
            $TX_VALUE = $userTransaction->value;
            $cart_id = $userTransaction->cart_id;
        }

        if (strtoupper($signature) == strtoupper($signature_made)) {
            $data = [
                'estado_tx' => $estado_tx,
                'reference_code' => $reference_code,
                'description' => $description,
                'currency' => $currency,
                'TX_VALUE' => $TX_VALUE,
                'not_confirmed' => $notConfirmed,
                'cart_id' => $cart_id,
                'lap_payment_method_type' => $lap_payment_method_type,
                'lap_payment_method' => $lap_payment_method,
                'installments_number' => $installments_number,
                'cus' => $cus,
                'pse_bank' => $pse_bank,
            ];

            return View::make('transactions.show', $data);
        } else {
            //TODO error validando firma digital
            $data = [
                'Error Validando Firma Digital',
            ];

            return Response::json($data);
        }
    }

    public function confirmation()
    {
        $input_all = Input::all();
        Log::info("Confirmation input: " . json_encode($input_all));

        $merchantId = Input::get('merchant_id');
        $merchant_id = Config::get('payu.merchantId');

        $state_pol = Input::get('state_pol');

        $reference_code_pol = Input::get('reference_code_pol');
        $reference_pol = Input::get('reference_pol');
        $reference_sale = Input::get('reference_sale');#referenceCode
        $response_message_pol = Input::get('response_message_pol');

        $payment_method = Input::get('payment_method');
        $payment_method_type = Input::get('payment_method_type');
        $payment_method_id = Input::get('payment_method_id');
        $payment_method_state = Input::get('payment_method_state');
        $payment_method_name = Input::get('payment_method_name');

        $authorization_code = Input::get('authorization_code');

        //$buyer_email = Input::get('buyer_email');
        $email_buyer = Input::get('email_buyer');

        $currency = Input::get('currency');
        $risk = Input::get('risk');
        $value = Input::get('value');
        $new_value = number_format($value, 1, '.', '');
        Log::info("Value/New value: $value/$new_value");
        $tax = Input::get('tax');
        $aditional_value = Input::get('aditional_value');

        $transaction_date = Input::get('transaction_date');
        $transaction_id = Input::get('transaction_id');
        $transaction_bank_id = Input::get('transaction_bank_id');

        $cus = Input::get('cus');
        $pse_bank = Input::get('pse_bank');
        $pse_reference1 = Input::get('pseReference1');
        $pse_reference2 = Input::get('pseReference2');
        $pse_reference3 = Input::get('pseReference3');

        $test = Input::get('test');

        $billing_address = Input::get('billing_address');
        $billing_city = Input::get('billing_city');
        $billing_country = Input::get('billing_country');
        $shipping_address = Input::get('shipping_address');
        $shipping_city = Input::get('shipping_city');
        $shipping_country = Input::get('shipping_country');

        $phone = Input::get('phone');
        $office_phone = Input::get('office_phone');

        $account_number_ach = Input::get('account_number_ach');
        $account_type_ach = Input::get('account_type_ach');

        $administrative_fee = Input::get('administrative_fee');
        $administrative_fee_base = Input::get('administrative_fee_base');
        $administrative_fee_tax = Input::get('administrative_fee_tax');

        $airline_code = Input::get('airline_code');

        $attempts = Input::get('attempts');

        $bank_id = Input::get('bank');

        $commission_pol = Input::get('commission_pol');
        $commission_pol_currency = Input::get('commission_pol_currency');

        $customer_number = Input::get('customer_number');

        $date = Input::get('date');

        $signature_str = Config::get('payu.ApiKey')."~$merchant_id~$reference_sale~$new_value~$currency~$state_pol";
        $signature_made = md5($signature_str);
        Log::info("signature: md5($signature_str) = $signature_made");
        $sign = Input::get('sign');
        Log::info("signature: $sign");

        $error_code_bank = Input::get('error_code_bank');
        $error_message_bank = Input::get('error_message_bank');

        $ip = Input::get('ip');
        $nickname_buyer = Input::get('nickname_buyer');
        $nickname_seller = Input::get('nickname_seller');

        $installments_number = Input::get('installments_number');

        $description = Input::get('description');
        $extra1 = Input::get('extra1');
        $extra2 = Input::get('extra2');

        if (strtoupper($sign) == strtoupper($signature_made)) {
            $transactions = Transaction::whereReferenceCode($reference_sale)->get();
            if ($transactions->isEmpty()) {
                Log::error("Error: no transaction");
                throw new Exception('Error Processing Request', 2);
            } else {
                $transaction = $transactions->filter(function ($t) use ($transaction_id) {
                    return $t->payu_id === $transaction_id;
                })->first();
                if ($transaction === null) {
                    $transaction = $transactions->filter(function ($t) use ($transaction_id) {
                        return $t->payu_id === null;
                    })->first();
                    if ($transaction === null) {
                        Log::error("Error: still no transaction");
                        throw new Exception('Error Processing Request', 2);
                    }
                }
            }

            $transaction->description = $description;
            $transaction->reference_pol = $reference_pol;
            $transaction->payu_id = $transaction_id;
            $transaction->state_pol = $state_pol;
            if ($state_pol == 4) {
                $valid_pv = Parameter::whereAttribute('order_status')->first()->pValue('valid');
                $pickup_pv = Parameter::whereAttribute('shipment_status')->first()->pValue('pickup');
                $order = new Order;
                $order->user_id = $transaction->user_id;
                $order->cart_id = $transaction->cart_id;
                $order->transaction_id = $transaction->id;
                $order->order_status = $valid_pv->id;
                $order->save();
                $order->touch();
                $shipments = [];
                
                foreach ($transaction->cart->items()->get() as $key => $item) {
                    $shipment = new Shipment;
                    $shipment->order_id = $order->id;
                    $shipment->published_book_id = $item->id;
                    $shipment->shipment_status = $pickup_pv->id;
                    $shipments[] = $shipment;
                }
                $order->shipments()->saveMany($shipments);

                Mail::send('emails.neworder', array('key' => 'value'), function($message)
                {
                    $message->subject('¡Nueva venta en Kinbu!');
                    $message->to('ventas@kinbu.co');
                });

                foreach ($transaction->cart->items()->get() as $key => $item) {
                DB::setFetchMode(PDO::FETCH_ASSOC);
                    $seller_email = DB::table('published_books as a')
                    ->join('users as b', 'a.user_id', '=', 'b.id')
                    ->where('a.id', $item->id)
                    ->select('b.email')
                    ->get();

                    $selling = Shipment::with('publication')->where('published_book_id', '=', $item->id)->first();
                    $b = array('shipment' => $selling);

                    Mail::send('emails.publicationsold', $b, function($message) use ($seller_email)
                    {
                        $message->subject('¡Has vendido un libro!');
                        $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                        $message->to($seller_email[0]["email"]);
                    });
                DB::setFetchMode(PDO::FETCH_CLASS);
                }

                Log::info("email_buyer: $email_buyer");

                $orderclient = Order::with('shipments', 'shipments.publication')->where('transaction_id', '=', $transaction->id)->first();
                $client_value = number_format($value, 2, '.', ' ');

                $a = array('order' => $orderclient, 'total' => $client_value);

                Mail::send('emails.purchase', $a, function($message) use ($email_buyer)
                {
                    $message->subject('¡Compra confirmada!');
                    $message->to($email_buyer);
                });
                $pvb = Parameter::whereAttribute('cart_status')->first()->pValue('bought');
                $cart = ShoppingCart::find($transaction->cart_id);                
                $cart->cart_status = $pvb->id;
                $cart->save();
                $cart->touch();

            }
            else {
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $whatrf_code = DB::table('transactions')
                    ->where('transactions.id', $transaction->id)
                    ->select('reference_code')
                    ->first();
                DB::setFetchMode(PDO::FETCH_CLASS);
                $get = explode("-",$whatrf_code["reference_code"]);
                if(!isset($get[1])){
                $attempt = 1;
                $reference_code2 = $whatrf_code["reference_code"].'-'.$attempt;
                $referenceCode = $reference_code2;
                }
                else {
                $get[1]++;
                $reference_code2 = implode("-", $get);
                $referenceCode = $reference_code2;
                }
                $transaction->reference_code = $referenceCode;

                $pv = Parameter::whereAttribute('cart_status')->first()->pValue('active');
                $activecarts = DB::table('shopping_carts')
                    ->where('shopping_carts.user_id', $transaction->user_id)
                    ->where('shopping_carts.cart_status', $pv->id)
                    ->select('shopping_carts.cart_status');

                if ($activecarts->count() > 0) {
                $cart = ShoppingCart::find($transaction->cart_id);
                $pvd = Parameter::whereAttribute('cart_status')->first()->pValue('deleted');               
                $cart->cart_status = $pvd->id;
                $cart->save();
                $cart->touch();

                DB::setFetchMode(PDO::FETCH_ASSOC);

                $activedcart = DB::table('shopping_carts')
                    ->where('user_id', $transaction->user_id)
                    ->where('cart_status', $pv->id)
                    ->select('id')
                    ->first();

                DB::setFetchMode(PDO::FETCH_CLASS);

                $newcart = ShoppingCart::find($activedcart["id"]);

                foreach ($transaction->cart->items()->get() as $key => $item) {
                    $newcart->items()->attach($item->id); 
                }

                }
                else{
                $cart = ShoppingCart::find($transaction->cart_id);                
                $cart->cart_status = $pv->id;
                $cart->save();
                $cart->touch();
                }

            }

            $transaction->payment_method_name = $payment_method_name;
            $transaction->payment_method_type = $payment_method_type;
            $transaction->response_message_pol = $response_message_pol;
            $transaction->cus = $cus;
            $transaction->pse_bank = $pse_bank;
            $transaction->authorization_code = $authorization_code;
            $transaction->transaction_date = $transaction_date;
            $transaction->save();
            $transaction->touch();
        } else {
            Log::error("Error: signatures do not match");
            throw new Exception('Error Processing Request', 99);
        }
    }
}
