<?php

class FutureUsersController extends BaseController {

    protected $fuser;

    public function __construct(FutureUser $fuser) {
        $this->fuser = $fuser;
    }

    /**
     * Display a listing of futureusers
     *
     * @return Response
     */
    public function index() {
        $futureusers = Futureuser::all();

        return View::make('futureusers.index', compact('futureusers'));
    }

    /**
     * Store a newly created futureuser in storage.
     *
     * @return Response
     */
    public function store() {
        if (Input::ajax()) {
            $data = Input::all();

            if (!$this->fuser->fill($data)->isValid()) {
                return Response::json(['success' => false, 'errors' => $this->fuser->errors]);
            }

            $this->fuser->save();

            return Response::json(['success' => true, 'email' => "Correo almacenado."]);
        } else {
            return Redirect::route('home');
        }
    }

    /**
     * Remove the specified futureuser from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        Futureuser::destroy($id);

        return Redirect::route('futureusers.index');
    }

}
