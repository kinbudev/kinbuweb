<?php

use Carbon\Carbon;

class UsersController extends BaseController
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //TODO Improve users index
        $users = $this->user->all();
        if ($users->count() == 0) {
            return 'No users';
        } else {
            if(Auth::check()){
                if(Auth::user()->isAdmin()) {
                return $users->toArray();
                }
                else {
                return Redirect::route('home');
                }
            }
            else {
                return Redirect::route('home');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::check()) {
            return Redirect::route('home')->withMessage('Ya iniciaste sesi&oacute;n');
        } else {
            return View::make('session.signup')->withSocial(Helpers::getSocialAuthUrls());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //TODO check if this works
        $data = Input::all();

        if (!$this->user->fill($data)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->user->errors);
        }

        $this->user->password = Hash::make($this->user->password);
        $this->user->save();

        $person = new Person(Input::all());

        $this->user->person()->save($person);

        Auth::login($this->user);

        $a = array('name' => $person->firstname);

        Mail::send('emails.registro', $a, function($message)
            {
                $message->subject('Bienvenido a la gran experiencia.');
                $message->to(Auth::user()->email);
            });

        return Redirect::intended('market');
    }

        /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store2()
    {
        //TODO check if this works
        $data = Input::all();

        if (!$this->user->fill($data)->isValid()) {
            return Redirect::back()->withInput()->withErrors($this->user->errors);
        }

        $this->user->password = Hash::make($this->user->password);
        $this->user->save();

        $person = new Person(Input::all());

        $this->user->person()->save($person);

        Auth::login($this->user);

        $a = array('name' => $person->firstname);

        Mail::send('emails.registro', $a, function($message)
            {
                $message->subject('Bienvenido a la gran experiencia.');
                $message->to(Auth::user()->email);
            });

        return Redirect::to('publications/create');
    }

    /**
     * Display the specified resource.
     *
     * @param string $users
     *
     * @return Response
     */
    public function show($users, $open_edit = false)
    {
        $allinput = Input::all();
        //echo json_encode($allinput);
        $this->user = User::with(/*'books_read' */ 'books_on_sale', 'books_on_exchange', 'authentications')
            ->whereUsername($users)->get()->first();

        if ($this->user) {
            $is_logged_user_friend = Auth::check() && in_array(Auth::user()->id, $this->user->friends()->lists('id'));
            $commentscount = $this->user->comments()->count();

            //TODO check against ParameterValue value not id, maybe do a join instead
            /* $bs_read_id = Parameter::whereAttribute('book_read_status')->first()->pValueId('read');
            $books_read = $this->user->books_read->filter(function ($book) use ($bs_read_id) {
                return $book->pivot->book_read_status == $bs_read_id;
            });

            $bs_reading_id = Parameter::whereAttribute('book_read_status')->first()->pValueId('reading');
            $books_reading = $this->user->books_read->filter(function ($book) use($bs_reading_id) {
                return $book->pivot->book_read_status == $bs_reading_id;
            }); */ //LOS LIBROS QUE LEE EL USUARIO YA NO UTILIZARAN

            $data = [
                'user' => $this->user,
                'comments' => $this->user->latest_comments()->get(),
                'comments_count' => $commentscount,
                'is_logged_user_friend' => $is_logged_user_friend,
                'is_self' => Auth::check() && Auth::user()->id == $this->user->id,
                #'books_read' => $books_read,
                #'books_reading' => $books_reading,
                'medals' => $this->user->allMedals(),
                'open_edit' => $open_edit
            ];

            return View::make('profiles.user')->with($data);
        } else {
            return Redirect::route('users.index'); // TODO: Beter show a user not found page
        }
    }

    public function relationships($username)
    {
        $user = User::whereUsername($username)->first();
        $friends_ids = $user->friends()->lists('id');
        $friends = User::with('person')->whereIn('id', $friends_ids)->get();

        if (Input::get('only_friends')){
            return Response::json($friends);
        }

        $requesters_ids = DB::table('user_relationships')
        ->where('user_id', $user->id)
        ->where('request_status', Parameter::whereAttribute('request_status')->first()->pValueId('unattended'))
        ->lists('requester_id');
        $requesters = User::with('person')->whereIn('id', $requesters_ids)->get();

        $params = [
            'friends' => $friends,
            'requesters' => $requesters,
        ];

        return View::make('users.relationships')->with($params);
    }

    public function friends($username)
    {
        $friends_ids = User::whereUsername($username)->get()->first()->friends()->lists('id');
        $friends = User::with('person')->whereIn('id', $friends_ids)->get();

        return View::make('users.friends')->with('friends', $friends);
    }

    public function requestFriend($username)
    {
        $requested = User::whereUsername($username)->first();
        $params = [
            'request_status' => Parameter::whereAttribute('request_status')->first()->pValueId('unattended'),
            'requested_at' => new DateTime(),
        ];
        $mensaje = '';
        try {
            $result = true;
            Auth::user()->requestedFriends()->attach($requested->id, $params);
        } catch (Illuminate\Database\QueryException $qe) {
            $result = false;
            $mensaje = 'Ya enviaste una peticion o ya eres su amigo.';
        }
        $response = [
            'sucess' => true,
            'result' => $result,
            'mensaje' => $mensaje,
        ];

        return Response::json($response);
    }

    public function updateFriendRequest($username)
    {
        $response = [];
        if (Auth::check() && Auth::user()->username == $username) {
            //¿?
            $this->user = Auth::user();
            $requester_id = Input::get('requester_id');
            $tipo = Input::get('tipo');
            $requester = User::find($requester_id);
            $attributes = [
                'request_status' => Parameter::whereAttribute('request_status')->first()->pValueId($tipo),
            ];
            if ($tipo == 'acknowledged') {
                $attributes['acknowledged_at'] = new DateTime();
            }
            $result = $requester->requestedFriends()->updateExistingPivot(Auth::user()->id, $attributes);
            $requester->updateMedal(3);
            $response['medals'] = $this->user->updateMedal(3);
            $response['sucess'] = true;
            $response['result'] = $result;
        } else {
            $response['sucess'] = false;
        }

        return Response::json($response);
    }

    public function comments($username)
    {
        $offset = Input::get('start');
        $comments = User::whereUsername($username)->first()->comments()->orderBy('created_at', 'desc')->skip($offset)->take(5)->get();

        return View::make('bookcomments.simple')->withComments($comments);
    }

    public function completedTour($username)
    {
        $response = ['success' => false];
        if (Auth::check() && Auth::user()->username == $username) {
            $this->user = Auth::user();
            if (!$this->user->hasMedal(1)) {
                $this->user->actions()->attach(1);//TODO get the action id
                $currentLevel = $this->user->level;
                $currentLevelAcumulate = $this->user->person->level_acumulate;
                if ($this->user->updateLevel()) {
                    $newLevel = $this->user->level;
                    $newLevelAcumulate = $this->user->person->level_acumulate;
                    $diff = $newLevelAcumulate - $currentLevelAcumulate;
                    $response['notifications'][] = "+$diff puntos";
                    if ($currentLevel < $newLevel) {
                        $response['notifications'][] = "Has alcanzado el nivel $newLevel";
                    }
                    $response['medals'] = $this->user->updateMedal(1);
                } else {
                    $errors['user'] = 'cannot update user level';
                }
                $response['success'] = true;
            }
        }

        return Response::json($response);
    }

    public function notifications()
    {
        $user = Auth::user();
        $now = new DateTime();
        $then = Session::get('user_last_login');
        //if (Session::has('notifications.last_retrieve')) {
        //    $then = Session::get('notifications.last_retrieve');
        //}

        $notifications = ['empty' => ''];
        //Nuevas peticiones de amistad (si funciona)
        $requesters = DB::table('user_relationships')
            ->where('user_id', $user->id)
            ->where('request_status', Parameter::whereAttribute('request_status')->first()->pValueId('unattended'))
            ->whereBetween('created_at', [$then, $now])
            ->count();
        //$requesters = User::with('person')->whereIn('id', $requesters_ids)->get();

        if ($requesters > 0) {
            $notifications['requesters'] = $requesters;

            /*Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to(Auth::user()->email);
                });*/

        }

        $acknowledged_id = Parameter::whereAttribute('request_status')->first()->pValueId('acknowledged');

        //Peticiones aceptadas (si funciona)
        $users_id = DB::table('user_relationships')
            ->where('requester_id', $user->id)
            ->where('request_status', $acknowledged_id)
            ->whereBetween('acknowledged_at', [$then, $now])
            ->lists('user_id');
        $users = User::whereIn('id', $users_id)->get();

        if ($users->count() > 0) {
            $notifications['friend_users'] = $users;

            /*Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from('Soporte@Kinbu.co', 'Kinbu');
                    $message->to(Auth::user()->email);
                });*/
        
        }

        $shipments = DB::table('shipments as a')
            ->join('orders as b', 'a.order_id', '=', 'b.id')
            ->join('published_books as c', 'a.published_book_id', '=', 'c.id')
            #->join('books as d', 'c.book_id', '=', 'd.id')
            ->join('parameter_values as e', 'b.order_status', '=', 'e.id')
            ->join('parameter_values as f', 'a.shipment_status', '=', 'f.id')
            ->whereBetween('a.updated_at', [$then, $now])
            ->where('b.user_id', $user->id)
            ->select('c.published_book_title as book', 'b.id as order_id', 'e.description as order_status', 'f.description as shipment_status',  'a.created_at', 'a.updated_at');

        if ($shipments->count() > 0) {
            $notifications['shipments'] = $shipments->get();

            /*Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to(Auth::user()->email);
                });*/
        
        }

        //Filtro de fechas no está funcionando porque se utuliza un between de un segundo más que no alcanza a coger la primera publicación desde que la persona se desconectó
/*
SQL que muestra publicaciones de amigos que aceptaste invitacion:

select concat(f.firstname, ' ', f.lastname) as name, `e`.`username`, `a`.`published_book_title` as `title`, `a`.`id` from `published_books` as `a` inner join `user_relationships` as `c` on `a`.`user_id` = `c`.`requester_id` inner join `users` as `e` on `a`.`user_id` = `e`.`id` inner join `people` as `f` on `e`.`id` = `f`.`user_id` where `c`.`request_status` = 3 and `c`.`request_status` = 3 and (`c`.`requester_id` = 107 or `c`.`user_id` = 107)

SQL que muestra publicaciones de amigos que te aceptaron invitación:

select concat(f.firstname, ' ', f.lastname) as name, `e`.`username`, `a`.`published_book_title` as `title`, `a`.`id` from `published_books` as `a` inner join `user_relationships` as `b` on `a`.`user_id` = `b`.`user_id` inner join `users` as `e` on `a`.`user_id` = `e`.`id` inner join `people` as `f` on `e`.`id` = `f`.`user_id` where `b`.`request_status` = 3 and (`b`.`requester_id` = 107) 
*/
    /*  MASTER NOTIFICATION OF PUBLICATIONS (contain the two next querys, the master needs improvement)
        $published_books = DB::table('published_books as a')
            ->join('user_relationships as b', 'a.user_id', '=', 'b.user_id')
            ->join('user_relationships as c', 'a.user_id', '=', 'c.requester_id')
            #->join('books as d', 'a.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->where('b.request_status', $acknowledged_id)
            ->where('c.request_status', $acknowledged_id)
            ->where(function ($query) use ($user)
            {
                $query->where('b.requester_id', $user->id)
                ->orWhere('c.user_id', $user->id);
            })
            //->whereBetween('a.created_at', [$then, $now])
            ->groupBy('a.id', 'e.id')
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as name"), 'e.username', 'a.published_book_title as title', 'a.id')
            ->toSql();
            dd($published_books);*/

        //Publications from sent friendship user
        $published_books1 = DB::table('published_books as a')
            ->join('user_relationships as b', 'a.user_id', '=', 'b.user_id')
            #->join('books as d', 'a.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->where('b.request_status', $acknowledged_id)
            ->where(function ($query) use ($user)
            {
                $query->where('b.requester_id', $user->id);
            })
            ->whereBetween('a.created_at', [$then, $now])
            ->where('e.username', '<>', Auth::user()->username)
            ->groupBy('a.id', 'e.id')
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as name"), 'e.username', 'a.published_book_title as title', 'a.id');


        if ($published_books1->count() > 0) {
            $notifications['published_books1'] = $published_books1->get();

            /* Mails can't work here, user needs a weekly mail with the lastest friend's publications.
            Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to(Auth::user()->email);
                });
            */
        }            

        //Publications from received friendship users
        $published_books2 = DB::table('published_books as a')
            ->join('user_relationships as c', 'a.user_id', '=', 'c.requester_id')
            #->join('books as d', 'a.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->where('c.request_status', $acknowledged_id)
            ->where('c.request_status', $acknowledged_id)
            ->where(function ($query) use ($user)
            {
                $query->where('c.requester_id', $user->id)
                ->orWhere('c.user_id', $user->id);
            })
            ->whereBetween('a.created_at', [$then, $now])
            ->where('e.username', '<>', Auth::user()->username)
            ->groupBy('a.id', 'e.id')
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as name"), 'e.username', 'a.published_book_title as title', 'a.id');


        if ($published_books2->count() > 0) {
            $notifications['published_books2'] = $published_books2->get();

            /* Mails can't work here, user needs a weekly mail with the lastest friend's publications.
            Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to(Auth::user()->email);
                });
            */
        }





/*
        $books_read = DB::table('books_read as a')
            ->join('user_relationships as b', 'a.user_id', '=', 'b.user_id')
            ->join('user_relationships as c', 'a.user_id', '=', 'c.requester_id')
            #->join('books as d', 'a.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->join('parameter_values as g', 'a.book_read_status', '=', 'g.id')
            ->where('b.request_status', $acknowledged_id)
            ->where('c.request_status', $acknowledged_id)
            ->where(function ($query) use ($user)
            {
                $query->where('b.requester_id', $user->id)
                ->orWhere('c.user_id', $user->id);
            })
            ->where(function ($query) use ($then, $now)
            {
                $query->whereBetween('a.created_at', [$then, $now])
                ->orWhereBetween('a.updated_at', [$then, $now]);
            })
            ->groupBy('a.book_id', 'a.user_id')
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as name"), 'e.username', 'g.pvalue', 'd.title', 'a.book_id');
        //
        if ($books_read->count() > 0) {
            $notifications['books_read'] = $books_read->get();
        }
*/
/*
        $book_comments = DB::table('book_comments as a')
            ->join('user_relationships as b', 'a.user_id', '=', 'b.user_id')
            ->join('user_relationships as c', 'a.user_id', '=', 'c.requester_id')
            ->join('books as d', 'a.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->where('b.request_status', $acknowledged_id)
            ->where('c.request_status', $acknowledged_id)
            ->where(function ($query) use ($user)
            {
                $query->where('b.requester_id', $user->id)
                ->orWhere('c.user_id', $user->id);
            })
            ->whereBetween('a.created_at', [$then, $now])
            ->groupBy('a.id', 'e.id')
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as name"), 'e.username', 'd.title', 'd.id');
        //
        if ($book_comments->count() > 0) {
            $notifications['book_comments'] = $book_comments->get();
        }
*/
        $pv = Parameter::whereAttribute('cart_status')->first()->pValue('bought');
        $selling_books = DB::table('published_book_shopping_cart as a')
            ->join('shopping_carts as b', 'a.shopping_cart_id', '=', 'b.id')
            ->join('published_books as c', 'a.published_book_id', '=', 'c.id')
            #->join('books as d', 'c.book_id', '=', 'd.id')
            ->where('c.user_id', $user->id)
            ->where('b.cart_status', $pv->id)
            ->whereBetween('b.updated_at', [$then, $now])
            ->select('c.published_book_title as title', 'c.id');

// TODO: REVISAR CODIGO DE NOTIFICACIONES DE JAVASCRIPT
        if ($selling_books->count() > 0) {
            $notifications['selling_books'] = $selling_books->get();

            /*Mail::send('emails.registro', array('key' => 'value'), function($message)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to(Auth::user()->email);
                });
            */
        }
        /*
        SQL of the below: SELECT A.user_id as seller_id, C.user_id as buyer_id, B.email as seller_email FROM homestead.shipments inner join published_books as A on shipments.published_book_id = A.id inner join users as B on A.user_id = B.id inner join orders as C on shipments.order_id = C.id;
        */
        //Mail when a book is sold
/*DB::setFetchMode(PDO::FETCH_ASSOC);
    
    $adding = Carbon::now()->addMinutes(10);

        $mailseller = DB::table('shipments as a')
            ->join('published_books as b', 'a.published_book_id', '=', 'b.id')
            ->join('users as c', 'b.user_id', '=', 'c.id')
            ->join('orders as d', 'a.order_id', '=', 'd.id')
            ->where('d.user_id', $user->id)
            ->where('a.shipment_status', '=', '228')
            ->whereBetween('a.created_at', [$now, $adding])
            ->select('b.user_id', 'c.email')
            ->get();

            if(!empty($mailseller)){

                Mail::send('emails.registro', $mailseller, function($message) use ($mailseller)
                {
                    $message->subject('Bienvenido a la gran experiencia.');
                    $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                    $message->to($mailseller[0]["email"]);
                });

            }
DB::setFetchMode(PDO::FETCH_CLASS);*/


        $messages = DB::table('users as a')
            ->join('messages as b', 'a.id', '=', 'b.from_id')
            ->join('people as c', 'a.id', '=', 'c.user_id')
            ->where('b.to_id', $user->id)
            ->whereBetween('b.created_at', [$then, $now])
            ->groupBy('a.id')
            ->select(DB::raw("concat(c.firstname, ' ', c.lastname) as name"), 'a.username');
        //
        if ($messages->count() > 0) {
            $notifications['messages'] = $messages->get();
        }

        $exchanges = DB::table('books_exchanged as a')
            ->join('published_books as b', 'a.published_book_id1', '=', 'b.id')
            ->join('users as c', 'b.user_id', '=', 'c.id')
            #->join('books as d', 'b.book_id', '=', 'd.id')
            ->join('users as e', 'a.user_actor_id', '=', 'e.id')
            ->join('people as f', 'e.id', '=', 'f.user_id')
            ->where('c.id', $user->id)
            ->whereBetween('a.created_at', [$then, $now])
            ->select(DB::raw("concat(f.firstname, ' ', f.lastname) as actor"), 'b.published_book_title as title');
        //
        if ($exchanges->count() > 0) {
            $notifications['exchanges'] = $exchanges->get();
        }

        //Session::has('notifications.last_retrieve', $now);
        Session::put('user_last_login', $now);
        

        return Response::json($notifications);
    }

    public function medals()
    {
        $user = Auth::user();

        $allMedals = $user->allMedals();

        foreach ($allMedals as $medal) {
            $progress = ['text' => [], 'glue' => ''];
            switch ($medal->id) {
                case 1:
                    if ($user->actions()->where('actions.id',1)->count() === 1) {
                        $progress['text'][] = "Ya realizaste el tour.";
                    } else {
                        $progress['text'][] = "No has realizado el tour.";
                    }
                    break;
                case 2:# TODO BOOSTER acelera nivel
                    $book_read_status = Parameter::whereAttribute('book_read_status')->first()->id;
                    $books_read = DB::table(DB::raw("(select * from parameter_values where parameter_id = $book_read_status) as pv"))
                        ->leftJoin(DB::raw("(select * from books_read where user_id = {$user->id}) as br"),'br.book_read_status', '=', 'pv.id')
                        ->groupBy('pv.id')
                        ->select('pv.pvalue as status', DB::raw('count(br.book_id) as book_count'))
                        ->lists('book_count', 'status');

                    $items1 = str_plural("libro", $books_read['read']);
                    $items2 = str_plural("libro", $books_read['reading']);
                    $items3 = str_plural("libro", $books_read['will read']);
                    $progress['text'][] = "Has indicado que has le&iacute;do {$books_read['read']} $items1, que est&aacute;s leyendo {$books_read['reading']} $items2 y que leer&aacute;s {$books_read['will read']} $items3.";

                    break;
                case 3:
                    $friends = $user->friends()->count();
                    $items = str_plural("amigo", $friends);
                    $progress['text'][] = "Has hecho {$friends} $items.";

                    break;
                case 4:
                    # TODO Promotor
                    # Compartir la aplicacion 1, 5, 10 veces
                    break;
                case 5:
                    # TODO Caza Tesoros
                    # Redimir premios
                    break;
                case 6:# TODO BOOSTER libro gratis
                    $books = DB::table('book_user_recomendations')->where('recomender_id',$user->id)->groupBy('book_id');
                    $books = count($books->get());
                    $items = str_plural("libro", $books);
                    $progress['text'][] = "Has recomendado {$books} $items.";

                    break;
                case 7:
                    # TODO Mercante
                    # Intercambia, compra, o vende 1, 10, 50 libros
                    break;
                case 8:# TODO BOOSTER descuento en compra
                    $book_comments = $user->comments()->select('book_id', DB::raw('count(*) c'))->groupBy('book_id');
                    $book_comments = count($book_comments->get());
                    $book_ratings = $user->books_ratings()->count();

                    $items1 = str_plural("libro", $book_comments);
                    $items2 = str_plural("libro", $book_ratings);
                    $progress['text'][] = "Has comentado en {$book_comments} $items1 y has calificado {$book_ratings} $items2.";

                    break;
                case 9:
                    $user_medals = $user->medals();
                    $medallas = $user_medals->count();
                    $items = str_plural("medalla", $medallas);
                    $progress['text'][] = "Eres nivel {$user->level} y has conseguido {$medallas} $items.";

                    break;
                default:
                    throw new Exception("Error Decifrando medalla");
                    break;
            }
            $medal->progress = $progress;
            foreach ($medal->advance_medals as $key => $advance_medal) {
                $advance_medal->progress = $progress;
            }

        }

        return View::make('users.medals')->withMedals($allMedals);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $id
     *
     * @return Response
     */
    public function edit($users)
    {
        return $this->show($users, true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($users)
    {
        $this->user = Auth::user();
        if ($this->user->username != $users) {
            return Response::json(['success' => false, 'message' => '¡Acceso denegado!']);
        }

        $authentication = Authentication::where('user_id', $this->user->id)->first();

        if ($authentication) {
            $data = Input::except('password');
            $password = Input::get('password');
            $this->user->person()->update(Input::get('person'));
            $this->user->save();
            $this->user->touch();
            return Response::json(['success' => true, 'message' => 'Perfil actualizado']);
        }

        else {

        $data = Input::except('password');

        if (!$this->user->fill($data)->isUpdateValid()) {
            return Response::json(['success' => false, 'message' => 'Tienes errores en tu información.']);
        }

        $password = Input::get('password');

        if (Hash::check($password, $this->user->password)) {
            $this->user->person()->update(Input::get('person'));
            $this->user->save();
            $this->user->touch();
            return Response::json(['success' => true, 'message' => 'Perfil actualizado']);
        }

        return Response::json(['success' => false, 'message' => 'Debe ingresar su contraseña']);

        }
        
    }

    public function settings()
    {
        return View::make('users.settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
