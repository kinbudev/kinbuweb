<?php

class BagsController extends BaseController {

	/**
	 * Display a listing of bags
	 *
	 * @return Response
	 */
	public function index($users) {
		$bags = User::find($users)->bags();

		return View::make('bags.index', $data);
	}

	/**
	 * Show the form for creating a new bag
	 *
	 * @return Response
	 */
	public function create($users) {
		return View::make('bags.create');
	}

	/**
	 * Store a newly created bag in storage.
	 *
	 * @return Response
	 */
	public function store($users) {
		$validator = Validator::make($data = Input::all(), Bag::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Bag::create($data);

		return Redirect::route('users.bags.index',$users);
	}

	/**
	 * Display the specified bag.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($users,$id) {
		$bag = User::find($users)->bags()->findOrFail($id);

		return View::make('bags.show', compact('bag'));
	}

	/**
	 * Show the form for editing the specified bag.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($users,$id) {
		$bag = User::find($users)->bags()->find($id);

		return View::make('bags.edit', compact('bag'));
	}

	/**
	 * Update the specified bag in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($users,$id) {
		$bag = User::find($users)->bags()->findOrFail($id);

		$validator = Validator::make($data = Input::all(), Bag::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$bag->update($data);

		return Redirect::route('users.bags.index',$users);
	}

	/**
	 * Remove the specified bag from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($users,$id) {
		Bag::destroy($id);

		return Redirect::route('users.bags.index', $users);
	}

}
