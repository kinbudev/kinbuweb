<?php

use Carbon\Carbon;

class MessagesController extends BaseController
{
    protected $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of messages.
     *
     * @return Response
     */
    public function index()
    {
        $allConversations = DB::table('messages')
            ->select(DB::raw("if(from_id={$this->user->id}, to_id, from_id) as user"))
            ->addSelect(DB::raw('max(messages.created_at) as most_recent'))
            ->whereNull('messages.deleted_at')
            ->where(function ($q) {
                $q->where('from_id', $this->user->id)
                    ->orWhere('to_id', $this->user->id);
            })
            ->join('users', DB::raw("if(from_id={$this->user->id}, to_id, from_id)"), '=', 'users.id')
            ->groupBy('user')
            ->orderBy('messages.created_at', 'desc')
            ->addSelect('users.username')
            ->get();

        foreach ($allConversations as $key => $conversation) {
            $most_recent = Carbon::parse($conversation->most_recent);
            $allConversations[$key]->since = Functions::getDateAsString($most_recent);
        }

        $messages = $allConversations;

        return View::make('messages.index', compact('messages'));
    }

    /**
     * Store a newly created message in storage.
     *
     * @return Response
     */
    public function store()
    {
        $response = ['success' => false];
        $username = Input::get('user');
        $user = User::whereUsername($username)->firstOrFail();
        $data = [
            'from_id' => $this->user->id,
            'to_id' => $user->id,
            'content' => Input::get('message'),
        ];
        $message = new Message($data);
        $response['success'] = $message->save();
        if ($response['success']) {
            $response['message'] = "Mensaje enviado exitosamente";
            $message->get();
            $message->sent_by_user = $message->from_id == $this->user->id;
            $response['element'] = $message;
        } else {
            $response['message'] = "No se envio el mensaje";
        }

        return Response::json($response);
    }

    /**
     * Display the specified message.
     *
     * @param string $user
     *
     * @return Response
     */
    public function show($user)
    {
        $user = User::whereUsername($user)->firstOrFail();

        $conversation = Message::where(function ($q) {
                $q->where('from_id', $this->user->id)
                    ->orWhere('to_id', $this->user->id);
            })
            ->where(function ($q) use ($user) {
                $q->where('from_id', $user->id)
                    ->orWhere('to_id', $user->id);
            })
            ->orderBy('messages.created_at', 'asc')
            ->get();

        foreach ($conversation as $key => $message) {
            $conversation[$key]->sent_by_user = $message->from_id == $this->user->id;
        }

        $messages = $conversation;

        return View::make('messages.show', compact('messages', 'user'));
    }
}
