<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	public function index() {
		$data = [
			'user_count' => User::count(),
			'role_count' => Role::count(),
			'book_count' => Book::count(),
			'maincategory_count' => MainCategory::count(),
			'param_count' => Parameter::count(),
			'level_count' => Level::count(),
			'activity_count' => Activity::count(),
			'action_count' => Action::count(),
			'medal_count' => Medal::count(),
			'prize_count' => Prize::count(),
			'publication_count' => BookPublication::count(),
			'order_count' => Order::count(),
		];
		return View::make('admin.index', $data);
	}


}
