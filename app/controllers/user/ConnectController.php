<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class ConnectController extends BaseController
{
    /**
     * Handler.
     *
     * @param string $provider
     *
     * @return Response
     */
    public function connect($provider)
    {
        $user = static::getUser($provider);

        Auth::login($user);
        
        return Redirect::intended('/');

        //return Redirect::back()->with('user', $user);
    }

    /**
     * @param $provider
     *
     * @return User
     *
     * @throws Exception
     */
    private static function getUser($provider)
    {
        //TODO: this needs to be improved
        $config = Config::get("auth.providers.$provider");
        switch ($provider) {
            case 'facebook':
                session_start();
                $facebook = new Facebook($config);
                $helper = $facebook->getRedirectLoginHelper();
                $permissions = ['email'];
                $fbClient = $facebook->getClient();

                //TODO: WHRRYYYYY???!!! So ironic the helper needs HELP! WHO WOULD HAVE GUESSED???!!!
                //TODO: Actually this seems dangerous, only do it outside of production, but then...
                $helper->getPersistentDataHandler()->set('state', Input::get('state'));

                try {
                    $accessToken = $helper->getAccessToken(route('social.login', ['provider' => 'facebook']));

                    if (isset($accessToken)) {
                        if (!$accessToken->isLongLived()) {
                            $accessToken = $accessToken->extend();
                        }

                        $token = $accessToken->getValue();
                        $facebook->setDefaultAccessToken($token);

                        $response = $facebook->get('/me?fields=id,name,email,first_name,last_name');
                        $fbUser = $response->getGraphUser();
                        $social_id = $fbUser['id'];

                        //Check database for the existence of the user ID
                        try {
                            $auth = Authentication::whereSocialId($social_id)->firstOrFail();

                            return $auth->user;
                        } catch (ModelNotFoundException $e) {
                            //New user continue;
                        }

                        $email = $fbUser['email'];

                        $user = User::whereEmail($email)->first();
                        if ($user == null) {
                            $user = new User();
                            $user->email = $email;
                            $usrname = $fbUser['first_name'].''.$fbUser['last_name'];
                            $user->username = Functions::makeUsernameFromFullname($usrname, true);
                            $user->save();

                            $profile['firstname'] = $fbUser['first_name'];
                            $profile['lastname'] = $fbUser['last_name'];

                            $person = new Person();
                            $person->fill($profile);

                            $user->person()->save($person);

                            $a = array('name' => $user->person->firstname);        

                            Mail::send('emails.registro', $a, function($message) use ($user)

                            {
                                $message->subject('Bienvenido a la gran experiencia.');
                                $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                                $message->to($user->email);
                            });

                        }

                        $auth = new Authentication();
                        $auth->social_id = $social_id;
                        $auth->provider = $provider;
                        $auth->token = $token;
                        $auth->status = 1;
                        $user->authentications()->save($auth);


                        return $user;

                    } elseif ($helper->getError()) {
                        $exception = 'Unable to log in: '.$helper->getError().', ';
                        $exception .= $helper->getErrorReason().', ';
                        $exception .= $helper->getErrorDescription();

                        throw new Exception($exception, $helper->getErrorCode());
                    }
                } catch (FacebookResponseException $e) {
                    echo 'Response: '.$e->getMessage();
                } catch (FacebookSDKException $e) {
                    echo 'SDK: '.$e->getMessage();
                }

                exit;
            case 'google':
                session_start();
                $google = Helpers::getGoogleClient($config);

                $accessToken = Session::get('token');
                if (empty($accessToken)) {
                    $stateI = Input::get('state');
                    $stateS = Session::get('state');
                    if ($stateI != $stateS) {
                        return '401 Invalid';
                    }
                    Session::set('state', '');

                    $code = Input::get('code');
                    if (isset($code) && $code) {
                        $google->authenticate($code);
                        Session::set('access_token', $google->getAccessToken());
                        Redirect::route('social.login', ['provider' => $provider]);
                    }

                    $accessToken = Session::get('access_token');
                    if (isset($accessToken) && $accessToken) {
                        $google->setAccessToken($accessToken);
                        $token_data = $google->verifyIdToken();

                        $social_id = $token_data->getUserId();

                        //Check database for the existence of the user ID
                        try {
                            $auth = Authentication::whereSocialId($social_id)->firstOrFail();

                            return $auth->user;
                        } catch (ModelNotFoundException $e) {
                            //New user continue;
                        }

                        $plus = new Google_Service_Plus($google);

                        $guser = $plus->people->get('me');

                        $email = $guser->getEmails()[0]->value;
                        $user = User::whereEmail($email)->first();
                        if ($user == null) {
                            $user = new User();
                            $user->email = $email;

                            if (empty($guser->nickname)) {
                                $usrname = $guser->getName()['givenName'].''.$guser->getName()['familyName'];
                                $user->username = Functions::makeUsernameFromFullname($usrname, true);
                            } else {
                                $user->username = Functions::makeUsernameFromFullname($guser->nickname, true);
                            }
                            $user->save();

                            $profile['firstname'] = $guser->getName()['givenName'];
                            $profile['lastname'] = $guser->getName()['familyName'];
                            $profile['about_me'] = $guser->aboutMe;

                            $person = new Person();
                            $person->fill($profile);

                            $user->person()->save($person);
                            
                            $a = array('name' => $user->person->firstname);        

                            Mail::send('emails.registro', $a, function($message) use ($user)

                            {
                                $message->subject('Bienvenido a la gran experiencia.');
                                $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                                $message->to($user->email);
                            });

                        }
                        $auth = new Authentication();
                        $auth->social_id = $social_id;
                        $auth->provider = $provider;
                        $auth->token = $accessToken;
                        $auth->status = 1;
                        $user->authentications()->save($auth);
                        

                        return $user;
                    }
                } else {
                    return 'Already logged in';
                }

                exit;
            default:
                throw new Exception("Provider $provider does not exists.");
                break;
        }
    }
}
