<?php

use \Illuminate\Support\MessageBag;

class UserPublicationsController extends \BaseController
{
    use LoggedInUserTrait;

    protected $user;
    protected $book;
    protected $publication;

    public function __construct(User $user, Book $book, BookPublication $publication)
    {
        $this->user = $user;
        $this->book = $book;
        $this->publication = $publication;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($username)
    {
        $type = Input::get('type');
        $this->user = User::whereUsername($username)->first();
        $on_sale = $this->user->publications()->onSale()->get();
        $on_exchange = $this->user->publications()->onExchange()->get();

        $params = [
            'user' => $this->user,
            'on_sale' => $on_sale,
            'on_exchange' => $on_exchange,
        ];

        return View::make('publications.index')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($username)
    {
        if ($this->isLoggedInUser($username)) {
            $this->user = Auth::user();
            $pub_types = Parameter::whereAttribute('publication_type')->first()->values()->get();
            $cover_type = Parameter::whereAttribute('cover_type')->first()->values()->get();
            $book_status = Parameter::whereAttribute('book_status')->first()->values()->get();
            $characteristic_type = Parameter::whereAttribute('characteristic_type')->first()->values()->get();
            $plaza_types = Parameter::whereAttribute('plaza_type')->first()->values()->get();
            //éste bloque de codigo abajo divide a account_number en dos, después de un "+" y antes del "+", sí no hay un "+" significa que es una cuenta de banco normal, de otra forma es un numero para pagos en celular. Éstas variables se mandan a través de $params para mostrar información relevante en el formulario de publicación.
            $get = explode("+",$this->user->person->account_number);
            $account_number = "";
            $phonepay = "";
            if(!isset($get[1])){
                $account_number = $this->user->person->account_number;
            }
            else {
                $phonepay = $get[1];
            }

            $params = [
                'user' => $this->user,
                'account_number' => $account_number,
                'phonepay' => $phonepay,
                'publication_types' => $pub_types,
                'cover_type' => $cover_type,
                'book_status' => $book_status,
                'characteristic_types' => $characteristic_type,
                'plaza_types' => $plaza_types,
            ];

            return View::make('publications.create')->with($params);
        } else {
            return Redirect::route('users.publications.index', [$username]);
        }
    }

    /**
     * Show the form for creating a new resource without $username.
     *
     * @return Response
     */
    public function create2()
    {
        if (Auth::check()) {
            $this->user = Auth::user();
        }
        $pub_types = Parameter::whereAttribute('publication_type')->first()->values()->get();
        $cover_type = Parameter::whereAttribute('cover_type')->first()->values()->get();
        $book_status = Parameter::whereAttribute('book_status')->first()->values()->get();
        $characteristic_type = Parameter::whereAttribute('characteristic_type')->first()->values()->get();
        $plaza_types = Parameter::whereAttribute('plaza_type')->first()->values()->get();
        $account_number = "42424242";
        $phonepay = "";

        if (Auth::check()) {
        $params = [
            'user' => $this->user,
            'account_number' => $account_number,
            'phonepay' => $phonepay,
            'publication_types' => $pub_types,
            'cover_type' => $cover_type,
            'book_status' => $book_status,
            'characteristic_types' => $characteristic_type,
            'plaza_types' => $plaza_types,
        ];
        }
        else{
        $params = [
            'publication_types' => $pub_types,
            'cover_type' => $cover_type,
            'book_status' => $book_status,
            'characteristic_types' => $characteristic_type,
            'plaza_types' => $plaza_types,
        ];
        }

        return View::make('publications.create')->with($params)->withSocial(Helpers::getSocialAuthUrls());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($username)
    {
        if ($this->isLoggedInUser($username)) {
            $this->user = Auth::user();
            $person = Input::only('person');
            $typeofpay = Input::get('typeofpay');
            $person = $person['person'];
            if (isset($person) && array_key_exists('account_number', $person)
                && ($person['account_number'] = trim($person['account_number'])) !== "") {
                if ($this->user->person->account_number !== $person['account_number']) {
                    $this->user->person->account_number = $typeofpay.''.$person['account_number']; //ésta linea agrega un "+" antes del campo cuenta de banco, el cual hace referencia a que es un número para pagos en celular
                    $this->user->person->save();
                    $this->user->person->touch();
                }
            } else {
                $errors = new MessageBag();
                $errors->add('person[account_number]', 'Debe dar informacion bancaria');
                return Redirect::back()->withInput()->withErrors($errors);
            }
            $data = Input::except('person');
            /*
            $this->book = Book::find($data['book_id']);
            if (!$this->book) {
                $errors = new MessageBag();
                $errors->add('book_id', 'Debe escoger un libro');
                return Redirect::back()->withInput()->withErrors($errors); 
            } */

            $this->publication = new BookPublication();

            $pub_types = Parameter::whereAttribute('publication_type')->first()->values()->get();
            $publication_type_length = count($data['publication_type']);
            if ($publication_type_length > 0) {
                if ($publication_type_length == 2) {
                    $this->publication->publication_type = $pub_types[2]->id; //TODO: ambos
                } else {
                    $this->publication->publication_type = $data['publication_type'][0];
                }
            } else {
                var_dump($pub_types);
                echo "<br />";
                die(var_dump($data['publication_type']));
                return Redirect::back()->withInput();
            }

            if ($pub_types[1]->id == $this->publication->publication_type) {
                $this->publication->price = 0;
            } else {
                $this->publication->price = $data['price'];
            }

            $book_status = Parameter::whereAttribute('book_status')->first()->values()->get();
            $this->publication->book_status = $data['book_status'];
            if ($book_status[0]->id != $this->publication->book_status) {
                $this->publication->amount = 1;
            } else {
                $this->publication->amount = $data['amount'];
            }

            $this->publication->published_book_title = $data['published_book_title'];
            $this->publication->description = $data['description'];
            $this->publication->pickup_fullname = $data['pickup_fullname'];
            $this->publication->pickup_address = $data['pickup_address'];
            $this->publication->pickup_phone = $data['pickup_phone'];
            $this->publication->pickup_city = $data['pickup_city'];
            $this->publication->cover_type = $data['cover_type'];
            $this->publication->plaza_type = $data['plaza_type'];
            $this->publication->url = "";

            $types = Parameter::whereAttribute('publication_status')->first()->values()->get();
            $this->publication->publication_status = $types[2]->id;//in review
            $this->publication->user_id = $this->user->id;
            #$this->publication->book_id = $this->book->id;

            if (!$this->publication->isValid()) {
                var_dump($this->publication->errors);
                die("publicacion no valida");
                return Redirect::back()->withInput()->withErrors($this->publication->errors);
            }

            $this->publication->save();
            $this->publication->touch();

            //Lower case everything
            $string = strtolower($data['published_book_title']);
            //Make alphanumeric (removes all other characters)
            $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
            //Clean up multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);
            //Convert whitespaces and underscore to dash
            $string = preg_replace("/[\s_]/", "-", $string);
            $this->publication->url = $string.'-'.$this->publication->id;

            $this->publication->save();
            $this->publication->touch();

            if (array_key_exists('photos', $data)) {
                $photos = $data['photos'];
                $publicationPhotos = [];
                foreach ($photos as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 1;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }
            if (array_key_exists('photos2', $data)) {
                $photos2 = $data['photos2'];
                $publicationPhotos = [];
                foreach ($photos2 as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 2;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }
            if (array_key_exists('photos3', $data)) {
                $photos3 = $data['photos3'];
                $publicationPhotos = [];
                foreach ($photos3 as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 3;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }

            if (array_key_exists('characteristic_types', $data)) {
                $characteristic_types = $data['characteristic_types'];
                if (! $this->publication->characteristics()->contains($characteristic_types)) {
                $this->publication->characteristics()->attach($characteristic_types);
                }
            } else {
                //TODO No publication characteristics, remind user
            }

            return Redirect::route('users.publications.show', [$username, $this->publication->url]);
        } else {
            return Redirect::route('users.publications.index', [$username]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($username, $url)
    {
        $this->user = User::whereUsername($username)->first();
        $this->publication = $this->user->publications()->whereUrl($url)->firstOrFail();
        $isInCart = false;
        if(Auth::check()){
        $cart = Auth::user()->active_carts()->first();
        $isInCart = $cart != null && $cart->items()->get()->contains($url);
        }
        #$isBought = $this->user->books_bought()->get()->contains($id); 
         return View::make('publications.show')->withPublication($this->publication)->withIsInCart($isInCart);
    }

    public function sold($username)
    {
        $this->user = User::whereUsername($username)->first();

        $sold = BookPublication::join('shipments', 'published_books.id', '=', 'shipments.published_book_id')
        ->join('users', 'published_books.user_id', '=', 'users.id')
        ->where('published_books.user_id', Auth::user()->id)
        ->select('published_books.*')
        ->get()->map(function($value)
        {
            return $value;
        });

        return View::make('publications.sold')->with([
             'user' => $this->user,
             'sold'=> $sold,

            ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($username, $url)
    {
        if ($this->isLoggedInUser($username) || Auth::user()->isAdmin()) {
            $this->user = User::whereUsername($username)->first();
            $this->publication = $this->user->publications()->whereUrl($url)->firstOrFail();
            $photo1 = $this->publication->photos()->where('position', 1)->first();
            $photo2 = $this->publication->photos()->where('position', 2)->first();
            $photo3 = $this->publication->photos()->where('position', 3)->first();
            $pub_types = Parameter::whereAttribute('publication_type')->first()->values()->get();
            $cover_type = Parameter::whereAttribute('cover_type')->first()->values()->get();
            $book_status = Parameter::whereAttribute('book_status')->first()->values()->get();
            $characteristic_type = Parameter::whereAttribute('characteristic_type')->first()->values()->get();
            $plaza_types = Parameter::whereAttribute('plaza_type')->first()->values()->get();
            $params = [
                'user' => $this->user,
                'publication_types' => $pub_types,
                'cover_type' => $cover_type,
                'book_status' => $book_status,
                'characteristic_types' => $characteristic_type,
                'plaza_types' => $plaza_types,
                'photo1' => $photo1,
                'photo2' => $photo2,
                'photo3' => $photo3,
            ];

            return View::make('publications.edit')->withPublication($this->publication)->with($params);
        } else {
            return Redirect::route('users.publications.index', [$username]);
        }
    }

    /**
     * Remove photo from publication.
     *
     * @param int $id
     *
     * @return Response
     */
    public function removePhoto($id)
    {
            $response = [ 'success' => false, ];
            $photo= BookPublicationPhoto::find($id);
            $photo->forceDelete();
            // delete image
            $response['success'] = true;
            $response['mensaje'] = "Imagen eliminada";
        return Response::json($response);
    }

    /**
     * Rotate 90° photo from publication.
     *
     * @param int $id
     *
     * @return Response
     */
    public function rotatePhoto($id)
    {
        /*if ($this->isLoggedInUser($username)) {*/
            $response = [ 'success' => false, ];
            $photo = BookPublicationPhoto::find($id);
            $image_url = storage_path().'/cache/img/publication/'.$photo->published_book_id.'/'.$photo->image_file_name;
            $img = Image::make($image_url);
            // rotate image 90 degrees clockwise
            $img->rotate(-90);
            $img->save();
            $response['success'] = true;
            $response['mensaje'] = "Imagen rotada";
            return Response::json($response);
        /*} else {*/
        /*}*/
    }

    /**
     * Cop photo from publication.
     *
     * @param int $id
     *
     * @return Response
     */
    public function cropPhoto($id)
    {
        /*if ($this->isLoggedInUser($username)) {*/
            $response = [ 'success' => false, ];
            $photo = BookPublicationPhoto::find($id);
            $image_url = storage_path().'/cache/img/publication/'.$photo->published_book_id.'/'.$photo->image_file_name;
            $img = Image::make($image_url);
            // rotate image 90 degrees clockwise
            $img->rotate(-90);
            $img->save();
            $response['success'] = true;
            $response['mensaje'] = "Imagen rotada";
            return Response::json($response);
        /*} else {*/
        /*}*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($username, $id)
    {
        /*if ($this->isLoggedInUser($username)) {*/
            $this->user = Auth::user();
            $person = Input::only('person');
            //$typeofpay = Input::get('typeofpay');
            $person = $person['person'];
            /*if (isset($person) && array_key_exists('account_number', $person)
                && ($person['account_number'] = trim($person['account_number'])) !== "") {
                if ($this->user->person->account_number !== $person['account_number']) {
                    $this->user->person->account_number = $typeofpay.''.$person['account_number']; //ésta linea agrega un "+" antes del campo cuenta de banco, el cual hace referencia a que es un número para pagos en celular
                    $this->user->person->save();
                    $this->user->person->touch();
                }
            } else {
                $errors = new MessageBag();
                $errors->add('person[account_number]', 'Debe dar informacion bancaria');
                return Redirect::back()->withInput()->withErrors($errors);
            }
            */$data = Input::except('person');
            
            /*$this->book = Book::find($data['book_id']);
            if (!$this->book) {
                $errors = new MessageBag();
                $errors->add('book_id', 'Debe escoger un libro');
                return Redirect::back()->withInput()->withErrors($errors); 
            }*/

            $this->publication = BookPublication::find($id);

            $pub_types = Parameter::whereAttribute('publication_type')->first()->values()->get();
            $publication_type_length = count($data['publication_type']);
            if ($publication_type_length > 0) {
                if ($publication_type_length == 2) {
                    $this->publication->publication_type = $pub_types[2]->id; //TODO: ambos
                } else {
                    $this->publication->publication_type = $data['publication_type'][0];
                }
            } else {
                var_dump($pub_types);
                echo "<br />";
                die(var_dump($data['publication_type']));
                return Redirect::back()->withInput();
            }

            if ($pub_types[1]->id == $this->publication->publication_type) {
                $this->publication->price = 0;
            } else {
                $this->publication->price = $data['price'];
            }

            $book_status = Parameter::whereAttribute('book_status')->first()->values()->get();
            $this->publication->book_status = $data['book_status'];
            if ($book_status[0]->id != $this->publication->book_status) {
                $this->publication->amount = 1;
                if (array_key_exists('characteristic_types', $data)) {
                    $characteristic_types = $data['characteristic_types'];
                    foreach ($characteristic_types as $key => $characteristic_type) {

                        $characteristics_exists = $this->publication->characteristics()->where('characteristic_type', $characteristic_type)->exists();

                        if (!$characteristics_exists) {
                        $this->publication->characteristics()->attach($characteristic_type);
                        }
                    }
                } else {
                //TODO No publication characteristics, remind user
                }
            } else {
                $this->publication->amount = $data['amount'];
                $this->publication->characteristics()->detach();
            }

            $this->publication->published_book_title = $data['published_book_title'];
            $this->publication->description = $data['description'];
            /*$this->publication->pickup_fullname = $data['pickup_fullname'];
            $this->publication->pickup_address = $data['pickup_address'];
            $this->publication->pickup_phone = $data['pickup_phone'];
            $this->publication->pickup_city = $data['pickup_city'];*/
            $this->publication->cover_type = $data['cover_type'];
            $this->publication->plaza_type = $data['plaza_type'];
            $this->publication->url = "";

            /*$types = Parameter::whereAttribute('publication_status')->first()->values()->get();
            $this->publication->publication_status = $types[2]->id;//in review
            $this->publication->user_id = $this->user->id;
            #$this->publication->book_id = $this->book->id;*/

            /*if (!$this->publication->isValid()) {
                var_dump($this->publication->errors);
                die("publicacion no valida");
                return Redirect::back()->withInput()->withErrors($this->publication->errors);
            }*/

            $this->publication->save();
            $this->publication->touch();

            //Lower case everything
            $string = strtolower($data['published_book_title']);
            //Make alphanumeric (removes all other characters)
            $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
            //Clean up multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);
            //Convert whitespaces and underscore to dash
            $string = preg_replace("/[\s_]/", "-", $string);
            $this->publication->url = $string.'-'.$this->publication->id;

            $this->publication->save();
            $this->publication->touch();

            if (array_key_exists('photos', $data)) {
                $photos = $data['photos'];
                $publicationPhotos = [];
                foreach ($photos as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 1;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }
            if (array_key_exists('photos2', $data)) {
                $photos2 = $data['photos2'];
                $publicationPhotos = [];
                foreach ($photos2 as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 2;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }
            if (array_key_exists('photos3', $data)) {
                $photos3 = $data['photos3'];
                $publicationPhotos = [];
                foreach ($photos3 as $key => $photo) {
                    if ($photo != null) {
                        $publicationPhoto = new BookPublicationPhoto();
                        $publicationPhoto['description'] = '';
                        $publicationPhoto['image_file_name'] = $photo->getClientOriginalName();
                        $publicationPhoto['image_content_type'] = $photo->getClientMimeType();
                        $publicationPhoto['image_file_size'] = $photo->getClientSize();
                        $publicationPhoto['image_updated_at'] = Functions::currentTimestamp();
                        $publicationPhoto['position'] = 3;
                        $publicationPhotos[] = $publicationPhoto;
                        $photo->move(storage_path().'/cache/img/publication/'.$this->publication->id.'/', $publicationPhoto['image_file_name']);
                    }
                }
                $this->publication->photos()->saveMany($publicationPhotos);
            } else {
                //TODO No publication photos, remind user
            }

            return Redirect::route('users.publications.show', [$this->user->username, $this->publication->url]);
        /*} else {*/
        //}
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($username, $id)
    {
        return Redirect::route('users.publications.index', [$username]);
    }
}
