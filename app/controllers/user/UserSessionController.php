<?php

class UserSessionController extends \BaseController {


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Auth::check()) {
            return Redirect::intended();
        } else {
            return View::make('session.login')->withSocial(Helpers::getSocialAuthUrls());
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $identifier = Input::get('identifier');
        $password = Input::get('password');

        if (Auth::attempt(['username' => $identifier, 'password' => $password])
            || Auth::attempt(['email' => $identifier, 'password' => $password])
        ) {
            return Redirect::intended('market');
        }

        return Redirect::back()->withInput()->withErrors(['login_error' => 'Usuario o contraseña incorrecta.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy() {
        Auth::logout();

        return Redirect::route('login');
    }


}
