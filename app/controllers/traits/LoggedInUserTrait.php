<?php

/**
 *
 */
trait LoggedInUserTrait
{
    function isLoggedInUser($username)
    {
        return Auth::check() && Auth::user()->username === $username;
    }
}
