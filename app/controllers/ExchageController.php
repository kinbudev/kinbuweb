<?php

class ExchageController extends \BaseController
{

    protected $user;

    function __construct()
    {
        $this->user = Auth::user();
    }

    public function index()
    {
        $data = new stdClass;
        $data->ready = $this->user->exchanges()->ready()->get();
        $data->exchanged = $this->user->exchanges()->exchanged()->get();
        $data->rejected = $this->user->exchanges()->rejected()->get();
        $data->accepted = $this->user->exchanges()->accepted()->get();
        return View::make('exchanges.index')->withData($data);
    }

    public function show($id)
    {
        if (Request::ajax()) {
            $response = [ 'success' => false ];
            $exchange = DB::table('books_exchanged as a')
                ->join('published_books as b', 'a.published_book_id1', '=', 'b.id')
                ->join('users as c', 'b.user_id', '=', 'c.id')
                ->join('books as d', 'b.book_id', '=', 'd.id')
                ->join('users as e', 'a.user_actor_id', '=', 'e.id')
                ->join('people as f', 'e.id', '=', 'f.user_id')
                ->where('c.id', $this->user->id)
                ->where('a.id', $id)
                ->select('a.*', DB::raw("concat(f.firstname, ' ', f.lastname) as actor"), 'e.username', 'd.title');
            $data = $exchange->get()[0];
            if ($data) {
                $details = [];
                foreach (json_decode($data->details) as $key => $detail) {
                    $details[] = BookPublication::with('book')->findOrFail($detail);
                }
                $data->publications = $details;
                $response['data'] = $data;
                $response['success'] = true;
            }
            return Response::json($response);
        }
        App::abort(404);
    }

    public function for_exchange()
    {
        if (Request::ajax()) {
            $data = $this->user->publications_for_exchange()->get();
            $response = [
                'success' => $data->count() > 0,
                'data' => $data
            ];
            return Response::json($response);
        }
        App::abort(404);
    }

    public function reject($id)
    {
        if (Request::ajax()) {
            $response = ['success' => false];
            $exchange = DB::table('books_exchanged as a')
                ->join('published_books as b', 'a.published_book_id1', '=', 'b.id')
                ->join('users as c', 'b.user_id', '=', 'c.id')
                ->where('c.id', $this->user->id)
                ->where('a.id', $id)
                ->select('a.*');
            if ($exchange->count() > 0) {
                $exchange = BookExchange::find($id);
                $pv = Parameter::whereAttribute('exchange_status')->first()->pValue('rejected');
                $exchange->exchange_status = $pv->id;
                $exchange->save();
                $response['success'] = true;
            }
            return Response::json($response);
        }
        App::abort(404);
    }

    /**
     * Display a listing of the resource.
     * POST /exchange.
     *
     * @return Response
     */
    public function store()
    {
        $response = [ 'success' => false, ];
        $books_for_exchange = $this->user->publications_for_exchange()->get();
        $publications = Input::get('publications');
        if ($books_for_exchange->count() > 0) {
            $publication = BookPublication::findOrFail(Input::get('publication_id'));
            $exchanges = $publication->exchanges();
            if ($exchanges->count() > 0) {//TODO handle multiple exchanges
                $response['message'] = "Ya hay un proceso de intercambio para este libro";
            } else {
                //TODO get the exchangables
                //$exchangables = $books_for_exchange->lists('id');
                $pv = Parameter::whereAttribute('exchange_status')->first()->pValue('ready');
                $exchange = new BookExchange;
                $exchange->published_book_id1 = $publication->id;
                $exchange->user_actor_id = $this->user->id;
                $exchange->exchange_status = $pv->id;
                $exchange->details = json_encode($publications);
                $exchanges->save($exchange);
                $response['success'] = true;
                $response['message'] = "Ya notificaremos al usuario. Espera su respuesta";
            }
        } else {
            $response['message'] = "Debes tener libros disponibles para intercambiar";
        }
        return Response::json($response);
    }

    public function accept($id)
    {
        if (Request::ajax()) {
            $response = ['success' => false];
            $exchange = DB::table('books_exchanged as a')
                ->join('published_books as b', 'a.published_book_id1', '=', 'b.id')
                ->join('users as c', 'b.user_id', '=', 'c.id')
                ->where('c.id', $this->user->id)
                ->where('a.id', $id)
                ->select('a.*');
            if ($exchange->count() > 0) {
                $publicationId = Input::get('publication');
                $exchange = BookExchange::find($id);
                $pv = Parameter::whereAttribute('exchange_status')->first()->pValue('accepted');
                $exchange->exchange_status = $pv->id;
                $exchange->published_book_id2 = $publicationId;
                $exchange->save();
                $exchange->touch();
                $response['success'] = true;
            }
            return Response::json($response);
        }
        App::abort(404);
    }

    public function payment($id)
    {
        if (Request::ajax()) {
            $exchange = DB::table('books_exchanged as a')
                ->join('published_books as b', 'a.published_book_id1', '=', 'b.id')
                ->join('users as c', 'b.user_id', '=', 'c.id')
                ->where('c.id', $this->user->id)
                ->where('a.id', $id)
                ->select('a.*');
            $exchange = $exchange->get()[0];

            if ($exchange->user_actor_id == $this->user->id) {
                $book = BookPublication::with('book')->find($exchange->published_book_id1)->book;
            } else {
                $book = BookPublication::with('book')->find($exchange->published_book_id2)->book;
            }
            $exchange->user_id = $this->user->id;
            $cajero = new KinbuCashier(null, $exchange);
            $data = $cajero->getPayUParamsForExchange();
            $payuform = View::make('shoppingcart.payuform', $data)->render();
            $response = [
                'payuform' => $payuform,
                'amount' => $data['amount'],
                'book' => $book
            ];
            return Response::json($response);
        }
        App::abort(404);
    }

    public function transaction($id)
    {
        $response = ['success' => false,];
        $reference_code = Input::get('referenceCode');
        $amount = Input::get('amount');

        $exchange = BookExchange::findOrFail($id);

        $canBeBought = $this->cart->canBeBought();
        if ($canBeBought === true) {
            $pv = Parameter::whereAttribute('publication_type')->first()->pValue('sale');
            $trdata = ['reference_code' => $reference_code, 'value' => $amount, 'cart_id' => $cart_id, 'type' => $pv->id];
            $transaction = new Transaction($trdata);
            $model = $this->user->transactions()->save($transaction);
            $response['success'] = $model !== false;
            if ($model === false) {
                $response['mensaje'] = "Ocurrió un error con el inicio de la transaccion";
            } else {
                $pv = Parameter::whereAttribute('cart_status')->first()->pValue('inactive');
                $this->cart->cart_status = $pv->id;
                $this->cart->save();
                $this->cart->touch();
            }
        } else {
            $response['mensaje'] = "En el momento no puedes comprar {$canBeBought->book->title} Revisa la publicaci&oacute;n";
        }
        return Response::json($response);
    }
}
