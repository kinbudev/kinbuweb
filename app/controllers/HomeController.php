<?php

use Carbon\Carbon;

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    |
    */

    public function showHome() {
        return View::make('home');
    }

    public function showMarket() {

        $now = Carbon::now();
        $then = Carbon::now()->subMonth();

        $eliteBooks = BookPublication::join('parameter_values', 'published_books.plaza_type', '=', 'parameter_values.id')
        ->select('published_books.*')
        ->where(function($query) use($then, $now)
        {
            $query//->whereBetween('published_books.created_at', [$then, $now])
                  ->where('parameter_values.pvalue', '=', 'best');           
        })
        ->orderBy('published_books.created_at', 'desc')->take(12)
        ->get()->filter(function($publication) {
            return $publication->can_be_bought;
        })->chunk(6)->map(function($value)
        {
            return $value->chunk(3);
        });


        $meliteBooks = BookPublication::join('parameter_values', 'published_books.plaza_type', '=', 'parameter_values.id')
        ->select('published_books.*')
        ->where(function($query) use($then, $now)
        {
            $query//->whereBetween('published_books.created_at', [$then, $now])
                  ->where('parameter_values.pvalue', '=', 'best');           
        })
        ->orderBy('published_books.created_at', 'desc')->take(12)
        ->get()->filter(function($publication) {
            return $publication->can_be_bought;
        })->chunk(1)->map(function($value)
        {
            return $value->chunk(1);
        });

        
        $exchangeBooks = BookPublication::join('parameter_values', 'published_books.publication_type', '=', 'parameter_values.id') 
        ->select('published_books.*')
        //->whereBetween('published_books.created_at', [$then, $now])
        ->where(function($query) use($then, $now)
        {
            $query->where('parameter_values.pvalue', '=', 'exchange')
                  ->orWhere('parameter_values.pvalue', '=', 'both'); //TODO: add exchange publications which has been recently updated
        })
        ->orderBy('published_books.created_at', 'desc')->take(12)
        ->get()->filter(function($publication){
            return $publication->can_be_bought;
        })->chunk(6)->map(function($value)
        {
            return $value->chunk(3);
        });


        $recentBoughtExchanged = BookPublication::join('shipments', 'published_books.id', '=', 'shipments.published_book_id')
        ->join('orders', 'shipments.order_id', '=', 'orders.id')
        ->join('parameter_values', 'orders.order_status', '=', 'parameter_values.id')
        ->select('published_books.*')
        //->whereBetween('orders.created_at', [$then, $now])
        ->where(function($query) use($then, $now)
        {
            $query->where('parameter_values.pvalue', '=', 'valid')
                  ->orWhere('parameter_values.pvalue', '=', 'partial')
                  ->orWhere('parameter_values.pvalue', '=', 'sent')
                  ->orWhere('parameter_values.pvalue', '=', 'received');
        })
        ->groupBy('published_books.id')
        ->orderBy('orders.created_at', 'desc')
        ->get()->chunk(6)->map(function($value)
        {
            return $value->chunk(3);
        });

        $mrecentBoughtExchanged = BookPublication::join('shipments', 'published_books.id', '=', 'shipments.published_book_id')
        ->join('orders', 'shipments.order_id', '=', 'orders.id')
        ->join('parameter_values', 'orders.order_status', '=', 'parameter_values.id')
        ->select('published_books.*')
        //->whereBetween('orders.created_at', [$then, $now])
        ->where(function($query) use($then, $now)
        {
            $query->where('parameter_values.pvalue', '=', 'valid')
                  ->orWhere('parameter_values.pvalue', '=', 'partial')
                  ->orWhere('parameter_values.pvalue', '=', 'sent')
                  ->orWhere('parameter_values.pvalue', '=', 'received');
        })
        ->groupBy('published_books.id')
        ->orderBy('orders.created_at', 'desc')
        ->get()->chunk(1)->map(function($value)
        {
            return $value->chunk(1);
        });


        return View::make('market.index')->with([

             'eliteBooks'=> $eliteBooks,
             'meliteBooks'=> $meliteBooks,
             'exchangeBooks'=> $exchangeBooks,
             'recentBoughtExchanged'=> $recentBoughtExchanged,
             'mrecentBoughtExchanged'=> $mrecentBoughtExchanged,

            ]);
    }


        /*
        $books = Book::join('books_ratings','books.id', '=', 'books_ratings.book_id')
        ->select('books.*')->addSelect(DB::raw('avg(books_ratings.rating) rating'))
        ->where(function ($query) use($then, $now)
        {
            $query->whereBetween('books_ratings.created_at', [$then, $now])
                ->orWhereBetween('books_ratings.updated_at', [$then, $now]);
        })
        ->groupBy('books.id')->having(DB::raw('rating'),'>=', 4)->take(12)
        ->get()->chunk(6)->map(function ($value)
        {
            return $value->chunk(3);
        });

        */
        /*
        $mostCommentsBooks = Book::join('book_comments','books.id', '=', 'book_comments.book_id')
        ->select('books.*')->addSelect(DB::raw('count(*) count'))
        ->whereBetween('book_comments.created_at', [$then, $now])
        ->groupBy('books.id')->orderBy('count','desc')->take(12)
        ->get()->chunk(6)->map(function ($value)
        {
            return $value->chunk(3);
        });
        */
    /*
        return View::make('market.index')->with([
            'books' => $books,
            'mostCommented' => $mostCommentsBooks
        ]);

    }
    */


    public function search()
    {
        $query = Input::get('q');
        //dd($query);

        if (strlen($query) > 3) {
            $publications = BookPublication::search($query)->get()->filter(function ($publication) {
                return $publication->can_be_bought;
            });
            $elitePublications = $publications->filter(function ($publication) {
                return $publication->plaza === 'best';
            });
            $standardPublications = $publications->filter(function ($publication) {
                return $publication->plaza === 'good';
            });
            $basicPublications = $publications->filter(function ($publication) {
                return $publication->plaza === 'normal';
            });

            $users = User::search($query)->get();

            Input::flash();
            return View::make('search', compact('query', 'elitePublications', 'standardPublications', 'basicPublications', 'users'));
        } else {
            return Redirect::back()->withInput();
        }
    }

    public function showTerms() {
        return View::make('terms');
    }

    public function showPrivacy() {
        return View::make('privacy');
    }

    public function showHowpublish() {
        return View::make('howpublish');
    }

    public function showRegistro() {
        return View::make('emails.registro');
    }
}
