<?php

class RatingsController extends BaseController {

	public function __construct() {
		$this->beforeFilter('csrf', ['on' => 'post']);
		$this->beforeFilter('ajax', ['only' => ['index', 'show', 'edit']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($book_id) {
		$response = ['success' => TRUE];
		$rating = Book::find($book_id)->rating();
		$response['rating'] = $rating;
		return Response::json($response);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($book_id) {
		$response = ['success' => TRUE];
		$response['errors'] = [];
		if (Auth::check()) {
			$user = Auth::user();
			$rating = Input::get('rating');
			Book::find($book_id)->ratings()->attach($user->id, ['rating' => $rating]);
			$user->actions()->attach(5);
			$currentLevel = $user->level;
	        $currentLevelAcumulate = $user->person->level_acumulate;
			if ($user->updateLevel()) {
				$newLevel = $user->level;
	            $newLevelAcumulate = $user->person->level_acumulate;
	            $diff = $newLevelAcumulate - $currentLevelAcumulate;
	            $response['notifications'][] = "+$diff puntos";
	            if ($currentLevel < $newLevel) {
	                $response['notifications'][] = "Has alcanzado el nivel $newLevel";
	            }
				$response['medals'] = $user->updateMedal(8);
			} else {
				$response['errors'] = 'cannot update user level';
			}
			$response['mensaje'] = 'Libro calificado.';
		} else {
			$response['mensaje'] = "No permitido.";
		}

		return Response::json($response);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
