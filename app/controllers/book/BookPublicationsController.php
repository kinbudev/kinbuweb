<?php

class BookPublicationsController extends \BaseController
{
    protected $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($book_id)
    {
        $book = Book::findOrFail($book_id);
        $on_sale = $book->publications()->onSale()->get();
        $on_exchange = $book->publications()->onExchange()->get();
        $params = [
            'book' => $book,
            'on_sale' => $on_sale,
            'on_exchange' => $on_exchange,
        ];

        return View::make('publications.index')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($book_id, $id)
    {
        $book = Book::with('publications')->findOrFail($book_id);
        $publication = $book->publications()->whereId($id)->firstOrFail();
        $isInCart = false;
        $cart = $this->user->active_carts()->first();
        $isInCart = $cart != null && $cart->items()->get()->contains($id);
        return View::make('publications.show')->withPublication($publication)->withIsInCart($isInCart);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
