<?php

use Illuminate\Database\QueryException;
use \Illuminate\Support\MessageBag;

class BookCommentsController extends BaseController {

    /**
     * Display a listing of bookcomments
     *
     * @return Response
     */
    public function index() {
        $bookcomments = BookComment::all();

        return View::make('bookcomments.index', compact('bookcomments'));
    }

    /**
     * Show the form for creating a new bookcomment
     *
     * @return Response
     */
    public function create() {
        return View::make('bookcomments.create');
    }

    /**
     * Store a newly created bookcomment in storage.
     *
     * @return Response
     */
    public function store() {

        $errors = new MessageBag();

        $logged_user = Auth::user();
        $user_id = Input::get('user_id');
        $book_id = Input::get('book_id');
        $notifications = [];
        if ($logged_user->id == $user_id) {
            $book = Book::find($book_id);
            if ($book) {
                $comment = Input::only(['book_id', 'user_id', 'content']);
                $comment['content'] = e($comment['content']);
                $bc = BookComment::create($comment);
                $logged_user->actions()->attach(2);//TODO get the action id
                $currentLevel = $logged_user->level;
                $currentLevelAcumulate = $logged_user->person->level_acumulate;
                $notifications[] = "Compartiste tu comentario de &eacute;ste libro";
                if ($logged_user->updateLevel()) {
                    $newLevel = $logged_user->level;
                    $newLevelAcumulate = $logged_user->person->level_acumulate;
                    $diff = $newLevelAcumulate - $currentLevelAcumulate;
                    $notifications[] = "+$diff puntos";
                    if ($currentLevel < $newLevel) {
                        $notifications[] = "Has alcanzado el nivel $newLevel";
                    }
                    $logged_user->updateMedal(8);
                } else {
                    $errors->add('user', 'cannot update user level');
                }
            } else {
                $errors->add('book', 'book not found');
            }
        } else {
            $errors->add('user', 'user not found');
        }

        return Redirect::back()->withErrors($errors)->with('knotifications', $notifications);


    }

    /**
     * Display the specified bookcomment.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        $bookcomment = BookComment::findOrFail($id);

        return View::make('bookcomments.show', compact('bookcomment'));
    }

    /**
     * Show the form for editing the specified bookcomment.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        $bookcomment = BookComment::find($id);

        return View::make('bookcomments.edit', compact('bookcomment'));
    }

    /**
     * Update the specified bookcomment in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {
        $bookcomment = BookComment::findOrFail($id);

        $validator = Validator::make($data = Input::all(), BookComment::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $bookcomment->update($data);

        return Redirect::route('bookcomments.index');
    }

    /**
     * Remove the specified bookcomment from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        BookComment::destroy($id);

        return Redirect::route('bookcomments.index');
    }

}
