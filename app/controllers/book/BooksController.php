<?php

class BooksController extends BaseController {

    protected $book;

    public function __construct(Book $book) {
        $this->book = $book;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $query = Input::get("q");
        if (isset($query)) {
            $books = Book::search($query)->paginate(18);
        } else {
            $books = $this->book->paginate(18);
        }

        if (!is_object($books)) {
            return View::make('grouped.book')->withNoBooks(true);
        } else {
            if (Request::ajax()) {
                return Response::json(View::make('grouped.list')->withBooks($books)->with('no_books', false)->render());
            } else {
                return View::make('grouped.book')->withBooks($books)->with('no_books', false);
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return 'create book form?';
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        return Input::all();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $books
     * @return Response
     */
    public function show($books) {
        $book = Book::with('authors')
            ->find($books);
        if (is_object($book)) {
            $comments = $book->comments;
            $logged_user_rating = 0;
            $book_read_state = '';
            if (Auth::check()) {
                $ratings = $book->ratings()->whereUserId(Auth::user()->id)->get(['rating']);
                if (count($ratings) > 0) {
                    $logged_user_rating = $ratings[0]->rating;
                }
                $book_read_state = DB::table('books_read')
                    ->leftJoin('parameter_values', 'books_read.book_read_status', '=', 'parameter_values.id')
                    ->whereBookId($books)
                    ->whereUserId(Auth::user()->id)
                    ->pluck('pvalue');
                $book_read_state = str_replace(' ', '_', $book_read_state);
            }

            return View::make('profiles.book')
                ->with('book', $book)
                ->with('comments', $comments)
                ->with('book_rating', $logged_user_rating)
                ->with('book_read_state', $book_read_state);
        } else {
            //TODO: BOOK NOT FOUND
            
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function recommend($id) {
        $response = [];
        $errors = [];
        $user = Auth::user();
        $user_id = $user->id;
        $friends_id = Input::get('friends_id');
        $insert = [];
        $update = [];
        $already_recommended = DB::table('book_user_recomendations')->where('recomender_id', $user_id)->where('book_id', $id)->lists('user_id');
        foreach ($friends_id as $friend_id) {
            if (in_array($friend_id, $already_recommended)) {
                $update[] = $friend_id;
            } else {
                $insert[] = array(
                    'recomender_id' => $user_id,
                    'user_id' => $friend_id,
                    'book_id' => $id,
                    'recomendation_status' => Parameter::whereAttribute('recomendation_status')->first()->pValueId('sent'),
                );
                $user->actions()->attach(4);//TODO get the action id
            }
        }
        $result = TRUE;
        if (!empty($insert)) {
            $result = DB::table('book_user_recomendations')->insert($insert);
        }
        if (!empty($update)) {
            foreach ($update as $friend_id) {
                DB::table('book_user_recomendations')
                ->where('recomender_id', $user_id)
                ->where('book_id', $id)
                ->where('user_id', $friend_id)
                ->update(['recomendation_status' => Parameter::whereAttribute('recomendation_status')->first()->pValueId('sent')]);
            }
        }
        $response['success'] = $result;
        $currentLevel = $user->level;
        $currentLevelAcumulate = $user->person->level_acumulate;
        if ($user->updateLevel()) {
            $newLevel = $user->level;
            $newLevelAcumulate = $user->person->level_acumulate;
            $diff = $newLevelAcumulate - $currentLevelAcumulate;
            $response['notifications'][] = "+$diff puntos";
            if ($currentLevel < $newLevel) {
                $response['notifications'][] = "Has alcanzado el nivel $newLevel";
            }
            $response['medals'] = $user->updateMedal(6);
        } else {
            $errors['user'] = 'cannot update user level';
        }
        $response['errors'] = $errors;
        return Response::json($response);
    }

    public function user_read_status($id)
    {
        $response = ['success' => TRUE];
        $errors = [];
        $rsparam_id = Parameter::whereAttribute('book_read_status')->first()->pValueId(Input::get('read_state'));
        $book_read_state = DB::table('books_read')
            ->leftJoin('parameter_values', 'books_read.book_read_status', '=', 'parameter_values.id')
            ->whereBookId($id)
            ->whereUserId(Auth::user()->id)
            ->pluck('parameter_values.id');
        $logged_user = Auth::user();
        if ($book_read_state) {
            if ($book_read_state == $rsparam_id) {
                $result = $logged_user->books_read()->detach($id);
            } else {
                $result = $logged_user->books_read()->updateExistingPivot($id, ['book_read_status' => $rsparam_id]);
            }
        } else {
            $result = $logged_user->books_read()->attach($id, ['book_read_status' => $rsparam_id]);
            // FIXME: Marcar libro solo da puntos cuando se hace por primera vez
            $logged_user->actions()->attach(3);//TODO get the action id
            $currentLevel = $logged_user->level;
            $currentLevelAcumulate = $logged_user->person->level_acumulate;
            if ($logged_user->updateLevel()) {
                $newLevel = $logged_user->level;
                $newLevelAcumulate = $logged_user->person->level_acumulate;
                $diff = $newLevelAcumulate - $currentLevelAcumulate;
                $response['notifications'][] = "+$diff puntos";
                if ($currentLevel < $newLevel) {
                    $response['notifications'][] = "Has alcanzado el nivel $newLevel";
                }
                $response['medals'] = $logged_user->updateMedal(2);
            } else {
                $errors['user'] = 'cannot update user level';
            }
        }
        $response['result'] = $result;
        $response['errors'] = $errors;
        return Response::json($response);
    }

}
