<?php

class MainCategoriesController extends BaseController {

    /**
     * Display a listing of maincategories
     *
     * @return Response
     */
    public function index() {
        $maincategories = MainCategory::all();

        return View::make('maincategories.index', compact('maincategories'));
    }

    /**
     * Show the form for creating a new maincategory
     *
     * @return Response
     */
    public function create() {
        return View::make('maincategories.create');
    }

    /**
     * Store a newly created maincategory in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make($data = Input::all(), MainCategory::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        MainCategory::create($data);

        return Redirect::route('maincategories.index');
    }

    /**
     * Display the specified maincategory.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        $maincategory = MainCategory::findOrFail($id);

        return View::make('maincategories.show', compact('maincategory'));
    }

    /**
     * Show the form for editing the specified maincategory.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        $maincategory = MainCategory::find($id);

        return View::make('maincategories.edit', compact('maincategory'));
    }

    /**
     * Update the specified maincategory in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {
        $maincategory = MainCategory::findOrFail($id);

        $validator = Validator::make($data = Input::all(), MainCategory::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $maincategory->update($data);

        return Redirect::route('maincategories.index');
    }

    /**
     * Remove the specified maincategory from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        MainCategory::destroy($id);

        return Redirect::route('maincategories.index');
    }

}
