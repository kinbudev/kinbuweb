<?php

class MailController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Send support mail and receive information of them
	 *
	 * @return Response
	 */
	public function supportmail()
	{
	$input_all = Input::all();
		Log::info("Confirmation input: " . json_encode($input_all));
		date_default_timezone_set('America/Bogota');
		$date = date('Y/F/d H:i:s');
		$email = Input::get('email');
		$name = Input::get('name');
        $subject = Input::get('subject');
        $user_id = Input::get('user_id');
        $text = Input::get('text');

        $a = array('user_id' => $user_id, 'text' => $text, 'name' => $name, 'date' => $date);

        Mail::send('emails.kinbu.supportneeded', $a, function($message) use ($email, $subject)
            {
                $message->subject($subject);
                $message->to('Soporte@kinbu.co');
                $message->from($email);
            });

        Mail::send('emails.receivedrequest', $a, function($message) use ($email)
            {
                $message->subject('Hemos recibido tu solicitud de soporte.');
                $message->to(Auth::user()->email);
            });

        return Redirect::back()->with('message', "Contacto enviado");
	}


	/**
	 * Send support mail and receive information of them
	 *
	 * @return Response
	 */
	public function bugmail()
	{
	$input_all = Input::all();
		Log::info("Confirmation input: " . json_encode($input_all));
		date_default_timezone_set('America/Bogota');
		$date = date('Y/F/d H:i:s');
		$email = Input::get('email');
		$name = Input::get('name');
        $subject = Input::get('subject');
        $user_id = Input::get('user_id');
        $text = Input::get('text');

        $a = array('user_id' => $user_id, 'text' => $text, 'name' => $name, 'date' => $date);

        Mail::send('emails.kinbu.bugfound', $a, function($message) use ($email, $subject)
            {
                $message->subject($subject);
                $message->to('Soporte@kinbu.co');
                $message->from($email);
            });

        Mail::send('emails.receivedrequest', $a, function($message) use ($email)
            {
                $message->subject('Hemos recibido tu solicitud de mejora');
                $message->to(Auth::user()->email);
            });

        return Redirect::back()->with('message', "Propuesta enviada");
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showSupport(/*$id*/)
	{
		return View::make('kinbu.support');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showBug(/*$id*/)
	{
		return View::make('kinbu.bug');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
