<?php

class BaseDetailAdminController extends BaseController {

    /**
    * Model Class name
    */
    protected $modelClass;

    /**
    * Parent Model Class name
    */
    protected $parentClass;

    /**
    * Parent detail relation name
    */
    protected $relation;

    /**
    * A class instance to retrieve the admin variable
    */
    protected $instance;

    function __construct() {
        $this->instance = new $this->modelClass;
    }

    /**
     * Display a listing of modelClass
     *
     * @return Response
     */
    public function index($parent_id) {
        $modelClass = $this->modelClass;
        $parentClass = $this->parentClass;
        $relation = $this->relation;

        $elements = $parentClass::find($parent_id)->$relation()->get();

        $data = [
            'title' => $this->instance->admin['title'],
			'model' => $this->instance->admin['model'],
			'actions' => $this->instance->admin['actions'],
			'elements' => $elements,
			'keys' => $this->instance->admin['grid_attrs'],
            'parent_id' => $parent_id
		];

		return View::make('kinbu.crud', $data);
    }

    /**
     * Show the form for creating a new modelClass
     *
     * @return Response
     */
    public function create($parent_id) {
        $data = [
            'location' => 'Crear un nuevo ' . $this->modelClass,
			'page_header' => 'Create ' . $this->modelClass,
			'model' => $this->instance->admin['model'],
			'attrs' => $this->instance->admin['form_attrs'],
            'parent_id' => $parent_id
		];
		return View::make('kinbu.form', $data);
    }

    /**
     * Store a newly created modelClass in storage.
     *
     * @return Response
     */
    public function store($parent_id) {
        $modelClass = $this->modelClass;
        $parentClass = $this->parentClass;
        $relation = $this->relation;

        $parent = $parentClass::find($parent_id);

        $data = Input::all();

        if (Request::hasFile('image')) {
			$image = Request::file('image');
			$data['image_file_name'] = $image->getClientOriginalName();
			$data['image_content_type'] = $image->getClientMimeType();
			$data['image_file_size'] = $image->getClientSize();
			$data['image_updated_at'] = Functions::currentTimestamp();
		}/* else if ($element->admin['model']['has_file']) { //TODO what if several files, or file is not image
            return Redirect::back()->withInput();
        }*/

        $validator = Validator::make($data, $modelClass::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $element = new $modelClass($data);

        $parent->$relation()->save($element);

        if (isset($image)) {//TODO what if not medals
            $image->move(storage_path() . '/cache/img/medals', $element->image_file_name);
        }

        return Redirect::route('admin.' . $parent->admin['model']['name'] . '.' . $element->admin['model']['name'] . '.index', [$parent_id]);
    }

    /**
     * Display the specified modelClass.
     *
     * @param  int $id
     * @return Response
     */
    public function show($parent_id, $id) {
        $modelClass = $this->modelClass;

        $element = $modelClass::findOrFail($id);

        $data = [
            'location' => 'Detalles ' . $modelClass,
			'page_header' => $modelClass . ' Details',
			'model' => $element->admin['model'],
			'element' => $element,
			'keys' => $element->admin['grid_attrs'],
            'parent_id' => $parent_id
		];

        return View::make('kinbu.item', $data);
    }

    /**
     * Show the form for editing the specified modelClass.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($parent_id, $id) {
        $modelClass = $this->modelClass;

        $element = $modelClass::findOrFail($id);

        $data = [
			'location' => 'Editar un ' . $modelClass,
			'page_header' => 'Edit ' . $modelClass,
			'model' => $element->admin['model'],
			'element' => $element,
			'attrs' => $element->admin['form_attrs'],
            'parent_id' => $parent_id
		];
		return View::make('kinbu.form', $data);
    }

    /**
     * Update the specified modelClass in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($parent_id, $id) {
        $modelClass = $this->modelClass;
        $parentClass = $this->parentClass;
        $relation = $this->relation;

        $element = $modelClass::findOrFail($id);

        $data = Input::only($element->getFillable());
        $data = array_filter($data, 'strlen');

        if (Request::hasFile('image')) {
			$image = Request::file('image');
			$data['image_file_name'] = $image->getClientOriginalName();
			$data['image_content_type'] = $image->getClientMimeType();
			$data['image_file_size'] = $image->getClientSize();
			$data['image_updated_at'] = Functions::currentTimestamp();
		} //TODO what if several files, or file is not image

        $validator = Validator::make($data, $modelClass::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $element->fill($data);
        $element->save();
        $element->touch();
        if (isset($image)) {//TODO what if not medals
            $image->move(storage_path() . '/cache/img/medals', $element->image_file_name);
        }

        $parent = $parentClass::find($parent_id);

        return Redirect::route('admin.' . $parent->admin['model']['name'] . '.' . $element->admin['model']['name'] . '.index', [$parent_id]);
    }

    /**
     * Remove the specified modelClass from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($parent_id, $id) {
        $modelClass = $this->modelClass;
        $modelClass::destroy($id);
        $parentClass = $this->parentClass;
        $parent = $parentClass::find($parent_id);

        return Redirect::route('admin.' . $parent->admin['model']['name'] . '.' . $this->instance->admin['model']['name'] . '.index', [$parent_id]);
    }

}
