<?php

class AdvanceMedalsController extends BaseDetailAdminController {

    protected $modelClass = "AdvanceMedal";

    protected $parentClass = "Medal";

    protected $relation = "advance_medals";

}
