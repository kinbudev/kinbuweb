<?php

/**
 * OrdersController
 */
class AdminBooksController extends BaseController
{

    function index()
    {
        $books = Book::all();
        return View::make('admin.books.index')->withBooks($books);
    }

    /**
     * Show the form for creating a new modelClass
     *
     * @return Response
     */
    public function create() {
        $languages = Parameter::whereAttribute('language')->first()->values()->get();
        $params = [
            'languages' => $languages
        ];
        return View::make('admin.books.form')->with($params);
    }

    /**
     * Store a newly created modelClass in storage.
     *
     * @return Response
     */
    public function store() {
        $data = Input::except('author');//
        $book = new Book();
        $book->ISBN = $data['ISBN'];
        $book->title = $data['title'];
        $book->about = $data['about'];
        $book->release_date = $data['release_date'];
        $book->editorial = $data['editorial'];
        $book->pages = $data['pages'];
        $book->image = $data['image'];
        $book->language = $data['language'];
        $book->save();

        $author_data = Input::only('author');
        $author_data = $author_data['author'];
        if (isset($author_data['id']) && !empty($author_data['id'])) {
            $author = Author::find($author_data['id']);
        } else {
            $author = new Author();
        }
        $author->firstname = $author_data['firstname'];
        $author->lastname = $author_data['lastname'];
        $author->about = $author_data['about'];
        $author->save();
        $author->touch();
        $book->authors()->attach($author->id);

        return Redirect::route('books.show', $book->id);
    }

    function show($books)
    {
        $book = Book::findOrFail($books);
        return View::make('books.show')->withBook($book);
    }

    /**
     * Show the form for editing the specified modelClass.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($books) {
        $book = Book::with('authors')->findOrFail($books);
        $languages = Parameter::whereAttribute('language')->first()->values()->get();
        $params = [
            'book' => $book,
            'languages' => $languages
        ];
        return View::make('admin.books.form')->with($params);
    }

    /**
     * Update the specified modelClass in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($books) {
        $data = Input::all();
        $book = Book::findOrFail($books);
        $book->ISBN = $data['ISBN'];
        $book->title = $data['title'];
        $book->about = $data['about'];
        $book->release_date = $data['release_date'];
        $book->editorial = $data['editorial'];
        $book->pages = $data['pages'];
        $book->image = $data['image'];
        $book->language = $data['language'];
        $book->save();
        $book->touch();

        $a = Input::only(['authors', 'author']);
        if (isset($a['authors'])) {
            $authors = $a['authors'];
            foreach ($authors as $key => $value) {
                $author = Author::find($value['id']);
                $author->firstname = $value['firstname'];
                $author->lastname = $value['lastname'];
                $author->about = $value['about'];
                $author->save();
                $author->touch();
            }
        } else {

        }

        return Redirect::route('books.show', $book->id);
    }

}
