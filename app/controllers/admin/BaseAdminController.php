<?php

class BaseAdminController extends BaseController {

    /**
    * Model Class name
    */
    protected $modelClass;

    /**
    * A class instance to retrieve the admin variable
    */
    protected $instance;

    function __construct() {
        $this->instance = new $this->modelClass;
    }

    /**
     * Display a listing of modelClass
     *
     * @return Response
     */
    public function index() {
        $modelClass = $this->modelClass;
        $elements = $modelClass::all();

        $data = [
			'title' => $this->instance->admin['title'],
			'model' => $this->instance->admin['model'],
			'actions' => $this->instance->admin['actions'],
			'elements' => $elements,
			'keys' => $this->instance->admin['grid_attrs']
		];

		return View::make('kinbu.crud', $data);
    }

    /**
     * Show the form for creating a new modelClass
     *
     * @return Response
     */
    public function create() {
        $data = [
			'location' => 'Crear un nuevo ' . $this->modelClass,
			'page_header' => 'Create ' . $this->modelClass,
			'model' => $this->instance->admin['model'],
			'attrs' => $this->instance->admin['form_attrs']
		];
		return View::make('kinbu.form', $data);
    }

    /**
     * Store a newly created modelClass in storage.
     *
     * @return Response
     */
    public function store() {
        $modelClass = $this->modelClass;

        $data = Input::all();

        if (Request::hasFile('image')) {
			$image = Request::file('image');
			$data['image_file_name'] = $image->getClientOriginalName();
			$data['image_content_type'] = $image->getClientMimeType();
			$data['image_file_size'] = $image->getClientSize();
			$data['image_updated_at'] = Functions::currentTimestamp();
		}/* else if ($element->admin['model']['has_file']) { //TODO what if several files, or file is not image
            return Redirect::back()->withInput();
        }*/

        $validator = Validator::make($data, $modelClass::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $element = $modelClass::create($data);
        if (isset($image)) {//TODO what if not medals
            $image->move(storage_path() . '/cache/img/medals', $element->image_file_name);
        }

        return Redirect::route('admin.' . $this->instance->admin['model']['name'] . '.index');
    }

    /**
     * Display the specified modelClass.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        $modelClass = $this->modelClass;
        $element = $modelClass::findOrFail($id);

        $data = [
			'location' => 'Detalles ' . $modelClass,
			'page_header' => $modelClass . ' Details',
			'model' => $this->instance->admin['model'],
			'element' => $element,
			'keys' => $this->instance->admin['grid_attrs']
		];

		return View::make('kinbu.item', $data);
    }

    /**
     * Show the form for editing the specified modelClass.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        $modelClass = $this->modelClass;
        $element = $modelClass::find($id);

        $data = [
			'location' => 'Editar ' . $modelClass,
			'page_header' => 'Edit ' . $modelClass,
			'model' => $this->instance->admin['model'],
			'element' => $element,
			'attrs' => $this->instance->admin['form_attrs']
		];

		return View::make('kinbu.form', $data);
    }

    /**
     * Update the specified modelClass in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {
        $modelClass = $this->modelClass;
        $element = $modelClass::findOrFail($id);

        $data = Input::only($element->getFillable());
        $data = array_filter($data, 'strlen');

        if (Request::hasFile('image')) {
			$image = Request::file('image');
			$data['image_file_name'] = $image->getClientOriginalName();
			$data['image_content_type'] = $image->getClientMimeType();
			$data['image_file_size'] = $image->getClientSize();
			$data['image_updated_at'] = Functions::currentTimestamp();
		}// TODO what if several files, or file is not image

        $validator = Validator::make($data, $modelClass::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $element->fill($data);
        $element->save();
        $element->touch();
        if (isset($image)) {//TODO what if not medals
            $image->move(storage_path() . '/cache/img/medals', $element->image_file_name);
        }

        return Redirect::route('admin.' . $this->instance->admin['model']['name'] . '.index');
    }

    /**
     * Remove the specified modelClass from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        $modelClass = $this->modelClass;
        $modelClass::destroy($id);

        return Redirect::route('admin.' . $this->instance->admin['model']['name'] . '.index');
    }

}
