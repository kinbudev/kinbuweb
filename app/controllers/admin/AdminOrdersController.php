<?php

/**
 * OrdersController
 */

class AdminOrdersController extends BaseController
{

    function index()
    {
        $orders = Order::all();
        return View::make('admin.orders.index')->withOrders($orders);
    }

    function show($id)
    {
        $order = Order::with('shipments', 'shipments.publication')->find($id);
        return View::make('admin.orders.show')->withOrder($order);
    }

    function shipment_statuses()
    {
        $statuses = Parameter::whereAttribute('shipment_status')->first()
            ->values()->get(['id', 'pvalue', 'description']);
        return Response::json($statuses);
    }

    public function shipment_update($order, $shipment)
    {
        $response = [ "success" => false ];
        $shipment = Shipment::findOrFail($shipment);
        $shipmentOrder = $shipment->order()->first();
        if ($shipmentOrder->id != $order) {
            throw new Exception("Error Processing Request", 1);
        }

        $shipment->identifier = Input::get('identifier');
        $shipment->shipment_status = Input::get('status');

        $response['success'] = $shipment->save();
        $shipment->touch();
        if ($response['success'] === true) {
            $shipmentOrder->updateStatus();
            $order = Order::with('shipments', 'shipments.publication')->find($shipment->id);
            if($order->status == 'Recibido'){
            DB::setFetchMode(PDO::FETCH_ASSOC);
                $order1 = DB::table('orders')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->where('orders.id', '=', $shipment->id)
                ->select('*')
                ->get();

            $a = array('order' => $order);
                    Mail::send('emails.sentshipment', $a, function($message) use ($order1)
                    {
                        $message->subject('Bienvenido a la gran experiencia.');
                        $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
                        $message->to($order1[0]["email"]);
                    });
            DB::setFetchMode(PDO::FETCH_CLASS);
            }
            $response['message'] = "Envio actualizado correctamente.";
        } else {
            $response['message'] = "No se pudo actualizar el envio";
        }

        return Response::json($response);
    }

}
