<?php

class PublicationsController extends BaseController {

	/**
	 * Display a listing of bookpublications
	 *
	 * @return Response
	 */
	public function index() {
		$bookpublications = BookPublication::all();

		/*$data = [
			'title' => BookPublication::$title,
			'model' => BookPublication::$model,
			'actions' => BookPublication::$actions,
			'elements' => $bookpublications,
			'keys' => BookPublication::$grid_attrs
		];*/
		$data = [
			'bookpublications' => $bookpublications,
		];

		return View::make('publications', $data);
	}

	/**
	 * Show the form for creating a new bookpublication
	 *
	 * @return Response
	 */
	public function create() {
		$data = [
			'location' => 'BookPublication',
			'page_header' => 'Create BookPublication',
			'model' => BookPublication::$model,
			'attrs' => BookPublication::$form_attrs
		];
		return View::make('kinbu.form', $data);
	}

	/**
	 * Store a newly created bookpublication in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$validator = Validator::make($data = Input::all(), BookPublication::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		BookPublication::create($data);

		return Redirect::route('publications.index');
	}

	/**
	 * Display the specified bookpublication.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$bookpublication = BookPublication::findOrFail($id);

		$data = [
			'location' => 'BookPublication',
			'page_header' => 'BookPublication Details',
			'model' => BookPublication::$model,
			'element' => $bookpublication,
			'keys' => BookPublication::$grid_attrs
		];

		return View::make('kinbu.item', $data);
	}

	/**
	 * Show the form for editing the specified bookpublication.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$bookpublication = BookPublication::find($id);

		$data = [
			'location' => 'BookPublication',
			'page_header' => 'Edit BookPublication',
			'model' => BookPublication::$model,
			'element' => $bookpublication,
			'attrs' => BookPublication::$form_attrs
		];

		return View::make('kinbu.form', $data);
	}

	/**
	 * Update the specified bookpublication in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$bookpublication = BookPublication::findOrFail($id);

		$validator = Validator::make($data = Input::all(), BookPublication::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$bookpublication->update($data);

		return Redirect::route('publications.index');
	}

	/**
	 * Remove the specified bookpublication from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		BookPublication::destroy($id);

		return Redirect::route('publications.index');
	}

}
