<?php

class ShoppingCartController extends BaseController
{
    use LoggedInUserTrait;

    protected $user;
    protected $cart;

    /**
     * @param User         $users
     * @param ShoppingCart $cart
     */
    public function __construct(User $user, ShoppingCart $cart)
    {
        $this->user = Auth::user();
        $this->cart = $cart;
    }

    /**
     * Add an item to the user shopping cart.
     *
     * @return Response
     */
    public function addToCart()
    {
        $response = [ 'success' => false, ];
        $this->cart = $this->getOrCreateUserActiveCart($this->user);
        $publication_id = Input::get('publication_id');
        $publication = BookPublication::find($publication_id);
        //TODO si publicacion ya esta en el carrito, no volver a agregar
        if ($this->isLoggedInUser($publication->user->username)) {
            $response['mensaje'] = "No puedes comprar una publicacion propia";
        } else {
            $this->cart->items()->attach($publication_id);
            $response['success'] = true;
            $response['mensaje'] = "Agregado existosamete";
        }
        return Response::json($response);
    }

    /**
     * Remove an item from the user shopping cart.
     *
     * @return Response
     */
    public function removeFromCart($id)
    {
        $response = [ 'success' => false, ];
        $this->cart = $this->getOrCreateUserActiveCart($this->user);
        $publication = BookPublication::findOrFail($id);
        //TODO si publicacion no esta en el carrito, advertir
        if ($this->isLoggedInUser($publication->user->username)) {
            $response['mensaje'] = "No puedes comprar una publicacion propia";
        } else {
            $this->cart->items()->detach($id);
            $response['success'] = true;
            $response['mensaje'] = "Eliminado existosamete";
        }
        return Response::json($response);
    }

    /**
     * Show the user shopping cart.
     *
     * @return View
     */
    public function cart()
    {
        if (!Auth::user()->has_location_data){
            return Redirect::to('market')->with('messages', 'Carrito vacío, agrega libros para acceder al carrito');
        }
        else{
        $this->cart = $this->getOrCreateUserActiveCart($this->user);
        $cajero = new KinbuCashier($this->cart);
        $data = $cajero->getPayUParams();
        $payuform = View::make('shoppingcart.payuform', $data);
        $inactive_carts = $this->user->inactive_carts();
        return View::make('shoppingcart.checkout')
            ->withCart($this->cart)
            ->withItems($this->cart->items)
            ->withSubtotal($data['subtotal'])
            ->withSubtotalnumber($data['subtotalnumber'])
            ->withTotalEnvio($data['totalEnvio'])
            ->withAmount($data['amount'])
            ->withPayuform($payuform)
            ->withInactiveCarts($inactive_carts);
        }
    }

    /**
     * Show the user the shopping carts in process.
     *
     * @return View
     */
    public function index()
    {
        $carts = $this->user->inactive_carts();
        return View::make('shoppingcart.index')->withCarts($carts);
    }

    /**
     * Show the user shopping cart.
     *
     * @param id $id
     *
     * @return View
     */
    public function show($id)
    {
        $this->cart = $this->user->inactive_carts()->first(function ($key, $cart) use($id)
        {
            return $cart->id == $id;
        });
        $cajero = new KinbuCashier($this->cart);
        $data = $cajero->getPayUParams();
        $payuform = View::make('shoppingcart.payuform', $data);//TODO add form conditionally
        $inactive_carts = $this->user->inactive_carts();
        return View::make('shoppingcart.show')
            ->withCart($this->cart)
            ->withItems($this->cart->items)
            ->withSubtotal($data['subtotal'])
            ->withTotalEnvio($data['totalEnvio'])
            ->withPayuform($payuform)
            ->withInactiveCarts($inactive_carts);
    }

    public function transaction()
    {
        $response = [ 'success' => false, ];
        $reference_code = Input::get('referenceCode');
        $amount = Input::get('amount');
        $cart_id = explode('.', $reference_code)[0];
        $this->cart = ShoppingCart::findOrFail($cart_id); //This to Payucontroller at rejected
        $this->cart->updatePickup(Input::only('pickup_fullname', 'pickup_address', 'pickup_phone', 'pickup_city'));
        $canBeBought = $this->cart->canBeBought();
        if ($canBeBought === true) {
            $pv = Parameter::whereAttribute('publication_type')->first()->pValue('sale');
            $trdata = ['reference_code' => $reference_code, 'value' => $amount, 'cart_id' => $cart_id, 'type' => $pv->id];
            $transaction = new Transaction($trdata);
            $model = $this->user->transactions()->save($transaction);
            $response['success'] = $model !== false;
            if ($model === false) {
                $response['mensaje'] = "Ocurrió un error con el inicio de la transaccion";
            } else {
                $pv = Parameter::whereAttribute('cart_status')->first()->pValue('inactive'); //This to Payucontroller at rejected (change to active to keep on the publication)
                $this->cart->cart_status = $pv->id;
                $this->cart->save();
                $this->cart->touch();
            }
        } else {
            $response['mensaje'] = "En el momento no puedes comprar {$canBeBought->book->title} Revisa la publicaci&oacute;n";
        }
        return Response::json($response);
    }

    private function getOrCreateUserActiveCart($user)
    {
        $active_carts = $user->active_carts();
        $cartsCount = $active_carts->count();
        if ($cartsCount == 1) {
            return $active_carts->first();
        } elseif ($cartsCount > 1) {
            throw new Exception('Error Processing Request', 1);
        } else { //Crear carrito
            $pv = Parameter::whereAttribute('cart_status')->first()->pValue('active');
            $shoppingCart = new ShoppingCart([
                'cart_status' => $pv->id,
                'pickup_fullname' => $user->name,
                'pickup_address' => $user->person->address,
                'pickup_phone' => $user->person->phone,
                'pickup_city' => $user->person->city
            ]);
            $user->shopping_carts()->save($shoppingCart);
            return $shoppingCart;
        }
    }

}
