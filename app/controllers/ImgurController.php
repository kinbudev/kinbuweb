<?php

class Imgurcontroller extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Upload images and receive url of them
	 *
	 * @return Response
	 */
	public function uploadimg()
	{
	$img=$_FILES['img'];
	if(isset($_POST['submit'])){ 
 			if($img['name']==''){  
  			echo "<h2>An Image Please.</h2>";
 		}else{
  			$filename = $img['tmp_name'];
  			$client_id="9c38cca60fcc3f9";
  			$handle = fopen($filename, "r");
  			$data = fread($handle, filesize($filename));
  			$pvars   = array('image' => base64_encode($data));
  			$timeout = 120;
  			$curl = curl_init();
  			curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
  			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
  			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
  			curl_setopt($curl, CURLOPT_POST, 1);
  			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  			curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
  			$out = curl_exec($curl);
  			curl_close ($curl);
  			$pms = json_decode($out,true);
  			$url=$pms['data']['link'];
  			$url = str_replace( 'http://', 'https://', $url );
  			if($url!=""){
   				echo "<h2>Uploaded Without Any Problem</h2>";
   				echo "<img src='$url'/>";
				DB::table('users')
            	->where('id', Auth::user()->id)
            	->update(['imgurl' => $url]);
            	$update = User::find(Auth::user()->id)->touch();
  			}else{
   			echo "<h2>There's a Problem</h2>";
   			echo $pms['data']['error'];  
  			} 
 		}
	}
        return Redirect::back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showUpload(/*$id*/)
	{
        return View::make('profiles.upload');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
