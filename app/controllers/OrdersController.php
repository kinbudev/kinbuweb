<?php

/**
 * OrdersController
 */
class OrdersController extends BaseController
{
    protected $user;

    function __construct()
    {
        $this->user = Auth::user();
    }

    function index()
    {
        $orders = $this->user->orders;
        return View::make('orders.index')->withOrders($orders);
    }

    function show($id)
    {
        $order = $this->user->orders()->with('shipments', 'shipments.publication')->findOrFail($id);
        return View::make('orders.show')->withOrder($order);
    }

}
