<?php

class ParameterValuesController extends BaseDetailAdminController {

    protected $modelClass = "ParameterValue";

    protected $parentClass = "Parameter";

    protected $relation = "values";

}
