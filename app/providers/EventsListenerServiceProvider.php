<?php

use Illuminate\Support\ServiceProvider;

class EventsListenerServiceProvider extends ServiceProvider {

    public function boot()
    {
        Event::listen('auth.login', function ($user)
        {
            Session::put('user_last_login', $user->last_login);
            $user->last_login = new DateTime;
            $user->save();
        });
    }

    public function register()
    {
    }

}
