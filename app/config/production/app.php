<?php
/**
 * Created by PhpStorm.
 * User: jedabero
 * Date: 28/01/15
 * Time: 10:59 AM
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => true,


];
