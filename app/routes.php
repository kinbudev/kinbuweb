<?php

Event::listen('illuminate.query', function () {
    Log::info('SQL', func_get_args());
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['before' => ['auth', 'is_admin'], 'prefix' => 'admin'], function () {
    Route::get('/', ['uses' => 'AdminController@index', 'as' => 'admin']);
    Route::resource('parameters', 'ParametersController');
    Route::resource('parameters.parameter_values', 'ParameterValuesController');
    Route::resource('levels', 'LevelsController');
    Route::resource('activities', 'ActivitiesController');
    Route::resource('actions', 'ActionsController');
    Route::resource('medals', 'MedalsController');
    Route::resource('medals.advance_medals', 'AdvanceMedalsController');
    Route::resource('medals.medal_boosters', 'MedalBoostersController');
    #Route::resource('advance_medals.medal_boosters', 'MedalBoostersController');
    Route::resource('roles', 'RolesController');
    Route::resource('prizes', 'PrizesController');

    Route::resource('books', 'AdminBooksController', ['except' => ['destroy']]);
    Route::resource('publications', 'PublicationsController');
    Route::resource('orders', 'AdminOrdersController');

    Route::get('shipment_statuses', ['uses' => 'AdminOrdersController@shipment_statuses', 'as' => 'orders.shipment_statuses']);
    Route::get('orders/{order}/shipment_update/{shipment}', ['uses' => 'AdminOrdersController@shipment_update', 'as' => 'orders.shipment_update']);
});

Route::group(['before' => ['auth']], function () {

    Route::get('settings', ['uses' => 'UsersController@settings', 'as' => 'user.settings']);
    Route::get('notifications', ['uses' => 'UsersController@notifications', 'as' => 'notifications']);
    Route::get('medals', ['uses' => 'UsersController@medals', 'as' => 'user.medals']);

    Route::get('users/{users}/publications/{url}/update',['uses' => 'UserPublicationsController@update', 'as' => 'users.publications.update']);
    Route::get('users/{users}/publications/sold',['uses' => 'UserPublicationsController@sold', 'as' => 'users.publications.sold']);
    Route::post('cropPhoto/{id}',['uses' => 'UserPublicationsController@cropPhoto', 'as' => 'cropPhoto']);
    Route::post('rotatePhoto/{id}',['uses' => 'UserPublicationsController@rotatePhoto', 'as' => 'rotatePhoto']);
    Route::any('removePhoto/{id}',['uses' => 'UserPublicationsController@removePhoto', 'as' => 'removePhoto']);
    Route::post('addToCart', ['uses' => 'ShoppingCartController@addToCart', 'as' => 'add_to_cart']);
    Route::post('removeFromCart/{id}', ['uses' => 'ShoppingCartController@removeFromCart', 'as' => 'remove_from_cart']);
    Route::get('cart', ['uses' => 'ShoppingCartController@cart', 'as' => 'cart']);
    Route::get('carts', ['uses' => 'ShoppingCartController@index', 'as' => 'carts']);
    Route::get('carts/{id}', ['uses' => 'ShoppingCartController@show', 'as' => 'carts.show']);
    Route::post('transaction', ['uses' => 'ShoppingCartController@transaction', 'as' => 'transaction']);
    Route::resource('messages', 'MessagesController', ['except' => ['create', 'edit', 'update', 'destroy']]);
    Route::resource('orders', 'OrdersController', ['only' => ['index', 'show']]);
    Route::get('exchanges', ['uses' => 'ExchageController@index', 'as' => 'exchanges']);
    Route::get('exchanges/{id}', ['uses' => 'ExchageController@show']);
    Route::post('exchanges/{id}/accept', ['uses' => 'ExchageController@accept']);
    Route::post('exchanges/{id}/reject', ['uses' => 'ExchageController@reject']);
    Route::get('exchanges/{id}/payment', ['uses' => 'ExchageController@payment']);
    Route::post('exchange', ['uses' => 'ExchageController@store', 'as' => 'exchange']);
    Route::get('for_exchange', ['uses' => 'ExchageController@for_exchange', 'as' => 'for_exchange']);
    Route::post('books/{id}/recommend', ['uses' => 'BooksController@recommend', 'as' => 'recommend']);
    Route::post('books/{id}/user_read_status', ['uses' => 'BooksController@user_read_status', 'as' => 'book_user_read_status']);

});

Route::resource('publications', 'PublicationsController');
Route::resource('users.publications', 'UserPublicationsController');
Route::resource('books.publications', 'BookPublicationsController');
Route::get('login', ['uses' => 'UserSessionController@create', 'as' => 'login']);
Route::post('login', 'UserSessionController@store');

Route::get('login/{provider}', ['uses' => 'ConnectController@connect', 'as' => 'social.login']);

Route::get('logout', ['uses' => 'UserSessionController@destroy', 'as' => 'logout']);
if (Auth::check()) {
    Route::get('/', ['uses' => 'HomeController@showMarket', 'as' => 'home']);
} else {
    Route::get('/', ['uses' => 'HomeController@showHome', 'as' => 'home']);
}
Route::get('home', ['uses' => 'HomeController@showHome', 'as' => 'named_home']);
Route::get('market', ['uses' => 'HomeController@showMarket', 'as' => 'market']);
Route::any('search', ['uses' => 'HomeController@search', 'as' => 'search']);

Route::get('signup', ['uses' => 'UsersController@create', 'as' => 'signup']);

//Route::get('mail', ['uses' => 'MailController@send', 'as' => 'send']);
/*Route::get('send_test_email', function(){
    Mail::send('emails.registro', array('key' => 'value'), function($message)
    {
        $message->subject('Bienvenido a la gran experiencia');
        $message->from(env('CONTACT_MAIL'), env('CONTACT_NAME'));
        $message->to('luis02lopez@hotmail.com');
    });
});
*/
//Route::get('send', ['uses' => 'MailController@send', 'as' => 'send'] );

Route::get('payu_response', ['uses' => 'PayUController@response','as' => 'payu_response']);
Route::post('payu_confirm', ['uses' => 'PayUController@confirmation','as' => 'payu_confirm']);

Route::post('users/{username}/completedTour', ['uses' => 'UsersController@completedTour', 'as' => 'users.completed_tour']);
Route::post('users/{username}/comments', ['uses' => 'UsersController@comments', 'as' => 'users.comments']);
Route::any('users/{username}/relationships', ['uses' => 'UsersController@relationships', 'as' => 'users.relationships']);
Route::post('users/{username}/friends', ['uses' => 'UsersController@friends', 'as' => 'users.friends']);
Route::post('users/{username}/requestfriend', ['uses' => 'UsersController@requestFriend', 'as' => 'users.requestfriend']);
Route::post('users/{username}/update_friend_request', ['uses' => 'UsersController@updateFriendRequest', 'as' => 'users.update_friend_request']);
Route::resource('users', 'UsersController', ['except' => ['create']]);
Route::resource('users.bags', 'BagsController');

Route::resource('BookComments', 'BookCommentsController');
Route::resource('books.ratings', 'RatingsController');

Route::resource('books', 'BooksController');
Route::resource('authors', 'AuthorsController');

Route::resource('allcategories', 'MainCategoriesController');
Route::resource('categories', 'CategoriesController');

Route::get('img/medals/{file_name}', function ($file_name)
{
    $file = storage_path() . "/cache/img/medals/$file_name";
    return Response::download($file);
});

Route::get('img/publication/{id}/{file_name}', function ($id, $file_name)
{
    $file = storage_path() . "/cache/img/publication/$id/$file_name";
    return Response::download($file);
});
Route::get('site/terms', ['uses' => 'HomeController@showTerms', 'as' => 'terms']);
Route::get('site/privacy', ['uses' => 'HomeController@showPrivacy', 'as' => 'privacy']);
Route::get('howpublish', ['uses' => 'HomeController@showHowpublish', 'as' => 'howpublish']);
Route::get('registro', ['uses' => 'HomeController@ShowRegistro', 'as' => 'registro']);
Route::controller('password', 'RemindersController');
Route::get('users/{username}/upload', ['uses' => 'ImgurController@showUpload', 'as' => 'upload']);
Route::post('/uploadedimg', ['uses' => 'ImgurController@uploadimg', 'as' => 'uploadimg']);
Route::get('support', ['uses' => 'MailController@showSupport', 'as' => 'support']);
Route::post('supportmail', ['uses' => 'MailController@supportmail', 'as' => 'supportmail']);
Route::get('bug', ['uses' => 'MailController@showBug', 'as' => 'bug']);
Route::post('bugmail', ['uses' => 'MailController@bugmail', 'as' => 'bugmail']);
Route::get('publications/create',['uses' => 'UserPublicationsController@create2', 'as' => 'users.publications.create2']);
Route::post('users2',['uses' => 'UsersController@store2', 'as' => 'users.store2']);