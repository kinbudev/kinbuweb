<?php

class KinbuCashier
{
    /**
     * The cart used to get information about the transaction.
     *
     * @var \Models\ShoppingCart
     */
    protected $cart;

    /**
     * Shipping value.
     *
     * @var float
     */
    protected $shipping;

    /**
     * Micropayments limit.
     *
     * @var float
     */
    protected $micropayLimit;

    /**
     * plaza types.
     *
     * @var array
     */
    protected $plazaTypes;

    /**
     * TODO: store this in DB as a parameter
     * plaza percentages.
     *
     * @var array
     */
    protected static $plazaPercents = [
        'normal' => 0.10,
        'good' => 0.15,
        'best' => 0.20,
    ];

    /**
     * Create cashier.
     */
    public function __construct(ShoppingCart $shoppingCart = null, $exchange = null)
    {
        $this->cart = $shoppingCart;
        //TODO: Obtener parametros
        $this->shipping = 7500;
        $this->micropayLimit = 30000;
        $this->plazaTypes = Parameter::whereAttribute('plaza_type')->first()->values()->get()->lists('pvalue', 'id');

        $this->exchange = $exchange;
    }

    public function getPayUParams()
    {
        $addresses = [];
        $subtotal = 0;
        $comisionKinbu = 0;
        $pids = [];
        $books = [];
        foreach ($this->cart->items()->get() as $key => $item) {
            $subtotal += $item->price;
            $finAddress = $item->pickup_city.' - '.$item->pickup_address;
            if (!in_array($finAddress, $addresses)) {
                $addresses[] = $addresses;
            }
            $comisionKinbu += $this->calculateCommission($item->price, $item->plaza_type);
            $pids[] = $item->id;
            $books[] = $item->published_book_title;
        }
        $totalEnvio = $this->shipping * count($addresses);
        $amount = $subtotal + $totalEnvio;

        $accountId = $this->getProperAccountId($amount);
        $books = implode(', ', $books);
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $checkrfcode = DB::table('transactions')
                ->where('cart_id', $this->cart->id)
                ->orderBy('id', 'desc')
                ->select('reference_code')->first();
        DB::setFetchMode(PDO::FETCH_CLASS);
        if (empty($checkrfcode)) {
            $referenceCode = $this->createReferenceCode($this->cart->id, $this->cart->user_id, $pids);
        }
        else{
            $referenceCode = $checkrfcode["reference_code"];
        }
        $signature = $this->createSignature($referenceCode, $amount);

        return [
            'accountId' => $accountId,
            'description' => "Compra de libros: $books",
            'referenceCode' => $referenceCode,
            'amount' => number_format($amount, 2, '.', ''),
            'subtotal' => number_format($subtotal, 0, ',', '.'),
            'subtotalnumber' => $subtotal,
            'totalEnvio' => number_format($totalEnvio, 0, ',', '.'),
            'tax' => number_format($comisionKinbu * 0, 2, '.', ''),
            'taxReturnBase' => number_format(0, 2, '.', ''),
            'signature' => $signature,
        ];
    }

    public function getPayUParamsForExchange()
    {
        if ($this->exchange == null) {
            throw new Exception("Error Processing Data", 2);
        }
        $amount = $this->shipping;

        $accountId = $this->getProperAccountId($amount);
        $referenceCode = $this->createReferenceCodeForExchange($this->exchange->id, $this->exchange->user_id);
        $signature = $this->createSignature($referenceCode, $amount);

        return [
            'accountId' => $accountId,
            'description' => "Intercambio de libros",
            'referenceCode' => $referenceCode,
            'amount' => number_format($amount, 2, '.', ''),
            'tax' => number_format(0 * 0, 2, '.', ''),
            'taxReturnBase' => number_format(0, 2, '.', ''),
            'signature' => $signature,
            'btntext' => "Pagar"
        ];
    }

    private function calculateCommission($price, $plaza_type)
    {
        $plazaPercent = self::$plazaPercents[$this->plazaTypes[$plaza_type]];
        return $price * $plazaPercent;
    }

    private function getProperAccountId($amount)
    {
        if ($amount <= $this->micropayLimit) {
            return Config::get('payu.accountIdMicro');
        } else { # > $this->micropayLimit
            return Config::get('payu.accountIdMacro');
        }
    }

    private function createReferenceCode($cart_id, $user_id, $pids)
    {
        return $cart_id.'.'.$user_id.'('.implode(',', $pids).')';
    }

    private function createReferenceCodeForExchange($exchange_id, $user_id)
    {
        return "$exchange_id.$user_id";
    }

    private function createSignature($referenceCode, $amount)
    {
        $ApiKey = Config::get('payu.ApiKey');
        $merchantId = Config::get('payu.merchantId');
        $currency = Config::get('payu.currency');
        $amountFormated = number_format($amount,1,'.','');
        $cadena = $ApiKey.'~'.$merchantId.'~'.$referenceCode.'~'.$amountFormated.'~'.$currency;
        return md5($cadena);
    }

}
