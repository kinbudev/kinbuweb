<?php

/**
 * Gets the value of an environment variable. Supports boolean, empty and null.
 *
 * @param string $key
 * @param mixed  $fallback
 *
 * @return mixed
 */
function env($key, $fallback = null)
{
    $value = getenv($key);
    if ($value === false) {
        return value($fallback);
    }
    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;
        case 'false':
        case '(false)':
            return false;
        case 'empty':
        case '(empty)':
            return '';
        case 'null':
        case '(null)':
            return;
    }
    if (strlen($value) > 1 && Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
        return substr($value, 1, -1);
    }

    return $value;
}

use Facebook\Facebook;

class Helpers
{
    /**
     * Returns an array with the installed providers for social login.
     *
     * @return array
     */
    public static function getSocialAuthUrls()
    {
        //TODO: How could this be done via a Service Provider

        $links['facebook'] = static::setUpFacebookAuthUrl();

        $links['google'] = static::setUpGoogleAuthUrl();

        return $links;
    }

    /**
     * @param $config
     *
     * @return Google_Client
     */
    public static function getGoogleClient($config)
    {
        $google = new Google_Client();
        $google->setClientId($config['id']);
        $google->setClientSecret($config['secret']);
        $google->setRedirectUri(URL::to('/').'/'.$config['redirect']);
        $google->setScopes('profile  email');

        return $google;
    }

    private static function setUpGoogleAuthUrl()
    {
        session_start();
        $config = Config::get('auth.providers.google');
        $google = static::getGoogleClient($config);
        $link = static::getGoogleLink($google);
        session_destroy();

        return $link;
    }

    private static function getGoogleLink(Google_Client $google, $scopes = [])
    {
        $state = md5(rand());
        Session::set('state', $state);
        $google->setState($state);
        $loginUrl = $google->createAuthUrl();
        //$link = HTML::link($loginUrl, 'Log in with Google', ['class' => 'btn btn-info']);
        return $loginUrl;
    }

    private static function setUpFacebookAuthUrl()
    {
        $config = Config::get('auth.providers.facebook');
        session_start(); //I fucking hate facebook
        $facebook = new Facebook($config);

        $link = static::getFacebookLink($facebook, $config['permissions']);
        session_destroy();

        return $link;
    }

    private static function getFacebookLink(Facebook $facebook, $permissions)
    {
        $helper = $facebook->getRedirectLoginHelper();
        $loginUrl = $helper->getLoginUrl(
            route('social.login', ['provider' => 'facebook']),
            $permissions);

        //$link = HTML::link($loginUrl, 'Log in with Facebook', ['class' => 'btn btn-info']);

        return $loginUrl;
    }
}
