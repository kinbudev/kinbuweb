<?php

/**
 * Created by PhpStorm.
 * User: jedabero
 * Date: 22/11/14
 * Time: 05:14 PM.
 */
class Functions
{
    public static function splitName($name)
    {
        $namearr = explode(' ', $name);

        return [
            'first' => array_shift($namearr),
            'second' => implode(' ', $namearr),
        ];
    }

    /**
     * @param $name
     * @param bool $isUsername
     *
     * @return string
     *
     * @throws Exception
     */
    public static function makeUsernameFromFullname($name, $isUsername = false)
    {
        if (empty($name)) {
            throw new Exception('Empty name');
        }
        $username = $name;
        if (!$isUsername) {
            $namearr = explode(' ', $name);

            foreach ($namearr as $key => $segment) {
                $namearr[$key] = substr_replace(strtolower($segment), '', 2);
            }

            $username = implode('', $namearr);
        }

        $num = 0;
        while (User::whereUsername($username.$num)->first()) {
            $num = $num + 1;
        }

        $username .= $num ? $num : '';

        return $username;
    }

    public static function fixBirthday($user)
    {
        $day = $user->day;
        unset($user->day);
        $month = $user->month;
        unset($user->month);
        $year = $user->year;
        unset($user->year);
        $user->birthday = $year.'-'.$month.'-'.$day;
    }

    public static function getFirstYMDHIS($time)
    {
        if (is_string($time)) {
            $time = date_create($time);
        }

        if ($time->y) {
            return $time->y.' a&ntilde;os';//' years';
        } elseif ($time->m) {
            return $time->m.' meses';//' months';
        } elseif ($time->d) {
            return $time->d.' d&iacute;as';//' days';
        } elseif ($time->h) {
            return $time->h.' horas';//' hours';
        } elseif ($time->i) {
            return $time->i.' minutos';//' minutes';
        } elseif ($time->s) {
            return $time->s.' segundos';//' seconds';
        }
    }

    public static function getDateAsString($date)
    {
        $time = date_diff(date_create('now'), $date);
        if (is_string($time)) {
            $time = date_create($time);
        }

        if ($time->y) {
            return $date;
        } elseif ($time->m || $time->d) {
            return $date->format("j M H:i");
        } elseif ($time->h) {
            return $date->format("H:i");
        } elseif ($time->i) {
            return $time->i.' minutos';//' minutes';
        } elseif ($time->s) {
            return $time->s.' segundos';//' seconds';
        }
    }

    public static function truncateTable($table)
    {
        if (Config::get('database.default') == 'mysql') {
            $table = str_replace(' CASCADE', '', $table);
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            DB::table($table)->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        } elseif (Config::get('database.default') == 'pgsql') {
            DB::statement("TRUNCATE TABLE $table");
        }
    }

    public static function currentTimestamp()
    {
        if (Config::get('database.default') == 'mysql') {
            return DB::raw('CURRENT_TIMESTAMP');
        } elseif (Config::get('database.default') == 'pgsql') {
            return DB::raw('now()::timestamp(0)');
        }
    }
}
