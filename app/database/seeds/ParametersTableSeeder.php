<?php


class ParametersTableSeeder extends Seeder
{
    public function run()
    {
        if (Parameter::count() === 0) {
            $param = Parameter::create([
                'attribute' => 'status',
                'description' => 'Satus of an entity',
            ]);
            $param->addValues(['inactive', 'active']);

            $param = Parameter::create([
                'attribute' => 'request_status',
                'description' => 'Satus of friendship request',
            ]);

            $param->addValues(['acknowledged', 'ignored', 'unattended']);

            $param = Parameter::create([
                'attribute' => 'event_type',
                'description' => 'Type of an event',
            ]);

            $param->addValues([['sale', 0, 'Venta'], ['exchange', null, 'Intercambio']]);

            $param = Parameter::create([
                'attribute' => 'cover_type',
                'description' => 'Type of cover',
            ]);

            $param->addValues([
                ['hardcover', 0, 'Dura'],
                ['paperbacks', 1, 'Blanda'],
            ]);

            $param = Parameter::create([
                'attribute' => 'language',
                'description' => 'Language codes',
            ]);

            $param->addValues([
                ['aa', null, 'Afar'],
                ['ab', null, 'Abkhaz'],
                ['ae', null, 'Avestan'],
                ['af', null, 'Afrikaans'],
                ['ak', null, 'Akan'],
                ['am', null, 'Amharic'],
                ['an', null, 'Aragonese'],
                ['ar', null, 'Arabic'],
                ['as', null, 'Assamese'],
                ['av', null, 'Avaric'],
                ['ay', null, 'Aymara'],
                ['az', null, 'Azerbaijani'],
                ['ba', null, 'Bashkir'],
                ['be', null, 'Belarusian'],
                ['bg', null, 'Bulgarian'],
                ['bh', null, 'Bihari'],
                ['bi', null, 'Bislama'],
                ['bm', null, 'Bambara'],
                ['bn', null, 'Bengali'],
                ['bo', null, 'Tibetan Standard, Tibetan, Central'],
                ['br', null, 'Breton'],
                ['bs', null, 'Bosnian'],
                ['ca', null, 'Catalan; Valencian'],
                ['ce', null, 'Chechen'],
                ['ch', null, 'Chamorro'],
                ['co', null, 'Corsican'],
                ['cr', null, 'Cree'],
                ['cs', null, 'Czech'],
                ['cu', null, 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic'],
                ['cv', null, 'Chuvash'],
                ['cy', null, 'Welsh'],
                ['da', null, 'Danish'],
                ['de', null, 'German'],
                ['dv', null, 'Divehi; Dhivehi; Maldivian;'],
                ['dz', null, 'Dzongkha'],
                ['ee', null, 'Ewe'],
                ['el', null, 'Greek, Modern'],
                ['en', null, 'English'],
                ['eo', null, 'Esperanto'],
                ['es', null, 'Spanish; Castilian'],
                ['et', null, 'Estonian'],
                ['eu', null, 'Basque'],
                ['fa', null, 'Persian'],
                ['ff', null, 'Fula; Fulah; Pulaar; Pular'],
                ['fi', null, 'Finnish'],
                ['fj', null, 'Fijian'],
                ['fo', null, 'Faroese'],
                ['fr', null, 'French'],
                ['fy', null, 'Western Frisian'],
                ['ga', null, 'Irish'],
                ['gd', null, 'Scottish Gaelic; Gaelic'],
                ['gl', null, 'Galician'],
                ['gn', null, 'Guarani'],
                ['gu', null, 'Gujarati'],
                ['gv', null, 'Manx'],
                ['ha', null, 'Hausa'],
                ['he', null, 'Hebrew (modern)]'],
                ['hi', null, 'Hindi'],
                ['ho', null, 'Hiri Motu'],
                ['hr', null, 'Croatian'],
                ['ht', null, 'Haitian; Haitian Creole'],
                ['hu', null, 'Hungarian'],
                ['hy', null, 'Armenian'],
                ['hz', null, 'Herero'],
                ['ia', null, 'Interlingua'],
                ['id', null, 'Indonesian'],
                ['ie', null, 'Interlingue'],
                ['ig', null, 'Igbo'],
                ['ii', null, 'Nuosu'],
                ['ik', null, 'Inupiaq'],
                ['io', null, 'Ido'],
                ['is', null, 'Icelandic'],
                ['it', null, 'Italian'],
                ['iu', null, 'Inuktitut'],
                ['ja', null, 'Japanese'],
                ['jv', null, 'Javanese'],
                ['ka', null, 'Georgian'],
                ['kg', null, 'Kongo'],
                ['ki', null, 'Kikuyu, Gikuyu'],
                ['kj', null, 'Kwanyama, Kuanyama'],
                ['kk', null, 'Kazakh'],
                ['kl', null, 'Kalaallisut, Greenlandic'],
                ['km', null, 'Khmer'],
                ['kn', null, 'Kannada'],
                ['ko', null, 'Korean'],
                ['kr', null, 'Kanuri'],
                ['ks', null, 'Kashmiri'],
                ['ku', null, 'Kurdish'],
                ['kv', null, 'Komi'],
                ['kw', null, 'Cornish'],
                ['ky', null, 'Kirghiz, Kyrgyz'],
                ['la', null, 'Latin'],
                ['lb', null, 'Luxembourgish, Letzeburgesch'],
                ['lg', null, 'Luganda'],
                ['li', null, 'Limburgish, Limburgan, Limburger'],
                ['ln', null, 'Lingala'],
                ['lo', null, 'Lao'],
                ['lt', null, 'Lithuanian'],
                ['lu', null, 'Luba-Katanga'],
                ['lv', null, 'Latvian'],
                ['mg', null, 'Malagasy'],
                ['mh', null, 'Marshallese'],
                ['mi', null, 'Maori'],
                ['mk', null, 'Macedonian'],
                ['ml', null, 'Malayalam'],
                ['mn', null, 'Mongolian'],
                ['mr', null, 'Marathi'],
                ['ms', null, 'Malay'],
                ['mt', null, 'Maltese'],
                ['my', null, 'Burmese'],
                ['na', null, 'Nauru'],
                ['nb', null, 'Norwegian Bokm&aring;l'],
                ['nd', null, 'North Ndebele'],
                ['ne', null, 'Nepali'],
                ['ng', null, 'Ndonga'],
                ['nl', null, 'Dutch'],
                ['nn', null, 'Norwegian Nynorsk'],
                ['no', null, 'Norwegian'],
                ['nr', null, 'South Ndebele'],
                ['nv', null, 'Navajo, Navaho'],
                ['ny', null, 'Chichewa; Chewa; Nyanja'],
                ['oc', null, 'Occitan'],
                ['oj', null, 'Ojibwe, Ojibwa'],
                ['om', null, 'Oromo'],
                ['or', null, 'Oriya'],
                ['os', null, 'Ossetian, Ossetic'],
                ['pa', null, 'Panjabi, Punjabi'],
                ['pi', null, 'Pali'],
                ['pl', null, 'Polish'],
                ['ps', null, 'Pashto, Pushto'],
                ['pt', null, 'Portuguese'],
                ['qu', null, 'Quechua'],
                ['rm', null, 'Romansh'],
                ['rn', null, 'Kirundi'],
                ['ro', null, 'Romanian, Moldavian, Moldovan'],
                ['ru', null, 'Russian'],
                ['rw', null, 'Kinyarwanda'],
                ['sa', null, 'Sanskrit'],
                ['sc', null, 'Sardinian'],
                ['sd', null, 'Sindhi'],
                ['se', null, 'Northern Sami'],
                ['sg', null, 'Sango'],
                ['si', null, 'Sinhala, Sinhalese'],
                ['sk', null, 'Slovak'],
                ['sl', null, 'Slovene'],
                ['sm', null, 'Samoan'],
                ['sn', null, 'Shona'],
                ['so', null, 'Somali'],
                ['sq', null, 'Albanian'],
                ['sr', null, 'Serbian'],
                ['ss', null, 'Swati'],
                ['st', null, 'Southern Sotho'],
                ['su', null, 'Sundanese'],
                ['sv', null, 'Swedish'],
                ['sw', null, 'Swahili'],
                ['ta', null, 'Tamil'],
                ['te', null, 'Telugu'],
                ['tg', null, 'Tajik'],
                ['th', null, 'Thai'],
                ['ti', null, 'Tigrinya'],
                ['tk', null, 'Turkmen'],
                ['tl', null, 'Tagalog'],
                ['tn', null, 'Tswana'],
                ['to', null, 'Tonga (Tonga Islands)'],
                ['tr', null, 'Turkish'],
                ['ts', null, 'Tsonga'],
                ['tt', null, 'Tatar'],
                ['tw', null, 'Twi'],
                ['ty', null, 'Tahitian'],
                ['ug', null, 'Uighur, Uyghur'],
                ['uk', null, 'Ukrainian'],
                ['ur', null, 'Urdu'],
                ['uz', null, 'Uzbek'],
                ['ve', null, 'Venda'],
                ['vi', null, 'Vietnamese'],
                ['vo', null, 'Volap&uuml;k'],
                ['wa', null, 'Walloon'],
                ['wo', null, 'Wolof'],
                ['xh', null, 'Xhosa'],
                ['yi', null, 'Yiddish'],
                ['yo', null, 'Yoruba'],
                ['za', null, 'Zhuang, Chuang'],
                ['zh', null, 'Chinese'],
                ['zu', null, 'Zulu'],
            ]);

            $param = Parameter::create([
                'attribute' => 'recomendation_status',
                'description' => 'Status of a Recomendation',
            ]);

            $param->addValues(['sent', 'taken', 'ignored']);

            $param = Parameter::create([
                'attribute' => 'publication_type',
                'description' => 'Type of a publication',
            ]);

            $param->addValues([
                ['sale', 0, 'Venta'],
                ['exchange', 1, 'Intercambio'],
                ['both', 2, 'Ambos'],
            ]);

            $param = Parameter::create([
                'attribute' => 'book_status',
                'description' => 'Status of a book',
            ]);

            $param->addValues([
                ['good', 0, 'Nuevo'],
                ['used', 1, 'Usado'],
            ]);

            $param = Parameter::create([
                'attribute' => 'publication_status',
                'description' => 'Status of on sale book',
            ]);

            $param->addValues([
                ['ready', 0, 'Listo'],
                ['removed', 1, 'Removido'],
                ['in review', 2, 'En revisión'],
                ['sold', 3, 'Vendido'],
            ]);

            $param = Parameter::create([
                'attribute' => 'plaza_type',
                'description' => 'Plaza',
            ]);

            $param->addValues([
                ['normal', 0, 'Kinbu Basic;(90% ganancia para vendedor y 10% para Kinbu) Visibilidad normal'],
                ['good', 1, 'Kinbu Standard;(85% ganancia para vendedor y 15% para Kinbu) Entre lista de destacados'],
                ['best', 2, 'Kinbu Elite;(80% ganancia para vendedor y 20% para Kinbu) En página principal y primero entre los destacados'],
            ]);

            $param = Parameter::create([
                'attribute' => 'characteristic_type',
                'description' => 'Type of caracteristics of a book',
            ]);

            $param->addValues([
                ['all pages', 0, 'P&aacute;ginas completas'],
                ['no scratches', 1, 'Sin rayones'],
                ['no stains', 2, 'Sin manchas'],
                ['no dog ears', 3, 'Sin doblez en p&aacute;ginas'],
            ]);

            $param = Parameter::create([
                'attribute' => 'exchange_status',
                'description' => 'Status of on exchange book',
            ]);

            $param->addValues([
                ['ready', 0, 'Listo'],
                ['exchanged', 1, 'Intercambiado'],
                ['rejected', 2, 'Rechazado'],
                ['accepted', 3, 'Aceptado'],
            ]);

            $param = Parameter::create([
                'attribute' => 'book_read_status',
                'description' => 'Reading status of a book by a user',
            ]);

            $param->addValues([
                ['read', 0, 'Le&iacute;do'],
                ['reading', 1, 'Leyendo'],
                ['will read', 2, 'Leer&eacute;'],
            ]);

            $param = Parameter::create([
                'attribute' => 'cart_status',
                'description' => 'Estado del carrito de compras',
            ]);

            $param->addValues([
                ['deleted', 0, 'Eliminado'],
                ['inactive', 1, 'Inactivo'],
                ['active', 2, 'Activo'],
                ['bought', 3, 'Comprado'],
            ]);

            $param = Parameter::create([
                'attribute' => 'order_status',
                'description' => 'Estado del pedido',
            ]);

            $param->addValues([
                ['valid', 0, 'Valido'],
                ['partial', 1, 'Parcial'],
                ['sent', 2, 'Enviado'],
                ['received', 3, 'Recibido'],
            ]);

            $param = Parameter::create([
                'attribute' => 'shipment_status',
                'description' => 'Estado del envio',
            ]);

            $param->addValues([
                ['pickup', 0, 'Preparando'],
                ['to_send', 1, 'Recogido'],
                ['sent', 2, 'Enviado'],
                ['received', 3, 'Entregado'],
            ]);
        } else {
            echo 'Parameters already created.';
        }
    }
}
