<?php

class KFaker extends \Faker\Provider\Base
{

    /**
     * @var boolean
     */
    private $useJSONFile;

    /**
     * @var array
     */
    private $ISBNs;

    public function __construct(Faker\Generator $generator)
    {
        parent::__construct($generator);

        $isbnFilePath = storage_path() . "/ISBN.json";
        $this->useJSONFile = false;
        if (File::exists($isbnFilePath)) {
            $this->useJSONFile = true;
            $file = File::get($isbnFilePath);
            $this->ISBNs = json_decode($file);
        }
    }

    public function ISBN()
    {   
        if ($this->useJSONFile) {
            return $this->generator->randomElement($this->ISBNs);
        }
        return $this->generator->isbn10();
    }
}
