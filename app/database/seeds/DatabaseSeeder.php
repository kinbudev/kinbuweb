<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('ParametersTableSeeder');

        $this->call('LevelsTableSeeder');
        $this->call('ActivitiesTableSeeder');
        $this->call('ActionsTableSeeder');
        $this->call('MedalsTableSeeder');
        $this->call('AdvanceMedalsTableSeeder');
        $this->call('PrizesTableSeeder');
        $this->call('MedalBoostersTableSeeder');

        $this->call('RolesTableSeeder');

        if (Person::count() == 0) {
            $user = User::create([
                'username'  => 'kinbu',
                'email'     => 'kinbu@kinbu.co',
                'password'  => Hash::make('WhenPigsFly')
            ]);
            $user->roles()->attach([1,2,3]);
            Person::create([
                'firstname' => 'Kinbu',
                'lastname'  => 'Admin',
                'about_me'  => 'We Are Kinbu',
                'address'   => 'fake addres',
                'phone'     => 'fake phone',
                'city'      => 'fake city',
                'user_id'   => 1
            ]);
            echo "Admin user created\n";
        }

        // Populating people users messages, their relations
        $this->call('UsersTableSeeder');
        $this->call('PeopleTableSeeder');
        $this->call('UserRelationshipTableSeeder');
        $this->call('MessagesTableSeeder');

        // Populating books authors categories and their relations
        // $this->call('BooksTableSeeder');
        /*$this->call('AuthorsTableSeeder');
        $this->call('WrittenByTableSeeder');
        $this->call('MainCategoriesTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('BookCategoryTableSeeder');
        $this->call('AuthorCategoryTableSeeder');*/

        // Populating book-user relations
        $this->call('BookCommentsTableSeeder');

    }

}
