<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run() {

		if (Role::count() == 0) {
			Role::create([
				'role' => 'admin',
				'description' => 'Administrador'
			]);
			Role::create([
				'role' => 'mod',
				'description' => 'Moderador'
			]);
			Role::create([
				'role' => 'user',
				'description' => 'Usuario'
			]);
		} else {
            echo "Roles already created.";
        }

	}

}
