<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

    public function run() {

        if (User::count() <= 1) {
            $faker = Faker::create();
            $faker->seed('54628');
            $user_role_id = Role::whereRole('user')->get(['id'])[0]->id;

            foreach (range(1, 100) as $index) {
                $user = User::create([
                    'username'    => $faker->unique()->userName,
                    'email'       => $faker->unique()->email,
                    'password'    => Hash::make(12345678)
                ]);
                $user->roles()->attach($user_role_id);
            }
        } else {
            echo "Users already created.";
        }

    }

}
