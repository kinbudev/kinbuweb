<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserRelationshipTableSeeder extends Seeder {

    public function run() {

        if (DB::table('user_relationships')->count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 100) as $index) {

                $users_ids = [];

                $rss = Parameter::whereAttribute('request_status')->first()->valuesIdArray();

                $user = User::find($index);

                foreach (range(1, $faker->numberBetween(1, 15)) as $ind) {

                    $ca = $user->created_at;
                    $enddate = (new DateTime($ca))->add(new DateInterval("P{$faker->numberBetween(1, 100)}D"))->format('Y-m-d');

                    $rdate = $faker->dateTimeBetween($ca, $enddate);
                    $adate = $faker->dateTimeBetween($rdate, $enddate);

                    $friend_id = $faker->unique()->numberBetween(1, 100);
                    $user_friends = $user->friends()->get();
                    $already_friends = (! is_array($user_friends)) ? $user_friends->contains($friend_id) : !empty($user_friends);
                    if (!$already_friends) {
                        $users_ids[$friend_id] = [
                            'request_status'  => $faker->randomElement($rss),
                            'requested_at'    => $rdate,
                            'acknowledged_at' => $adate
                        ];
                    }

                }

                $faker->unique($reset = true);

                if(!empty($users_ids)) {
                    $user->requestedFriends()->attach($users_ids);
                }

            }
        } else {
            echo "User relationships already created.";
        }

    }

}
