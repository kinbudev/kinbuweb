<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PeopleTableSeeder extends Seeder {

    public function run() {

        if (Person::count() <= 1) {

            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 100) as $index) {

                $user_id = $faker->unique()->numberBetween(2, 101);

                Person::create([
                    'firstname' => $faker->firstName,
                    'lastname'  => $faker->lastName,
                    'about_me'  => $faker->text(),
                    'address'   => $faker->address,
                    'phone'     => $faker->phoneNumber,
                    'city'      => $faker->city,
                    'user_id'   => $user_id
                ]);

            }
        } else {
            echo "People already created.";
        }

    }

}
