<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MessagesTableSeeder extends Seeder {

    public function run() {

        if (Message::count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 1024) as $index) {

                $from = $faker->numberBetween(1, 100);

                do {
                    $to = $faker->numberBetween(1, 100);
                } while ($from == $to);

                $timestamp = $faker->dateTimeBetween('2015-1-1', '2015-12-31');

                Message::create([
                    'from_id'    => $from,
                    'to_id'      => $to,
                    'content'    => $faker->sentence(4),
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ]);
            }
        } else {
            echo "Messages already created";
        }
    }

}
