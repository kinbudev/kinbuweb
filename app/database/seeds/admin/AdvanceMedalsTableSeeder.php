<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AdvanceMedalsTableSeeder extends Seeder {

	public function run() {

		if (AdvanceMedal::count() == 0) {
			$medals_with_sub = Medal::where('id', '>', 1)->get();
			foreach ($medals_with_sub as $key => $medal) {
				$int = AdvanceMedal::create([
					'name' => $medal->name . " Intermedio", 'description' => "Intermedio",
					'medal_id' => $medal->id,
					'image_file_name' => "stub",
					'image_content_type' => "stub",
					'image_file_size' => 0
				]);
				$medal->advance_medals()->save($int);
				$adv = AdvanceMedal::create([
					'name' => $medal->name . " Avanzado", 'description' => "Avanzado",
					'medal_id' => $medal->id,
					'image_file_name' => "stub",
					'image_content_type' => "stub",
					'image_file_size' => 0
				]);
				$medal->advance_medals()->save($adv);
			}
		} else {
			echo "AdvanceMedals already created";
		}
	}

}
