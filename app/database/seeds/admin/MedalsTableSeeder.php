<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MedalsTableSeeder extends Seeder {

	public function run() {

		if (Medal::count() == 0) {
			Medal::create(['name' => "Explorador", 'description' => "Explorador",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Retador", 'description' => "Retador",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Conector", 'description' => "Conector",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Promotor", 'description' => "Promotor",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Caza Tesosos", 'description' => "Caza Tesoros",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Consejero", 'description' => "Consejero",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Mercante", 'description' => "Mercante",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Editor", 'description' => "Editor",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
			Medal::create(['name' => "Rey", 'description' => "Rey",
				'image_file_name' => "stub",
				'image_content_type' => "stub",
				'image_file_size' => 0
			]);
		} else {
			echo "Medals already created";
		}
	}

}
