<?php

class LevelsTableSeeder extends Seeder {

	public function run() {

		if (Level::count() == 0) {
			Level::create(['level' => 1, 'points' => 1, 'acumulate' => 0]);
			Level::create(['level' => 2, 'points' => 2, 'acumulate' => 1]);
			Level::create(['level' => 3, 'points' => 4, 'acumulate' => 3]);
			Level::create(['level' => 4, 'points' => 6, 'acumulate' => 7]);
			Level::create(['level' => 5, 'points' => 8, 'acumulate' => 13]);
			Level::create(['level' => 6, 'points' => 10, 'acumulate' => 21]);
			Level::create(['level' => 7, 'points' => 14, 'acumulate' => 31]);
			Level::create(['level' => 8, 'points' => 18, 'acumulate' => 45]);
			Level::create(['level' => 9, 'points' => 22, 'acumulate' => 63]);
			Level::create(['level' => 10, 'points' => 26, 'acumulate' => 85]);
			Level::create(['level' => 11, 'points' => 32, 'acumulate' => 111]);
			Level::create(['level' => 12, 'points' => 38, 'acumulate' => 143]);
			Level::create(['level' => 13, 'points' => 46, 'acumulate' => 181]);
			Level::create(['level' => 14, 'points' => 54, 'acumulate' => 227]);
			Level::create(['level' => 15, 'points' => 66, 'acumulate' => 281]);
			Level::create(['level' => 16, 'points' => 78, 'acumulate' => 347]);
			Level::create(['level' => 17, 'points' => 96, 'acumulate' => 425]);
			Level::create(['level' => 18, 'points' => 114, 'acumulate' => 521]);
			Level::create(['level' => 19, 'points' => 635, 'acumulate' => 635]);
			Level::create(['level' => 20, 'points' => 0, 'acumulate' => 765]);
		} else {
			echo 'Levels already created';
		}

	}

}
