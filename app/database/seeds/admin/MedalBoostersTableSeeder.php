<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MedalBoostersTableSeeder extends Seeder {

	public function run() {

		if (MedalBooster::count() == 0) {
			$medal_retador = Medal::find(2);
			$int = MedalBooster::create([
				'name' => "Nivel 1.2x",
				'description' => "Acelera en 1.2x subir niveles",
				'percentage' => 20,
				'medal_id' => $medal_retador->id, 'medal_type' => 'Medal'
			]);
			$medal_retador->medal_boosters()->save($int);
			$advance_medal_1 = $medal_retador->advance_medals()->get()[0];
			$int = MedalBooster::create([
				'name' => "Nivel 1.3x",
				'description' => "Acelera en 1.3x subir niveles",
				'percentage' => 30,
				'medal_id' => $advance_medal_1->id, 'medal_type' => 'AdvanceMedal'
			]);
			$advance_medal_1->medal_boosters()->save($int);
			$advance_medal_2 = $medal_retador->advance_medals()->get()[1];
			$int = MedalBooster::create([
				'name' => "Nivel 1.5x",
				'description' => "Acelera en 1.5x subir niveles",
				'percentage' => 50,
				'medal_id' => $advance_medal_2->id, 'medal_type' => 'AdvanceMedal'
			]);
			$advance_medal_2->medal_boosters()->save($int);

			$premio_desc = Prize::find(1);
			$medal_editor = Medal::find(8);
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"Descuento en compra\" 2 % + rápido",
				'percentage' => 2,
				'medal_id' => $medal_editor->id, 'medal_type' => 'Medal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$medal_editor->medal_boosters()->save($int);
			$advance_medal_1 = $medal_editor->advance_medals()->get()[0];
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"Descuento en compra\" 3 % + rápido",
				'percentage' => 3,
				'medal_id' => $advance_medal_1->id, 'medal_type' => 'AdvanceMedal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$advance_medal_1->medal_boosters()->save($int);
			$advance_medal_2 = $medal_editor->advance_medals()->get()[1];
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"Descuento en compra\" 5 % + rápido",
				'percentage' => 5,
				'medal_id' => $advance_medal_2->id, 'medal_type' => 'AdvanceMedal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$advance_medal_2->medal_boosters()->save($int);

			$premio_desc = Prize::find(2);
			$medal_consejero = Medal::find(6);
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"Libro gratis\" 2 % + rápido",
				'percentage' => 2,
				'medal_id' => $medal_consejero->id, 'medal_type' => 'Medal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$medal_consejero->medal_boosters()->save($int);
			$advance_medal_1 = $medal_consejero->advance_medals()->get()[0];
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"libro gratis\" 3 % + rápido",
				'percentage' => 3,
				'medal_id' => $advance_medal_1->id, 'medal_type' => 'AdvanceMedal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$advance_medal_1->medal_boosters()->save($int);
			$advance_medal_2 = $medal_consejero->advance_medals()->get()[1];
			$int = MedalBooster::create([
				'name' => "Llena premio",
				'description' => "Llena premio \"libro gratis\" 5 % + rápido",
				'percentage' => 5,
				'medal_id' => $advance_medal_2->id, 'medal_type' => 'AdvanceMedal',
				'aplicable_id' => $premio_desc->id, 'aplicable_type' => 'Prize'
			]);
			$advance_medal_2->medal_boosters()->save($int);

		} else {
			echo "MedalBoosters already created";
		}

	}

}
