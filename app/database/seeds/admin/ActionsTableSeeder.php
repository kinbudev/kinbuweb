<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ActionsTableSeeder extends Seeder {

	public function run() {

		if (Action::count() == 0) {
			Action::create(['name' => "Tour", 'description' => "Solo se puede realizar una vez.", 'value' => 1]);
			Action::create(['name' => "Comentarios", 'description' => "Comentarios", 'value' => 2]);
			Action::create(['name' => "Marcar libro", 'description' => "Puede ser leido, leyendo o leerá", 'value' => 2]);
			Action::create(['name' => "Recomendaciones", 'description' => "Se asignan a otro usuario.", 'value' => 4]);
			Action::create(['name' => "Calificar libro", 'description' => "Una sola vez por libro", 'value' => 4]);
			Action::create(['name' => "Publicar un libro", 'description' => "Compartir venta de un libro.", 'value' => 5]);
			Action::create(['name' => "Intercambiar libro", 'description' => "Intercambiar libro", 'value' => 10]);
			Action::create(['name' => "Venta de libro", 'description' => "Venta de libro", 'value' => 10]);
			Action::create(['name' => "Compra de libro", 'description' => "Compra de libro", 'value' => 10]);
		} else {
			echo "Actions already created.";
		}

	}

}
