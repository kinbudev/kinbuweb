<?php

class ActivitiesTableSeeder extends Seeder {

	public function run() {

		if (Activity::count() == 0) {
			Activity::create(['name' => "Completar tour", 'description' => "Realizar todos los pasos para conocer la aplicación."]);
			Activity::create(['name' => "Marcar libros", 'description' => "Realizar todos los pasos para conocer la aplicación."]);
			Activity::create(['name' => "Añadir amigos", 'description' => "El usuario añade amigos a su perfil."]);
			Activity::create(['name' => "Compartir aplicación", 'description' => "El usuario comparte la apliacion en sus redes sociales."]);
			Activity::create(['name' => "Redimir premios", 'description' => "El usuario redime los premios."]);
			Activity::create(['name' => "Recomendar libros", 'description' => "El usuario reomienda libros a otros usuarios."]);
			Activity::create(['name' => "Intercambiar, comprar o vender", 'description' => "El usuario hace uso del marketplace."]);
			Activity::create(['name' => "Comentar y calificar", 'description' => "El usuario comenta y califica libros en los perfiles del libro."]);
			Activity::create(['name' => "Alcanza niveles y medallas", 'description' => "El usuario alcanza etapas de las medallas y logra escalar hasta niveles."]);
		} else {
			echo 'Activities already created';
		}

	}

}
