<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PrizesTableSeeder extends Seeder {

	public function run() {

		if (Prize::count() == 0) {
			Prize::create(['name' => "Descuento en compra", 'description' => "Descuento en proximo libro", 'value' => 5000.00]);
			Prize::create(['name' => "Libro gratis", 'description' => "Siguiente libro gratis o descuento", 'value' => 10000.00]);
		} else {
			echo "Prices already created.";
		}

	}

}
