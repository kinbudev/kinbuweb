<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AuthorCategoryTableSeeder extends Seeder {

    public function run() {

        if (DB::table('author_category')->count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 64) as $index) {

                $cat_ids = [];

                foreach (range(1, $faker->numberBetween(1, 7)) as $ind) {
                    $cat_ids[] = $faker->unique()->numberBetween(1, 180);
                }

                $faker->unique($reset = true);

                $author = Author::find($index);

                $author->categories()->attach($cat_ids);

            }
        } else {
            echo "Author Categories already created";
        }

    }

}
