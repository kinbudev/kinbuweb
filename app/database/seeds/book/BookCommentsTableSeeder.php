<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BookCommentsTableSeeder extends Seeder {

    public function run() {

        if (BookComment::count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 200) as $index) {

                $bid = $index;

                foreach (range(1, $faker->numberBetween(1, 8)) as $i) {
                    $uid = $faker->unique()->numberBetween(1, 100);

                    $ca = User::find($uid)->created_at;
                    $enddate = (new DateTime($ca))->add(new DateInterval("P{$faker->numberBetween(1, 10)}D"))->format('Y-m-d');

                    $date = $faker->dateTimeBetween($ca, $enddate);

                    BookComment::create([
                        'book_id' => $bid,
                        'user_id' => $uid,
                        'content' => $faker->sentence(10),
                        'created_at' => $date,
                        'updated_at' => $date
                    ]);

                }

                $faker->unique($reset = true);
            }
        } else {
        	echo "BookComments already created";
        }

    }

}
