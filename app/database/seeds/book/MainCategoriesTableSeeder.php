<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MainCategoriesTableSeeder extends Seeder {

    public function run() {

        if (MainCategory::count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 18) as $index) {

                MainCategory::create([
                    'name' => $faker->sentence(2),
                    'description' => $faker->sentence(5)
                ]);
            }
		} else {
			echo "MainCategorie already created";
		}

    }

}
