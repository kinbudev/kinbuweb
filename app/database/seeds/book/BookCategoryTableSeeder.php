<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BookCategoryTableSeeder extends Seeder {

    public function run() {

        if (DB::table('book_category')->count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 200) as $index) {
                $cat_ids = [];

                foreach (range(1, $faker->numberBetween(1, 7)) as $ind) {
                    $cat_ids[] = $faker->unique()->numberBetween(1, 180);
                }

                $faker->unique($reset = true);

                $book = Book::find($index);

                $book->categories()->attach($cat_ids);
            }
        } else {
        	echo "Book Categories already created";
        }

    }

}
