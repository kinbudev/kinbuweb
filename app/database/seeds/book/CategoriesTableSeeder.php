<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

    public function run() {

        if (Category::count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 180) as $index) {

                Category::create([
                    'name'             => $faker->sentence(1),
                    'description'      => $faker->sentence(5),
                    'main_category_id' => $faker->numberBetween(1, 18)
                ]);

            }
        } else {
        	echo "Categories already created";
        }

    }

}
