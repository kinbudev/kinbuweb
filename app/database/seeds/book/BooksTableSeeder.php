<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BooksTableSeeder extends Seeder
{
    public function run()
    {
        echo 'REMOVING ALL WRITTEN_BY'.PHP_EOL;
        Functions::truncateTable('written_by');

        echo 'REMOVING ALL AUTHORS'.PHP_EOL;
        Functions::truncateTable('authors');

        echo 'REMOVING ALL BOOKS'.PHP_EOL;
        Functions::truncateTable('books');

        $booksFilePath = storage_path().'/books.json';

        if (File::exists($booksFilePath)) {
            $file = File::get($booksFilePath);
            $books = json_decode($file);

            foreach ($books as $key => $book) {
                $kbook = Book::where('ISBN', $book->ISBN)->first();
                if (isset($kbook)) {
                    echo 'ISBN already exists: '.$book->ISBN.PHP_EOL;
                    continue;
                }
                $kbook = Book::create([
                    'ISBN' => $book->ISBN,
                    'title' => $book->title,
                    'about' => '',
                    'pages' =>  0,
                    'editorial' => $book->editorial,
                    'cover_type' => 0,
                    'language' => 51,
                    'image' => 'www.librerianacional.com'.$book->imagen
                ]);
                foreach ($book->authors as $key => $author) {
                    $names = explode(' ', preg_replace('/\s+/', ' ',$author));
                    $firstName = $names[0];

                    if (count($names) > 1) {
                        $lastName = implode(' ', array_slice($names, 1));
                    } else {
                        $lastName = '';
                    }

                    $kauthor = Author::whereFirstname($firstName)->whereLastname($lastName)->first();
                    if (!isset($kauthor)) {
                        $kauthor = Author::create([
                            'firstname' => $firstName,
                            'lastname' => $lastName,
                            'about' => '',
                            'birthdate' => '1980-01-01',
                        ]);
                    }
                    try {
                        $kbook->authors()->attach($kauthor->id);
                    } catch (Exception $e) {
                        echo 'Double author: '.$e->getMessage().PHP_EOL;
                    }
                }
            }
        } else {
            throw new Exception('You must have a books.json file in your storage folder', 1);
        }
    }
}
