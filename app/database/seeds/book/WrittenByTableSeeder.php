<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class WrittenByTableSeeder extends Seeder {

    public function run() {

        if (DB::table('written_by')->count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 200) as $index) {

                $author_ids = [];

                foreach (range(1, $faker->numberBetween(1, 5)) as $ind) {
                    $author_ids[] = $faker->unique()->numberBetween(1, 64);
                }

                $faker->unique($reset = true);

                $book = Book::find($index);

                $book->authors()->attach($author_ids);
            }
		} else {
			echo " already created";
		}

    }

}
