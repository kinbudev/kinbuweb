<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

use Carbon\Carbon as Carbon;

class AuthorsTableSeeder extends Seeder {

    public function run() {
        if (Author::count() == 0) {
            $faker = Faker::create();
            $faker->seed('54628');

            foreach (range(1, 64) as $index) {

                $deathdate = $faker->boolean() ? $faker->date('Y-m-d') : null;
                if ($deathdate) {
                    $dd = Carbon::createFromTimestamp(strtotime($deathdate))->subYears(20);
                    $birthdate = $faker->date($dd->format('Y-m-d'));
                } else {
                    $birthdate = $faker->date('Y-m-d', '1999-12-31');
                }
                Author::create([
                    'firstname' => $faker->firstName,
                    'lastname'  => $faker->lastName,
                    'about'     => $faker->sentence(5),
                    'website'   => $faker->domainName . '/' . $faker->userName,
                    'birthdate' => $birthdate,
                    'deathdate' => $deathdate

                ]);
            }
        } else {
            echo "Authors already created";
        }

    }

}
