<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvanceMedalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('advance_medals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
			$table->string('name');
			$table->string('description');
			$table->integer('medal_id')->unsigned();
			$table->foreign('medal_id')->references('id')->on('medals');
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
			$table->string('image_file_name');
			$table->string('image_content_type');
			$table->integer('image_file_size');
			$table->timestamp('image_updated_at')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('advance_medals');
	}

}
