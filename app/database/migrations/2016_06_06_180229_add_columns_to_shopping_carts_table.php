<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToShoppingCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table('shopping_carts', function(Blueprint $table)
		{
			$table->string('pickup_fullname');
			$table->string('pickup_address');
			$table->string('pickup_phone');
			$table->string('pickup_city');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table('shopping_carts', function(Blueprint $table)
		{
			$table->dropColumn('pickup_fullname');
			$table->dropColumn('pickup_address');
			$table->dropColumn('pickup_phone');
			$table->dropColumn('pickup_city');
		});
	}

}
