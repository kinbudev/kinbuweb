<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBooksExchanged extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('ALTER TABLE books_exchanged MODIFY published_book_id2 INTEGER UNSIGNED NULL');
		DB::statement('UPDATE books_exchanged SET published_book_id2 = NULL WHERE published_book_id2 = 0');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		Schema::table('books_exchanged', function(Blueprint $table)
		{
			$table->integer('user_actor_id')->unsigned();
            $table->foreign('user_actor_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('details')->nullable();
			$table->dropColumn('rating1');
            $table->dropColumn('rating2');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('UPDATE books_exchanged SET published_book_id2 = 0 WHERE published_book_id2 IS NULL');
		DB::statement('ALTER TABLE books_exchanged MODIFY published_book_id2 INTEGER UNSIGNED NOT NULL');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		Schema::table('books_exchanged', function(Blueprint $table)
		{
			$table->tinyInteger('rating2')->unsigned();
			$table->tinyInteger('rating1')->unsigned();
			$table->dropColumn('details');
			$table->dropForeign('user_actor_id');
		});
	}

}
