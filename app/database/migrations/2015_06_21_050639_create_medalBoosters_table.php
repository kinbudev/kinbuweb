<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedalBoostersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('medal_boosters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
			$table->string('name');
			$table->string('description');
			$table->decimal('percentage',5,2);
			$table->integer('medal_id')->unsigned();
			$table->string('medal_type');
			$table->integer('aplicable_id')->unsigned()->default(0);
			$table->string('aplicable_type')->nullable();
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('medal_boosters');
	}

}
