<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersAndShipmentsTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('cart_id')->unsigned()->nullable();
            $table->foreign('cart_id')->references('id')->on('shopping_carts')->onDelete('cascade');
            $table->bigInteger('transaction_id')->unsigned();
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->integer('order_status')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('published_book_id')->unsigned()->index();
            $table->foreign('published_book_id')->references('id')->on('published_books')->onDelete('cascade');
            $table->integer('shipment_status')->unsigned();
            $table->string('identifier')->nullable();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
		Schema::drop('shipments');
        Schema::drop('orders');
    }
}
