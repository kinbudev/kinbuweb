<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShoppingCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('shopping_carts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cart_status')->unsigned();
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});

		Schema::create('published_book_shopping_cart', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('published_book_id')->unsigned()->index();
			$table->foreign('published_book_id')->references('id')->on('published_books')->onDelete('cascade');
			$table->integer('shopping_cart_id')->unsigned()->index();
			$table->foreign('shopping_cart_id')->references('id')->on('shopping_carts')->onDelete('cascade');
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('published_book_shopping_cart');
		Schema::drop('shopping_carts');
	}

}
