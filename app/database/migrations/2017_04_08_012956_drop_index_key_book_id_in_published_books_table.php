<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIndexKeyBookIdInPublishedBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('published_books', function(Blueprint $table)
		{
			$table->dropForeign(['user_id']);
			DB::statement('SET FOREIGN_KEY_CHECKS = 0');
			$table->dropUnique(['user_id', 'book_id']);
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('published_books', function(Blueprint $table)
		{
			
			$table->unique(['user_id', 'book_id']);

		});
	}

}
