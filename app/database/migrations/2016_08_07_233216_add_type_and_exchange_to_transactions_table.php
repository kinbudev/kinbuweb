<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTypeAndExchangeToTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('ALTER TABLE transactions MODIFY cart_id INTEGER UNSIGNED NULL');
		DB::statement('UPDATE transactions SET cart_id = NULL WHERE cart_id = 0');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->integer('type')->unsigned();
			$table->integer('exchange_id')->unsigned()->nullable();
			$table->foreign('exchange_id')->references('id')->on('books_exchanged')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('UPDATE transactions SET cart_id = 0 WHERE cart_id IS NULL');
		DB::statement('ALTER TABLE transactions MODIFY cart_id INTEGER UNSIGNED NOT NULL');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->dropColumn('type');
			$table->dropColumn('exchange_id');
		});
	}

}
