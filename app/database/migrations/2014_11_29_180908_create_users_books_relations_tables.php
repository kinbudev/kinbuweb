<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBooksRelationsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('book_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('content', 1023);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('books_ratings', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->primary(['user_id', 'book_id']);
            $table->unique(['user_id', 'book_id']);
            $table->tinyInteger('rating')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('books_read', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->primary(['user_id', 'book_id']);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('book_user_recomendations', function (Blueprint $table) {
            $table->integer('recomender_id')->unsigned();
            $table->foreign('recomender_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->primary(['recomender_id', 'user_id', 'book_id']);
            $table->integer('recomendation_status')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('published_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->decimal('price', 13, 3)->unsigned();
            $table->integer('publication_type')->unsigned();
            $table->integer('book_status')->unsigned();
            $table->integer('cover_type')->unsigned();
            $table->integer('publication_status')->unsigned();
            $table->integer('plaza_type')->unsigned();
            $table->string('description');
            $table->string('pickup_fullname');
            $table->string('pickup_address');
            $table->string('pickup_phone');
            $table->string('pickup_city');
            $table->unique(['user_id', 'book_id']);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('published_books_photos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('published_book_id')->unsigned();
            $table->foreign('published_book_id')->references('id')->on('published_books')->onDelete('cascade');
			$table->string('description');
			$table->string('image_file_name');
			$table->string('image_content_type');
			$table->integer('image_file_size');
			$table->timestamp('image_updated_at')->nullable();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
		});

        Schema::create('published_books_characteristics', function(Blueprint $table) {
            $table->integer('published_book_id')->unsigned();
            $table->foreign('published_book_id')->references('id')->on('published_books')->onDelete('cascade');
            $table->integer('characteristic_type')->unsigned();
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
		});

        // Schema::create('on_exchange_for', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('books_on_exchange_id')->unsigned();
        //     $table->foreign('books_on_exchange_id')->references('id')->on('books_on_exchange')->onDelete('cascade');
        //     $table->integer('book_id')->unsigned();
        //     $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
        //     $table->timestamp('created_at')->default(Functions::currentTimestamp());
        //     $table->timestamp('updated_at');
        //     $table->softDeletes();
        // });

        Schema::create('books_bought', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('published_book_id')->unsigned();
            $table->foreign('published_book_id')->references('id')->on('published_books')->onDelete('cascade');
            $table->primary(['user_id', 'published_book_id']);
            $table->tinyInteger('rating')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('books_exchanged', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('published_book_id1')->unsigned();
            $table->foreign('published_book_id1')->references('id')->on('published_books');
            $table->integer('published_book_id2')->unsigned();
            $table->foreign('published_book_id2')->references('id')->on('published_books');
            $table->integer('exchange_status')->unsigned();
            $table->tinyInteger('rating1')->unsigned();
            $table->tinyInteger('rating2')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('books_exchanged');
        Schema::drop('books_bought');
        Schema::drop('published_books_characteristics');
        Schema::drop('published_books_photos');
        Schema::drop('published_books');
        Schema::drop('book_user_recomendations');
        Schema::drop('books_read');
        Schema::drop('books_ratings');
        Schema::drop('book_comments');

    }

}
