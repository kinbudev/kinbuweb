<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('attribute');
            $table->string('description');
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('parameter_values', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->integer('parameter_id')->unsigned();
            $table->foreign('parameter_id')->references('id')->on('parameters')->onDelete('cascade');
            $table->string('pvalue', 511);
            $table->string('porder', 3)->nullable();
            $table->string('description')->nullable();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('parameter_values');
        Schema::drop('parameters');
    }

}
