<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('levels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
			$table->integer('level');
			$table->integer('points');
			$table->integer('acumulate');
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('levels');
	}

}
