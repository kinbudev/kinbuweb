<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddImageToBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table('books', function(Blueprint $table)
		{
			$table->string('image')->nullable()->after('about');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table('books', function(Blueprint $table)
		{
			$table->dropColumn('image');
		});
	}

}
