<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBookReadStatusToBooksReadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::table('books_read', function(Blueprint $table)
		{
			$table->integer('book_read_status')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table('books_read', function(Blueprint $table)
		{
			$table->dropColumn('book_read_status');
		});
	}

}
