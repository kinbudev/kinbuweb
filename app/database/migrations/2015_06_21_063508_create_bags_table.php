<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('bags', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
			$table->decimal('amount',10,2);
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('prize_id')->unsigned();
			$table->foreign('prize_id')->references('id')->on('prizes');
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('bags');
	}

}
