<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('transactions', function(Blueprint $table)
		{
			$table->bigIncrements('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('cart_id')->unsigned();
			$table->foreign('cart_id')->references('id')->on('shopping_carts')->onDelete('cascade');
			$table->string('reference_code');
			$table->decimal('value', 14, 2);
			$table->string('description')->nullable();
			$table->string('reference_pol')->nullable();
			$table->string('payu_id', 36)->nullable();
			$table->string('state_pol', 32)->nullable();
			$table->integer('payment_method_name')->nullable();
			$table->integer('payment_method_type')->nullable();
			$table->string('response_message_pol')->nullable();
			$table->string('cus', 64)->nullable();
			$table->string('pse_bank')->nullable();
			$table->string('authorization_code', 12)->nullable();
			$table->datetime('transaction_date')->nullable();
			$table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('transactions');
	}

}
