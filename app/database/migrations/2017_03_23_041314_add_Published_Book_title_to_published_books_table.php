<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublishedBookTitleToPublishedBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('published_books', function(Blueprint $table)
		{
			$table->string('published_book_title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('published_books', function(Blueprint $table)
		{
			$table->dropColumn('published_book_title');
		});
	}

}
