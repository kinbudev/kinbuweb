<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('ISBN')->unique();
            $table->string('title');
            $table->string('about', 1023);
            $table->date('release_date')->nullable();
            $table->integer('pages');
            $table->string('editorial');
            $table->integer('cover_type')->unsigned();
            $table->integer('language')->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('authors', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('about', 1023);
            $table->string('website')->nullable();
            $table->date('birthdate');
            $table->date('deathdate')->nullable();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('written_by', function (Blueprint $table) {
            $table->integer('book_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
            $table->primary(['book_id', 'author_id']);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('main_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('name');
            $table->string('description');
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('name');
            $table->string('description');
            $table->integer('main_category_id')->unsigned();
            $table->foreign('main_category_id')->references('id')->on('main_categories')->onDelete('cascade');
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('book_category', function (Blueprint $table) {
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->primary(['book_id', 'category_id']);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('author_category', function (Blueprint $table) {
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->primary(['author_id', 'category_id']);
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->unsigned()->default(Parameter::$status['active']);
            $table->string('about', 511);
            $table->string('website');
            $table->string('address');
            $table->string('contact');
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('book_library', function (Blueprint $table) {
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->integer('library_id')->unsigned();
            $table->foreign('library_id')->references('id')->on('libraries')->onDelete('cascade');
            $table->primary(['book_id', 'library_id']);
            $table->decimal('price', 13, 3)->unsigned();
            $table->timestamp('created_at')->default(Functions::currentTimestamp());
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('book_library');
        Schema::drop('libraries');
        Schema::drop('author_category');
        Schema::drop('book_category');
        Schema::drop('categories');
        Schema::drop('main_categories');
        Schema::drop('written_by');
        Schema::drop('authors');
        Schema::drop('books');

    }

}
