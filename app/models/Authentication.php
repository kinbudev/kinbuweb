<?php

class Authentication extends Eloquent {

    protected $fillable = ['social_id', 'provider', 'token', 'token_secret', 'status'];

    public function user() {
        return $this->belongsTo('User');
    }


}