<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ParameterValue extends BaseAdminModel {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $table = 'parameter_values';
    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = ['parameter_id', 'pvalue', 'porder', 'description'];

    public function parameter() {
        return $this->belongsTo('Parameter');
    }

    function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Parameter Values';

        $this->admin['model']['name'] = 'parameter_values';
        $this->admin['model']['parent'] = 'parameters';

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;
        $this->admin['actions']['delete']['can'] = TRUE;

        $this->admin['form_attrs']['pvalue'] = ['title' => 'Value', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];

        $this->admin['grid_attrs']['pvalue'] = 'Value';
        $this->admin['grid_attrs']['description'] = 'Description';
    }

}
