<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BookPublicationPhoto extends Eloquent
{
    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'published_books_photos';

    // Add your validation rules here
    public static $rules = [
        'image_file_name' => 'required',
        'image_content_type' => 'required',
        'image_file_size' => 'required',
        'image_updated_at' => 'required',
    ];
    public $errors;
    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    // Don't forget to fill this array
    protected $fillable = [];

    protected $appends = ['image', 'image_url'];

    public function book_publication()
    {
        return $this->belongsTo('BookPublication', 'published_book_id');
    }

    public function getImageAttribute()
    {
        return HTML::image('img/publication/'.$this->published_book_id.'/'.$this->image_file_name, '-', ['class' => 'img-responsive center-block']);
    }

    public function getImageUrlAttribute()
    {
        return 'img/publication/'.$this->published_book_id.'/'.$this->image_file_name;
    }
}
