<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BookPublication extends Eloquent
{
    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'published_books';

    // Add your validation rules here
    public static $rules = [
        'published_book_title' => 'required',
        'description' => 'required',
        'pickup_fullname' => 'required',
        'pickup_address' => 'required',
        'pickup_phone' => 'required',
        'pickup_city' => 'required',
    ];
    public $errors;
    public function isValid()
    {
        $validation = Validator::make($this->attributes, static::$rules);
        $validation->sometimes('price', 'required|numeric|min:1000', function ($input) {
            $types = Parameter::whereAttribute('publication_type')->first()->values()->get();
            return $input->publication_type != $types[1]->id;//exchange
        });

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    // Don't forget to fill this array
    protected $fillable = [];

    protected $appends = ['type', 'book_state', 'cover', 'status', 'plaza', 'can_be_bought', 'image'];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function carts() {
        return $this->belongsToMany('ShoppingCart', 'published_book_shopping_cart', 'published_book_id', 'shopping_cart_id');
    }

    public function type()
    {
        return $this->belongsTo('ParameterValue', 'publication_type');
    }

    public function getTypeAttribute()
    {
        return $this->type()->lists('pvalue')[0];
    }

    public function getTypeDescriptionAttribute()
    {
        return $this->type()->lists('description')[0];
    }

    public function book_status()
    {
        return $this->belongsTo('ParameterValue', 'book_status');
    }

    public function getBookStateAttribute()
    {
        return $this->book_status()->lists('pvalue')[0];
    }

    public function getBookStateDescriptionAttribute()
    {
        return $this->book_status()->lists('description')[0];
    }

    public function cover()
    {
        return $this->belongsTo('ParameterValue', 'cover_type');
    }

    public function getCoverAttribute()
    {
        return $this->cover()->lists('pvalue')[0];
    }

    public function getCoverDescriptionAttribute()
    {
        return $this->cover()->lists('description')[0];
    }

    public function status()
    {
        return $this->belongsTo('ParameterValue', 'publication_status');
    }

    public function getStatusAttribute()
    {
        return $this->status()->lists('pvalue')[0];
    }

    public function getStateAttribute()
    {
        return $this->status()->lists('description')[0];
    }

    public function plaza()
    {
        return $this->belongsTo('ParameterValue', 'plaza_type');
    }

    public function getPlazaAttribute()
    {
        return $this->plaza()->lists('pvalue')[0];
    }

    public function getPlazaDescriptionAttribute()
    {
        return $this->plaza()->lists('description')[0];
    }

    public function getAmountAttribute()
    {
        $notAvailable = $this->carts()->inactive()->get()->count();
        $notAvailable += $this->carts()->bought()->get()->count();
        return $this->attributes['amount'] - $notAvailable;
    }

    public function getCanBeBoughtAttribute()
    {
        return $this->amount > 0;
    }

    public function photos()
    {
        return $this->hasMany('BookPublicationPhoto', 'published_book_id');
    }

    public function characteristics()
    {
        return $this->belongsToMany('ParameterValue', 'published_books_characteristics', 'published_book_id', 'characteristic_type');
    }

    public function exchanges()
    {
        return $this->hasMany('BookExchange', 'published_book_id1');
    }

    public function exchanges_for()
    {
        return $this->hasMany('BookExchange', 'published_book_id2');
    }

    public function scopeByType($query, $type)
    {
        $both = Parameter::whereAttribute('publication_type')->first()->pValue('both');
        $pv = Parameter::whereAttribute('publication_type')->first()->pValue($type);
        return $query->where(function ($query) use($pv, $both)
        {
            $query->where('publication_type', $pv->id)
                  ->orWhere('publication_type', $both->id);
        });
    }

    public function scopeOnSale($query)
    {
        return $this->scopeByType($query, 'sale');
    }

    public function scopeOnExchange($query)
    {
        return $this->scopeByType($query, 'exchange');
    }

    public function getImageAttribute()
    {
        $photos = $this->photos()->where('position', 1);
        if ($photos->count() > 0 )
        {
            $url = $photos->first()->image_url;
        }
        else 
        {
            $url = "";
        }
        return HTML::image( $url, 'Foto Publicacion', ['class' => 'book-cover-img', 'width' => 150, 'height' => 200]);
    }

    //for search querys

        public function scopeSearch($query, $search)
    {
        return $query->where(function ($q) use($search)
        {
            $q->where('published_book_title', 'LIKE', "%$search%")
              ->orWhere('description', 'LIKE', "%$search%");
        })
        #->whereBetween('published_books.plaza_type', [211, 213])
        ->orderBy('published_books.plaza_type', 'desc');
    }
    

}
