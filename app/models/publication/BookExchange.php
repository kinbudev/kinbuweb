<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BookExchange extends Eloquent
{
    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'books_exchanged';

    // Don't forget to fill this array
    protected $fillable = [];

    protected $appends = ['exchange_state', 'details'];

    public function publication()
    {
        return $this->belongsTo('BookPublication', 'published_book_id1');
    }

    public function forPublication()
    {
        return $this->belongsTo('BookPublication', 'published_book_id2');
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_actor_id');
    }

    public function exchange_status()
    {
        return $this->belongsTo('ParameterValue', 'exchange_status');
    }

    public function getExchangeStateAttribute()
    {
        return $this->exchange_status()->lists('pvalue')[0];
    }

    public function getDetailsAttribute()
    {
        if ($this->attributes['details']) {
            return json_decode($this->attributes['details']);
        }
        return $this->attributes['details'];
    }

    public function scopeByState($query, $type)
    {
        $pv = Parameter::whereAttribute('exchange_status')->first()->pValue($type);
        return $query->whereExchangeStatus($pv->id);
    }

    public function scopeReady($query)
    {
        return $this->scopeByState($query, 'ready');
    }

    public function scopeExchanged($query)
    {
        return $this->scopeByState($query, 'exchanged');
    }

    public function scopeRejected($query)
    {
        return $this->scopeByState($query, 'rejected');
    }

    public function scopeAccepted($query)
    {
        return $this->scopeByState($query, 'accepted');
    }

}
