<?php

class Transaction extends Eloquent
{
    protected $fillable = ['reference_code', 'value', 'cart_id', 'user_id'];

    public function user() {
        return $this->belongsTo('User');
    }

    public function cart() {
        return $this->belongsTo('ShoppingCart', 'cart_id');
    }

    public function exchange()
    {
        return $this->belongsTo('BookExchange', 'exchange_id');
    }

    public function type()
    {
        return $this->belongsTo('ParameterValue', 'type');
    }

    public function getTypeAttribute()
    {
        return $this->type()->lists('pvalue')[0];
    }

}
