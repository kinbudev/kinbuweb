<?php

class Order extends Eloquent
{
    protected $fillable = ['user_id', 'cart_id', /*'exchange_id',*/ 'transaction_id'];

    public function user() {
        return $this->belongsTo('User');
    }

    public function cart() {
        return $this->belongsTo('ShoppingCart');
    }

    public function transaction() {
        return $this->belongsTo('Transaction');
    }

    public function shipments() {
        return $this->hasMany('Shipment');
    }

    public function status()
    {
        return $this->belongsTo('ParameterValue', 'order_status');
    }

    public function getStatusAttribute()
    {
        return $this->status()->lists('description')[0];
    }

    public function updateStatus()
    {
        $shipmentsStatuses = Parameter::whereAttribute('shipment_status')->first()
            ->values()->lists('id', 'pvalue');
        $ordersStatuses = Parameter::whereAttribute('order_status')->first()
            ->values()->lists('id', 'pvalue');
        $shipments = $this->shipments()->get();
        $amountPickup = 0;
        $amountToSend = 0;
        $amountSent = 0;
        $amountReceived = 0;
        foreach ($shipments as $key => $shipment) {
            switch ($shipment->shipment_status) {
                case $shipmentsStatuses['to_send']:
                    $amountToSend = $amountToSend + 1;
                    break;
                case $shipmentsStatuses['sent']:
                    $amountSent = $amountSent + 1;
                    break;
                case $shipmentsStatuses['received']:
                    $amountReceived = $amountReceived + 1;
                    break;
                default:
                    $amountPickup = $amountPickup + 1;
                    break;
            }
        }

        if ($amountToSend > 0) {
            $this->order_status = $ordersStatuses['partial'];
        } elseif ($amountPickup == 0 && $amountSent > 0) {
            $this->order_status = $ordersStatuses['sent'];
        } elseif ($amountReceived > 0 && $amountReceived < $shipments->count()) {
            $this->order_status = $ordersStatuses['partial'];
        } elseif ($amountReceived == $shipments->count()) {
            $this->order_status = $ordersStatuses['received'];
        }

        $this->save();
        $this->touch();
        return [
            'amountPickup' => $amountPickup,
            'amountToSend' => $amountToSend,
            'amountSent' => $amountSent,
            'amountReceived' => $amountReceived,
            'total' => $shipments->count()
        ];
    }


}
