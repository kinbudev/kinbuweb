<?php

class FutureUser extends Eloquent {

    // Add your validation rules here
    public static $rules = [
        'email' => 'email|required|unique:future_users'
    ];

    // Don't forget to fill this array
    protected $fillable = ['email'];

    public $errors;

    public function isValid() {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) return true;

        $this->errors = $validation->messages();

        return false;
    }

}