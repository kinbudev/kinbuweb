<?php

class ShoppingCart extends Eloquent
{
    protected $fillable = ['cart_status', 'pickup_fullname', 'pickup_address', 'pickup_phone', 'pickup_city'];

    public function user() {
        return $this->belongsTo('User');
    }

    public function status()
    {
        return $this->belongsTo('ParameterValue', 'cart_status');
    }

    public function getStatusAttribute()
    {
        return $this->status()->lists('description')[0];
    }

    public function items() {
        return $this->belongsToMany('BookPublication', 'published_book_shopping_cart', 'shopping_cart_id', 'published_book_id');
    }

    public function transactions() {
        return $this->hasMany('Transaction', 'cart_id');
    }

    public function scopeByStatus($query, $status)
    {
        $pv = Parameter::whereAttribute('cart_status')->first()->pValue($status);
        return $query->whereCartStatus($pv->id);
    }

    public function scopeActive($query)
    {
        return $this->scopeByStatus($query, 'active');
    }

    public function scopeInactive($query)
    {
        return $this->scopeByStatus($query, 'inactive');
    }

    public function scopeBought($query)
    {
        return $this->scopeByStatus($query, 'bought');
    }

    public function scopeDeleted($query)
    {
        return $this->scopeByStatus($query, 'deleted');
    }

    /**
     * returns true if all items in cart can be bought
     * otherwise returns the first item that cannot be bought
     */
    public function canBeBought()
    {
        $items = $this->items()->get();
        foreach ($items as $key => $publication) {
            if (!$publication->can_be_bought) {
                return $publication;
            }
        }
        return true;
    }

    public function updatePickup($pickup)
    {
        $this->pickup_fullname = $pickup['pickup_fullname'];
        $this->pickup_address = $pickup['pickup_address'];
        $this->pickup_phone = $pickup['pickup_phone'];
        $this->pickup_city = $pickup['pickup_city'];
        $this->save();
        $this->touch();
    }

}
