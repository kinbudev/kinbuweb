<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Parameter extends BaseAdminModel {

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = ['attribute', 'description'];

    public static $status = [
        'active'   => 1,
        'inactive' => 0
    ];

    function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Parameters';

        $this->admin['model']['name'] = 'parameters';
        $this->admin['model']['details'] = [
            'values' => 'ParameterValue'
        ];

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;

        $this->admin['form_attrs']['attribute'] = ['title' => 'Attribute', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];

        $this->admin['grid_attrs']['attribute'] = 'Attribute';
        $this->admin['grid_attrs']['description'] = 'Description';
    }

    public function addValue($pvalue, $porder = null, $description = null) {
        return ParameterValue::create([
            'parameter_id'  => $this->id,
            'pvalue'        => $pvalue,
            'porder'        => $porder,
            'description'   => $description
        ]);
    }

    public function addValues($values) {

        foreach ($values as $pvalue) {
            if (is_array($pvalue)) {
                if (array_key_exists('pvalue', $pvalue)) {
                    $paramval = $pvalue['pvalue'];
                } else {
                    $paramval = $pvalue[0];
                }
                if (array_key_exists('porder', $pvalue)) {
                    $porder = $pvalue['porder'];
                } else {
                    $porder = $pvalue[1];
                }
                if (array_key_exists('description', $pvalue)) {
                    $description = $pvalue['description'];
                } else {
                    $description = $pvalue[2];
                }

                $this->addValue($paramval, $porder, $description);

            } else {
                $this->addValue($pvalue);
            }
        }

    }

    public function values() {
        return $this->hasMany('ParameterValue');
    }

    public function valuesIdArray() {
        return $this->values()->get(['id'])->fetch('id')->toArray();
    }

    public function pValue($pvalue) {
        return $this->values()->wherePvalue($pvalue)->first();
    }

    public function pValueId($pvalue) {
        return $this->pValue($pvalue)->id;
    }

}
