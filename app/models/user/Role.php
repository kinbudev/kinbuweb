<?php

class Role extends BaseAdminModel {
	protected $fillable = [];

	public function users() {
		return $this->belongsToMany('User');
	}

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Roles';

        $this->admin['model']['name'] = 'roles';

        $this->admin['form_attrs']['role'] = ['title' => 'Role', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];

        $this->admin['grid_attrs']['role'] = 'Role';
        $this->admin['grid_attrs']['description'] = 'Description';
    }

}
