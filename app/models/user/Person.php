<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Person extends Eloquent
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = ['firstname', 'lastname', 'about_me', 'address', 'phone', 'city', 'account_number'];

    protected $appends = ['name', 'has_contact_data', 'has_location_data'];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function getNameAttribute()
    {
        return $this->attributes['firstname'].' '.$this->attributes['lastname'];
    }

    public function getHasContactDataAttribute()
    {
        return !empty($this->address) && !empty($this->city) && !empty($this->phone);
    }

    public function getHasLocationDataAttribute()
    {
        return !empty($this->address) || !empty($this->city);
    }

}
