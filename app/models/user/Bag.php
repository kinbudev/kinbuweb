<?php

class Bag extends Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['amount'];

	public function user() {
		$this->belongsTo('User');
	}

	public function prize() {
		$this->belongsTo('Prize');
	}

}
