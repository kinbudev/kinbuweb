<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait, SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $fillable = ['username', 'email', 'password', 'password_confirmation'];

    protected $appends = ['name', 'has_location_data', 'level', 'gravatar', 'member_time'];

    public static $rules = [
        'username'  => 'required|unique:users',
        'email'     => 'required|email|unique:users',
        'password'              => 'required|min:8|confirmed',
        'password_confirmation' => 'required'
    ];

    public static $update_rules = [
        'username' => 'required|unique:users,username,',
        'password' => 'required',
    ];

    public $errors;

    public function isValid() {
        $validation = Validator::make($this->attributes, static::$rules);
        if ($validation->passes()) {
            unset($this->password_confirmation);
            return true;
        }
        $this->errors = $validation->messages();
        return false;
    }

    public function isUpdateValid() {
        $rules = static::$update_rules;
        $rules['username'] .= $this->id;
        $validation = Validator::make($this->attributes, $rules);
        if ($validation->passes()) {
            return true;
        }
        $this->errors = $validation->messages();
        return false;
    }

    public function person() {
        return $this->hasOne('Person');
    }

    /*public function books_ratings() {
        return $this->belongsToMany('Book', 'books_ratings')->withPivot('rating');
    }*/

    public function comments() {
        return $this->hasMany('BookComment');
    }

    public function latest_comments() {
        return $this->comments()->takeLatest(5);
    }

    public function messages_send() {
        return $this->hasMany('Message', 'from_id');
    }

    public function messages_received() {
        return $this->hasMany('Message', 'to_id');
    }

    public function shopping_carts() {
        return $this->hasMany('ShoppingCart');
    }

    public function active_carts() {
        return $this->shopping_carts()->active()->get();
    }

    public function inactive_carts() {
        return $this->shopping_carts()->inactive()->get();
    }

    public function transactions() {
        return $this->hasMany('Transaction');
    }

    public function orders() {
        return $this->hasMany('Order');
    }

    public function requestedFriends() {
        return $this->belongsToMany('User', 'user_relationships', 'requester_id')->withTimestamps();
    }


    public function friends() {

        $rs = Parameter::whereAttribute('request_status')->first()->pValueId('acknowledged');

        $id = $this->id;
        $raw = "IF(requester_id=$id, user_id, IF(user_id=$id, requester_id, 'NO')) as id";

        return DB::table('user_relationships')
            ->select(DB::raw($raw))
            ->where('request_status', '=', $rs)
            ->where(function ($query) {
                $query
                    ->where('requester_id', '=', $this->id)
                    ->orWhere('user_id', '=', $this->id);
            });
    }


    public function user_ratings() {
        return $this->belongsToMany('User', 'user_ratings', 'user_rated_id', 'user_id');
    }

    public function rating() {
        $ratings = $this->user_ratings()->get();
        if ($ratings instanceof User) {
            return $ratings->rating;
        } elseif ($ratings instanceof \Illuminate\Database\Eloquent\Collection) {
            if ($ratings->count() == 0) {
                return 'No ratings';
            }
            $sum = 0;
            foreach ($ratings as $user_rating) {
                $sum += $user_rating->rating;
            }

            return $sum / $ratings->count();
        }
    }

    public function books_read() {
        return $this->belongsToMany('Book', 'books_read')->withPivot('book_read_status');
    } //ESTE METODO YA NO SE DEBE UTILIZAR

    public function publications() {
        return $this->hasMany('BookPublication', 'user_id', 'id');
    }

    public function publications_for_exchange() {
        return $this->publications()->with('BookPublication')->onExchange();
    }

    public function exchanges()
    {
        return $this->hasManyThrough('BookExchange', 'BookPublication', 'user_id', 'published_book_id1');
    }

    public function exchanges_for()
    {
        return $this->hasManyThrough('BookExchange', 'BookPublication', 'user_id', 'published_book_id2');
    }

    public function exchanges_created()
    {
        return $this->hasMany('BookExchange');
    }

    public function books_bought() {
        $bought = BookPublication::join('shipments', 'published_books.id', '=', 'shipments.published_book_id')
        ->join('orders', 'shipments.order_id', '=', 'orders.id')
                  ->where('orders.user_id', '=', $this->id )
        ->join('parameter_values', 'orders.order_status', '=', 'parameter_values.id')
                  ->where('parameter_values.pvalue', '=', 'received')
        ->select('published_books.*');

        return $bought;
        //No está devolviendo libros comprados del usuario actual.
    }

    public function books_on_sale() {
        $pt = Parameter::whereAttribute('publication_type')->first()->values()->whereIn('pvalue', ['sale', 'both'])->lists('id');
        return $this->hasMany('BookPublication')->whereIn('publication_type', $pt);
    }

    public function books_on_exchange() {
        $pt = Parameter::whereAttribute('publication_type')->first()->values()->whereIn('pvalue', ['exchange', 'both'])->lists('id');
        return $this->hasMany('BookPublication')->whereIn('publication_type', $pt);
    }

    public function authentications() {
        return $this->hasMany('Authentication');
    }

    public function roles() {
        return $this->belongsToMany('Role');
    }

    public function isAdmin() {
        return $this->roles()->whereRole('admin')->count() == 1;
    }

    public function updateLevel() {
        $accumulate = $this->actions()->sum('value');
        $this->person->level_acumulate = $accumulate;
        $this->person->touch();
        return $this->person->save();
    }

    public function updateMedal($medal) {
        $medals = [];
        switch ($medal) {
            case 1:
                if ($this->actions()->where('actions.id',1)->count() === 1) {
                    $user_medals = $this->medals();
                    if (!in_array(1, $user_medals->lists('id'))) {
                        $user_medals->attach(1);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(1);
                    }
                }
                break;
            case 2:# TODO BOOSTER acelera nivel
                $books_read = DB::table('books as b')
                    ->join('books_read as br','b.id', '=', 'br.book_id')
                    ->join('parameter_values as pv', 'br.book_read_status', '=', 'pv.id')
                    ->where('br.user_id',$this->id)
                    ->groupBy('br.book_read_status')
                    ->select('pv.pvalue as status', DB::raw('count(br.book_id)' . ' as book_count'))
                    ->lists('book_count', 'status');

                if ( ! array_key_exists('read', $books_read)) break;
                if ( ! array_key_exists('reading', $books_read)) break;
                if ( ! array_key_exists('will read', $books_read)) break;
                if ($books_read['read'] >= 2 && $books_read['reading'] >= 1 && $books_read['will read'] >= 2) {
                    $user_medals = $this->medals();
                    if (!in_array(2, $user_medals->lists('id'))) {
                        $user_medals->attach(2);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(2);
                    }
                    if ($books_read['read'] >= 10 && $books_read['will read'] >= 10) {
                        $user_medals = $this->advance_medals();
                        $advance_medals_id_list = $user_medals->lists('id');
                        if (!in_array(1, $advance_medals_id_list)) {
                            $user_medals->attach(1);
                            $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(1);
                        }
                        if ($books_read['read'] >= 50 && $books_read['will read'] >= 50) {
                            if (!in_array(2, $advance_medals_id_list)) {
                                $user_medals->attach(2);
                                $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(2);
                            }
                        }

                    }

                }
                break;
            case 3:
                $friends = $this->friends()->count();
                if($friends >= 5) {
                    $user_medals = $this->medals();
                    if (!in_array(3, $user_medals->lists('id'))) {
                        $user_medals->attach(3);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(3);
                    }
                    if ($friends >= 10) {
                        $user_medals = $this->advance_medals();
                        $advance_medals_id_list = $user_medals->lists('id');
                        if (!in_array(3, $advance_medals_id_list)) {
                            $user_medals->attach(3);
                            $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(3);
                        }
                        if ($friends >= 50) {
                            if (!in_array(4, $advance_medals_id_list)) {
                                $user_medals->attach(4);
                                $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(4);
                            }
                        }
                    }
                }
                break;
            case 4:
                # TODO Promotor
                # Compartir la aplicacion 1, 5, 10 veces
                break;
            case 5:
                # TODO Caza Tesoros
                # Redimir premios
                break;
            case 6:# TODO BOOSTER libro gratis
                $books = DB::table('book_user_recomendations')->where('recomender_id',$this->id)->groupBy('book_id');
                $books = count($books->get());
                if($books >= 5) {
                    $user_medals = $this->medals();
                    if (!in_array(6, $user_medals->lists('id'))) {
                        $user_medals->attach(6);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(6);
                    }
                    if ($books >= 10) {
                        $user_medals = $this->advance_medals();
                        $advance_medals_id_list = $user_medals->lists('id');
                        if (!in_array(9, $advance_medals_id_list)) {
                            $user_medals->attach(9);
                            $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(9);
                        }
                        if ($books >= 50) {
                            if (!in_array(10, $advance_medals_id_list)) {
                                $user_medals->attach(10);
                                $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(10);
                            }
                        }
                    }
                }
                break;
            case 7:
                # TODO Mercante
                # Intercambia, compra, o vende 1, 10, 50 libros
                break;
            /*
            case 8:# TODO BOOSTER descuento en compra
                $book_comments = $this->comments()->select('book_id', DB::raw('count(*) c'))->groupBy('book_id');
                $book_comments = count($book_comments->get());
                $book_ratings = $this->books_ratings()->count();
                if($book_comments >= 5 && $book_ratings >= 5) {
                    $user_medals = $this->medals();
                    if (!in_array(8, $user_medals->lists('id'))) {
                        $user_medals->attach(8);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(8);
                    }
                    if ($book_comments >= 10 && $book_ratings >= 10) {
                        $user_medals = $this->advance_medals();
                        $advance_medals_id_list = $user_medals->lists('id');
                        if (!in_array(13, $advance_medals_id_list)) {
                            $user_medals->attach(13);
                            $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(13);
                        }
                        if ($book_comments >= 50 && $book_ratings >= 50) {
                            if (!in_array(14, $advance_medals_id_list)) {
                                $user_medals->attach(14);
                                $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(14);
                            }
                        }
                    }
                }
                break;
            */
            case 9:
                $user_medals = $this->medals();
                $medallas = $user_medals->count();
                $total_medallas = Medal::all()->count() - 1;
                if ($this->level >= 10 && $medallas >= $total_medallas) {
                    if (!in_array(9, $user_medals->lists('id'))) {
                        $user_medals->attach(9);
                        $medals[] = Medal::select(['name', 'description', 'image_file_name'])->find(9);
                    }
                    $user_medals = $this->advance_medals();
                    $medallas = $user_medals->whereDescription('Intermedio')->count();
                    $total_medallas = AdvanceMedal::whereDescription('Intermedio')->count() - 1;
                    if ($this->level >= 15 && $medallas >= $total_medallas) {
                        $advance_medals_id_list = $user_medals->lists('id');
                        if (!in_array(15, $advance_medals_id_list)) {
                            $user_medals->attach(15);
                            $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(15);
                        }
                        $medallas = $user_medals->whereDescription('Avanzado')->count();
                        $total_medallas = AdvanceMedal::whereDescription('Avanzado')->count() - 1;
                        if ($this->level >= 20 && $medallas >= $total_medallas) {
                            if (!in_array(16, $advance_medals_id_list)) {
                                $user_medals->attach(16);
                                $medals[] = AdvanceMedal::select(['name', 'description', 'image_file_name'])->find(16);
                            }
                        }
                    }
                }
                break;
            default:
                throw new Exception("Error Decifrando medalla");
                break;
        }
        return $medals;
    }

    public function hasMedal($id)
    {
        return $this->medals()->whereId($id)->count() === 1;
    }

    public function hasAdvanceMedal($id)
    {
        return $this->advance_medals()->whereId($id)->count() === 1;
    }

    public function medals() {
        return $this->morphedByMany('Medal', 'medalable', 'medal_user')->whereStatus(1);
    }

    public function advance_medals() {
        return $this->morphedByMany('AdvanceMedal', 'medalable', 'medal_user')->whereStatus(1);
    }

    public function allMedals() {
        $medals = $this->medals()->get();
        $advance_medals = $this->advance_medals()->get();
        $allMedals = Medal::with('advance_medals')->whereStatus(1)->get();
        foreach ($allMedals as $medal) {
            if ($medals->contains($medal->id)) {
                $medal->owned_by_user = true;
                foreach ($medal->advance_medals as $key => $advance_medal) {
                    $advance_medal->owned_by_user = $advance_medals->contains($advance_medal->id);
                }
            } else {
                $medal->owned_by_user = false;
            }
        }
        return $allMedals;
    }

    public function bags() {
		return $this->hasMany('Bag');
	}

    public function actions() {
        return $this->belongsToMany('Action');
    }

    //Scopes

    public function scopeReadingNow($query) {
        return $query->orderBy('created_at', 'desc')->take(1);
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(function ($q) use($search)
        {
            $q->where('username', 'LIKE', "%$search%")->orWhereHas('person', function ($q) use($search)
            {
                $q->where(function ($q) use($search)
                {
                    $q->where('firstname', 'LIKE', "%$search%")->orWhere('lastname', 'LIKE', "%$search%");
                });
            });
        });
    }

    //Other props

    public function getNameAttribute() {
        if(!empty($this->person->firstname) && !empty($this->person->lastname)) {
            return $this->person->name;
        } else {
            return $this->username;
        }
    }

    public function getHasContactDataAttribute()
    {
        return $this->person->has_contact_data;
    }

    public function getHasLocationDataAttribute()
    {
        return $this->person->has_location_data;
    }

    public function getLevelAttribute() {
        return $this->person()->join('levels', 'level_acumulate', '>=','acumulate')->max('levels.level');
    }

    public function getGravatarAttribute() {
        return $this->getGravatar(80, 'mm', 'g', true, ['class' => 'media-object userpp']);
    }

    public function getGravatar($s = 80, $d = 'mm', $r = 'g', $img = false, $attrs = []) {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($this->email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($attrs as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }

        return $url;
    }

    public function getMemberTimeAttribute() {
        return Functions::getFirstYMDHIS(date_diff(date_create('now'), $this->created_at));
    }

}
