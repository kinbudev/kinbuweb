<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Message extends Eloquent
{
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $appends = ['since'];

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = ['from_id', 'to_id', 'content'];

    /**
     * @return User the user who send this message
     */
    public function sender()
    {
        return $this->belongsTo('User', 'from_id');
    }

    /**
     * @return User the user who received this message
     */
    public function receiver()
    {
        return $this->belongsTo('User', 'to_id');
    }

    public function getSinceAttribute()
    {
        return Functions::getDateAsString($this->created_at);
    }
}
