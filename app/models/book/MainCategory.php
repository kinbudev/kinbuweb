<?php

class MainCategory extends Eloquent {

    protected $guarded = ['id'];
    protected $fillable = ['name', 'description'];

    public function categories() {
        return $this->hasMany('Category');
    }
}