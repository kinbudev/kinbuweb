<?php

/**
 * Created by PhpStorm.
 * User: jedabero
 * Date: 21/11/14
 * Time: 07:19 PM
 */

use Carbon\Carbon;
 use Illuminate\Database\Eloquent\SoftDeletingTrait;

class BookComment extends Eloquent {

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $table = 'book_comments';
    protected $fillable = [
        'content', 'book_id', 'user_id'
    ];

    protected $appends = ['created_at_formatted'];

    public function user() {
        return $this->belongsTo('User');
    }

    public function book() {
        return $this->belongsTo('Book');
    }

    public function scopeLatest($query) {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeTakeLatest($query, $amount = 4) {
        return $this->scopeLatest($query)->take($amount);
    }

    public function scopeThisPeriod($query, $period = 'month')
    {
        $dt = Carbon::now();
        switch ($period) {
            case 'day':
                return $query->whereDate('created_at', '>=', $dt->subDay());
            case 'week':
                return $query->whereDate('created_at', '>=', $dt->subWeek());
            case 'year':
                return $query->whereDate('created_at', '>=', $dt->subYear());
            case 'month':
            default:
                return $query->whereDate('created_at', '>=', $dt->subMonth());
        }
    }

    public function scopeLastDay($query)
    {
        return $this->scopeLatest($this->scopeThisPeriod($query, 'day'));
    }

    public function scopeLastWeek($query)
    {
        return $this->scopeLatest($this->scopeThisPeriod($query, 'week'));
    }

    public function scopeLastYear($query)
    {
        return $this->scopeLatest($this->scopeThisPeriod($query, 'year'));
    }

    public function scopeLastMonth($query)
    {
        return $this->scopeLatest($this->scopeThisPeriod($query));
    }

    public function getCreatedAtFormattedAttribute()
    {
        return Functions::getDateAsString($this->created_at);
    }

}
