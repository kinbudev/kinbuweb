<?php

/**
 * Created by PhpStorm.
 * User: jedabero
 * Date: 21/11/14
 * Time: 07:18 PM
 */
class Author extends Eloquent {

    public function books() {
        return $this->belongsToMany('Book', 'written_by')->withTimestamps();
    }

    public function categories() {
        return $this->belongsToMany('Category')->withTimestamps();
    }

    public function getNameAttribute() {
        return $this->attributes['firstname'] . ' ' . $this->attributes['lastname'];
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(function ($q) use($search)
        {
            $q->where('firstname', 'LIKE', "%$search%")->orWhere('lastname', 'LIKE', "%$search%");
        });
    }
}
