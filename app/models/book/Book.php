<?php

/**
 * Created by PhpStorm.
 * User: jedabero
 * Date: 21/11/14
 * Time: 07:17 PM
 */
class Book extends Eloquent {

    protected $appends = ['language_value'];

    public function ratings() {
        return $this->belongsToMany('User', 'books_ratings')->withPivot('rating');
    }

    public function rating() {
        return number_format($this->ratings()->avg('rating'),2);
    }

    public function comments() {
        return $this->hasMany('BookComment')->orderBy('created_at', 'DESC');
    }

    public function authors() {
        return $this->belongsToMany('Author', 'written_by')->withTimestamps();
    }

    public function categories() {
        return $this->belongsToMany('Category')->withTimestamps();
    }

    public function read_by() {
        return $this->belongsToMany('User', 'books_read')->withPivot('book_read_status');
    }

    public function bought_by() {
        return $this->belongsToMany('User', 'books_bought')->withTimestamps();
    }

    public function sold_by() {
        $pt = Parameter::whereAttribute('publication_type')->values()->whereIn('pvalue', ['sale', 'both'])->lists('id');
        return $this->belongsToMany('User', 'published_books')->whereIn('publication_type', $pt);
    }

    public function on_exchange_by() {
        $pt = Parameter::whereAttribute('publication_type')->values()->whereIn('pvalue', ['exchange', 'both'])->lists('id');
        return $this->belongsToMany('User', 'published_books')->whereIn('publication_type', $pt);
    }

    public function publications() {
        return $this->hasMany('BookPublication', 'book_id', 'id')->orderBy('plaza_type', 'DESC');
    }

    public function lowestPrice()
    {
        return $this->publications()->min('price');
    }

    //Scopes

    public function scopeSearch($query, $search)
    {
        return $query->where(function ($q) use($search)
        {
            $q->where('title', 'LIKE', "%$search%")
              ->orWhere('ISBN', 'LIKE', "%$search%");
        });
    }

    public function language()
    {
        return $this->belongsTo('ParameterValue', 'language');
    }

    public function getLanguageValueAttribute()
    {
        return $this->language()->lists('description')[0];
    }

    public function getCoverImg($size = 'M', $default = 'false', $tag = false, $alt = 'Cover', $attr = []) {
        if ($this->image) {
            $url= '//'.$this->image;
        } else {
            $url = 'http://covers.openlibrary.org/b/ISBN/';
            $url .= $this->ISBN;
            $url .= "-$size.jpg?default=$default";
        }

        if ($tag) {
            $url = HTML::image($url, $alt, $attr);
        }

        return $url;
    }


}
