<?php

class Category extends Eloquent {

    protected $guarded = ['id'];
    protected $fillable = ['name', 'description', 'main_category_id'];


    public function main_category() {
        return $this->belongsTo('MainCategory');
    }

    public function authors() {
        return $this->belongsToMany('Author', 'author_category');
    }

    public function books() {
        return $this->belongsToMany('Book', 'book_category');
    }

}