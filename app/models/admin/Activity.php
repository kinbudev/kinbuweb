<?php

class Activity extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'description'];

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Activities';

        $this->admin['model']['name'] = 'activities';

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;
		$this->admin['actions']['delete']['can'] = TRUE;

        $this->admin['form_attrs']['name'] = ['title' => 'Name', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];

        $this->admin['grid_attrs']['name'] = 'Name';
        $this->admin['grid_attrs']['description'] = 'Description';
    }

}
