<?php

class Action extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'description', 'value'];

	public function users() {
		return $this->belongsToMany('User');
	}

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Actions';

        $this->admin['model']['name'] = 'actions';

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;
		$this->admin['actions']['delete']['can'] = TRUE;

        $this->admin['form_attrs']['name'] = ['title' => 'Name', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];
		$this->admin['form_attrs']['value'] = ['title' => 'Value', 'type' => 'number'];

        $this->admin['grid_attrs']['name'] = 'Name';
        $this->admin['grid_attrs']['description'] = 'Description';
		$this->admin['grid_attrs']['value'] = 'Value';
    }

}
