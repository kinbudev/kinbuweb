<?php

class Level extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Levels';

        $this->admin['model']['name'] = 'levels';

        $this->admin['actions']['create']['can'] = FALSE;
        $this->admin['actions']['edit']['can'] = FALSE;
		$this->admin['actions']['delete']['can'] = FALSE;

        $this->admin['form_attrs']['level'] = ['title' => 'Level', 'type' => 'number'];
        $this->admin['form_attrs']['points'] =  ['title' => 'Points', 'type' => 'number'];
		$this->admin['form_attrs']['acumulate'] = ['title' => 'Acumulate', 'type' => 'number'];

        $this->admin['grid_attrs']['level'] = 'Level';
        $this->admin['grid_attrs']['points'] = 'Points';
		$this->admin['grid_attrs']['acumulate'] = 'Acumulate';
    }

}
