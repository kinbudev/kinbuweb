<?php

class MedalBooster extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'description', 'percentage'];

	public function medal() {
		return $this->morphTo();
	}

	public function aplicable() {
		return $this->morphTo();
	}

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Medal Boosters';

        $this->admin['model']['name'] = 'medal_boosters';
        $this->admin['model']['parent'] = 'medals';

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;
        $this->admin['actions']['delete']['can'] = TRUE;

        $this->admin['form_attrs']['name'] = ['title' => 'Name', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];
		$this->admin['form_attrs']['percentage'] = ['title' => 'Percentage', 'type' => 'number'];

        $this->admin['grid_attrs']['name'] = 'Name';
        $this->admin['grid_attrs']['description'] = 'Description';
		$this->admin['grid_attrs']['percentage'] = 'Percentage';
    }


}
