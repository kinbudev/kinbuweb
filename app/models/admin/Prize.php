<?php

class Prize extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'description', 'value'];

	public function medal_boosters() {
		return $this->morphMany('MedalBooster', 'aplicable');
	}

	public function bags() {
		return $this->hasMany('Bag');
	}

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Prizes';

        $this->admin['model']['name'] = 'prizes';
        $this->admin['model']['details'] = [
            'medal_boosters' => 'MedalBooster'
        ];

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;
		$this->admin['actions']['delete']['can'] = TRUE;

        $this->admin['form_attrs']['name'] = ['title' => 'Name', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];
		$this->admin['form_attrs']['value'] = ['title' => 'Value', 'type' => 'number'];

        $this->admin['grid_attrs']['name'] = 'Name';
        $this->admin['grid_attrs']['description'] = 'Description';
		$this->admin['grid_attrs']['value'] = 'Value';
    }

}
