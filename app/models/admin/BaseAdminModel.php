<?php

abstract class BaseAdminModel extends Eloquent {

    public $admin = [
        'title' => '',
        'model' => [
            'name' => '',
            'parent' => FALSE,
            'has_file' => FALSE,
    		'details' => array()
        ],
        'actions' => [
    		'create' => [
                'can' => FALSE,
                'text' => 'Create new'
            ],
    		'edit' => [
                'can' => FALSE,
                'text' => 'Edit'
            ],
    		'delete' => [
                'can' => FALSE,
                'text' => 'Delete'
            ]
    	],
        'form_attrs' => [
    		#'column' => ['title' => 'Column', 'type' => 'ColumnType'],
    		#'parent_id' => ['type' => 'hidden']
    	],
        'grid_attrs' => [
            'id' => 'ID',
            #'column' => 'Column'
        ]
    ];

}
