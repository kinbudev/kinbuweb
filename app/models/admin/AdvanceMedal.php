<?php

class AdvanceMedal extends BaseAdminModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['status', 'name', 'description', 'medal_id', 'image_file_name', 'image_content_type', 'image_file_size'];

	protected $appends = ['image'];

	protected $touches = ['medal'];

	public function medal() {
		return $this->belongsTo('Medal');
	}

	public function medal_boosters() {
		return $this->morphMany('MedalBooster', 'medal');
	}

	public function users() {
		return $this->morphToMany('User', 'medalable', 'medal_user');
	}

	function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->admin['title'] = 'Advance Medals';

        $this->admin['model']['name'] = 'advance_medals';
        $this->admin['model']['parent'] = 'medals';
		$this->admin['model']['has_file'] = TRUE;
		$this->admin['model']['details'] = [
            'medal_boosters' => 'MedalBooster'
        ];

        $this->admin['actions']['create']['can'] = TRUE;
        $this->admin['actions']['edit']['can'] = TRUE;

		$this->admin['form_attrs']['status'] = ['title' => 'Estado', 'type' => 'number'];
		$this->admin['form_attrs']['name'] = ['title' => 'Name', 'type' => 'text'];
        $this->admin['form_attrs']['description'] =  ['title' => 'Description', 'type' => 'textarea'];
		$this->admin['form_attrs']['image'] = ['title' => 'Image', 'type' => 'file'];

        $this->admin['grid_attrs']['status'] = 'Estado';
        $this->admin['grid_attrs']['name'] = 'Name';
        $this->admin['grid_attrs']['description'] = 'Description';
		$this->admin['grid_attrs']['image'] = 'Image';
    }

	public function getImageAttribute() {
		return HTML::image('img/medals/' . $this->image_file_name, '-', ['class' => 'thumb']);
	}

}
