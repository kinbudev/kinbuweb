<?php

class Shipment extends Eloquent
{
    protected $fillable = ['identifier', 'order_id', 'published_book_id'];

    public function order() {
        return $this->belongsTo('Order');
    }

    public function publication() {
        return $this->belongsTo('BookPublication', 'published_book_id');
    }

    public function status()
    {
        return $this->belongsTo('ParameterValue', 'shipment_status');
    }

    public function getStatusAttribute()
    {
        return $this->status()->lists('description')[0];
    }

}
