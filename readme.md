## Kinbu platform

### Introducción

Kinbu es una AppWeb dedicada a lectores. Creada para la eficacia, para hacer la vida de las personas que les gustar leer, más fácil. Brindando interactividad, accesibilidad y seguridad a todos nuestros usuarios. En Kinbu, sabemos que no hay mejor que degustar la textura de un libro impreso. Pero en algunos casos esos libros son muy costosos o puede que esté agotado en la ciudad.
Con Kinbu los lectores podrán comprar, vender e intercambiar libros impresos entre ellos con la seguridad que ofrece Kinbu en devoluciones y garantías. En Kinbu los lectores pueden ganar libros gratis y descuentos en compras, medallas y alcanzar niveles. Recomendar libros, compartir comentarios y valorar libros.

## Official Documentation

Próximo lanzamiento, 2015.

### License

Copyright (c) 2014-2016 Kinbu S.A.S Nit: 900.899.893-3 Barranquilla, Colombia.
