//Tour source http://linkedin.github.io/hopscotch/

(function(w, $, k, h) {
    var tourBtn = $('#start-tour-btn');
    var kinbuTour = {
        id: 'kinbu-tour',
        steps: [{
            target: 'bellow-jumbotron',
            placement: 'top',
            xOffset: 'center',
            arrowOffset: 'center',
            title: 'Bienvenido al mejor sitio para lectores en todo Internet',
            content: 'Un lugar totalmente completo donde compartimos lo mejor del mundo, los libros.'
        }, {
            target: 'bellow-items',
            placement: 'top',
            xOffset: 'center',
            arrowOffset: 'center',
            content: 'Aquí puedes conseguir cualquier libro que desees, puedes comprar libros nuevos o usados, también puedes venderlos. Todo con una gran seguridad, garantías y devoluciones.'
        }, {
            target: 'search-navbar',
            placement: 'bottom',
            xOffset: 'center',
            arrowOffset: 'center',
            content: 'Con la barra de navegación puedes encontrar lo mejores libros y los mejores lectores de todo Kinbu.',
            multipage: true,
            onNext: function() {
                w.location = k.Constants.UserProfileUrl;
            }
        }, {
            target: '.feed',
            placement: 'left',
            content: 'En tú perfil puedes revisar tus libros comprados, compartir con los demás qué libros estás leyendo y degustando en éstos momentos.',
            /*onNext: function() {
                $.ajax({
                    url: k.Constants.CompletedTourUrl,
                    method: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}'
                    }
                }).done(function(response) {
                    if (response.medals) {
                        var medals = k.Notification.medals(response.medals);
                    }
                    if ('notifications' in response) {
                        response.notifications.forEach(function (notification, index, array) {
                            KINBU.Notification.show(notification);
                        })
                    }
                }).fail(function(request, status) {
                    console.log(status);
                });
            }*/
        }, {
            target: 'se-vende',
            placement: 'left',
            content: 'Vende libros a toda la comunidad, no te preocuparás sobre el envío (ni siquiera por su paquete) o tu pago, nosotros nos encargaremos de todo eso.',
        }, { 
            target: 'usermenu',
            placement: 'bottom',
            xOffset: 'center',
            arrowOffset: 'center',
            content: 'En "Pedidos" podrás revisar en donde y en qué estado se encuentran tus valiosos libros',
        }, {
            target: 'infofooter',
            placement: 'top',
            xOffset: 'center',
            arrowOffset: 'center',
            content: 'Nuestro objetivo es claro, que puedas conseguir libros a menor precio, con seguridad y facilidad.',
        }],
        i18n: {
            nextBtn: "Siguiente",
            doneBtn: "Terminar Tour"
        }
    };
    //administrador del mensaje inicial
    var mediaquery = window.matchMedia("(min-width: 768px)");
    var callloutManager = h.getCalloutManager();
    if (tourBtn.length) {
        if (!h.isActive && h.getState() != null) {
            try {
                h.endTour(true);
            } catch (e) {}
        }
        //convirtiendo el mensaje inicial en responsive, sí la pantalla es menor que 767px no se mostrará
        if(mediaquery.matches){
                callloutManager.createCallout({
                id: 'tour-callout',
                target: 'start-tour-btn',
                placement: 'right',
                content: '¿Cómo funciona?',
                width: 200
                });
            } 
        
        tourBtn.click(function(event) {
            if (!h.isActive) {
                callloutManager.removeAllCallouts();
                h.startTour(kinbuTour);
            }
        });
    } 
    else {
        if (h.getState() === "kinbu-tour:3") {
            h.startTour(kinbuTour);
        }
    }
}(window, jQuery, KINBU, hopscotch))
