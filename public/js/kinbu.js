/*!
 * Module KINBU v0.1.3
 * Namespaces:
 * - HTML: HTMLHelper
 * - Notification: Simple Notification System
 * - Msg: Modal helper
 * Wrapper and helper for functions used commonly around the site
 */

var KINBU = (function($) {
    'use strict';
    var k = {
        version: '0.1.3'
    };

    function HTMLHelper() {
        var me = this;
        var textHolder = $('<textarea/>');
        me.encodeEntities = function(text) {
            return textHolder.text(text).html();
        };
        me.decodeEntities = function(text) {
            return textHolder.html(text).text();
        };
    }

    k.HTML = new HTMLHelper();

    var notificationTypes = ['success', 'info', 'warning', 'danger'];

    function Notification(content, type) { //TODO: try to use browser native Notification, and fallback to this
        if (notificationTypes.indexOf(type) < 0) {
             type = 'info alert-kinbu';
        }
        var me = this;
        if (typeof content === "string") {
            me.content = $("<div />", {
                class: 'alert-body',
                html: content
            })
        } else if (content instanceof $ && !content.hasClass('alert-body')) {
            content.addClass('alert-body');
            me.content = content;
        } else {
            me.content = content;
        }
        me.body = $('<div />', {
            class: 'alert alert-' + type + ' alert-dismissible',
            role: 'alert'
        });
        me.dismissBtn = $('<button />', {
            type: 'button',
            class: 'close',
            'data-dismiss': 'alert',
            'aria-label': 'Close'
        }).append(
            $('<span />', {
                'aria-hidden': true,
                html: '&times;'
            })
        );
        me.body.append(me.dismissBtn, me.content);
        me.body.hide();
        return me;
    }

    Notification.prototype.show = function(opts) {
        var me = this;
        opts = $.extend({
            fixed: false,
            fIn: 'fast',
            delay: 5000,
            fOut: 2000,
            completeOutFn: function() {
                me.body.alert('close');
            }
        }, opts || {});
        if (opts.fixed) {
            me.body.fadeIn(opts.fIn);
        } else {
            me.body.fadeIn(opts.fIn).delay(opts.delay).fadeOut(opts.fOut, opts.completeOutFn);
        }
        return me;
    };

    Notification.prototype.addToNotificationContainer = function() {
        var me = this;
        return me.addToContainer('.notification-container'); //XXX: hardcoded container
    };

    Notification.prototype.addToContainer = function(container) {
        var me = this;
        var containerType = typeof container;
        if (containerType === "string") {
            container = $(container);
        }
        if (!(container instanceof $ && container.length > 0)) {
            throw new Error("Container must be a selector string or jQuery object with at least one existing match.");
        }
        me.body.appendTo(container);
        return me;
    };

    function MedalContent(medal) {
        var me = this;

        var image = $('<a />', {
            href: '#'
        });
        image.append($(medal.image).addClass('media-object'));
        var mediaLeft = $('<div />', {
            class: 'media-left'
        });
        mediaLeft.append($(medal.image));
        var medalName = $('<h4 />', {
            class: 'media-heading',
            html: medal.name
        });
        var mediaBody = $('<div />', {
            class: 'media-body'
        });
        mediaBody.append(medalName, medal.description)

        me.content = $('<div />', {
            class: 'media medal'
        });
        me.content.append(mediaLeft, mediaBody);
        return me;
    }

    k.Notification = {
        show: function(content, showOpts) {
            var notification = new Notification(content);
            notification.addToNotificationContainer().show(showOpts);
            return notification;
        },
        showIn: function(content, container, showOpts) {
            var notification = new Notification(content);
            notification.addToContainer(container).show(showOpts);
            return notification;
        },
        medals: function(medals) {
            function showMedalNotification(element, index, array) {
                var medalContent = new MedalContent(element)
                var notification = new Notification(medalContent.content, 'success');
                return notification.addToNotificationContainer().show({fixed: true});
            }
            return medals.map(showMedalNotification);
        },
        warning: function (content, showOpts) {
            var notification = new Notification(content, 'warning');
            notification.addToNotificationContainer().show(showOpts);
            return notification;
        },
        error: function (content, showOpts) {
            var notification = new Notification(content, 'danger');
            notification.addToNotificationContainer().show(showOpts);
            return notification;
        }
    };

    function Modal(title, message, footer) {
        var me = this;
        me.modal = $("#msg-modal");
        me.modal_title = me.modal.find("h4.modal-title");
        me.modal_body = me.modal.find("div.modal-body");
        me.modal_footer = me.modal.find("div.modal-footer");
        me.modal_title.html("");
        me.modal_body.html("");
        me.modal_footer.html("");
        me.modal_title.append(title);
        me.modal_body.append(message);
        me.modal_footer.append(footer);
        me.modal.modal("show");
        return me;
    }

    k.Msg = { create: Modal };

    return k;
}(jQuery));
